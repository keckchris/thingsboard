/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.msa;


import org.junit.ClassRule;
import org.junit.extensions.cpsuite.ClasspathSuite;
import org.junit.runner.RunWith;
import org.testcontainers.containers.DockerComposeContainer;
import org.testcontainers.containers.wait.strategy.Wait;

import java.io.File;
import java.time.Duration;


@RunWith(ClasspathSuite.class)
@ClasspathSuite.ClassnameFilters({"org.thingsboard.server.msa.*Test"})
public class ContainerTestSuite {

    @ClassRule public static ThingsBoardDbInstaller installTb = new ThingsBoardDbInstaller();
    private static DockerComposeContainer testContainer;

    @ClassRule
    public static DockerComposeContainer getTestContainer() {
        if (testContainer == null) {
            boolean skipTailChildContainers = Boolean.valueOf(
                    System.getProperty("blackBoxTests.skipTailChildContainers"));
            testContainer = new DockerComposeContainer(new File("./../../docker/docker-compose.yml"),
                                                       new File("./../../docker/docker-compose.postgres.yml"), new File(
                    "./../../docker/docker-compose.postgres.volumes.yml")).withPull(false)
                                                                          .withLocalCompose(true)
                                                                          .withTailChildContainers(
                                                                                  !skipTailChildContainers)
                                                                          .withEnv(installTb.getEnv())
                                                                          .withEnv("LOAD_BALANCER_NAME", "")
                                                                          .withExposedService("haproxy", 80,
                                                                                              Wait.forHttp(
                                                                                                      "/swagger-ui.html")
                                                                                                  .withStartupTimeout(
                                                                                                          Duration.ofSeconds(
                                                                                                                  400)));
        }
        return testContainer;
    }

}
