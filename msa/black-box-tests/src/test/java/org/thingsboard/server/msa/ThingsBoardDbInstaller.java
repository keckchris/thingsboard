/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.msa;


import org.junit.rules.ExternalResource;
import org.testcontainers.utility.Base58;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ThingsBoardDbInstaller extends ExternalResource {

    private final static String POSTGRES_DATA_VOLUME = "tb-postgres-test-data-volume";
    private final static String TB_LOG_VOLUME = "tb-log-test-volume";
    private final static String TB_COAP_TRANSPORT_LOG_VOLUME = "tb-coap-transport-log-test-volume";
    private final static String TB_HTTP_TRANSPORT_LOG_VOLUME = "tb-http-transport-log-test-volume";
    private final static String TB_MQTT_TRANSPORT_LOG_VOLUME = "tb-mqtt-transport-log-test-volume";

    private final DockerComposeExecutor dockerCompose;

    private final String postgresDataVolume;
    private final String tbLogVolume;
    private final String tbCoapTransportLogVolume;
    private final String tbHttpTransportLogVolume;
    private final String tbMqttTransportLogVolume;
    private final Map<String, String> env;

    public ThingsBoardDbInstaller() {
        List<File> composeFiles = Arrays.asList(new File("./../../docker/docker-compose.yml"),
                                                new File("./../../docker/docker-compose.postgres.yml"),
                                                new File("./../../docker/docker-compose.postgres.volumes.yml"));

        String identifier = Base58.randomString(6).toLowerCase();
        String project = identifier + Base58.randomString(6).toLowerCase();

        this.postgresDataVolume = project + "_" + POSTGRES_DATA_VOLUME;
        this.tbLogVolume = project + "_" + TB_LOG_VOLUME;
        this.tbCoapTransportLogVolume = project + "_" + TB_COAP_TRANSPORT_LOG_VOLUME;
        this.tbHttpTransportLogVolume = project + "_" + TB_HTTP_TRANSPORT_LOG_VOLUME;
        this.tbMqttTransportLogVolume = project + "_" + TB_MQTT_TRANSPORT_LOG_VOLUME;

        this.dockerCompose = new DockerComposeExecutor(composeFiles, project);

        this.env = new HashMap<>();
        this.env.put("POSTGRES_DATA_VOLUME", this.postgresDataVolume);
        this.env.put("TB_LOG_VOLUME", this.tbLogVolume);
        this.env.put("TB_COAP_TRANSPORT_LOG_VOLUME", this.tbCoapTransportLogVolume);
        this.env.put("TB_HTTP_TRANSPORT_LOG_VOLUME", this.tbHttpTransportLogVolume);
        this.env.put("TB_MQTT_TRANSPORT_LOG_VOLUME", this.tbMqttTransportLogVolume);
        this.dockerCompose.withEnv(this.env);
    }

    public Map<String, String> getEnv() {
        return this.env;
    }

    @Override
    protected void before() {
        try {

            this.dockerCompose.withCommand("volume create " + this.postgresDataVolume);
            this.dockerCompose.invokeDocker();

            this.dockerCompose.withCommand("volume create " + this.tbLogVolume);
            this.dockerCompose.invokeDocker();

            this.dockerCompose.withCommand("volume create " + this.tbCoapTransportLogVolume);
            this.dockerCompose.invokeDocker();

            this.dockerCompose.withCommand("volume create " + this.tbHttpTransportLogVolume);
            this.dockerCompose.invokeDocker();

            this.dockerCompose.withCommand("volume create " + this.tbMqttTransportLogVolume);
            this.dockerCompose.invokeDocker();

            this.dockerCompose.withCommand("up -d redis postgres");
            this.dockerCompose.invokeCompose();

            this.dockerCompose.withCommand("run --no-deps --rm -e INSTALL_TB=true -e LOAD_DEMO=true tb1");
            this.dockerCompose.invokeCompose();

        } finally {
            try {
                this.dockerCompose.withCommand("down -v");
                this.dockerCompose.invokeCompose();
            } catch (Exception e) {
            }
        }
    }

    @Override
    protected void after() {
        this.copyLogs(this.tbLogVolume, "./target/tb-logs/");
        this.copyLogs(this.tbCoapTransportLogVolume, "./target/tb-coap-transport-logs/");
        this.copyLogs(this.tbHttpTransportLogVolume, "./target/tb-http-transport-logs/");
        this.copyLogs(this.tbMqttTransportLogVolume, "./target/tb-mqtt-transport-logs/");

        this.dockerCompose.withCommand("volume rm -f " + this.postgresDataVolume + " " + this.tbLogVolume + " " +
                                       this.tbCoapTransportLogVolume + " " + this.tbHttpTransportLogVolume + " " +
                                       this.tbMqttTransportLogVolume);
        this.dockerCompose.invokeDocker();
    }

    private void copyLogs(String volumeName, String targetDir) {
        File tbLogsDir = new File(targetDir);
        tbLogsDir.mkdirs();

        this.dockerCompose.withCommand(
                "run -d --rm --name tb-logs-container -v " + volumeName + ":/root alpine tail -f /dev/null");
        this.dockerCompose.invokeDocker();

        this.dockerCompose.withCommand("cp tb-logs-container:/root/. " + tbLogsDir.getAbsolutePath());
        this.dockerCompose.invokeDocker();

        this.dockerCompose.withCommand("rm -f tb-logs-container");
        this.dockerCompose.invokeDocker();
    }

}
