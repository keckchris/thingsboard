/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.actors.rpc;


import akka.actor.ActorRef;
import akka.actor.OneForOneStrategy;
import akka.actor.Props;
import akka.actor.SupervisorStrategy;
import org.thingsboard.server.actors.ActorSystemContext;
import org.thingsboard.server.actors.service.ContextAwareActor;
import org.thingsboard.server.actors.service.ContextBasedCreator;
import org.thingsboard.server.actors.service.DefaultActorService;
import org.thingsboard.server.common.msg.TbActorMsg;
import org.thingsboard.server.common.msg.cluster.ClusterEventMsg;
import org.thingsboard.server.common.msg.cluster.ServerAddress;
import org.thingsboard.server.common.msg.cluster.ServerType;
import org.thingsboard.server.gen.cluster.ClusterAPIProtos;
import org.thingsboard.server.service.cluster.discovery.ServerInstance;
import scala.concurrent.duration.Duration;

import java.util.*;


/**
 * @author Andrew Shvayka
 */
public class RpcManagerActor extends ContextAwareActor {

    private final Map<ServerAddress, SessionActorInfo> sessionActors;
    private final Map<ServerAddress, Queue<ClusterAPIProtos.ClusterMessage>> pendingMsgs;
    private final ServerAddress instance;
    private final SupervisorStrategy strategy = new OneForOneStrategy(3, Duration.create("1 minute"), t -> {
        this.log.warn("Unknown failure", t);
        return SupervisorStrategy.resume();
    });

    private RpcManagerActor(ActorSystemContext systemContext) {
        super(systemContext);
        this.sessionActors = new HashMap<>();
        this.pendingMsgs = new HashMap<>();
        this.instance = systemContext.getDiscoveryService().getCurrentServer().getServerAddress();

        systemContext.getDiscoveryService().getOtherServers().stream().filter(
                otherServer -> otherServer.getServerAddress().compareTo(this.instance) > 0).forEach(
                otherServer -> this.onCreateSessionRequest(
                        new RpcSessionCreateRequestMsg(UUID.randomUUID(), otherServer.getServerAddress(), null)));
    }

    private void onCreateSessionRequest(RpcSessionCreateRequestMsg msg) {
        if (msg.getRemoteAddress() != null) {
            if (!this.sessionActors.containsKey(msg.getRemoteAddress())) {
                ActorRef actorRef = this.createSessionActor(msg);
                this.register(msg.getRemoteAddress(), msg.getMsgUid(), actorRef);
            }
        }
        else {
            this.createSessionActor(msg);
        }
    }

    private ActorRef createSessionActor(RpcSessionCreateRequestMsg msg) {
        this.log.info("[{}] Creating session actor.", msg.getMsgUid());
        ActorRef actor = this.context().actorOf(
                Props.create(new RpcSessionActor.ActorCreator(this.systemContext, msg.getMsgUid()))
                     .withDispatcher(DefaultActorService.RPC_DISPATCHER_NAME));
        actor.tell(msg, this.context().self());
        return actor;
    }

    private void register(ServerAddress remoteAddress, UUID uuid, ActorRef sender) {
        this.sessionActors.put(remoteAddress, new SessionActorInfo(uuid, sender));
        this.log.info("[{}][{}] Registering session actor.", remoteAddress, uuid);
        Queue<ClusterAPIProtos.ClusterMessage> data = this.pendingMsgs.remove(remoteAddress);
        if (data != null) {
            this.log.info("[{}][{}] Forwarding {} pending messages.", remoteAddress, uuid, data.size());
            data.forEach(msg -> sender.tell(new RpcSessionTellMsg(msg), ActorRef.noSender()));
        }
        else {
            this.log.info("[{}][{}] No pending messages to forward.", remoteAddress, uuid);
        }
    }

    @Override
    public void onReceive(Object msg) {
        if (msg instanceof ClusterAPIProtos.ClusterMessage) {
            this.onMsg((ClusterAPIProtos.ClusterMessage) msg);
        }
        else if (msg instanceof RpcBroadcastMsg) {
            this.onMsg((RpcBroadcastMsg) msg);
        }
        else if (msg instanceof RpcSessionCreateRequestMsg) {
            this.onCreateSessionRequest((RpcSessionCreateRequestMsg) msg);
        }
        else if (msg instanceof RpcSessionConnectedMsg) {
            this.onSessionConnected((RpcSessionConnectedMsg) msg);
        }
        else if (msg instanceof RpcSessionDisconnectedMsg) {
            this.onSessionDisconnected((RpcSessionDisconnectedMsg) msg);
        }
        else if (msg instanceof RpcSessionClosedMsg) {
            this.onSessionClosed((RpcSessionClosedMsg) msg);
        }
        else if (msg instanceof ClusterEventMsg) {
            this.onClusterEvent((ClusterEventMsg) msg);
        }
    }

    @Override
    protected boolean process(TbActorMsg msg) {
        //TODO Move everything here, to work with TbActorMsg
        return false;
    }
    private void onMsg(ClusterAPIProtos.ClusterMessage msg) {
        if (msg.hasServerAddress()) {
            ServerAddress address = new ServerAddress(msg.getServerAddress().getHost(),
                                                      msg.getServerAddress().getPort(), ServerType.CORE);
            SessionActorInfo session = this.sessionActors.get(address);
            if (session != null) {
                this.log.debug("{} Forwarding msg to session actor: {}", address, msg);
                session.getActor().tell(msg, ActorRef.noSender());
            }
            else {
                this.log.debug("{} Storing msg to pending queue: {}", address, msg);
                Queue<ClusterAPIProtos.ClusterMessage> queue = this.pendingMsgs.get(address);
                if (queue == null) {
                    queue = new LinkedList<>();
                    this.pendingMsgs.put(
                            new ServerAddress(msg.getServerAddress().getHost(), msg.getServerAddress().getPort(),
                                              ServerType.CORE), queue);
                }
                queue.add(msg);
            }
        }
        else {
            this.log.warn("Cluster msg doesn't have server address [{}]", msg);
        }
    }
    private void onMsg(RpcBroadcastMsg msg) {
        this.log.debug("Forwarding msg to session actors {}", msg);
        this.sessionActors.keySet().forEach(address -> {
            ClusterAPIProtos.ClusterMessage msgWithServerAddress = msg.getMsg().toBuilder().setServerAddress(
                    ClusterAPIProtos.ServerAddress.newBuilder()
                                                  .setHost(address.getHost())
                                                  .setPort(address.getPort())
                                                  .build()).build();
            this.onMsg(msgWithServerAddress);
        });
        this.pendingMsgs.values().forEach(queue -> queue.add(msg.getMsg()));
    }
    private void onClusterEvent(ClusterEventMsg msg) {
        ServerAddress server = msg.getServerAddress();
        if (server.compareTo(this.instance) > 0) {
            if (msg.isAdded()) {
                this.onCreateSessionRequest(new RpcSessionCreateRequestMsg(UUID.randomUUID(), server, null));
            }
            else {
                this.onSessionClose(false, server);
            }
        }
    }
    private void onSessionClose(boolean reconnect, ServerAddress remoteAddress) {
        this.log.info("[{}] session closed. Should reconnect: {}", remoteAddress, reconnect);
        SessionActorInfo sessionRef = this.sessionActors.get(remoteAddress);
        if (sessionRef != null && this.context().sender() != null && this.context().sender().equals(sessionRef.actor)) {
            this.context().stop(sessionRef.actor);
            this.sessionActors.remove(remoteAddress);
            this.pendingMsgs.remove(remoteAddress);
            if (reconnect) {
                this.onCreateSessionRequest(new RpcSessionCreateRequestMsg(sessionRef.sessionId, remoteAddress, null));
            }
        }
    }
    @Override
    public void postStop() {
        this.sessionActors.clear();
        this.pendingMsgs.clear();
    }

    private void onSessionConnected(RpcSessionConnectedMsg msg) {
        this.register(msg.getRemoteAddress(), msg.getId(), this.context().sender());
    }

    private void onSessionDisconnected(RpcSessionDisconnectedMsg msg) {
        boolean reconnect = msg.isClient() && this.isRegistered(msg.getRemoteAddress());
        this.onSessionClose(reconnect, msg.getRemoteAddress());
    }

    private void onSessionClosed(RpcSessionClosedMsg msg) {
        boolean reconnect = msg.isClient() && this.isRegistered(msg.getRemoteAddress());
        this.onSessionClose(reconnect, msg.getRemoteAddress());
    }
    @Override
    public SupervisorStrategy supervisorStrategy() {
        return this.strategy;
    }
    private boolean isRegistered(ServerAddress address) {
        for (ServerInstance server : this.systemContext.getDiscoveryService().getOtherServers()) {
            if (server.getServerAddress().equals(address)) {
                return true;
            }
        }
        return false;
    }


    public static class ActorCreator extends ContextBasedCreator<RpcManagerActor> {

        private static final long serialVersionUID = 1L;

        public ActorCreator(ActorSystemContext context) {
            super(context);
        }

        @Override
        public RpcManagerActor create() {
            return new RpcManagerActor(this.context);
        }

    }

}
