/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.actors.ruleChain;


import akka.actor.ActorRef;
import com.datastax.driver.core.utils.UUIDs;
import org.springframework.util.StringUtils;
import org.thingsboard.rule.engine.api.*;
import org.thingsboard.server.actors.ActorSystemContext;
import org.thingsboard.server.common.data.id.DeviceId;
import org.thingsboard.server.common.data.id.EntityId;
import org.thingsboard.server.common.data.id.RuleNodeId;
import org.thingsboard.server.common.data.id.TenantId;
import org.thingsboard.server.common.data.rpc.ToDeviceRpcRequestBody;
import org.thingsboard.server.common.data.rule.RuleNode;
import org.thingsboard.server.common.msg.TbMsg;
import org.thingsboard.server.common.msg.TbMsgMetaData;
import org.thingsboard.server.common.msg.cluster.ServerAddress;
import org.thingsboard.server.common.msg.cluster.ServerType;
import org.thingsboard.server.common.msg.rpc.ToDeviceRpcRequest;
import org.thingsboard.server.dao.alarm.AlarmService;
import org.thingsboard.server.dao.asset.AssetService;
import org.thingsboard.server.dao.attributes.AttributesService;
import org.thingsboard.server.dao.customer.CustomerService;
import org.thingsboard.server.dao.device.DeviceService;
import org.thingsboard.server.dao.entityview.EntityViewService;
import org.thingsboard.server.dao.relation.RelationService;
import org.thingsboard.server.dao.rule.RuleChainService;
import org.thingsboard.server.dao.tenant.TenantService;
import org.thingsboard.server.dao.timeseries.TimeseriesService;
import org.thingsboard.server.dao.user.UserService;
import org.thingsboard.server.service.script.RuleNodeJsScriptEngine;
import scala.concurrent.duration.Duration;

import java.util.Collections;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;


/**
 * Created by ashvayka on 19.03.18.
 */
class DefaultTbContext implements TbContext {

    private final ActorSystemContext mainCtx;
    private final RuleNodeCtx nodeCtx;

    public DefaultTbContext(ActorSystemContext mainCtx, RuleNodeCtx nodeCtx) {
        this.mainCtx = mainCtx;
        this.nodeCtx = nodeCtx;
    }

    @Override
    public void tellNext(TbMsg msg, String relationType) {
        this.tellNext(msg, Collections.singleton(relationType), null);
    }

    @Override
    public void tellNext(TbMsg msg, String relationType, Throwable th) {
        this.tellNext(msg, Collections.singleton(relationType), th);
    }

    @Override
    public void tellNext(TbMsg msg, Set<String> relationTypes) {
        this.tellNext(msg, relationTypes, null);
    }

    @Override
    public void tellSelf(TbMsg msg, long delayMs) {
        //TODO: add persistence layer
        this.scheduleMsgWithDelay(new RuleNodeToSelfMsg(msg), delayMs, this.nodeCtx.getSelfActor());
    }

    private void scheduleMsgWithDelay(Object msg, long delayInMs, ActorRef target) {
        this.mainCtx.getScheduler().scheduleOnce(Duration.create(delayInMs, TimeUnit.MILLISECONDS), target, msg,
                                                 this.mainCtx.getActorSystem().dispatcher(),
                                                 this.nodeCtx.getSelfActor());
    }

    @Override
    public void tellFailure(TbMsg msg, Throwable th) {
        if (this.nodeCtx.getSelf().isDebugMode()) {
            this.mainCtx.persistDebugOutput(this.nodeCtx.getTenantId(), this.nodeCtx.getSelf().getId(), msg,
                                            TbRelationTypes.FAILURE, th);
        }
        this.nodeCtx.getChainActor().tell(new RuleNodeToRuleChainTellNextMsg(this.nodeCtx.getSelf().getId(),
                                                                             Collections.singleton(
                                                                                     TbRelationTypes.FAILURE), msg),
                                          this.nodeCtx.getSelfActor());
    }

    @Override
    public void updateSelf(RuleNode self) {
        this.nodeCtx.setSelf(self);
    }

    @Override
    public TbMsg newMsg(String type, EntityId originator, TbMsgMetaData metaData, String data) {
        return new TbMsg(UUIDs.timeBased(), type, originator, metaData.copy(), data,
                         this.nodeCtx.getSelf().getRuleChainId(), this.nodeCtx.getSelf().getId(),
                         this.mainCtx.getQueuePartitionId());
    }

    @Override
    public TbMsg transformMsg(TbMsg origMsg, String type, EntityId originator, TbMsgMetaData metaData, String data) {
        return new TbMsg(origMsg.getId(), type, originator, metaData.copy(), data, origMsg.getRuleChainId(),
                         origMsg.getRuleNodeId(), this.mainCtx.getQueuePartitionId());
    }

    @Override
    public RuleNodeId getSelfId() {
        return this.nodeCtx.getSelf().getId();
    }

    @Override
    public TenantId getTenantId() {
        return this.nodeCtx.getTenantId();
    }

    @Override
    public AttributesService getAttributesService() {
        return this.mainCtx.getAttributesService();
    }

    @Override
    public CustomerService getCustomerService() {
        return this.mainCtx.getCustomerService();
    }

    @Override
    public TenantService getTenantService() {
        return this.mainCtx.getTenantService();
    }

    @Override
    public UserService getUserService() {
        return this.mainCtx.getUserService();
    }

    @Override
    public AssetService getAssetService() {
        return this.mainCtx.getAssetService();
    }

    @Override
    public DeviceService getDeviceService() {
        return this.mainCtx.getDeviceService();
    }

    @Override
    public AlarmService getAlarmService() {
        return this.mainCtx.getAlarmService();
    }

    @Override
    public RuleChainService getRuleChainService() {
        return this.mainCtx.getRuleChainService();
    }

    @Override
    public RuleEngineRpcService getRpcService() {
        return new RuleEngineRpcService() {
            @Override
            public void sendRpcReply(DeviceId deviceId, int requestId, String body) {
                DefaultTbContext.this.mainCtx.getDeviceRpcService().sendReplyToRpcCallFromDevice(
                        DefaultTbContext.this.nodeCtx.getTenantId(), deviceId, requestId, body);
            }

            @Override
            public void sendRpcRequest(RuleEngineDeviceRpcRequest src, Consumer<RuleEngineDeviceRpcResponse> consumer) {
                ToDeviceRpcRequest request = new ToDeviceRpcRequest(src.getRequestUUID(),
                                                                    DefaultTbContext.this.nodeCtx.getTenantId(),
                                                                    src.getDeviceId(), src.isOneway(),
                                                                    src.getExpirationTime(),
                                                                    new ToDeviceRpcRequestBody(src.getMethod(),
                                                                                               src.getBody()));
                DefaultTbContext.this.mainCtx.getDeviceRpcService().forwardServerSideRPCRequestToDeviceActor(request,
                                                                                                             response -> {
                                                                                                                 if (src.isRestApiCall()) {
                                                                                                                     ServerAddress
                                                                                                                             requestOriginAddress;
                                                                                                                     if (!StringUtils
                                                                                                                                  .isEmpty(
                                                                                                                                          src.getOriginHost())) {
                                                                                                                         requestOriginAddress =
                                                                                                                                 new ServerAddress(
                                                                                                                                         src.getOriginHost(),
                                                                                                                                         src.getOriginPort(),
                                                                                                                                         ServerType.CORE);
                                                                                                                     }
                                                                                                                     else {
                                                                                                                         requestOriginAddress =
                                                                                                                                 DefaultTbContext.this.mainCtx
                                                                                                                                         .getRoutingService()
                                                                                                                                         .getCurrentServer();
                                                                                                                     }
                                                                                                                     DefaultTbContext.this.mainCtx
                                                                                                                             .getDeviceRpcService()
                                                                                                                             .processResponseToServerSideRPCRequestFromRuleEngine(
                                                                                                                                     requestOriginAddress,
                                                                                                                                     response);
                                                                                                                 }
                                                                                                                 consumer.accept(
                                                                                                                         RuleEngineDeviceRpcResponse
                                                                                                                                 .builder()
                                                                                                                                 .deviceId(
                                                                                                                                         src.getDeviceId())
                                                                                                                                 .requestId(
                                                                                                                                         src.getRequestId())
                                                                                                                                 .error(response.getError())
                                                                                                                                 .response(
                                                                                                                                         response.getResponse())
                                                                                                                                 .build());
                                                                                                             });
            }
        };
    }

    @Override
    public RuleEngineTelemetryService getTelemetryService() {
        return this.mainCtx.getTsSubService();
    }

    @Override
    public TimeseriesService getTimeseriesService() {
        return this.mainCtx.getTsService();
    }

    @Override
    public RelationService getRelationService() {
        return this.mainCtx.getRelationService();
    }

    @Override
    public EntityViewService getEntityViewService() {
        return this.mainCtx.getEntityViewService();
    }

    @Override
    public ListeningExecutor getJsExecutor() {
        return this.mainCtx.getJsExecutor();
    }

    @Override
    public ListeningExecutor getMailExecutor() {
        return this.mainCtx.getMailExecutor();
    }

    @Override
    public ListeningExecutor getDbCallbackExecutor() {
        return this.mainCtx.getDbCallbackExecutor();
    }

    @Override
    public ListeningExecutor getExternalCallExecutor() {
        return this.mainCtx.getExternalCallExecutorService();
    }

    @Override
    public MailService getMailService() {
        if (this.mainCtx.isAllowSystemMailService()) {
            return this.mainCtx.getMailService();
        }
        else {
            throw new RuntimeException("Access to System Mail Service is forbidden!");
        }
    }

    @Override
    public ScriptEngine createJsScriptEngine(String script, String... argNames) {
        return new RuleNodeJsScriptEngine(this.mainCtx.getJsSandbox(), this.nodeCtx.getSelf().getId(), script,
                                          argNames);
    }

    @Override
    public String getNodeId() {
        return this.mainCtx.getNodeIdProvider().getNodeId();
    }

    private void tellNext(TbMsg msg, Set<String> relationTypes, Throwable th) {
        if (this.nodeCtx.getSelf().isDebugMode()) {
            relationTypes.forEach(relationType -> this.mainCtx.persistDebugOutput(this.nodeCtx.getTenantId(),
                                                                                  this.nodeCtx.getSelf().getId(), msg,
                                                                                  relationType, th));
        }
        this.nodeCtx.getChainActor().tell(
                new RuleNodeToRuleChainTellNextMsg(this.nodeCtx.getSelf().getId(), relationTypes, msg),
                this.nodeCtx.getSelfActor());
    }

}
