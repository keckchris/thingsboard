/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.actors.tenant;


import akka.actor.*;
import akka.japi.Function;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import org.thingsboard.server.actors.ActorSystemContext;
import org.thingsboard.server.actors.device.DeviceActorCreator;
import org.thingsboard.server.actors.device.DeviceActorToRuleEngineMsg;
import org.thingsboard.server.actors.ruleChain.RuleChainManagerActor;
import org.thingsboard.server.actors.service.ContextBasedCreator;
import org.thingsboard.server.actors.service.DefaultActorService;
import org.thingsboard.server.actors.shared.rulechain.TenantRuleChainManager;
import org.thingsboard.server.common.data.EntityType;
import org.thingsboard.server.common.data.id.DeviceId;
import org.thingsboard.server.common.data.id.RuleChainId;
import org.thingsboard.server.common.data.id.TenantId;
import org.thingsboard.server.common.data.rule.RuleChain;
import org.thingsboard.server.common.msg.TbActorMsg;
import org.thingsboard.server.common.msg.aware.DeviceAwareMsg;
import org.thingsboard.server.common.msg.aware.RuleChainAwareMsg;
import org.thingsboard.server.common.msg.plugin.ComponentLifecycleMsg;
import org.thingsboard.server.common.msg.system.ServiceToRuleEngineMsg;
import scala.concurrent.duration.Duration;


public class TenantActor extends RuleChainManagerActor {

    private final TenantId tenantId;
    private final BiMap<DeviceId, ActorRef> deviceActors;
    private final SupervisorStrategy strategy = new OneForOneStrategy(3, Duration.create("1 minute"),
                                                                      new Function<Throwable,
                                                                                          SupervisorStrategy.Directive>() {
                                                                          @Override
                                                                          public SupervisorStrategy.Directive apply(
                                                                                  Throwable t) {
                                                                              TenantActor.this.log.warn(
                                                                                      "[{}] Unknown failure",
                                                                                      TenantActor.this.tenantId, t);
                                                                              if (t instanceof ActorInitializationException) {
                                                                                  return SupervisorStrategy.stop();
                                                                              }
                                                                              else {
                                                                                  return SupervisorStrategy.resume();
                                                                              }
                                                                          }
                                                                      });

    private TenantActor(ActorSystemContext systemContext, TenantId tenantId) {
        super(systemContext, new TenantRuleChainManager(systemContext, tenantId));
        this.tenantId = tenantId;
        this.deviceActors = HashBiMap.create();
    }

    @Override
    public void preStart() {
        this.log.info("[{}] Starting tenant actor.", this.tenantId);
        try {
            this.initRuleChains();
            this.log.info("[{}] Tenant actor started.", this.tenantId);
        } catch (Exception e) {
            this.log.warn("[{}] Unknown failure", this.tenantId, e);
        }
    }

    @Override
    public void postStop() {
        this.log.info("[{}] Stopping tenant actor.", this.tenantId);
    }

    @Override
    public SupervisorStrategy supervisorStrategy() {
        return this.strategy;
    }
    @Override
    protected boolean process(TbActorMsg msg) {
        switch (msg.getMsgType()) {
            case CLUSTER_EVENT_MSG:
                this.broadcast(msg);
                break;
            case COMPONENT_LIFE_CYCLE_MSG:
                this.onComponentLifecycleMsg((ComponentLifecycleMsg) msg);
                break;
            case SERVICE_TO_RULE_ENGINE_MSG:
                this.onServiceToRuleEngineMsg((ServiceToRuleEngineMsg) msg);
                break;
            case DEVICE_ACTOR_TO_RULE_ENGINE_MSG:
                this.onDeviceActorToRuleEngineMsg((DeviceActorToRuleEngineMsg) msg);
                break;
            case TRANSPORT_TO_DEVICE_ACTOR_MSG:
            case DEVICE_ATTRIBUTES_UPDATE_TO_DEVICE_ACTOR_MSG:
            case DEVICE_CREDENTIALS_UPDATE_TO_DEVICE_ACTOR_MSG:
            case DEVICE_NAME_OR_TYPE_UPDATE_TO_DEVICE_ACTOR_MSG:
            case DEVICE_RPC_REQUEST_TO_DEVICE_ACTOR_MSG:
            case SERVER_RPC_RESPONSE_TO_DEVICE_ACTOR_MSG:
                this.onToDeviceActorMsg((DeviceAwareMsg) msg);
                break;
            case RULE_CHAIN_TO_RULE_CHAIN_MSG:
            case REMOTE_TO_RULE_CHAIN_TELL_NEXT_MSG:
                this.onRuleChainMsg((RuleChainAwareMsg) msg);
                break;
            default:
                return false;
        }
        return true;
    }
    @Override
    protected void processTermination(Terminated message) {
        ActorRef terminated = message.actor();
        if (terminated instanceof LocalActorRef) {
            boolean removed = this.deviceActors.inverse().remove(terminated) != null;
            if (removed) {
                this.log.debug("[{}] Removed actor:", terminated);
            }
            else {
                this.log.warn("[{}] Removed actor was not found in the device map!");
            }
        }
        else {
            throw new IllegalStateException("Remote actors are not supported!");
        }
    }
    private void onComponentLifecycleMsg(ComponentLifecycleMsg msg) {
        ActorRef target = this.getEntityActorRef(msg.getEntityId());
        if (target != null) {
            if (msg.getEntityId().getEntityType() == EntityType.RULE_CHAIN) {
                RuleChain ruleChain = this.systemContext.getRuleChainService().
                                                                                      findRuleChainById(this.tenantId,
                                                                                                        new RuleChainId(
                                                                                                                msg.getEntityId()
                                                                                                                   .getId()));
                this.ruleChainManager.visit(ruleChain, target);
            }
            target.tell(msg, ActorRef.noSender());
        }
        else {
            this.log.debug("[{}] Invalid component lifecycle msg: {}", this.tenantId, msg);
        }
    }

    private void onServiceToRuleEngineMsg(ServiceToRuleEngineMsg msg) {
        if (this.ruleChainManager.getRootChainActor() != null) {
            this.ruleChainManager.getRootChainActor().tell(msg, this.self());
        }
        else {
            this.log.info("[{}] No Root Chain: {}", this.tenantId, msg);
        }
    }

    private void onDeviceActorToRuleEngineMsg(DeviceActorToRuleEngineMsg msg) {
        if (this.ruleChainManager.getRootChainActor() != null) {
            this.ruleChainManager.getRootChainActor().tell(msg, this.self());
        }
        else {
            this.log.info("[{}] No Root Chain: {}", this.tenantId, msg);
        }
    }

    private void onToDeviceActorMsg(DeviceAwareMsg msg) {
        this.getOrCreateDeviceActor(msg.getDeviceId()).tell(msg, ActorRef.noSender());
    }
    private void onRuleChainMsg(RuleChainAwareMsg msg) {
        this.ruleChainManager.getOrCreateActor(this.context(), msg.getRuleChainId()).tell(msg, this.self());
    }
    private ActorRef getOrCreateDeviceActor(DeviceId deviceId) {
        return this.deviceActors.computeIfAbsent(deviceId, k -> {
            this.log.debug("[{}][{}] Creating device actor.", this.tenantId, deviceId);
            ActorRef deviceActor = this.context().actorOf(
                    Props.create(new DeviceActorCreator(this.systemContext, this.tenantId, deviceId))
                         .withDispatcher(DefaultActorService.CORE_DISPATCHER_NAME), deviceId.toString());
            this.context().watch(deviceActor);
            this.log.debug("[{}][{}] Created device actor: {}.", this.tenantId, deviceId, deviceActor);
            return deviceActor;
        });
    }


    public static class ActorCreator extends ContextBasedCreator<TenantActor> {

        private static final long serialVersionUID = 1L;

        private final TenantId tenantId;

        public ActorCreator(ActorSystemContext context, TenantId tenantId) {
            super(context);
            this.tenantId = tenantId;
        }

        @Override
        public TenantActor create() {
            return new TenantActor(this.context, this.tenantId);
        }

    }

}
