/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.actors.ruleChain;


import akka.actor.ActorContext;
import akka.actor.ActorRef;
import akka.actor.Props;
import com.datastax.driver.core.utils.UUIDs;
import lombok.extern.slf4j.Slf4j;
import org.thingsboard.server.actors.ActorSystemContext;
import org.thingsboard.server.actors.device.DeviceActorToRuleEngineMsg;
import org.thingsboard.server.actors.service.DefaultActorService;
import org.thingsboard.server.actors.shared.ComponentMsgProcessor;
import org.thingsboard.server.common.data.EntityType;
import org.thingsboard.server.common.data.id.EntityId;
import org.thingsboard.server.common.data.id.RuleChainId;
import org.thingsboard.server.common.data.id.RuleNodeId;
import org.thingsboard.server.common.data.id.TenantId;
import org.thingsboard.server.common.data.plugin.ComponentLifecycleEvent;
import org.thingsboard.server.common.data.plugin.ComponentLifecycleState;
import org.thingsboard.server.common.data.relation.EntityRelation;
import org.thingsboard.server.common.data.rule.RuleChain;
import org.thingsboard.server.common.data.rule.RuleNode;
import org.thingsboard.server.common.msg.TbMsg;
import org.thingsboard.server.common.msg.cluster.ClusterEventMsg;
import org.thingsboard.server.common.msg.cluster.ServerAddress;
import org.thingsboard.server.common.msg.plugin.ComponentLifecycleMsg;
import org.thingsboard.server.common.msg.system.ServiceToRuleEngineMsg;
import org.thingsboard.server.dao.rule.RuleChainService;

import java.util.*;
import java.util.stream.Collectors;


/**
 * @author Andrew Shvayka
 */
@Slf4j
public class RuleChainActorMessageProcessor extends ComponentMsgProcessor<RuleChainId> {

    private static final long DEFAULT_CLUSTER_PARTITION = 0L;
    private final ActorRef parent;
    private final ActorRef self;
    private final Map<RuleNodeId, RuleNodeCtx> nodeActors;
    private final Map<RuleNodeId, List<RuleNodeRelation>> nodeRoutes;
    private final RuleChainService service;

    private RuleNodeId firstId;
    private RuleNodeCtx firstNode;
    private boolean started;
    private String ruleChainName;

    RuleChainActorMessageProcessor(TenantId tenantId,
                                   RuleChainId ruleChainId,
                                   ActorSystemContext systemContext,
                                   ActorRef parent,
                                   ActorRef self) {
        super(systemContext, tenantId, ruleChainId);
        this.parent = parent;
        this.self = self;
        this.nodeActors = new HashMap<>();
        this.nodeRoutes = new HashMap<>();
        this.service = systemContext.getRuleChainService();
        this.ruleChainName = ruleChainId.toString();
    }

    @Override
    public String getComponentName() {
        return null;
    }

    @Override
    public void onClusterEventMsg(ClusterEventMsg msg) {

    }

    @Override
    public void start(ActorContext context) {
        if (!this.started) {
            RuleChain ruleChain = this.service.findRuleChainById(this.tenantId, this.entityId);
            this.ruleChainName = ruleChain.getName();
            List<RuleNode> ruleNodeList = this.service.getRuleChainNodes(this.tenantId, this.entityId);
            log.trace("[{}][{}] Starting rule chain with {} nodes", this.tenantId, this.entityId, ruleNodeList.size());
            // Creating and starting the actors;
            for (RuleNode ruleNode : ruleNodeList) {
                log.trace("[{}][{}] Creating rule node [{}]: {}", this.entityId, ruleNode.getId(), ruleNode.getName(),
                          ruleNode);
                ActorRef ruleNodeActor = this.createRuleNodeActor(context, ruleNode);
                this.nodeActors.put(ruleNode.getId(),
                                    new RuleNodeCtx(this.tenantId, this.self, ruleNodeActor, ruleNode));
            }
            this.initRoutes(ruleChain, ruleNodeList);
            this.started = true;
        }
        else {
            this.onUpdate(context);
        }
    }

    @Override
    public void onUpdate(ActorContext context) {
        RuleChain ruleChain = this.service.findRuleChainById(this.tenantId, this.entityId);
        this.ruleChainName = ruleChain.getName();
        List<RuleNode> ruleNodeList = this.service.getRuleChainNodes(this.tenantId, this.entityId);
        log.trace("[{}][{}] Updating rule chain with {} nodes", this.tenantId, this.entityId, ruleNodeList.size());
        for (RuleNode ruleNode : ruleNodeList) {
            RuleNodeCtx existing = this.nodeActors.get(ruleNode.getId());
            if (existing == null) {
                log.trace("[{}][{}] Creating rule node [{}]: {}", this.entityId, ruleNode.getId(), ruleNode.getName(),
                          ruleNode);
                ActorRef ruleNodeActor = this.createRuleNodeActor(context, ruleNode);
                this.nodeActors.put(ruleNode.getId(),
                                    new RuleNodeCtx(this.tenantId, this.self, ruleNodeActor, ruleNode));
            }
            else {
                log.trace("[{}][{}] Updating rule node [{}]: {}", this.entityId, ruleNode.getId(), ruleNode.getName(),
                          ruleNode);
                existing.setSelf(ruleNode);
                existing.getSelfActor().tell(new ComponentLifecycleMsg(this.tenantId, existing.getSelf().getId(),
                                                                       ComponentLifecycleEvent.UPDATED), this.self);
            }
        }

        Set<RuleNodeId> existingNodes = ruleNodeList.stream().map(RuleNode::getId).collect(Collectors.toSet());
        List<RuleNodeId> removedRules = this.nodeActors.keySet()
                                                       .stream()
                                                       .filter(node -> !existingNodes.contains(node))
                                                       .collect(Collectors.toList());
        removedRules.forEach(ruleNodeId -> {
            log.trace("[{}][{}] Removing rule node [{}]", this.tenantId, this.entityId, ruleNodeId);
            RuleNodeCtx removed = this.nodeActors.remove(ruleNodeId);
            removed.getSelfActor().tell(new ComponentLifecycleMsg(this.tenantId, removed.getSelf().getId(),
                                                                  ComponentLifecycleEvent.DELETED), this.self);
        });

        this.initRoutes(ruleChain, ruleNodeList);
    }

    @Override
    public void stop(ActorContext context) {
        log.trace("[{}][{}] Stopping rule chain with {} nodes", this.tenantId, this.entityId, this.nodeActors.size());
        this.nodeActors.values().stream().map(RuleNodeCtx::getSelfActor).forEach(context::stop);
        this.nodeActors.clear();
        this.nodeRoutes.clear();
        context.stop(this.self);
        this.started = false;
    }

    private ActorRef createRuleNodeActor(ActorContext context, RuleNode ruleNode) {
        String dispatcherName = this.tenantId.getId().equals(EntityId.NULL_UUID)
                                ? DefaultActorService.SYSTEM_RULE_DISPATCHER_NAME
                                : DefaultActorService.TENANT_RULE_DISPATCHER_NAME;
        return context.actorOf(Props.create(
                new RuleNodeActor.ActorCreator(this.systemContext, this.tenantId, this.entityId, ruleNode.getId()))
                                    .withDispatcher(dispatcherName), ruleNode.getId().toString());
    }

    private void initRoutes(RuleChain ruleChain, List<RuleNode> ruleNodeList) {
        this.nodeRoutes.clear();
        // Populating the routes map;
        for (RuleNode ruleNode : ruleNodeList) {
            List<EntityRelation> relations = this.service.getRuleNodeRelations(TenantId.SYS_TENANT_ID,
                                                                               ruleNode.getId());
            log.trace("[{}][{}][{}] Processing rule node relations [{}]", this.tenantId, this.entityId,
                      ruleNode.getId(), relations.size());
            if (relations.size() == 0) {
                this.nodeRoutes.put(ruleNode.getId(), Collections.emptyList());
            }
            else {
                for (EntityRelation relation : relations) {
                    log.trace("[{}][{}][{}] Processing rule node relation [{}]", this.tenantId, this.entityId,
                              ruleNode.getId(), relation.getTo());
                    if (relation.getTo().getEntityType() == EntityType.RULE_NODE) {
                        RuleNodeCtx ruleNodeCtx = this.nodeActors.get(new RuleNodeId(relation.getTo().getId()));
                        if (ruleNodeCtx == null) {
                            throw new IllegalArgumentException(
                                    "Rule Node [" + relation.getFrom() + "] has invalid relation to Rule node [" +
                                    relation.getTo() + "]");
                        }
                    }
                    this.nodeRoutes.computeIfAbsent(ruleNode.getId(), k -> new ArrayList<>()).add(
                            new RuleNodeRelation(ruleNode.getId(), relation.getTo(), relation.getType()));
                }
            }
        }

        this.firstId = ruleChain.getFirstRuleNodeId();
        this.firstNode = this.nodeActors.get(this.firstId);
        this.state = ComponentLifecycleState.ACTIVE;
    }

    void onServiceToRuleEngineMsg(ServiceToRuleEngineMsg envelope) {
        log.trace("[{}][{}] Processing message [{}]: {}", this.entityId, this.firstId, envelope.getTbMsg().getId(),
                  envelope.getTbMsg());
        this.checkActive();
        if (this.firstNode != null) {
            log.trace("[{}][{}] Pushing message to first rule node", this.entityId, this.firstId);
            this.pushMsgToNode(this.firstNode, this.enrichWithRuleChainId(envelope.getTbMsg()), "");
        }
    }

    private void pushMsgToNode(RuleNodeCtx nodeCtx, TbMsg msg, String fromRelationType) {
        if (nodeCtx != null) {
            nodeCtx.getSelfActor().tell(
                    new RuleChainToRuleNodeMsg(new DefaultTbContext(this.systemContext, nodeCtx), msg,
                                               fromRelationType), this.self);
        }
    }

    private TbMsg enrichWithRuleChainId(TbMsg tbMsg) {
        // We don't put firstNodeId because it may change over time;
        return new TbMsg(tbMsg.getId(), tbMsg.getType(), tbMsg.getOriginator(), tbMsg.getMetaData().copy(),
                         tbMsg.getData(), this.entityId, null, this.systemContext.getQueuePartitionId());
    }

    void onDeviceActorToRuleEngineMsg(DeviceActorToRuleEngineMsg envelope) {
        this.checkActive();
        if (this.firstNode != null) {
            this.pushMsgToNode(this.firstNode, this.enrichWithRuleChainId(envelope.getTbMsg()), "");
        }
    }

    void onRuleChainToRuleChainMsg(RuleChainToRuleChainMsg envelope) {
        this.checkActive();
        if (envelope.isEnqueue()) {
            if (this.firstNode != null) {
                this.pushMsgToNode(this.firstNode, this.enrichWithRuleChainId(envelope.getMsg()),
                                   envelope.getFromRelationType());
            }
        }
        else {
            if (this.firstNode != null) {
                this.pushMsgToNode(this.firstNode, envelope.getMsg(), envelope.getFromRelationType());
            }
            else {
                //                TODO: Ack this message in Kafka
                //                TbMsg msg = envelope.getMsg();
                //                EntityId ackId = msg.getRuleNodeId() != null ? msg.getRuleNodeId() : msg
                // .getRuleChainId();
                //                queue.ack(tenantId, envelope.getMsg(), ackId.getId(), msg.getClusterPartition());
            }
        }
    }

    void onTellNext(RuleNodeToRuleChainTellNextMsg envelope) {
        this.checkActive();
        TbMsg msg = envelope.getMsg();
        EntityId originatorEntityId = msg.getOriginator();
        Optional<ServerAddress> address = this.systemContext.getRoutingService().resolveById(originatorEntityId);

        if (address.isPresent()) {
            this.onRemoteTellNext(address.get(), envelope);
        }
        else {
            this.onLocalTellNext(envelope);
        }
    }

    private void onRemoteTellNext(ServerAddress serverAddress, RuleNodeToRuleChainTellNextMsg envelope) {
        TbMsg msg = envelope.getMsg();
        log.debug("Forwarding [{}] msg to remote server [{}] due to changed originator id: [{}]", msg.getId(),
                  serverAddress, msg.getOriginator());
        envelope = new RemoteToRuleChainTellNextMsg(envelope, this.tenantId, this.entityId);
        this.systemContext.getRpcService().tell(
                this.systemContext.getEncodingService().convertToProtoDataMessage(serverAddress, envelope));
    }

    private void onLocalTellNext(RuleNodeToRuleChainTellNextMsg envelope) {
        TbMsg msg = envelope.getMsg();
        RuleNodeId originatorNodeId = envelope.getOriginator();
        List<RuleNodeRelation> relations = this.nodeRoutes.get(originatorNodeId).stream().filter(
                r -> this.contains(envelope.getRelationTypes(), r.getType())).collect(Collectors.toList());
        int relationsCount = relations.size();
        EntityId ackId = msg.getRuleNodeId() != null ? msg.getRuleNodeId() : msg.getRuleChainId();
        if (relationsCount == 0) {
            log.trace("[{}][{}][{}] No outbound relations to process", this.tenantId, this.entityId, msg.getId());
            if (ackId != null) {
                //                TODO: Ack this message in Kafka
                //                queue.ack(tenantId, msg, ackId.getId(), msg.getClusterPartition());
            }
        }
        else if (relationsCount == 1) {
            for (RuleNodeRelation relation : relations) {
                log.trace("[{}][{}][{}] Pushing message to single target: [{}]", this.tenantId, this.entityId,
                          msg.getId(), relation.getOut());
                this.pushToTarget(msg, relation.getOut(), relation.getType());
            }
        }
        else {
            for (RuleNodeRelation relation : relations) {
                EntityId target = relation.getOut();
                log.trace("[{}][{}][{}] Pushing message to multiple targets: [{}]", this.tenantId, this.entityId,
                          msg.getId(), relation.getOut());
                switch (target.getEntityType()) {
                    case RULE_NODE:
                        this.enqueueAndForwardMsgCopyToNode(msg, target, relation.getType());
                        break;
                    case RULE_CHAIN:
                        this.enqueueAndForwardMsgCopyToChain(msg, target, relation.getType());
                        break;
                }
            }
            //TODO: Ideally this should happen in async way when all targets confirm that the copied messages are
            // successfully written to corresponding target queues.
            if (ackId != null) {
                //                TODO: Ack this message in Kafka
                //                queue.ack(tenantId, msg, ackId.getId(), msg.getClusterPartition());
            }
        }
    }

    private boolean contains(Set<String> relationTypes, String type) {
        if (relationTypes == null) {
            return true;
        }
        for (String relationType : relationTypes) {
            if (relationType.equalsIgnoreCase(type)) {
                return true;
            }
        }
        return false;
    }

    private void pushToTarget(TbMsg msg, EntityId target, String fromRelationType) {
        switch (target.getEntityType()) {
            case RULE_NODE:
                this.pushMsgToNode(this.nodeActors.get(new RuleNodeId(target.getId())), msg, fromRelationType);
                break;
            case RULE_CHAIN:
                this.parent.tell(new RuleChainToRuleChainMsg(new RuleChainId(target.getId()), this.entityId, msg,
                                                             fromRelationType, false), this.self);
                break;
        }
    }

    private void enqueueAndForwardMsgCopyToNode(TbMsg msg, EntityId target, String fromRelationType) {
        RuleNodeId targetId = new RuleNodeId(target.getId());
        RuleNodeCtx targetNodeCtx = this.nodeActors.get(targetId);
        TbMsg copy = msg.copy(UUIDs.timeBased(), this.entityId, targetId, DEFAULT_CLUSTER_PARTITION);
        this.pushMsgToNode(targetNodeCtx, copy, fromRelationType);
    }

    private void enqueueAndForwardMsgCopyToChain(TbMsg msg, EntityId target, String fromRelationType) {
        RuleChainId targetRCId = new RuleChainId(target.getId());
        TbMsg copyMsg = msg.copy(UUIDs.timeBased(), targetRCId, null, DEFAULT_CLUSTER_PARTITION);
        this.parent.tell(
                new RuleChainToRuleChainMsg(new RuleChainId(target.getId()), this.entityId, copyMsg, fromRelationType,
                                            true), this.self);
    }

}
