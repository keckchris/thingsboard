/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.actors.shared;


import akka.actor.ActorContext;
import akka.actor.ActorRef;
import akka.actor.Scheduler;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.thingsboard.server.actors.ActorSystemContext;
import scala.concurrent.ExecutionContextExecutor;
import scala.concurrent.duration.Duration;

import java.util.concurrent.TimeUnit;


@Slf4j
public abstract class AbstractContextAwareMsgProcessor {

    protected final ActorSystemContext systemContext;
    protected final ObjectMapper mapper = new ObjectMapper();

    protected AbstractContextAwareMsgProcessor(ActorSystemContext systemContext) {
        super();
        this.systemContext = systemContext;
    }

    protected void schedulePeriodicMsgWithDelay(ActorContext ctx, Object msg, long delayInMs, long periodInMs) {
        this.schedulePeriodicMsgWithDelay(msg, delayInMs, periodInMs, ctx.self());
    }

    private void schedulePeriodicMsgWithDelay(Object msg, long delayInMs, long periodInMs, ActorRef target) {
        log.debug("Scheduling periodic msg {} every {} ms with delay {} ms", msg, periodInMs, delayInMs);
        this.getScheduler().schedule(Duration.create(delayInMs, TimeUnit.MILLISECONDS),
                                     Duration.create(periodInMs, TimeUnit.MILLISECONDS), target, msg,
                                     this.getSystemDispatcher(), null);
    }

    private Scheduler getScheduler() {
        return this.systemContext.getScheduler();
    }

    private ExecutionContextExecutor getSystemDispatcher() {
        return this.systemContext.getActorSystem().dispatcher();
    }

    protected void scheduleMsgWithDelay(ActorContext ctx, Object msg, long delayInMs) {
        this.scheduleMsgWithDelay(msg, delayInMs, ctx.self());
    }

    private void scheduleMsgWithDelay(Object msg, long delayInMs, ActorRef target) {
        log.debug("Scheduling msg {} with delay {} ms", msg, delayInMs);
        this.getScheduler().scheduleOnce(Duration.create(delayInMs, TimeUnit.MILLISECONDS), target, msg,
                                         this.getSystemDispatcher(), null);
    }


}
