/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.actors.ruleChain;


import akka.actor.OneForOneStrategy;
import akka.actor.SupervisorStrategy;
import org.thingsboard.server.actors.ActorSystemContext;
import org.thingsboard.server.actors.device.DeviceActorToRuleEngineMsg;
import org.thingsboard.server.actors.service.ComponentActor;
import org.thingsboard.server.actors.service.ContextBasedCreator;
import org.thingsboard.server.common.data.id.RuleChainId;
import org.thingsboard.server.common.data.id.TenantId;
import org.thingsboard.server.common.msg.TbActorMsg;
import org.thingsboard.server.common.msg.plugin.ComponentLifecycleMsg;
import org.thingsboard.server.common.msg.system.ServiceToRuleEngineMsg;
import scala.concurrent.duration.Duration;


public class RuleChainActor extends ComponentActor<RuleChainId, RuleChainActorMessageProcessor> {

    private final SupervisorStrategy strategy = new OneForOneStrategy(3, Duration.create("1 minute"), t -> {
        this.logAndPersist("Unknown Failure", ActorSystemContext.toException(t));
        return SupervisorStrategy.resume();
    });

    private RuleChainActor(ActorSystemContext systemContext, TenantId tenantId, RuleChainId ruleChainId) {
        super(systemContext, tenantId, ruleChainId);
        this.setProcessor(
                new RuleChainActorMessageProcessor(tenantId, ruleChainId, systemContext, this.context().parent(),
                                                   this.context().self()));
    }

    @Override
    protected boolean process(TbActorMsg msg) {
        switch (msg.getMsgType()) {
            case COMPONENT_LIFE_CYCLE_MSG:
                this.onComponentLifecycleMsg((ComponentLifecycleMsg) msg);
                break;
            case SERVICE_TO_RULE_ENGINE_MSG:
                this.processor.onServiceToRuleEngineMsg((ServiceToRuleEngineMsg) msg);
                break;
            case DEVICE_ACTOR_TO_RULE_ENGINE_MSG:
                this.processor.onDeviceActorToRuleEngineMsg((DeviceActorToRuleEngineMsg) msg);
                break;
            case RULE_TO_RULE_CHAIN_TELL_NEXT_MSG:
            case REMOTE_TO_RULE_CHAIN_TELL_NEXT_MSG:
                this.processor.onTellNext((RuleNodeToRuleChainTellNextMsg) msg);
                break;
            case RULE_CHAIN_TO_RULE_CHAIN_MSG:
                this.processor.onRuleChainToRuleChainMsg((RuleChainToRuleChainMsg) msg);
                break;
            case CLUSTER_EVENT_MSG:
                break;
            case STATS_PERSIST_TICK_MSG:
                this.onStatsPersistTick(this.id);
                break;
            default:
                return false;
        }
        return true;
    }

    @Override
    protected long getErrorPersistFrequency() {
        return this.systemContext.getRuleChainErrorPersistFrequency();
    }

    @Override
    public SupervisorStrategy supervisorStrategy() {
        return this.strategy;
    }

    public static class ActorCreator extends ContextBasedCreator<RuleChainActor> {

        private static final long serialVersionUID = 1L;

        private final TenantId tenantId;
        private final RuleChainId ruleChainId;

        public ActorCreator(ActorSystemContext context, TenantId tenantId, RuleChainId pluginId) {
            super(context);
            this.tenantId = tenantId;
            this.ruleChainId = pluginId;
        }

        @Override
        public RuleChainActor create() {
            return new RuleChainActor(this.context, this.tenantId, this.ruleChainId);
        }

    }

}
