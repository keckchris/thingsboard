/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.actors.device;


import org.thingsboard.rule.engine.api.msg.DeviceAttributesEventNotificationMsg;
import org.thingsboard.rule.engine.api.msg.DeviceNameOrTypeUpdateMsg;
import org.thingsboard.server.actors.ActorSystemContext;
import org.thingsboard.server.actors.service.ContextAwareActor;
import org.thingsboard.server.common.data.id.DeviceId;
import org.thingsboard.server.common.data.id.TenantId;
import org.thingsboard.server.common.msg.TbActorMsg;
import org.thingsboard.server.common.msg.timeout.DeviceActorClientSideRpcTimeoutMsg;
import org.thingsboard.server.common.msg.timeout.DeviceActorServerSideRpcTimeoutMsg;
import org.thingsboard.server.service.rpc.ToDeviceRpcRequestActorMsg;
import org.thingsboard.server.service.rpc.ToServerRpcResponseActorMsg;
import org.thingsboard.server.service.transport.msg.TransportToDeviceActorMsgWrapper;


public class DeviceActor extends ContextAwareActor {

    private final DeviceActorMessageProcessor processor;

    DeviceActor(ActorSystemContext systemContext, TenantId tenantId, DeviceId deviceId) {
        super(systemContext);
        this.processor = new DeviceActorMessageProcessor(systemContext, tenantId, deviceId);
    }

    @Override
    public void preStart() {
        this.log.debug("[{}][{}] Starting device actor.", this.processor.tenantId, this.processor.deviceId);
        try {
            this.processor.initSessionTimeout(this.context());
            this.log.debug("[{}][{}] Device actor started.", this.processor.tenantId, this.processor.deviceId);
        } catch (Exception e) {
            this.log.warn("[{}][{}] Unknown failure", this.processor.tenantId, this.processor.deviceId, e);
        }
    }

    @Override
    protected boolean process(TbActorMsg msg) {
        switch (msg.getMsgType()) {
            case TRANSPORT_TO_DEVICE_ACTOR_MSG:
                this.processor.process(this.context(), (TransportToDeviceActorMsgWrapper) msg);
                break;
            case DEVICE_ATTRIBUTES_UPDATE_TO_DEVICE_ACTOR_MSG:
                this.processor.processAttributesUpdate(this.context(), (DeviceAttributesEventNotificationMsg) msg);
                break;
            case DEVICE_CREDENTIALS_UPDATE_TO_DEVICE_ACTOR_MSG:
                this.processor.processCredentialsUpdate();
                break;
            case DEVICE_NAME_OR_TYPE_UPDATE_TO_DEVICE_ACTOR_MSG:
                this.processor.processNameOrTypeUpdate((DeviceNameOrTypeUpdateMsg) msg);
                break;
            case DEVICE_RPC_REQUEST_TO_DEVICE_ACTOR_MSG:
                this.processor.processRpcRequest(this.context(), (ToDeviceRpcRequestActorMsg) msg);
                break;
            case SERVER_RPC_RESPONSE_TO_DEVICE_ACTOR_MSG:
                this.processor.processToServerRPCResponse(this.context(), (ToServerRpcResponseActorMsg) msg);
                break;
            case DEVICE_ACTOR_SERVER_SIDE_RPC_TIMEOUT_MSG:
                this.processor.processServerSideRpcTimeout(this.context(), (DeviceActorServerSideRpcTimeoutMsg) msg);
                break;
            case DEVICE_ACTOR_CLIENT_SIDE_RPC_TIMEOUT_MSG:
                this.processor.processClientSideRpcTimeout(this.context(), (DeviceActorClientSideRpcTimeoutMsg) msg);
                break;
            case SESSION_TIMEOUT_MSG:
                this.processor.checkSessionsTimeout();
                break;
            default:
                return false;
        }
        return true;
    }

}
