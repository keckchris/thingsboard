/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.actors.shared;


import akka.actor.ActorContext;
import lombok.extern.slf4j.Slf4j;
import org.thingsboard.server.actors.ActorSystemContext;
import org.thingsboard.server.actors.stats.StatsPersistTick;
import org.thingsboard.server.common.data.id.EntityId;
import org.thingsboard.server.common.data.id.TenantId;
import org.thingsboard.server.common.data.plugin.ComponentLifecycleState;
import org.thingsboard.server.common.msg.cluster.ClusterEventMsg;


@Slf4j
public abstract class ComponentMsgProcessor<T extends EntityId> extends AbstractContextAwareMsgProcessor {

    protected final TenantId tenantId;
    protected final T entityId;
    protected ComponentLifecycleState state;

    protected ComponentMsgProcessor(ActorSystemContext systemContext, TenantId tenantId, T id) {
        super(systemContext);
        this.tenantId = tenantId;
        this.entityId = id;
    }

    public abstract String getComponentName();

    public abstract void onClusterEventMsg(ClusterEventMsg msg) throws Exception;

    public void onCreated(ActorContext context) throws Exception {
        this.start(context);
    }

    public abstract void start(ActorContext context) throws Exception;

    public void onUpdate(ActorContext context) throws Exception {
        this.restart(context);
    }

    private void restart(ActorContext context) throws Exception {
        this.stop(context);
        this.start(context);
    }

    public abstract void stop(ActorContext context) throws Exception;

    public void onActivate(ActorContext context) throws Exception {
        this.restart(context);
    }

    public void onSuspend(ActorContext context) throws Exception {
        this.stop(context);
    }

    public void onStop(ActorContext context) throws Exception {
        this.stop(context);
    }

    public void scheduleStatsPersistTick(ActorContext context, long statsPersistFrequency) {
        this.schedulePeriodicMsgWithDelay(context, new StatsPersistTick(), statsPersistFrequency,
                                          statsPersistFrequency);
    }

    protected void checkActive() {
        if (this.state != ComponentLifecycleState.ACTIVE) {
            log.warn("Rule chain is not active. Current state [{}] for processor [{}] tenant [{}]", this.state,
                     this.tenantId, this.entityId);
            throw new IllegalStateException("Rule chain is not active! " + this.entityId + " - " + this.tenantId);
        }
    }

}
