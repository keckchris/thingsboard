/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.actors.service;


import akka.actor.ActorRef;
import org.thingsboard.server.actors.ActorSystemContext;
import org.thingsboard.server.actors.shared.ComponentMsgProcessor;
import org.thingsboard.server.actors.stats.StatsPersistMsg;
import org.thingsboard.server.common.data.id.EntityId;
import org.thingsboard.server.common.data.id.TenantId;
import org.thingsboard.server.common.data.plugin.ComponentLifecycleEvent;
import org.thingsboard.server.common.msg.cluster.ClusterEventMsg;
import org.thingsboard.server.common.msg.plugin.ComponentLifecycleMsg;


/**
 * @author Andrew Shvayka
 */
public abstract class ComponentActor<T extends EntityId, P extends ComponentMsgProcessor<T>> extends ContextAwareActor {

    protected final TenantId tenantId;
    protected final T id;
    protected P processor;
    private long lastPersistedErrorTs = 0L;
    private long messagesProcessed;
    private long errorsOccurred;

    public ComponentActor(ActorSystemContext systemContext, TenantId tenantId, T id) {
        super(systemContext);
        this.tenantId = tenantId;
        this.id = id;
    }

    protected void setProcessor(P processor) {
        this.processor = processor;
    }

    @Override
    public void preStart() {
        try {
            this.log.debug("[{}][{}][{}] Starting processor.", this.tenantId, this.id, this.id.getEntityType());
            this.processor.start(this.context());
            this.logLifecycleEvent(ComponentLifecycleEvent.STARTED);
            if (this.systemContext.isStatisticsEnabled()) {
                this.scheduleStatsPersistTick();
            }
        } catch (Exception e) {
            this.log.warn("[{}][{}] Failed to start {} processor: {}", this.tenantId, this.id, this.id.getEntityType(),
                          e);
            this.logAndPersist("OnStart", e, true);
            this.logLifecycleEvent(ComponentLifecycleEvent.STARTED, e);
        }
    }

    @Override
    public void postStop() {
        try {
            this.log.debug("[{}][{}] Stopping processor.", this.tenantId, this.id, this.id.getEntityType());
            this.processor.stop(this.context());
            this.logLifecycleEvent(ComponentLifecycleEvent.STOPPED);
        } catch (Exception e) {
            this.log.warn("[{}][{}] Failed to stop {} processor: {}", this.tenantId, this.id, this.id.getEntityType(),
                          e.getMessage());
            this.logAndPersist("OnStop", e, true);
            this.logLifecycleEvent(ComponentLifecycleEvent.STOPPED, e);
        }
    }

    private void logLifecycleEvent(ComponentLifecycleEvent event) {
        this.logLifecycleEvent(event, null);
    }

    private void logAndPersist(String method, Exception e, boolean critical) {
        this.errorsOccurred++;
        if (critical) {
            this.log.warn("[{}][{}][{}] Failed to process {} msg: {}", this.id, this.tenantId,
                          this.processor.getComponentName(), method, e);
        }
        else {
            this.log.debug("[{}][{}][{}] Failed to process {} msg: {}", this.id, this.tenantId,
                           this.processor.getComponentName(), method, e);
        }
        long ts = System.currentTimeMillis();
        if (ts - this.lastPersistedErrorTs > this.getErrorPersistFrequency()) {
            this.systemContext.persistError(this.tenantId, this.id, method, e);
            this.lastPersistedErrorTs = ts;
        }
    }

    private void logLifecycleEvent(ComponentLifecycleEvent event, Exception e) {
        this.systemContext.persistLifecycleEvent(this.tenantId, this.id, event, e);
    }

    protected abstract long getErrorPersistFrequency();

    private void scheduleStatsPersistTick() {
        try {
            this.processor.scheduleStatsPersistTick(this.context(), this.systemContext.getStatisticsPersistFrequency());
        } catch (Exception e) {
            this.log.error(
                    "[{}][{}] Failed to schedule statistics store message. No statistics is going to be stored: {}",
                    this.tenantId, this.id, e.getMessage());
            this.logAndPersist("onScheduleStatsPersistMsg", e);
        }
    }

    protected void onComponentLifecycleMsg(ComponentLifecycleMsg msg) {
        this.log.debug("[{}][{}][{}] onComponentLifecycleMsg: [{}]", this.tenantId, this.id, this.id.getEntityType(),
                       msg.getEvent());
        try {
            switch (msg.getEvent()) {
                case CREATED:
                    this.processor.onCreated(this.context());
                    break;
                case UPDATED:
                    this.processor.onUpdate(this.context());
                    break;
                case ACTIVATED:
                    this.processor.onActivate(this.context());
                    break;
                case SUSPENDED:
                    this.processor.onSuspend(this.context());
                    break;
                case DELETED:
                    this.processor.onStop(this.context());
                    break;
                default:
                    break;
            }
            this.logLifecycleEvent(msg.getEvent());
        } catch (Exception e) {
            this.logAndPersist("onLifecycleMsg", e, true);
            this.logLifecycleEvent(msg.getEvent(), e);
        }
    }

    protected void onClusterEventMsg(ClusterEventMsg msg) {
        try {
            this.processor.onClusterEventMsg(msg);
        } catch (Exception e) {
            this.logAndPersist("onClusterEventMsg", e);
        }
    }

    protected void logAndPersist(String method, Exception e) {
        this.logAndPersist(method, e, false);
    }

    protected void onStatsPersistTick(EntityId entityId) {
        try {
            this.systemContext.getStatsActor().tell(
                    new StatsPersistMsg(this.messagesProcessed, this.errorsOccurred, this.tenantId, entityId),
                    ActorRef.noSender());
            this.resetStatsCounters();
        } catch (Exception e) {
            this.logAndPersist("onStatsPersistTick", e);
        }
    }

    private void resetStatsCounters() {
        this.messagesProcessed = 0;
        this.errorsOccurred = 0;
    }

    protected void increaseMessagesProcessedCount() {
        this.messagesProcessed++;
    }

}
