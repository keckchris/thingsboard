/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.actors.device;


import akka.actor.ActorContext;
import com.datastax.driver.core.utils.UUIDs;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.protobuf.InvalidProtocolBufferException;
import lombok.extern.slf4j.Slf4j;
import org.thingsboard.rule.engine.api.RpcError;
import org.thingsboard.rule.engine.api.msg.DeviceAttributesEventNotificationMsg;
import org.thingsboard.rule.engine.api.msg.DeviceNameOrTypeUpdateMsg;
import org.thingsboard.server.actors.ActorSystemContext;
import org.thingsboard.server.actors.shared.AbstractContextAwareMsgProcessor;
import org.thingsboard.server.common.data.DataConstants;
import org.thingsboard.server.common.data.Device;
import org.thingsboard.server.common.data.id.DeviceId;
import org.thingsboard.server.common.data.id.TenantId;
import org.thingsboard.server.common.data.kv.AttributeKey;
import org.thingsboard.server.common.data.kv.AttributeKvEntry;
import org.thingsboard.server.common.data.kv.KvEntry;
import org.thingsboard.server.common.data.rpc.ToDeviceRpcRequestBody;
import org.thingsboard.server.common.msg.TbMsg;
import org.thingsboard.server.common.msg.TbMsgDataType;
import org.thingsboard.server.common.msg.TbMsgMetaData;
import org.thingsboard.server.common.msg.rpc.ToDeviceRpcRequest;
import org.thingsboard.server.common.msg.session.SessionMsgType;
import org.thingsboard.server.common.msg.timeout.DeviceActorClientSideRpcTimeoutMsg;
import org.thingsboard.server.common.msg.timeout.DeviceActorServerSideRpcTimeoutMsg;
import org.thingsboard.server.gen.transport.TransportProtos;
import org.thingsboard.server.gen.transport.TransportProtos.*;
import org.thingsboard.server.service.rpc.FromDeviceRpcResponse;
import org.thingsboard.server.service.rpc.ToDeviceRpcRequestActorMsg;
import org.thingsboard.server.service.rpc.ToServerRpcResponseActorMsg;
import org.thingsboard.server.service.transport.msg.TransportToDeviceActorMsgWrapper;

import javax.annotation.Nullable;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;


/**
 * @author Andrew Shvayka
 */
@Slf4j
class DeviceActorMessageProcessor extends AbstractContextAwareMsgProcessor {

    final TenantId tenantId;
    final DeviceId deviceId;
    private final Map<UUID, SessionInfoMetaData> sessions;
    private final Map<UUID, SessionInfo> attributeSubscriptions;
    private final Map<UUID, SessionInfo> rpcSubscriptions;
    private final Map<Integer, ToDeviceRpcRequestMetadata> toDeviceRpcPendingMap;
    private final Map<Integer, ToServerRpcRequestMetadata> toServerRpcPendingMap;

    private final Gson gson = new Gson();
    private final JsonParser jsonParser = new JsonParser();

    private int rpcSeq = 0;
    private String deviceName;
    private String deviceType;
    private TbMsgMetaData defaultMetaData;

    DeviceActorMessageProcessor(ActorSystemContext systemContext, TenantId tenantId, DeviceId deviceId) {
        super(systemContext);
        this.tenantId = tenantId;
        this.deviceId = deviceId;
        this.sessions = new LinkedHashMap<>();
        this.attributeSubscriptions = new HashMap<>();
        this.rpcSubscriptions = new HashMap<>();
        this.toDeviceRpcPendingMap = new HashMap<>();
        this.toServerRpcPendingMap = new HashMap<>();
        this.initAttributes();
        this.restoreSessions();
    }

    private void initAttributes() {
        Device device = this.systemContext.getDeviceService().findDeviceById(this.tenantId, this.deviceId);
        this.deviceName = device.getName();
        this.deviceType = device.getType();
        this.defaultMetaData = new TbMsgMetaData();
        this.defaultMetaData.putValue("deviceName", this.deviceName);
        this.defaultMetaData.putValue("deviceType", this.deviceType);
    }

    private void restoreSessions() {
        log.debug("[{}] Restoring sessions from cache", this.deviceId);
        TransportProtos.DeviceSessionsCacheEntry sessionsDump = null;
        try {
            sessionsDump = TransportProtos.DeviceSessionsCacheEntry.parseFrom(
                    this.systemContext.getDeviceSessionCacheService().get(this.deviceId));
        } catch (InvalidProtocolBufferException e) {
            log.warn("[{}] Failed to decode device sessions from cache", this.deviceId);
            return;
        }
        if (sessionsDump.getSessionsCount() == 0) {
            log.debug("[{}] No session information found", this.deviceId);
            return;
        }
        for (TransportProtos.SessionSubscriptionInfoProto sessionSubscriptionInfoProto :
                sessionsDump.getSessionsList()) {
            SessionInfoProto sessionInfoProto = sessionSubscriptionInfoProto.getSessionInfo();
            UUID sessionId = this.getSessionId(sessionInfoProto);
            SessionInfo sessionInfo = new SessionInfo(TransportProtos.SessionType.ASYNC, sessionInfoProto.getNodeId());
            TransportProtos.SubscriptionInfoProto subInfo = sessionSubscriptionInfoProto.getSubscriptionInfo();
            SessionInfoMetaData sessionMD = new SessionInfoMetaData(sessionInfo, subInfo.getLastActivityTime());
            this.sessions.put(sessionId, sessionMD);
            if (subInfo.getAttributeSubscription()) {
                this.attributeSubscriptions.put(sessionId, sessionInfo);
                sessionMD.setSubscribedToAttributes(true);
            }
            if (subInfo.getRpcSubscription()) {
                this.rpcSubscriptions.put(sessionId, sessionInfo);
                sessionMD.setSubscribedToRPC(true);
            }
            log.debug("[{}] Restored session: {}", this.deviceId, sessionMD);
        }
        log.debug("[{}] Restored sessions: {}, rpc subscriptions: {}, attribute subscriptions: {}", this.deviceId,
                  this.sessions.size(), this.rpcSubscriptions.size(), this.attributeSubscriptions.size());
    }

    private UUID getSessionId(SessionInfoProto sessionInfo) {
        return new UUID(sessionInfo.getSessionIdMSB(), sessionInfo.getSessionIdLSB());
    }

    void processRpcRequest(ActorContext context, ToDeviceRpcRequestActorMsg msg) {
        ToDeviceRpcRequest request = msg.getMsg();
        ToDeviceRpcRequestBody body = request.getBody();
        ToDeviceRpcRequestMsg rpcRequest = ToDeviceRpcRequestMsg.newBuilder().setRequestId(this.rpcSeq++).setMethodName(
                body.getMethod()).setParams(body.getParams()).build();

        long timeout = request.getExpirationTime() - System.currentTimeMillis();
        if (timeout <= 0) {
            log.debug("[{}][{}] Ignoring message due to exp time reached, {}", this.deviceId, request.getId(),
                      request.getExpirationTime());
            return;
        }

        boolean sent = this.rpcSubscriptions.size() > 0;
        Set<UUID> syncSessionSet = new HashSet<>();
        this.rpcSubscriptions.forEach((key, value) -> {
            this.sendToTransport(rpcRequest, key, value.getNodeId());
            if (TransportProtos.SessionType.SYNC == value.getType()) {
                syncSessionSet.add(key);
            }
        });
        syncSessionSet.forEach(this.rpcSubscriptions::remove);

        if (request.isOneway() && sent) {
            log.debug("[{}] Rpc command response sent [{}]!", this.deviceId, request.getId());
            this.systemContext.getDeviceRpcService().processResponseToServerSideRPCRequestFromDeviceActor(
                    new FromDeviceRpcResponse(msg.getMsg().getId(), null, null));
        }
        else {
            this.registerPendingRpcRequest(context, msg, sent, rpcRequest, timeout);
        }
        if (sent) {
            log.debug("[{}] RPC request {} is sent!", this.deviceId, request.getId());
        }
        else {
            log.debug("[{}] RPC request {} is NOT sent!", this.deviceId, request.getId());
        }
    }

    private void sendToTransport(ToDeviceRpcRequestMsg rpcMsg, UUID sessionId, String nodeId) {
        DeviceActorToTransportMsg msg = DeviceActorToTransportMsg.newBuilder()
                                                                 .setSessionIdMSB(sessionId.getMostSignificantBits())
                                                                 .setSessionIdLSB(sessionId.getLeastSignificantBits())
                                                                 .setToDeviceRequest(rpcMsg)
                                                                 .build();
        this.systemContext.getRuleEngineTransportService().process(nodeId, msg);
    }

    private void registerPendingRpcRequest(ActorContext context,
                                           ToDeviceRpcRequestActorMsg msg,
                                           boolean sent,
                                           ToDeviceRpcRequestMsg rpcRequest,
                                           long timeout) {
        this.toDeviceRpcPendingMap.put(rpcRequest.getRequestId(), new ToDeviceRpcRequestMetadata(msg, sent));
        DeviceActorServerSideRpcTimeoutMsg timeoutMsg = new DeviceActorServerSideRpcTimeoutMsg(
                rpcRequest.getRequestId(), timeout);
        this.scheduleMsgWithDelay(context, timeoutMsg, timeoutMsg.getTimeout());
    }

    void processServerSideRpcTimeout(ActorContext context, DeviceActorServerSideRpcTimeoutMsg msg) {
        ToDeviceRpcRequestMetadata requestMd = this.toDeviceRpcPendingMap.remove(msg.getId());
        if (requestMd != null) {
            log.debug("[{}] RPC request [{}] timeout detected!", this.deviceId, msg.getId());
            this.systemContext.getDeviceRpcService().processResponseToServerSideRPCRequestFromDeviceActor(
                    new FromDeviceRpcResponse(requestMd.getMsg().getMsg().getId(), null,
                                              requestMd.isSent() ? RpcError.TIMEOUT : RpcError.NO_ACTIVE_CONNECTION));
        }
    }

    void process(ActorContext context, TransportToDeviceActorMsgWrapper wrapper) {
        TransportToDeviceActorMsg msg = wrapper.getMsg();
        if (msg.hasSessionEvent()) {
            this.processSessionStateMsgs(msg.getSessionInfo(), msg.getSessionEvent());
        }
        if (msg.hasSubscribeToAttributes()) {
            this.processSubscriptionCommands(context, msg.getSessionInfo(), msg.getSubscribeToAttributes());
        }
        if (msg.hasSubscribeToRPC()) {
            this.processSubscriptionCommands(context, msg.getSessionInfo(), msg.getSubscribeToRPC());
        }
        if (msg.hasPostAttributes()) {
            this.handlePostAttributesRequest(context, msg.getSessionInfo(), msg.getPostAttributes());
            this.reportLogicalDeviceActivity();
        }
        if (msg.hasPostTelemetry()) {
            this.handlePostTelemetryRequest(context, msg.getSessionInfo(), msg.getPostTelemetry());
            this.reportLogicalDeviceActivity();
        }
        if (msg.hasGetAttributes()) {
            this.handleGetAttributesRequest(context, msg.getSessionInfo(), msg.getGetAttributes());
        }
        if (msg.hasToDeviceRPCCallResponse()) {
            this.processRpcResponses(context, msg.getSessionInfo(), msg.getToDeviceRPCCallResponse());
        }
        if (msg.hasToServerRPCCallRequest()) {
            this.handleClientSideRPCRequest(context, msg.getSessionInfo(), msg.getToServerRPCCallRequest());
            this.reportLogicalDeviceActivity();
        }
        if (msg.hasSubscriptionInfo()) {
            this.handleSessionActivity(context, msg.getSessionInfo(), msg.getSubscriptionInfo());
        }
    }

    private void processSessionStateMsgs(SessionInfoProto sessionInfo, SessionEventMsg msg) {
        UUID sessionId = this.getSessionId(sessionInfo);
        if (msg.getEvent() == SessionEvent.OPEN) {
            if (this.sessions.containsKey(sessionId)) {
                log.debug("[{}] Received duplicate session open event [{}]", this.deviceId, sessionId);
                return;
            }
            log.debug("[{}] Processing new session [{}]", this.deviceId, sessionId);
            if (this.sessions.size() >= this.systemContext.getMaxConcurrentSessionsPerDevice()) {
                UUID sessionIdToRemove = this.sessions.keySet().stream().findFirst().orElse(null);
                if (sessionIdToRemove != null) {
                    this.notifyTransportAboutClosedSession(sessionIdToRemove, this.sessions.remove(sessionIdToRemove));
                }
            }
            this.sessions.put(sessionId, new SessionInfoMetaData(
                    new SessionInfo(TransportProtos.SessionType.ASYNC, sessionInfo.getNodeId())));
            if (this.sessions.size() == 1) {
                this.reportSessionOpen();
            }
            this.dumpSessions();
        }
        else if (msg.getEvent() == SessionEvent.CLOSED) {
            log.debug("[{}] Canceling subscriptions for closed session [{}]", this.deviceId, sessionId);
            this.sessions.remove(sessionId);
            this.attributeSubscriptions.remove(sessionId);
            this.rpcSubscriptions.remove(sessionId);
            if (this.sessions.isEmpty()) {
                this.reportSessionClose();
            }
            this.dumpSessions();
        }
    }

    private void processSubscriptionCommands(ActorContext context,
                                             SessionInfoProto sessionInfo,
                                             SubscribeToAttributeUpdatesMsg subscribeCmd) {
        UUID sessionId = this.getSessionId(sessionInfo);
        if (subscribeCmd.getUnsubscribe()) {
            log.debug("[{}] Canceling attributes subscription for session [{}]", this.deviceId, sessionId);
            this.attributeSubscriptions.remove(sessionId);
        }
        else {
            SessionInfoMetaData sessionMD = this.sessions.get(sessionId);
            if (sessionMD == null) {
                sessionMD = new SessionInfoMetaData(
                        new SessionInfo(TransportProtos.SessionType.SYNC, sessionInfo.getNodeId()));
            }
            sessionMD.setSubscribedToAttributes(true);
            log.debug("[{}] Registering attributes subscription for session [{}]", this.deviceId, sessionId);
            this.attributeSubscriptions.put(sessionId, sessionMD.getSessionInfo());
            this.dumpSessions();
        }
    }

    private void processSubscriptionCommands(ActorContext context,
                                             SessionInfoProto sessionInfo,
                                             SubscribeToRPCMsg subscribeCmd) {
        UUID sessionId = this.getSessionId(sessionInfo);
        if (subscribeCmd.getUnsubscribe()) {
            log.debug("[{}] Canceling rpc subscription for session [{}]", this.deviceId, sessionId);
            this.rpcSubscriptions.remove(sessionId);
        }
        else {
            SessionInfoMetaData sessionMD = this.sessions.get(sessionId);
            if (sessionMD == null) {
                sessionMD = new SessionInfoMetaData(
                        new SessionInfo(TransportProtos.SessionType.SYNC, sessionInfo.getNodeId()));
            }
            sessionMD.setSubscribedToRPC(true);
            log.debug("[{}] Registering rpc subscription for session [{}]", this.deviceId, sessionId);
            this.rpcSubscriptions.put(sessionId, sessionMD.getSessionInfo());
            this.sendPendingRequests(context, sessionId, sessionInfo);
            this.dumpSessions();
        }
    }

    private void handlePostAttributesRequest(ActorContext context,
                                             SessionInfoProto sessionInfo,
                                             PostAttributeMsg postAttributes) {
        JsonObject json = this.getJsonObject(postAttributes.getKvList());
        TbMsg tbMsg = new TbMsg(UUIDs.timeBased(), SessionMsgType.POST_ATTRIBUTES_REQUEST.name(), this.deviceId,
                                this.defaultMetaData.copy(), TbMsgDataType.JSON, this.gson.toJson(json), null, null,
                                0L);
        this.pushToRuleEngine(context, tbMsg);
    }

    private void reportLogicalDeviceActivity() {
        this.systemContext.getDeviceStateService().onDeviceActivity(this.deviceId);
    }

    private void handlePostTelemetryRequest(ActorContext context,
                                            SessionInfoProto sessionInfo,
                                            PostTelemetryMsg postTelemetry) {
        for (TsKvListProto tsKv : postTelemetry.getTsKvListList()) {
            JsonObject json = this.getJsonObject(tsKv.getKvList());
            TbMsgMetaData metaData = this.defaultMetaData.copy();
            metaData.putValue("ts", tsKv.getTs() + "");
            TbMsg tbMsg = new TbMsg(UUIDs.timeBased(), SessionMsgType.POST_TELEMETRY_REQUEST.name(), this.deviceId,
                                    metaData, TbMsgDataType.JSON, this.gson.toJson(json), null, null, 0L);
            this.pushToRuleEngine(context, tbMsg);
        }
    }

    private void handleGetAttributesRequest(ActorContext context,
                                            SessionInfoProto sessionInfo,
                                            GetAttributeRequestMsg request) {
        ListenableFuture<List<AttributeKvEntry>> clientAttributesFuture = this.getAttributeKvEntries(this.deviceId,
                                                                                                     DataConstants.CLIENT_SCOPE,
                                                                                                     this.toOptionalSet(
                                                                                                             request.getClientAttributeNamesList()));
        ListenableFuture<List<AttributeKvEntry>> sharedAttributesFuture = this.getAttributeKvEntries(this.deviceId,
                                                                                                     DataConstants.SHARED_SCOPE,
                                                                                                     this.toOptionalSet(
                                                                                                             request.getSharedAttributeNamesList()));
        int requestId = request.getRequestId();
        Futures.addCallback(Futures.allAsList(Arrays.asList(clientAttributesFuture, sharedAttributesFuture)),
                            new FutureCallback<List<List<AttributeKvEntry>>>() {
                                @Override
                                public void onSuccess(@Nullable List<List<AttributeKvEntry>> result) {
                                    GetAttributeResponseMsg responseMsg = GetAttributeResponseMsg.newBuilder()
                                                                                                 .setRequestId(
                                                                                                         requestId)
                                                                                                 .addAllClientAttributeList(
                                                                                                         DeviceActorMessageProcessor.this
                                                                                                                 .toTsKvProtos(
                                                                                                                         result.get(
                                                                                                                                 0)))
                                                                                                 .addAllSharedAttributeList(
                                                                                                         DeviceActorMessageProcessor.this
                                                                                                                 .toTsKvProtos(
                                                                                                                         result.get(
                                                                                                                                 1)))
                                                                                                 .build();
                                    DeviceActorMessageProcessor.this.sendToTransport(responseMsg, sessionInfo);
                                }

                                @Override
                                public void onFailure(Throwable t) {
                                    GetAttributeResponseMsg responseMsg = GetAttributeResponseMsg.newBuilder().setError(
                                            t.getMessage()).build();
                                    DeviceActorMessageProcessor.this.sendToTransport(responseMsg, sessionInfo);
                                }
                            });
    }

    private void processRpcResponses(ActorContext context,
                                     SessionInfoProto sessionInfo,
                                     ToDeviceRpcResponseMsg responseMsg) {
        UUID sessionId = this.getSessionId(sessionInfo);
        log.debug("[{}] Processing rpc command response [{}]", this.deviceId, sessionId);
        ToDeviceRpcRequestMetadata requestMd = this.toDeviceRpcPendingMap.remove(responseMsg.getRequestId());
        boolean success = requestMd != null;
        if (success) {
            this.systemContext.getDeviceRpcService().processResponseToServerSideRPCRequestFromDeviceActor(
                    new FromDeviceRpcResponse(requestMd.getMsg().getMsg().getId(), responseMsg.getPayload(), null));
        }
        else {
            log.debug("[{}] Rpc command response [{}] is stale!", this.deviceId, responseMsg.getRequestId());
        }
    }

    private void handleClientSideRPCRequest(ActorContext context,
                                            SessionInfoProto sessionInfo,
                                            TransportProtos.ToServerRpcRequestMsg request) {
        UUID sessionId = this.getSessionId(sessionInfo);
        JsonObject json = new JsonObject();
        json.addProperty("method", request.getMethodName());
        json.add("params", this.jsonParser.parse(request.getParams()));

        TbMsgMetaData requestMetaData = this.defaultMetaData.copy();
        requestMetaData.putValue("requestId", Integer.toString(request.getRequestId()));
        TbMsg tbMsg = new TbMsg(UUIDs.timeBased(), SessionMsgType.TO_SERVER_RPC_REQUEST.name(), this.deviceId,
                                requestMetaData, TbMsgDataType.JSON, this.gson.toJson(json), null, null, 0L);
        context.parent().tell(new DeviceActorToRuleEngineMsg(context.self(), tbMsg), context.self());

        this.scheduleMsgWithDelay(context, new DeviceActorClientSideRpcTimeoutMsg(request.getRequestId(),
                                                                                  this.systemContext.getClientSideRpcTimeout()),
                                  this.systemContext.getClientSideRpcTimeout());
        this.toServerRpcPendingMap.put(request.getRequestId(),
                                       new ToServerRpcRequestMetadata(sessionId, this.getSessionType(sessionId),
                                                                      sessionInfo.getNodeId()));
    }

    private void handleSessionActivity(ActorContext context,
                                       SessionInfoProto sessionInfoProto,
                                       TransportProtos.SubscriptionInfoProto subscriptionInfo) {
        UUID sessionId = this.getSessionId(sessionInfoProto);
        SessionInfoMetaData sessionMD = this.sessions.computeIfAbsent(sessionId, id -> new SessionInfoMetaData(
                new SessionInfo(TransportProtos.SessionType.ASYNC, sessionInfoProto.getNodeId()), 0L));

        sessionMD.setLastActivityTime(subscriptionInfo.getLastActivityTime());
        sessionMD.setSubscribedToAttributes(subscriptionInfo.getAttributeSubscription());
        sessionMD.setSubscribedToRPC(subscriptionInfo.getRpcSubscription());
        if (subscriptionInfo.getAttributeSubscription()) {
            this.attributeSubscriptions.putIfAbsent(sessionId, sessionMD.getSessionInfo());
        }
        if (subscriptionInfo.getRpcSubscription()) {
            this.rpcSubscriptions.putIfAbsent(sessionId, sessionMD.getSessionInfo());
        }
        this.dumpSessions();
    }

    private void notifyTransportAboutClosedSession(UUID sessionId, SessionInfoMetaData sessionMd) {
        DeviceActorToTransportMsg msg = DeviceActorToTransportMsg.newBuilder()
                                                                 .setSessionIdMSB(sessionId.getMostSignificantBits())
                                                                 .setSessionIdLSB(sessionId.getLeastSignificantBits())
                                                                 .setSessionCloseNotification(
                                                                         SessionCloseNotificationProto.getDefaultInstance())
                                                                 .build();
        this.systemContext.getRuleEngineTransportService().process(sessionMd.getSessionInfo().getNodeId(), msg);
    }

    private void reportSessionOpen() {
        this.systemContext.getDeviceStateService().onDeviceConnect(this.deviceId);
    }

    private void dumpSessions() {
        log.debug("[{}] Dumping sessions: {}, rpc subscriptions: {}, attribute subscriptions: {} to cache",
                  this.deviceId, this.sessions.size(), this.rpcSubscriptions.size(),
                  this.attributeSubscriptions.size());
        List<TransportProtos.SessionSubscriptionInfoProto> sessionsList = new ArrayList<>(this.sessions.size());
        this.sessions.forEach((uuid, sessionMD) -> {
            if (sessionMD.getSessionInfo().getType() == TransportProtos.SessionType.SYNC) {
                return;
            }
            SessionInfo sessionInfo = sessionMD.getSessionInfo();
            TransportProtos.SubscriptionInfoProto subscriptionInfoProto =
                    TransportProtos.SubscriptionInfoProto.newBuilder()
                                                         .setLastActivityTime(sessionMD.getLastActivityTime())
                                                         .setAttributeSubscription(sessionMD.isSubscribedToAttributes())
                                                         .setRpcSubscription(sessionMD.isSubscribedToRPC())
                                                         .build();
            TransportProtos.SessionInfoProto sessionInfoProto = TransportProtos.SessionInfoProto.newBuilder()
                                                                                                .setSessionIdMSB(
                                                                                                        uuid.getMostSignificantBits())
                                                                                                .setSessionIdLSB(
                                                                                                        uuid.getLeastSignificantBits())
                                                                                                .setNodeId(
                                                                                                        sessionInfo.getNodeId())
                                                                                                .build();
            sessionsList.add(TransportProtos.SessionSubscriptionInfoProto.newBuilder()
                                                                         .setSessionInfo(sessionInfoProto)
                                                                         .setSubscriptionInfo(subscriptionInfoProto)
                                                                         .build());
            log.debug("[{}] Dumping session: {}", this.deviceId, sessionMD);
        });
        this.systemContext.getDeviceSessionCacheService().put(this.deviceId,
                                                              TransportProtos.DeviceSessionsCacheEntry.newBuilder()
                                                                                                      .addAllSessions(
                                                                                                              sessionsList)
                                                                                                      .build()
                                                                                                      .toByteArray());
    }

    private void reportSessionClose() {
        this.systemContext.getDeviceStateService().onDeviceDisconnect(this.deviceId);
    }

    private void sendPendingRequests(ActorContext context, UUID sessionId, SessionInfoProto sessionInfo) {
        TransportProtos.SessionType sessionType = this.getSessionType(sessionId);
        if (!this.toDeviceRpcPendingMap.isEmpty()) {
            log.debug("[{}] Pushing {} pending RPC messages to new async session [{}]", this.deviceId,
                      this.toDeviceRpcPendingMap.size(), sessionId);
            if (sessionType == TransportProtos.SessionType.SYNC) {
                log.debug("[{}] Cleanup sync rpc session [{}]", this.deviceId, sessionId);
                this.rpcSubscriptions.remove(sessionId);
            }
        }
        else {
            log.debug("[{}] No pending RPC messages for new async session [{}]", this.deviceId, sessionId);
        }
        Set<Integer> sentOneWayIds = new HashSet<>();
        if (sessionType == TransportProtos.SessionType.ASYNC) {
            this.toDeviceRpcPendingMap.entrySet().forEach(
                    this.processPendingRpc(context, sessionId, sessionInfo.getNodeId(), sentOneWayIds));
        }
        else {
            this.toDeviceRpcPendingMap.entrySet().stream().findFirst().ifPresent(
                    this.processPendingRpc(context, sessionId, sessionInfo.getNodeId(), sentOneWayIds));
        }

        sentOneWayIds.forEach(this.toDeviceRpcPendingMap::remove);
    }

    private JsonObject getJsonObject(List<KeyValueProto> tsKv) {
        JsonObject json = new JsonObject();
        for (KeyValueProto kv : tsKv) {
            switch (kv.getType()) {
                case BOOLEAN_V:
                    json.addProperty(kv.getKey(), kv.getBoolV());
                    break;
                case LONG_V:
                    json.addProperty(kv.getKey(), kv.getLongV());
                    break;
                case DOUBLE_V:
                    json.addProperty(kv.getKey(), kv.getDoubleV());
                    break;
                case STRING_V:
                    json.addProperty(kv.getKey(), kv.getStringV());
                    break;
            }
        }
        return json;
    }

    private void pushToRuleEngine(ActorContext context, TbMsg tbMsg) {
        context.parent().tell(new DeviceActorToRuleEngineMsg(context.self(), tbMsg), context.self());
    }

    private ListenableFuture<List<AttributeKvEntry>> getAttributeKvEntries(DeviceId deviceId,
                                                                           String scope,
                                                                           Optional<Set<String>> names) {
        if (names.isPresent()) {
            if (!names.get().isEmpty()) {
                return this.systemContext.getAttributesService().find(this.tenantId, deviceId, scope, names.get());
            }
            else {
                return this.systemContext.getAttributesService().findAll(this.tenantId, deviceId, scope);
            }
        }
        else {
            return Futures.immediateFuture(Collections.emptyList());
        }
    }

    private Optional<Set<String>> toOptionalSet(List<String> strings) {
        if (strings == null || strings.isEmpty()) {
            return Optional.empty();
        }
        else {
            return Optional.of(new HashSet<>(strings));
        }
    }

    private List<TsKvProto> toTsKvProtos(@Nullable List<AttributeKvEntry> result) {
        List<TsKvProto> clientAttributes;
        if (result == null || result.isEmpty()) {
            clientAttributes = Collections.emptyList();
        }
        else {
            clientAttributes = new ArrayList<>(result.size());
            for (AttributeKvEntry attrEntry : result) {
                clientAttributes.add(this.toTsKvProto(attrEntry));
            }
        }
        return clientAttributes;
    }

    private void sendToTransport(GetAttributeResponseMsg responseMsg, SessionInfoProto sessionInfo) {
        DeviceActorToTransportMsg msg = DeviceActorToTransportMsg.newBuilder().setSessionIdMSB(
                sessionInfo.getSessionIdMSB()).setSessionIdLSB(sessionInfo.getSessionIdLSB()).setGetAttributesResponse(
                responseMsg).build();
        this.systemContext.getRuleEngineTransportService().process(sessionInfo.getNodeId(), msg);
    }

    private TransportProtos.SessionType getSessionType(UUID sessionId) {
        return this.sessions.containsKey(sessionId) ? TransportProtos.SessionType.ASYNC
                                                    : TransportProtos.SessionType.SYNC;
    }

    private Consumer<Map.Entry<Integer, ToDeviceRpcRequestMetadata>> processPendingRpc(ActorContext context,
                                                                                       UUID sessionId,
                                                                                       String nodeId,
                                                                                       Set<Integer> sentOneWayIds) {
        return entry -> {
            ToDeviceRpcRequest request = entry.getValue().getMsg().getMsg();
            ToDeviceRpcRequestBody body = request.getBody();
            if (request.isOneway()) {
                sentOneWayIds.add(entry.getKey());
                this.systemContext.getDeviceRpcService().processResponseToServerSideRPCRequestFromDeviceActor(
                        new FromDeviceRpcResponse(request.getId(), null, null));
            }
            ToDeviceRpcRequestMsg rpcRequest = ToDeviceRpcRequestMsg.newBuilder()
                                                                    .setRequestId(entry.getKey())
                                                                    .setMethodName(body.getMethod())
                                                                    .setParams(body.getParams())
                                                                    .build();
            this.sendToTransport(rpcRequest, sessionId, nodeId);
        };
    }

    private TsKvProto toTsKvProto(AttributeKvEntry attrEntry) {
        return TsKvProto.newBuilder().setTs(attrEntry.getLastUpdateTs()).setKv(this.toKeyValueProto(attrEntry)).build();
    }

    private KeyValueProto toKeyValueProto(KvEntry kvEntry) {
        KeyValueProto.Builder builder = KeyValueProto.newBuilder();
        builder.setKey(kvEntry.getKey());
        switch (kvEntry.getDataType()) {
            case BOOLEAN:
                builder.setType(KeyValueType.BOOLEAN_V);
                builder.setBoolV(kvEntry.getBooleanValue().get());
                break;
            case DOUBLE:
                builder.setType(KeyValueType.DOUBLE_V);
                builder.setDoubleV(kvEntry.getDoubleValue().get());
                break;
            case LONG:
                builder.setType(KeyValueType.LONG_V);
                builder.setLongV(kvEntry.getLongValue().get());
                break;
            case STRING:
                builder.setType(KeyValueType.STRING_V);
                builder.setStringV(kvEntry.getStrValue().get());
                break;
        }
        return builder.build();
    }

    void processClientSideRpcTimeout(ActorContext context, DeviceActorClientSideRpcTimeoutMsg msg) {
        ToServerRpcRequestMetadata data = this.toServerRpcPendingMap.remove(msg.getId());
        if (data != null) {
            log.debug("[{}] Client side RPC request [{}] timeout detected!", this.deviceId, msg.getId());
            this.sendToTransport(TransportProtos.ToServerRpcResponseMsg.newBuilder()
                                                                       .setRequestId(msg.getId())
                                                                       .setError("timeout")
                                                                       .build(), data.getSessionId(), data.getNodeId());
        }
    }

    private void sendToTransport(TransportProtos.ToServerRpcResponseMsg rpcMsg, UUID sessionId, String nodeId) {
        DeviceActorToTransportMsg msg = DeviceActorToTransportMsg.newBuilder()
                                                                 .setSessionIdMSB(sessionId.getMostSignificantBits())
                                                                 .setSessionIdLSB(sessionId.getLeastSignificantBits())
                                                                 .setToServerResponse(rpcMsg)
                                                                 .build();
        this.systemContext.getRuleEngineTransportService().process(nodeId, msg);
    }

    void processToServerRPCResponse(ActorContext context, ToServerRpcResponseActorMsg msg) {
        int requestId = msg.getMsg().getRequestId();
        ToServerRpcRequestMetadata data = this.toServerRpcPendingMap.remove(requestId);
        if (data != null) {
            log.debug("[{}] Pushing reply to [{}][{}]!", this.deviceId, data.getNodeId(), data.getSessionId());
            this.sendToTransport(TransportProtos.ToServerRpcResponseMsg.newBuilder()
                                                                       .setRequestId(requestId)
                                                                       .setPayload(msg.getMsg().getData())
                                                                       .build(), data.getSessionId(), data.getNodeId());
        }
        else {
            log.debug("[{}][{}] Pending RPC request to server not found!", this.deviceId, requestId);
        }
    }

    void processAttributesUpdate(ActorContext context, DeviceAttributesEventNotificationMsg msg) {
        if (this.attributeSubscriptions.size() > 0) {
            boolean hasNotificationData = false;
            AttributeUpdateNotificationMsg.Builder notification = AttributeUpdateNotificationMsg.newBuilder();
            if (msg.isDeleted()) {
                List<String> sharedKeys = msg.getDeletedKeys()
                                             .stream()
                                             .filter(key -> DataConstants.SHARED_SCOPE.equals(key.getScope()))
                                             .map(AttributeKey::getAttributeKey)
                                             .collect(Collectors.toList());
                if (!sharedKeys.isEmpty()) {
                    notification.addAllSharedDeleted(sharedKeys);
                    hasNotificationData = true;
                }
            }
            else {
                if (DataConstants.SHARED_SCOPE.equals(msg.getScope())) {
                    List<AttributeKvEntry> attributes = new ArrayList<>(msg.getValues());
                    if (attributes.size() > 0) {
                        List<TsKvProto> sharedUpdated = msg.getValues().stream().map(this::toTsKvProto).collect(
                                Collectors.toList());
                        if (!sharedUpdated.isEmpty()) {
                            notification.addAllSharedUpdated(sharedUpdated);
                            hasNotificationData = true;
                        }
                    }
                    else {
                        log.debug("[{}] No public server side attributes changed!", this.deviceId);
                    }
                }
            }
            if (hasNotificationData) {
                AttributeUpdateNotificationMsg finalNotification = notification.build();
                this.attributeSubscriptions.entrySet().forEach(sub -> {
                    this.sendToTransport(finalNotification, sub.getKey(), sub.getValue().getNodeId());
                });
            }
        }
        else {
            log.debug("[{}] No registered attributes subscriptions to process!", this.deviceId);
        }
    }

    private void sendToTransport(AttributeUpdateNotificationMsg notificationMsg, UUID sessionId, String nodeId) {
        DeviceActorToTransportMsg msg = DeviceActorToTransportMsg.newBuilder()
                                                                 .setSessionIdMSB(sessionId.getMostSignificantBits())
                                                                 .setSessionIdLSB(sessionId.getLeastSignificantBits())
                                                                 .setAttributeUpdateNotification(notificationMsg)
                                                                 .build();
        this.systemContext.getRuleEngineTransportService().process(nodeId, msg);
    }

    void processCredentialsUpdate() {
        this.sessions.forEach(this::notifyTransportAboutClosedSession);
        this.attributeSubscriptions.clear();
        this.rpcSubscriptions.clear();
        this.dumpSessions();
    }

    void processNameOrTypeUpdate(DeviceNameOrTypeUpdateMsg msg) {
        this.deviceName = msg.getDeviceName();
        this.deviceType = msg.getDeviceType();
        this.defaultMetaData = new TbMsgMetaData();
        this.defaultMetaData.putValue("deviceName", this.deviceName);
        this.defaultMetaData.putValue("deviceType", this.deviceType);
    }

    void initSessionTimeout(ActorContext context) {
        this.schedulePeriodicMsgWithDelay(context, SessionTimeoutCheckMsg.instance(),
                                          this.systemContext.getSessionInactivityTimeout(),
                                          this.systemContext.getSessionInactivityTimeout());
    }

    void checkSessionsTimeout() {
        long expTime = System.currentTimeMillis() - this.systemContext.getSessionInactivityTimeout();
        Map<UUID, SessionInfoMetaData> sessionsToRemove = this.sessions.entrySet().stream().filter(
                kv -> kv.getValue().getLastActivityTime() < expTime).collect(
                Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        sessionsToRemove.forEach((sessionId, sessionMD) -> {
            this.sessions.remove(sessionId);
            this.rpcSubscriptions.remove(sessionId);
            this.attributeSubscriptions.remove(sessionId);
            this.notifyTransportAboutClosedSession(sessionId, sessionMD);
        });
        if (!sessionsToRemove.isEmpty()) {
            this.dumpSessions();
        }
    }

}
