/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.install;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.thingsboard.server.service.component.ComponentDiscoveryService;
import org.thingsboard.server.service.install.*;


@Service
@Profile("install")
@Slf4j
public class ThingsboardInstallService {

    @Value("${install.upgrade:false}") private Boolean isUpgrade;

    @Value("${install.upgrade.from_version:1.2.3}") private String upgradeFromVersion;

    @Value("${install.load_demo:false}") private Boolean loadDemo;

    @Autowired private EntityDatabaseSchemaService entityDatabaseSchemaService;

    @Autowired private TsDatabaseSchemaService tsDatabaseSchemaService;

    @Autowired private DatabaseUpgradeService databaseUpgradeService;

    @Autowired private ComponentDiscoveryService componentDiscoveryService;

    @Autowired private ApplicationContext context;

    @Autowired private SystemDataLoaderService systemDataLoaderService;

    @Autowired private DataUpdateService dataUpdateService;

    public void performInstall() {
        try {
            if (this.isUpgrade) {
                log.info("Starting ThingsBoard Upgrade from version {} ...", this.upgradeFromVersion);

                switch (this.upgradeFromVersion) {
                    case "1.2.3": //NOSONAR, Need to execute gradual upgrade starting from upgradeFromVersion
                        log.info("Upgrading ThingsBoard from version 1.2.3 to 1.3.0 ...");

                        this.databaseUpgradeService.upgradeDatabase("1.2.3");

                    case "1.3.0":  //NOSONAR, Need to execute gradual upgrade starting from upgradeFromVersion
                        log.info("Upgrading ThingsBoard from version 1.3.0 to 1.3.1 ...");

                        this.databaseUpgradeService.upgradeDatabase("1.3.0");

                    case "1.3.1": //NOSONAR, Need to execute gradual upgrade starting from upgradeFromVersion
                        log.info("Upgrading ThingsBoard from version 1.3.1 to 1.4.0 ...");

                        this.databaseUpgradeService.upgradeDatabase("1.3.1");

                    case "1.4.0":
                        log.info("Upgrading ThingsBoard from version 1.4.0 to 2.0.0 ...");

                        this.databaseUpgradeService.upgradeDatabase("1.4.0");

                        this.dataUpdateService.updateData("1.4.0");

                    case "2.0.0":
                        log.info("Upgrading ThingsBoard from version 2.0.0 to 2.1.1 ...");

                        this.databaseUpgradeService.upgradeDatabase("2.0.0");

                    case "2.1.1":
                        log.info("Upgrading ThingsBoard from version 2.1.1 to 2.1.2 ...");

                        this.databaseUpgradeService.upgradeDatabase("2.1.1");
                    case "2.1.3":
                        log.info("Upgrading ThingsBoard from version 2.1.3 to 2.2.0 ...");

                        this.databaseUpgradeService.upgradeDatabase("2.1.3");

                        log.info("Updating system data...");

                        this.systemDataLoaderService.deleteSystemWidgetBundle("charts");
                        this.systemDataLoaderService.deleteSystemWidgetBundle("cards");
                        this.systemDataLoaderService.deleteSystemWidgetBundle("maps");
                        this.systemDataLoaderService.deleteSystemWidgetBundle("analogue_gauges");
                        this.systemDataLoaderService.deleteSystemWidgetBundle("digital_gauges");
                        this.systemDataLoaderService.deleteSystemWidgetBundle("gpio_widgets");
                        this.systemDataLoaderService.deleteSystemWidgetBundle("alarm_widgets");
                        this.systemDataLoaderService.deleteSystemWidgetBundle("control_widgets");
                        this.systemDataLoaderService.deleteSystemWidgetBundle("maps_v2");
                        this.systemDataLoaderService.deleteSystemWidgetBundle("gateway_widgets");
                        this.systemDataLoaderService.deleteSystemWidgetBundle("input_widgets");

                        this.systemDataLoaderService.loadSystemWidgets();

                        break;
                    default:
                        throw new RuntimeException(
                                "Unable to upgrade ThingsBoard, unsupported fromVersion: " + this.upgradeFromVersion);

                }
                log.info("Upgrade finished successfully!");

            }
            else {

                log.info("Starting ThingsBoard Installation...");

                log.info("Installing DataBase schema for entities...");

                this.entityDatabaseSchemaService.createDatabaseSchema();

                log.info("Installing DataBase schema for timeseries...");

                this.tsDatabaseSchemaService.createDatabaseSchema();

                log.info("Loading system data...");

                this.componentDiscoveryService.discoverComponents();

                this.systemDataLoaderService.createSysAdmin();
                this.systemDataLoaderService.createAdminSettings();
                this.systemDataLoaderService.loadSystemWidgets();
                //                systemDataLoaderService.loadSystemPlugins();
                //                systemDataLoaderService.loadSystemRules();

                if (this.loadDemo) {
                    log.info("Loading demo data...");
                    this.systemDataLoaderService.loadDemoData();
                }
                log.info("Installation finished successfully!");
            }


        } catch (Exception e) {
            log.error("Unexpected error during ThingsBoard installation!", e);
            throw new ThingsboardInstallException("Unexpected error during ThingsBoard installation!", e);
        } finally {
            SpringApplication.exit(this.context);
        }
    }

}
