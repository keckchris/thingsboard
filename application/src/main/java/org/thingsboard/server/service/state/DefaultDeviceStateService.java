/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.service.state;


import com.datastax.driver.core.utils.UUIDs;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Function;
import com.google.common.util.concurrent.*;
import com.google.protobuf.InvalidProtocolBufferException;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.thingsboard.server.actors.service.ActorService;
import org.thingsboard.server.common.data.DataConstants;
import org.thingsboard.server.common.data.Device;
import org.thingsboard.server.common.data.Tenant;
import org.thingsboard.server.common.data.id.DeviceId;
import org.thingsboard.server.common.data.id.TenantId;
import org.thingsboard.server.common.data.kv.AttributeKvEntry;
import org.thingsboard.server.common.data.page.TextPageLink;
import org.thingsboard.server.common.msg.TbMsg;
import org.thingsboard.server.common.msg.TbMsgDataType;
import org.thingsboard.server.common.msg.TbMsgMetaData;
import org.thingsboard.server.common.msg.cluster.SendToClusterMsg;
import org.thingsboard.server.common.msg.cluster.ServerAddress;
import org.thingsboard.server.common.msg.system.ServiceToRuleEngineMsg;
import org.thingsboard.server.dao.attributes.AttributesService;
import org.thingsboard.server.dao.device.DeviceService;
import org.thingsboard.server.dao.tenant.TenantService;
import org.thingsboard.server.gen.cluster.ClusterAPIProtos;
import org.thingsboard.server.service.cluster.routing.ClusterRoutingService;
import org.thingsboard.server.service.cluster.rpc.ClusterRpcService;
import org.thingsboard.server.service.telemetry.TelemetrySubscriptionService;

import javax.annotation.Nullable;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.*;
import java.util.concurrent.*;

import static org.thingsboard.server.common.data.DataConstants.*;


/**
 * Created by ashvayka on 01.05.18.
 */
@Service
@Slf4j
//TODO: refactor to use page links as cursor and not fetch all
public class DefaultDeviceStateService implements DeviceStateService {

    public static final String ACTIVITY_STATE = "active";
    public static final String LAST_CONNECT_TIME = "lastConnectTime";
    public static final String LAST_DISCONNECT_TIME = "lastDisconnectTime";
    public static final String LAST_ACTIVITY_TIME = "lastActivityTime";
    public static final String INACTIVITY_ALARM_TIME = "inactivityAlarmTime";
    public static final String INACTIVITY_TIMEOUT = "inactivityTimeout";
    public static final List<String> PERSISTENT_ATTRIBUTES = Arrays.asList(ACTIVITY_STATE, LAST_CONNECT_TIME,
                                                                           LAST_DISCONNECT_TIME, LAST_ACTIVITY_TIME,
                                                                           INACTIVITY_ALARM_TIME, INACTIVITY_TIMEOUT);
    private static final ObjectMapper json = new ObjectMapper();
    @Autowired private TenantService tenantService;

    @Autowired private DeviceService deviceService;

    @Autowired private AttributesService attributesService;

    @Autowired
    @Lazy
    private ActorService actorService;

    @Autowired private TelemetrySubscriptionService tsSubService;

    @Autowired private ClusterRoutingService routingService;

    @Autowired private ClusterRpcService clusterRpcService;

    @Value("${state.defaultInactivityTimeoutInSec}")
    @Getter
    private long defaultInactivityTimeoutInSec;

    @Value("${state.defaultStateCheckIntervalInSec}")
    @Getter
    private long defaultStateCheckIntervalInSec;

    // TODO in v2.1
    //    @Value("${state.defaultStatePersistenceIntervalInSec}")
    //    @Getter
    //    private long defaultStatePersistenceIntervalInSec;
    //
    //    @Value("${state.defaultStatePersistencePack}")
    //    @Getter
    //    private long defaultStatePersistencePack;

    private ListeningScheduledExecutorService queueExecutor;

    private ConcurrentMap<TenantId, Set<DeviceId>> tenantDevices = new ConcurrentHashMap<>();
    private ConcurrentMap<DeviceId, DeviceStateData> deviceStates = new ConcurrentHashMap<>();

    @PostConstruct
    public void init() {
        // Should be always single threaded due to absence of locks.
        this.queueExecutor = MoreExecutors.listeningDecorator(Executors.newSingleThreadScheduledExecutor());
        this.queueExecutor.submit(this::initStateFromDB);
        this.queueExecutor.scheduleAtFixedRate(this::updateState, this.defaultStateCheckIntervalInSec,
                                               this.defaultStateCheckIntervalInSec, TimeUnit.SECONDS);
        //TODO: schedule persistence in v2.1;
    }

    @PreDestroy
    public void stop() {
        if (this.queueExecutor != null) {
            this.queueExecutor.shutdownNow();
        }
    }

    @Override
    public void onDeviceAdded(Device device) {
        this.queueExecutor.submit(() -> this.onDeviceAddedSync(device));
    }

    @Override
    public void onDeviceUpdated(Device device) {
        this.queueExecutor.submit(() -> this.onDeviceUpdatedSync(device));
    }

    @Override
    public void onDeviceDeleted(Device device) {
        this.queueExecutor.submit(() -> this.onDeviceDeleted(device.getTenantId(), device.getId()));
    }

    @Override
    public void onDeviceConnect(DeviceId deviceId) {
        this.queueExecutor.submit(() -> this.onDeviceConnectSync(deviceId));
    }

    @Override
    public void onDeviceActivity(DeviceId deviceId) {
        this.queueExecutor.submit(() -> this.onDeviceActivitySync(deviceId));
    }

    @Override
    public void onDeviceDisconnect(DeviceId deviceId) {
        this.queueExecutor.submit(() -> this.onDeviceDisconnectSync(deviceId));
    }

    @Override
    public void onDeviceInactivityTimeoutUpdate(DeviceId deviceId, long inactivityTimeout) {
        this.queueExecutor.submit(() -> this.onInactivityTimeoutUpdate(deviceId, inactivityTimeout));
    }

    @Override
    public void onClusterUpdate() {
        this.queueExecutor.submit(this::onClusterUpdateSync);
    }

    @Override
    public void onRemoteMsg(ServerAddress serverAddress, byte[] data) {
        ClusterAPIProtos.DeviceStateServiceMsgProto proto;
        try {
            proto = ClusterAPIProtos.DeviceStateServiceMsgProto.parseFrom(data);
        } catch (InvalidProtocolBufferException e) {
            throw new RuntimeException(e);
        }
        TenantId tenantId = new TenantId(new UUID(proto.getTenantIdMSB(), proto.getTenantIdLSB()));
        DeviceId deviceId = new DeviceId(new UUID(proto.getDeviceIdMSB(), proto.getDeviceIdLSB()));
        if (proto.getDeleted()) {
            this.queueExecutor.submit(() -> this.onDeviceDeleted(tenantId, deviceId));
        }
        else {
            Device device = this.deviceService.findDeviceById(TenantId.SYS_TENANT_ID, deviceId);
            if (device != null) {
                if (proto.getAdded()) {
                    this.onDeviceAdded(device);
                }
                else if (proto.getUpdated()) {
                    this.onDeviceUpdated(device);
                }
            }
        }
    }

    private void onInactivityTimeoutUpdate(DeviceId deviceId, long inactivityTimeout) {
        if (inactivityTimeout == 0L) {
            return;
        }
        DeviceStateData stateData = this.deviceStates.get(deviceId);
        if (stateData != null) {
            long ts = System.currentTimeMillis();
            DeviceState state = stateData.getState();
            state.setInactivityTimeout(inactivityTimeout);
            boolean oldActive = state.isActive();
            state.setActive(ts < state.getLastActivityTime() + state.getInactivityTimeout());
            if (!oldActive && state.isActive() || oldActive && !state.isActive()) {
                this.saveAttribute(deviceId, ACTIVITY_STATE, state.isActive());
            }
        }
    }

    private void onDeviceDisconnectSync(DeviceId deviceId) {
        DeviceStateData stateData = this.getOrFetchDeviceStateData(deviceId);
        if (stateData != null) {
            long ts = System.currentTimeMillis();
            stateData.getState().setLastDisconnectTime(ts);
            this.pushRuleEngineMessage(stateData, DISCONNECT_EVENT);
            this.saveAttribute(deviceId, LAST_DISCONNECT_TIME, ts);
        }
    }

    private void onDeviceActivitySync(DeviceId deviceId) {
        DeviceStateData stateData = this.getOrFetchDeviceStateData(deviceId);
        if (stateData != null) {
            DeviceState state = stateData.getState();
            long ts = System.currentTimeMillis();
            state.setActive(true);
            stateData.getState().setLastActivityTime(ts);
            this.pushRuleEngineMessage(stateData, ACTIVITY_EVENT);
            this.saveAttribute(deviceId, LAST_ACTIVITY_TIME, ts);
            this.saveAttribute(deviceId, ACTIVITY_STATE, state.isActive());
        }
    }

    private void saveAttribute(DeviceId deviceId, String key, boolean value) {
        this.tsSubService.saveAttrAndNotify(TenantId.SYS_TENANT_ID, deviceId, DataConstants.SERVER_SCOPE, key, value,
                                            new AttributeSaveCallback(deviceId, key, value));
    }

    private void onDeviceConnectSync(DeviceId deviceId) {
        DeviceStateData stateData = this.getOrFetchDeviceStateData(deviceId);
        if (stateData != null) {
            long ts = System.currentTimeMillis();
            stateData.getState().setLastConnectTime(ts);
            this.pushRuleEngineMessage(stateData, CONNECT_EVENT);
            this.saveAttribute(deviceId, LAST_CONNECT_TIME, ts);
        }
    }

    private DeviceStateData getOrFetchDeviceStateData(DeviceId deviceId) {
        DeviceStateData deviceStateData = this.deviceStates.get(deviceId);
        if (deviceStateData == null) {
            if (!this.routingService.resolveById(deviceId).isPresent()) {
                Device device = this.deviceService.findDeviceById(TenantId.SYS_TENANT_ID, deviceId);
                if (device != null) {
                    try {
                        deviceStateData = this.fetchDeviceState(device).get();
                    } catch (InterruptedException | ExecutionException e) {
                        log.debug("[{}] Failed to fetch device state!", deviceId, e);
                    }
                }
            }
        }
        return deviceStateData;
    }

    private void pushRuleEngineMessage(DeviceStateData stateData, String msgType) {
        DeviceState state = stateData.getState();
        try {
            TbMsg tbMsg = new TbMsg(UUIDs.timeBased(), msgType, stateData.getDeviceId(), stateData.getMetaData().copy(),
                                    TbMsgDataType.JSON, json.writeValueAsString(state), null, null, 0L);
            this.actorService.onMsg(new SendToClusterMsg(stateData.getDeviceId(),
                                                         new ServiceToRuleEngineMsg(stateData.getTenantId(), tbMsg)));
        } catch (Exception e) {
            log.warn("[{}] Failed to push inactivity alarm: {}", stateData.getDeviceId(), state, e);
        }
    }

    private void saveAttribute(DeviceId deviceId, String key, long value) {
        this.tsSubService.saveAttrAndNotify(TenantId.SYS_TENANT_ID, deviceId, DataConstants.SERVER_SCOPE, key, value,
                                            new AttributeSaveCallback(deviceId, key, value));
    }

    private ListenableFuture<DeviceStateData> fetchDeviceState(Device device) {
        ListenableFuture<List<AttributeKvEntry>> attributes = this.attributesService.find(TenantId.SYS_TENANT_ID,
                                                                                          device.getId(),
                                                                                          DataConstants.SERVER_SCOPE,
                                                                                          PERSISTENT_ATTRIBUTES);
        return Futures.transform(attributes, new Function<List<AttributeKvEntry>, DeviceStateData>() {
            @Nullable
            @Override
            public DeviceStateData apply(@Nullable List<AttributeKvEntry> attributes) {
                long lastActivityTime = DefaultDeviceStateService.this.getAttributeValue(attributes, LAST_ACTIVITY_TIME,
                                                                                         0L);
                long inactivityAlarmTime = DefaultDeviceStateService.this.getAttributeValue(attributes,
                                                                                            INACTIVITY_ALARM_TIME, 0L);
                long inactivityTimeout = DefaultDeviceStateService.this.getAttributeValue(attributes,
                                                                                          INACTIVITY_TIMEOUT,
                                                                                          TimeUnit.SECONDS.toMillis(
                                                                                                  DefaultDeviceStateService.this.defaultInactivityTimeoutInSec));
                boolean active = System.currentTimeMillis() < lastActivityTime + inactivityTimeout;
                DeviceState deviceState = DeviceState.builder()
                                                     .active(active)
                                                     .lastConnectTime(DefaultDeviceStateService.this.getAttributeValue(
                                                             attributes, LAST_CONNECT_TIME, 0L))
                                                     .lastDisconnectTime(
                                                             DefaultDeviceStateService.this.getAttributeValue(
                                                                     attributes, LAST_DISCONNECT_TIME, 0L))
                                                     .lastActivityTime(lastActivityTime)
                                                     .lastInactivityAlarmTime(inactivityAlarmTime)
                                                     .inactivityTimeout(inactivityTimeout)
                                                     .build();
                TbMsgMetaData md = new TbMsgMetaData();
                md.putValue("deviceName", device.getName());
                md.putValue("deviceType", device.getType());
                return DeviceStateData.builder()
                                      .tenantId(device.getTenantId())
                                      .deviceId(device.getId())
                                      .metaData(md)
                                      .state(deviceState)
                                      .build();
            }
        });
    }

    private long getAttributeValue(List<AttributeKvEntry> attributes, String attributeName, long defaultValue) {
        for (AttributeKvEntry attribute : attributes) {
            if (attribute.getKey().equals(attributeName)) {
                return attribute.getLongValue().orElse(defaultValue);
            }
        }
        return defaultValue;
    }

    private void onDeviceDeleted(TenantId tenantId, DeviceId deviceId) {
        Optional<ServerAddress> address = this.routingService.resolveById(deviceId);
        if (!address.isPresent()) {
            this.deviceStates.remove(deviceId);
            Set<DeviceId> deviceIds = this.tenantDevices.get(tenantId);
            if (deviceIds != null) {
                deviceIds.remove(deviceId);
                if (deviceIds.isEmpty()) {
                    this.tenantDevices.remove(tenantId);
                }
            }
        }
        else {
            this.sendDeviceEvent(tenantId, deviceId, address.get(), false, false, true);
        }
    }

    private void sendDeviceEvent(TenantId tenantId,
                                 DeviceId deviceId,
                                 ServerAddress address,
                                 boolean added,
                                 boolean updated,
                                 boolean deleted) {
        log.trace("[{}][{}] Device is monitored on other server: {}", tenantId, deviceId, address);
        ClusterAPIProtos.DeviceStateServiceMsgProto.Builder builder =
                ClusterAPIProtos.DeviceStateServiceMsgProto.newBuilder();
        builder.setTenantIdMSB(tenantId.getId().getMostSignificantBits());
        builder.setTenantIdLSB(tenantId.getId().getLeastSignificantBits());
        builder.setDeviceIdMSB(deviceId.getId().getMostSignificantBits());
        builder.setDeviceIdLSB(deviceId.getId().getLeastSignificantBits());
        builder.setAdded(added);
        builder.setUpdated(updated);
        builder.setDeleted(deleted);
        this.clusterRpcService.tell(address, ClusterAPIProtos.MessageType.CLUSTER_DEVICE_STATE_SERVICE_MESSAGE,
                                    builder.build().toByteArray());
    }

    private void onClusterUpdateSync() {
        List<Tenant> tenants = this.tenantService.findTenants(new TextPageLink(Integer.MAX_VALUE)).getData();
        for (Tenant tenant : tenants) {
            List<ListenableFuture<DeviceStateData>> fetchFutures = new ArrayList<>();
            List<Device> devices = this.deviceService.findDevicesByTenantId(tenant.getId(),
                                                                            new TextPageLink(Integer.MAX_VALUE))
                                                     .getData();
            for (Device device : devices) {
                if (!this.routingService.resolveById(device.getId()).isPresent()) {
                    if (!this.deviceStates.containsKey(device.getId())) {
                        fetchFutures.add(this.fetchDeviceState(device));
                    }
                }
                else {
                    Set<DeviceId> tenantDeviceSet = this.tenantDevices.get(tenant.getId());
                    if (tenantDeviceSet != null) {
                        tenantDeviceSet.remove(device.getId());
                    }
                    this.deviceStates.remove(device.getId());
                }
            }
            try {
                Futures.successfulAsList(fetchFutures).get().forEach(this::addDeviceUsingState);
            } catch (InterruptedException | ExecutionException e) {
                log.warn("Failed to init device state service from DB", e);
            }
        }
    }

    private void initStateFromDB() {
        List<Tenant> tenants = this.tenantService.findTenants(new TextPageLink(Integer.MAX_VALUE)).getData();
        for (Tenant tenant : tenants) {
            List<ListenableFuture<DeviceStateData>> fetchFutures = new ArrayList<>();
            List<Device> devices = this.deviceService.findDevicesByTenantId(tenant.getId(),
                                                                            new TextPageLink(Integer.MAX_VALUE))
                                                     .getData();
            for (Device device : devices) {
                if (!this.routingService.resolveById(device.getId()).isPresent()) {
                    fetchFutures.add(this.fetchDeviceState(device));
                }
            }
            try {
                Futures.successfulAsList(fetchFutures).get().forEach(this::addDeviceUsingState);
            } catch (InterruptedException | ExecutionException e) {
                log.warn("Failed to init device state service from DB", e);
            }
        }
    }

    private void addDeviceUsingState(DeviceStateData state) {
        this.tenantDevices.computeIfAbsent(state.getTenantId(), id -> ConcurrentHashMap.newKeySet()).add(
                state.getDeviceId());
        this.deviceStates.put(state.getDeviceId(), state);
    }

    private void updateState() {
        long ts = System.currentTimeMillis();
        Set<DeviceId> deviceIds = new HashSet<>(this.deviceStates.keySet());
        for (DeviceId deviceId : deviceIds) {
            DeviceStateData stateData = this.deviceStates.get(deviceId);
            DeviceState state = stateData.getState();
            state.setActive(ts < state.getLastActivityTime() + state.getInactivityTimeout());
            if (!state.isActive() && (state.getLastInactivityAlarmTime() == 0L ||
                                      state.getLastInactivityAlarmTime() < state.getLastActivityTime())) {
                state.setLastInactivityAlarmTime(ts);
                this.pushRuleEngineMessage(stateData, INACTIVITY_EVENT);
                this.saveAttribute(deviceId, INACTIVITY_ALARM_TIME, ts);
                this.saveAttribute(deviceId, ACTIVITY_STATE, state.isActive());
            }
        }
    }

    private void onDeviceAddedSync(Device device) {
        Optional<ServerAddress> address = this.routingService.resolveById(device.getId());
        if (!address.isPresent()) {
            Futures.addCallback(this.fetchDeviceState(device), new FutureCallback<DeviceStateData>() {
                @Override
                public void onSuccess(@Nullable DeviceStateData state) {
                    DefaultDeviceStateService.this.addDeviceUsingState(state);
                }

                @Override
                public void onFailure(Throwable t) {
                    log.warn("Failed to register device to the state service", t);
                }
            });
        }
        else {
            this.sendDeviceEvent(device.getTenantId(), device.getId(), address.get(), true, false, false);
        }
    }

    private void onDeviceUpdatedSync(Device device) {
        Optional<ServerAddress> address = this.routingService.resolveById(device.getId());
        if (!address.isPresent()) {
            DeviceStateData stateData = this.getOrFetchDeviceStateData(device.getId());
            if (stateData != null) {
                TbMsgMetaData md = new TbMsgMetaData();
                md.putValue("deviceName", device.getName());
                md.putValue("deviceType", device.getType());
                stateData.setMetaData(md);
            }
        }
        else {
            this.sendDeviceEvent(device.getTenantId(), device.getId(), address.get(), false, true, false);
        }
    }

    private class AttributeSaveCallback implements FutureCallback<Void> {

        private final DeviceId deviceId;
        private final String key;
        private final Object value;

        AttributeSaveCallback(DeviceId deviceId, String key, Object value) {
            this.deviceId = deviceId;
            this.key = key;
            this.value = value;
        }

        @Override
        public void onSuccess(@Nullable Void result) {
            log.trace("[{}] Successfully updated attribute [{}] with value [{}]", this.deviceId, this.key, this.value);
        }

        @Override
        public void onFailure(Throwable t) {
            log.warn("[{}] Failed to update attribute [{}] with value [{}]", this.deviceId, this.key, this.value, t);
        }

    }

}
