/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.service.update;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.thingsboard.server.service.update.model.UpdateMessage;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;


@Service
@Slf4j
public class DefaultUpdateService implements UpdateService {

    private static final String INSTANCE_ID_FILE = ".instance_id";
    private static final String UPDATE_SERVER_BASE_URL = "https://updates.thingsboard.io";

    private static final String PLATFORM_PARAM = "platform";
    private static final String VERSION_PARAM = "version";
    private static final String INSTANCE_ID_PARAM = "instanceId";
    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
    Runnable checkUpdatesRunnable = () -> {
        try {
            log.trace("Executing check update method for instanceId [{}], platform [{}] and version [{}]",
                      this.instanceId, this.platform, this.version);
            ObjectNode request = new ObjectMapper().createObjectNode();
            request.put(PLATFORM_PARAM, this.platform);
            request.put(VERSION_PARAM, this.version);
            request.put(INSTANCE_ID_PARAM, this.instanceId.toString());
            JsonNode response = this.restClient.postForObject(UPDATE_SERVER_BASE_URL + "/api/thingsboard/updates",
                                                              request, JsonNode.class);
            this.updateMessage = new UpdateMessage(response.get("message").asText(),
                                                   response.get("updateAvailable").asBoolean());
        } catch (Exception e) {
            log.trace(e.getMessage());
        }
    };
    private ScheduledFuture checkUpdatesFuture = null;
    private RestTemplate restClient = new RestTemplate();

    private UpdateMessage updateMessage;

    private String platform;
    private String version;
    private UUID instanceId = null;
    @Value("${updates.enabled}") private boolean updatesEnabled;
    @PostConstruct
    private void init() {
        this.updateMessage = new UpdateMessage("", false);
        if (this.updatesEnabled) {
            try {
                this.platform = System.getProperty("platform", "unknown");
                this.version = this.getClass().getPackage().getImplementationVersion();
                if (this.version == null) {
                    this.version = "unknown";
                }
                this.instanceId = this.parseInstanceId();
                this.checkUpdatesFuture = this.scheduler.scheduleAtFixedRate(this.checkUpdatesRunnable, 0, 1,
                                                                             TimeUnit.HOURS);
            } catch (Exception e) {
                //Do nothing
            }
        }
    }

    private UUID parseInstanceId() throws IOException {
        UUID result = null;
        Path instanceIdPath = Paths.get(INSTANCE_ID_FILE);
        if (instanceIdPath.toFile().exists()) {
            byte[] data = Files.readAllBytes(instanceIdPath);
            if (data != null && data.length > 0) {
                try {
                    result = UUID.fromString(new String(data));
                } catch (IllegalArgumentException e) {
                    //Do nothing
                }
            }
        }
        if (result == null) {
            result = UUID.randomUUID();
            Files.write(instanceIdPath, result.toString().getBytes());
        }
        return result;
    }

    @PreDestroy
    private void destroy() {
        try {
            if (this.checkUpdatesFuture != null) {
                this.checkUpdatesFuture.cancel(true);
            }
            this.scheduler.shutdownNow();
        } catch (Exception e) {
            //Do nothing
        }
    }

    @Override
    public UpdateMessage checkUpdates() {
        return this.updateMessage;
    }

}
