/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.service.telemetry;


import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.protobuf.InvalidProtocolBufferException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.thingsboard.rule.engine.api.msg.DeviceAttributesEventNotificationMsg;
import org.thingsboard.rule.engine.api.util.DonAsynchron;
import org.thingsboard.server.actors.service.ActorService;
import org.thingsboard.server.common.data.DataConstants;
import org.thingsboard.server.common.data.EntityType;
import org.thingsboard.server.common.data.EntityView;
import org.thingsboard.server.common.data.id.*;
import org.thingsboard.server.common.data.kv.*;
import org.thingsboard.server.common.msg.cluster.SendToClusterMsg;
import org.thingsboard.server.common.msg.cluster.ServerAddress;
import org.thingsboard.server.dao.attributes.AttributesService;
import org.thingsboard.server.dao.entityview.EntityViewService;
import org.thingsboard.server.dao.timeseries.TimeseriesService;
import org.thingsboard.server.gen.cluster.ClusterAPIProtos;
import org.thingsboard.server.service.cluster.routing.ClusterRoutingService;
import org.thingsboard.server.service.cluster.rpc.ClusterRpcService;
import org.thingsboard.server.service.state.DefaultDeviceStateService;
import org.thingsboard.server.service.state.DeviceStateService;
import org.thingsboard.server.service.telemetry.sub.Subscription;
import org.thingsboard.server.service.telemetry.sub.SubscriptionErrorCode;
import org.thingsboard.server.service.telemetry.sub.SubscriptionState;
import org.thingsboard.server.service.telemetry.sub.SubscriptionUpdate;

import javax.annotation.Nullable;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;


/**
 * Created by ashvayka on 27.03.18.
 */
@Service
@Slf4j
public class DefaultTelemetrySubscriptionService implements TelemetrySubscriptionService {

    private final Map<EntityId, Set<Subscription>> subscriptionsByEntityId = new ConcurrentHashMap<>();
    private final Map<String, Map<Integer, Subscription>> subscriptionsByWsSessionId = new ConcurrentHashMap<>();
    @Autowired private TelemetryWebSocketService wsService;
    @Autowired private AttributesService attrService;
    @Autowired private TimeseriesService tsService;
    @Autowired private ClusterRoutingService routingService;
    @Autowired private ClusterRpcService rpcService;
    @Autowired private EntityViewService entityViewService;
    @Autowired
    @Lazy
    private DeviceStateService stateService;
    @Autowired
    @Lazy
    private ActorService actorService;
    private ExecutorService tsCallBackExecutor;
    private ExecutorService wsCallBackExecutor;

    @PostConstruct
    public void initExecutor() {
        this.tsCallBackExecutor = Executors.newSingleThreadExecutor();
        this.wsCallBackExecutor = Executors.newSingleThreadExecutor();
    }

    @PreDestroy
    public void shutdownExecutor() {
        if (this.tsCallBackExecutor != null) {
            this.tsCallBackExecutor.shutdownNow();
        }
        if (this.wsCallBackExecutor != null) {
            this.wsCallBackExecutor.shutdownNow();
        }
    }

    @Override
    public void addLocalWsSubscription(String sessionId, EntityId entityId, SubscriptionState sub) {
        long startTime = 0L;
        long endTime = 0L;
        if (entityId.getEntityType().equals(EntityType.ENTITY_VIEW) && TelemetryFeature.TIMESERIES.equals(
                sub.getType())) {
            EntityView entityView = this.entityViewService.findEntityViewById(TenantId.SYS_TENANT_ID,
                                                                              new EntityViewId(entityId.getId()));
            entityId = entityView.getEntityId();
            startTime = entityView.getStartTimeMs();
            endTime = entityView.getEndTimeMs();
            sub = this.getUpdatedSubscriptionState(entityId, sub, entityView);
        }
        Optional<ServerAddress> server = this.routingService.resolveById(entityId);
        Subscription subscription;
        if (server.isPresent()) {
            ServerAddress address = server.get();
            log.trace("[{}] Forwarding subscription [{}] for [{}] entity [{}] to [{}]", sessionId,
                      sub.getSubscriptionId(), entityId.getEntityType().name(), entityId, address);
            subscription = new Subscription(sub, true, address, startTime, endTime);
            this.tellNewSubscription(address, sessionId, subscription);
        }
        else {
            log.trace("[{}] Registering local subscription [{}] for [{}] entity [{}]", sessionId,
                      sub.getSubscriptionId(), entityId.getEntityType().name(), entityId);
            subscription = new Subscription(sub, true, null, startTime, endTime);
        }
        this.registerSubscription(sessionId, entityId, subscription);
    }

    private SubscriptionState getUpdatedSubscriptionState(EntityId entityId,
                                                          SubscriptionState sub,
                                                          EntityView entityView) {
        Map<String, Long> keyStates;
        if (sub.isAllKeys()) {
            keyStates = entityView.getKeys().getTimeseries().stream().collect(Collectors.toMap(k -> k, k -> 0L));
        }
        else {
            keyStates = sub.getKeyStates().entrySet().stream().filter(
                    entry -> entityView.getKeys().getTimeseries().contains(entry.getKey())).collect(
                    Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        }
        return new SubscriptionState(sub.getWsSessionId(), sub.getSubscriptionId(), sub.getTenantId(), entityId,
                                     sub.getType(), false, keyStates, sub.getScope());
    }

    private void tellNewSubscription(ServerAddress address, String sessionId, Subscription sub) {
        ClusterAPIProtos.SubscriptionProto.Builder builder = ClusterAPIProtos.SubscriptionProto.newBuilder();
        builder.setSessionId(sessionId);
        builder.setSubscriptionId(sub.getSubscriptionId());
        builder.setTenantId(sub.getSub().getTenantId().getId().toString());
        builder.setEntityType(sub.getEntityId().getEntityType().name());
        builder.setEntityId(sub.getEntityId().getId().toString());
        builder.setType(sub.getType().name());
        builder.setAllKeys(sub.isAllKeys());
        if (sub.getScope() != null) {
            builder.setScope(sub.getScope());
        }
        sub.getKeyStates().entrySet().forEach(
                e -> builder.addKeyStates(ClusterAPIProtos.SubscriptionKetStateProto.newBuilder()
                                                                                    .setKey(e.getKey())
                                                                                    .setTs(e.getValue())
                                                                                    .build()));
        this.rpcService.tell(address, ClusterAPIProtos.MessageType.CLUSTER_TELEMETRY_SUBSCRIPTION_CREATE_MESSAGE,
                             builder.build().toByteArray());
    }

    private void registerSubscription(String sessionId, EntityId entityId, Subscription subscription) {
        Set<Subscription> deviceSubscriptions = this.subscriptionsByEntityId.computeIfAbsent(entityId,
                                                                                             k -> ConcurrentHashMap.newKeySet());
        deviceSubscriptions.add(subscription);
        Map<Integer, Subscription> sessionSubscriptions = this.subscriptionsByWsSessionId.computeIfAbsent(sessionId,
                                                                                                          k -> new ConcurrentHashMap<>());
        sessionSubscriptions.put(subscription.getSubscriptionId(), subscription);
    }

    @Override
    public void cleanupLocalWsSessionSubscriptions(TelemetryWebSocketSessionRef sessionRef, String sessionId) {
        this.cleanupLocalWsSessionSubscriptions(sessionId);
    }

    @Override
    public void removeSubscription(String sessionId, int subscriptionId) {
        log.debug("[{}][{}] Going to remove subscription.", sessionId, subscriptionId);
        Map<Integer, Subscription> sessionSubscriptions = this.subscriptionsByWsSessionId.get(sessionId);
        if (sessionSubscriptions != null) {
            Subscription subscription = sessionSubscriptions.remove(subscriptionId);
            if (subscription != null) {
                this.processSubscriptionRemoval(sessionId, sessionSubscriptions, subscription);
            }
            else {
                log.debug("[{}][{}] Subscription not found!", sessionId, subscriptionId);
            }
        }
        else {
            log.debug("[{}] No session subscriptions found!", sessionId);
        }
    }

    @Override
    public void onNewRemoteSubscription(ServerAddress serverAddress, byte[] data) {
        ClusterAPIProtos.SubscriptionProto proto;
        try {
            proto = ClusterAPIProtos.SubscriptionProto.parseFrom(data);
        } catch (InvalidProtocolBufferException e) {
            throw new RuntimeException(e);
        }
        Map<String, Long> statesMap = proto.getKeyStatesList().stream().collect(
                Collectors.toMap(ClusterAPIProtos.SubscriptionKetStateProto::getKey,
                                 ClusterAPIProtos.SubscriptionKetStateProto::getTs));
        Subscription subscription = new Subscription(
                new SubscriptionState(proto.getSessionId(), proto.getSubscriptionId(),
                                      new TenantId(UUID.fromString(proto.getTenantId())),
                                      EntityIdFactory.getByTypeAndId(proto.getEntityType(), proto.getEntityId()),
                                      TelemetryFeature.valueOf(proto.getType()), proto.getAllKeys(), statesMap,
                                      proto.getScope()), false,
                new ServerAddress(serverAddress.getHost(), serverAddress.getPort(), serverAddress.getServerType()));

        this.addRemoteWsSubscription(serverAddress, proto.getSessionId(), subscription);
    }

    @Override
    public void onRemoteSubscriptionUpdate(ServerAddress serverAddress, byte[] data) {
        ClusterAPIProtos.SubscriptionUpdateProto proto;
        try {
            proto = ClusterAPIProtos.SubscriptionUpdateProto.parseFrom(data);
        } catch (InvalidProtocolBufferException e) {
            throw new RuntimeException(e);
        }
        SubscriptionUpdate update = this.convert(proto);
        String sessionId = proto.getSessionId();
        log.trace("[{}] Processing remote subscription onUpdate [{}]", sessionId, update);
        Optional<Subscription> subOpt = this.getSubscription(sessionId, update.getSubscriptionId());
        if (subOpt.isPresent()) {
            this.updateSubscriptionState(sessionId, subOpt.get(), update);
            this.wsService.sendWsMsg(sessionId, update);
        }
    }

    @Override
    public void onRemoteSubscriptionClose(ServerAddress serverAddress, byte[] data) {
        ClusterAPIProtos.SubscriptionCloseProto proto;
        try {
            proto = ClusterAPIProtos.SubscriptionCloseProto.parseFrom(data);
        } catch (InvalidProtocolBufferException e) {
            throw new RuntimeException(e);
        }
        this.removeSubscription(proto.getSessionId(), proto.getSubscriptionId());
    }

    @Override
    public void onRemoteSessionClose(ServerAddress serverAddress, byte[] data) {
        ClusterAPIProtos.SessionCloseProto proto;
        try {
            proto = ClusterAPIProtos.SessionCloseProto.parseFrom(data);
        } catch (InvalidProtocolBufferException e) {
            throw new RuntimeException(e);
        }
        this.cleanupRemoteWsSessionSubscriptions(proto.getSessionId());
    }

    @Override
    public void onRemoteAttributesUpdate(ServerAddress serverAddress, byte[] data) {
        ClusterAPIProtos.AttributeUpdateProto proto;
        try {
            proto = ClusterAPIProtos.AttributeUpdateProto.parseFrom(data);
        } catch (InvalidProtocolBufferException e) {
            throw new RuntimeException(e);
        }
        this.onAttributesUpdate(EntityIdFactory.getByTypeAndId(proto.getEntityType(), proto.getEntityId()),
                                proto.getScope(),
                                proto.getDataList().stream().map(this::toAttribute).collect(Collectors.toList()));
    }

    @Override
    public void onRemoteTsUpdate(ServerAddress serverAddress, byte[] data) {
        ClusterAPIProtos.TimeseriesUpdateProto proto;
        try {
            proto = ClusterAPIProtos.TimeseriesUpdateProto.parseFrom(data);
        } catch (InvalidProtocolBufferException e) {
            throw new RuntimeException(e);
        }
        this.onTimeseriesUpdate(EntityIdFactory.getByTypeAndId(proto.getEntityType(), proto.getEntityId()),
                                proto.getDataList().stream().map(this::toTimeseries).collect(Collectors.toList()));
    }

    @Override
    public void onClusterUpdate() {
        log.trace("Processing cluster onUpdate msg!");
        Iterator<Map.Entry<EntityId, Set<Subscription>>> deviceIterator =
                this.subscriptionsByEntityId.entrySet().iterator();
        while (deviceIterator.hasNext()) {
            Map.Entry<EntityId, Set<Subscription>> e = deviceIterator.next();
            Set<Subscription> subscriptions = e.getValue();
            Optional<ServerAddress> newAddressOptional = this.routingService.resolveById(e.getKey());
            if (newAddressOptional.isPresent()) {
                newAddressOptional.ifPresent(
                        serverAddress -> this.checkSubscriptionsNewAddress(serverAddress, subscriptions));
            }
            else {
                this.checkSubscriptionsPrevAddress(subscriptions);
            }
            if (subscriptions.size() == 0) {
                log.trace("[{}] No more subscriptions for this device on current server.", e.getKey());
                deviceIterator.remove();
            }
        }
    }

    private void checkSubscriptionsNewAddress(ServerAddress newAddress, Set<Subscription> subscriptions) {
        Iterator<Subscription> subscriptionIterator = subscriptions.iterator();
        while (subscriptionIterator.hasNext()) {
            Subscription s = subscriptionIterator.next();
            if (s.isLocal()) {
                if (!newAddress.equals(s.getServer())) {
                    log.trace("[{}] Local subscription is now handled on new server [{}]", s.getWsSessionId(),
                              newAddress);
                    s.setServer(newAddress);
                    this.tellNewSubscription(newAddress, s.getWsSessionId(), s);
                }
            }
            else {
                log.trace("[{}] Remote subscription is now handled on new server address: [{}]", s.getWsSessionId(),
                          newAddress);
                subscriptionIterator.remove();
                //TODO: onUpdate state of subscription by WsSessionId and other maps.
            }
        }
    }

    private void checkSubscriptionsPrevAddress(Set<Subscription> subscriptions) {
        for (Subscription s : subscriptions) {
            if (s.isLocal() && s.getServer() != null) {
                log.trace("[{}] Local subscription is no longer handled on remote server address [{}]",
                          s.getWsSessionId(), s.getServer());
                s.setServer(null);
            }
            else {
                log.trace("[{}] Remote subscription is on up to date server address.", s.getWsSessionId());
            }
        }
    }
    private void onTimeseriesUpdate(EntityId entityId, List<TsKvEntry> ts) {
        Optional<ServerAddress> serverAddress = this.routingService.resolveById(entityId);
        if (!serverAddress.isPresent()) {
            this.onLocalTimeseriesUpdate(entityId, ts);
        }
        else {
            this.tellRemoteTimeseriesUpdate(serverAddress.get(), entityId, ts);
        }
    }
    private void onLocalTimeseriesUpdate(EntityId entityId, List<TsKvEntry> ts) {
        this.onLocalSubUpdate(entityId, s -> TelemetryFeature.TIMESERIES == s.getType(), s -> {
            List<TsKvEntry> subscriptionUpdate = null;
            for (TsKvEntry kv : ts) {
                if (this.isInTimeRange(s, kv.getTs()) && (s.isAllKeys() || s.getKeyStates().containsKey(
                        (kv.getKey())))) {
                    if (subscriptionUpdate == null) {
                        subscriptionUpdate = new ArrayList<>();
                    }
                    subscriptionUpdate.add(kv);
                }
            }
            return subscriptionUpdate;
        });
    }
    private void tellRemoteTimeseriesUpdate(ServerAddress address, EntityId entityId, List<TsKvEntry> ts) {
        ClusterAPIProtos.TimeseriesUpdateProto.Builder builder = ClusterAPIProtos.TimeseriesUpdateProto.newBuilder();
        builder.setEntityId(entityId.getId().toString());
        builder.setEntityType(entityId.getEntityType().name());
        ts.forEach(v -> builder.addData(this.toKeyValueProto(v.getTs(), v).build()));
        this.rpcService.tell(address, ClusterAPIProtos.MessageType.CLUSTER_TELEMETRY_TS_UPDATE_MESSAGE,
                             builder.build().toByteArray());
    }
    private boolean isInTimeRange(Subscription subscription, long kvTime) {
        return (subscription.getStartTime() == 0 || subscription.getStartTime() <= kvTime) &&
               (subscription.getEndTime() == 0 || subscription.getEndTime() >= kvTime);
    }
    private void onAttributesUpdate(EntityId entityId, String scope, List<AttributeKvEntry> attributes) {
        Optional<ServerAddress> serverAddress = this.routingService.resolveById(entityId);
        if (!serverAddress.isPresent()) {
            this.onLocalAttributesUpdate(entityId, scope, attributes);
            if (entityId.getEntityType() == EntityType.DEVICE && DataConstants.SERVER_SCOPE.equalsIgnoreCase(scope)) {
                for (AttributeKvEntry attribute : attributes) {
                    if (attribute.getKey().equals(DefaultDeviceStateService.INACTIVITY_TIMEOUT)) {
                        this.stateService.onDeviceInactivityTimeoutUpdate(new DeviceId(entityId.getId()),
                                                                          attribute.getLongValue().orElse(0L));
                    }
                }
            }
        }
        else {
            this.tellRemoteAttributesUpdate(serverAddress.get(), entityId, scope, attributes);
        }
    }
    private void onLocalAttributesUpdate(EntityId entityId, String scope, List<AttributeKvEntry> attributes) {
        this.onLocalSubUpdate(entityId, s -> TelemetryFeature.ATTRIBUTES == s.getType() && (StringUtils.isEmpty(
                s.getScope()) || scope.equals(s.getScope())), s -> {
            List<TsKvEntry> subscriptionUpdate = null;
            for (AttributeKvEntry kv : attributes) {
                if (s.isAllKeys() || s.getKeyStates().containsKey(kv.getKey())) {
                    if (subscriptionUpdate == null) {
                        subscriptionUpdate = new ArrayList<>();
                    }
                    subscriptionUpdate.add(new BasicTsKvEntry(kv.getLastUpdateTs(), kv));
                }
            }
            return subscriptionUpdate;
        });
    }
    private void tellRemoteAttributesUpdate(ServerAddress address,
                                            EntityId entityId,
                                            String scope,
                                            List<AttributeKvEntry> attributes) {
        ClusterAPIProtos.AttributeUpdateProto.Builder builder = ClusterAPIProtos.AttributeUpdateProto.newBuilder();
        builder.setEntityId(entityId.getId().toString());
        builder.setEntityType(entityId.getEntityType().name());
        builder.setScope(scope);
        attributes.forEach(v -> builder.addData(this.toKeyValueProto(v.getLastUpdateTs(), v).build()));
        this.rpcService.tell(address, ClusterAPIProtos.MessageType.CLUSTER_TELEMETRY_ATTR_UPDATE_MESSAGE,
                             builder.build().toByteArray());
    }
    private void onLocalSubUpdate(EntityId entityId,
                                  Predicate<Subscription> filter,
                                  Function<Subscription, List<TsKvEntry>> f) {
        Set<Subscription> deviceSubscriptions = this.subscriptionsByEntityId.get(entityId);
        if (deviceSubscriptions != null) {
            deviceSubscriptions.stream().filter(filter).forEach(s -> {
                String sessionId = s.getWsSessionId();
                List<TsKvEntry> subscriptionUpdate = f.apply(s);
                if (subscriptionUpdate != null && !subscriptionUpdate.isEmpty()) {
                    SubscriptionUpdate update = new SubscriptionUpdate(s.getSubscriptionId(), subscriptionUpdate);
                    if (s.isLocal()) {
                        this.updateSubscriptionState(sessionId, s, update);
                        this.wsService.sendWsMsg(sessionId, update);
                    }
                    else {
                        this.tellRemoteSubUpdate(s.getServer(), sessionId, update);
                    }
                }
            });
        }
        else {
            log.debug("[{}] No device subscriptions to process!", entityId);
        }
    }
    private ClusterAPIProtos.KeyValueProto.Builder toKeyValueProto(long ts, KvEntry attr) {
        ClusterAPIProtos.KeyValueProto.Builder dataBuilder = ClusterAPIProtos.KeyValueProto.newBuilder();
        dataBuilder.setKey(attr.getKey());
        dataBuilder.setTs(ts);
        dataBuilder.setValueType(attr.getDataType().ordinal());
        switch (attr.getDataType()) {
            case BOOLEAN:
                Optional<Boolean> booleanValue = attr.getBooleanValue();
                booleanValue.ifPresent(dataBuilder::setBoolValue);
                break;
            case LONG:
                Optional<Long> longValue = attr.getLongValue();
                longValue.ifPresent(dataBuilder::setLongValue);
                break;
            case DOUBLE:
                Optional<Double> doubleValue = attr.getDoubleValue();
                doubleValue.ifPresent(dataBuilder::setDoubleValue);
                break;
            case STRING:
                Optional<String> stringValue = attr.getStrValue();
                stringValue.ifPresent(dataBuilder::setStrValue);
                break;
        }
        return dataBuilder;
    }
    private void cleanupRemoteWsSessionSubscriptions(String sessionId) {
        this.cleanupWsSessionSubscriptions(sessionId, false);
    }
    private void processSubscriptionRemoval(String sessionId,
                                            Map<Integer, Subscription> sessionSubscriptions,
                                            Subscription subscription) {
        EntityId entityId = subscription.getEntityId();
        if (subscription.isLocal() && subscription.getServer() != null) {
            this.tellRemoteSubClose(subscription.getServer(), sessionId, subscription.getSubscriptionId());
        }
        if (sessionSubscriptions.isEmpty()) {
            log.debug("[{}] Removed last subscription for particular session.", sessionId);
            this.subscriptionsByWsSessionId.remove(sessionId);
        }
        else {
            log.debug("[{}] Removed session subscription.", sessionId);
        }
        Set<Subscription> deviceSubscriptions = this.subscriptionsByEntityId.get(entityId);
        if (deviceSubscriptions != null) {
            boolean result = deviceSubscriptions.remove(subscription);
            if (result) {
                if (deviceSubscriptions.size() == 0) {
                    log.debug("[{}] Removed last subscription for particular device.", sessionId);
                    this.subscriptionsByEntityId.remove(entityId);
                }
                else {
                    log.debug("[{}] Removed device subscription.", sessionId);
                }
            }
            else {
                log.debug("[{}] Subscription not found!", sessionId);
            }
        }
        else {
            log.debug("[{}] No device subscriptions found!", sessionId);
        }
    }
    private void tellRemoteSubClose(ServerAddress address, String sessionId, int subscriptionId) {
        ClusterAPIProtos.SubscriptionCloseProto proto =
                ClusterAPIProtos.SubscriptionCloseProto.newBuilder().setSessionId(sessionId).setSubscriptionId(
                        subscriptionId).build();
        this.rpcService.tell(address, ClusterAPIProtos.MessageType.CLUSTER_TELEMETRY_SUBSCRIPTION_CLOSE_MESSAGE,
                             proto.toByteArray());
    }
    private SubscriptionUpdate convert(ClusterAPIProtos.SubscriptionUpdateProto proto) {
        if (proto.getErrorCode() > 0) {
            return new SubscriptionUpdate(proto.getSubscriptionId(),
                                          SubscriptionErrorCode.forCode(proto.getErrorCode()), proto.getErrorMsg());
        }
        else {
            Map<String, List<Object>> data = new TreeMap<>();
            proto.getDataList().forEach(v -> {
                List<Object> values = data.computeIfAbsent(v.getKey(), k -> new ArrayList<>());
                for (int i = 0; i < v.getTsCount(); i++) {
                    Object[] value = new Object[2];
                    value[0] = v.getTs(i);
                    value[1] = v.getValue(i);
                    values.add(value);
                }
            });
            return new SubscriptionUpdate(proto.getSubscriptionId(), data);
        }
    }
    private Optional<Subscription> getSubscription(String sessionId, int subscriptionId) {
        Subscription state = null;
        Map<Integer, Subscription> subMap = this.subscriptionsByWsSessionId.get(sessionId);
        if (subMap != null) {
            state = subMap.get(subscriptionId);
        }
        return Optional.ofNullable(state);
    }
    private void updateSubscriptionState(String sessionId, Subscription subState, SubscriptionUpdate update) {
        log.trace("[{}] updating subscription state {} using onUpdate {}", sessionId, subState, update);
        update.getLatestValues().entrySet().forEach(e -> subState.setKeyState(e.getKey(), e.getValue()));
    }
    private void addRemoteWsSubscription(ServerAddress address, String sessionId, Subscription subscription) {
        EntityId entityId = subscription.getEntityId();
        log.trace("[{}] Registering remote subscription [{}] for entity [{}] to [{}]", sessionId,
                  subscription.getSubscriptionId(), entityId, address);
        this.registerSubscription(sessionId, entityId, subscription);
        if (subscription.getType() == TelemetryFeature.ATTRIBUTES) {
            final Map<String, Long> keyStates = subscription.getKeyStates();
            DonAsynchron.withCallback(
                    this.attrService.find(subscription.getSub().getTenantId(), entityId, DataConstants.CLIENT_SCOPE,
                                          keyStates.keySet()), values -> {
                        List<TsKvEntry> missedUpdates = new ArrayList<>();
                        values.forEach(latestEntry -> {
                            if (latestEntry.getLastUpdateTs() > keyStates.get(latestEntry.getKey())) {
                                missedUpdates.add(new BasicTsKvEntry(latestEntry.getLastUpdateTs(), latestEntry));
                            }
                        });
                        if (!missedUpdates.isEmpty()) {
                            this.tellRemoteSubUpdate(address, sessionId,
                                                     new SubscriptionUpdate(subscription.getSubscriptionId(),
                                                                            missedUpdates));
                        }
                    }, e -> log.error("Failed to fetch missed updates.", e), this.tsCallBackExecutor);
        }
        else if (subscription.getType() == TelemetryFeature.TIMESERIES) {
            long curTs = System.currentTimeMillis();
            List<ReadTsKvQuery> queries = new ArrayList<>();
            subscription.getKeyStates().entrySet().forEach(e -> {
                if (curTs > e.getValue()) {
                    queries.add(new BaseReadTsKvQuery(e.getKey(), e.getValue() + 1L, curTs, 0, 1000, Aggregation.NONE));
                }
                else {
                    log.debug("[{}] Invalid subscription [{}], entityId [{}] curTs [{}]", sessionId, subscription,
                              entityId, curTs);
                }
            });
            if (!queries.isEmpty()) {
                DonAsynchron.withCallback(
                        this.tsService.findAll(subscription.getSub().getTenantId(), entityId, queries),
                        missedUpdates -> {
                            if (missedUpdates != null && !missedUpdates.isEmpty()) {
                                this.tellRemoteSubUpdate(address, sessionId,
                                                         new SubscriptionUpdate(subscription.getSubscriptionId(),
                                                                                missedUpdates));
                            }
                        }, e -> log.error("Failed to fetch missed updates.", e), this.tsCallBackExecutor);
            }
        }
    }
    private void tellRemoteSubUpdate(ServerAddress address, String sessionId, SubscriptionUpdate update) {
        ClusterAPIProtos.SubscriptionUpdateProto.Builder builder =
                ClusterAPIProtos.SubscriptionUpdateProto.newBuilder();
        builder.setSessionId(sessionId);
        builder.setSubscriptionId(update.getSubscriptionId());
        builder.setErrorCode(update.getErrorCode());
        if (update.getErrorMsg() != null) {
            builder.setErrorMsg(update.getErrorMsg());
        }
        update.getData().entrySet().forEach(e -> {
            ClusterAPIProtos.SubscriptionUpdateValueListProto.Builder dataBuilder =
                    ClusterAPIProtos.SubscriptionUpdateValueListProto.newBuilder();

            dataBuilder.setKey(e.getKey());
            e.getValue().forEach(v -> {
                Object[] array = (Object[]) v;
                dataBuilder.addTs((long) array[0]);
                dataBuilder.addValue((String) array[1]);
            });

            builder.addData(dataBuilder.build());
        });
        this.rpcService.tell(address, ClusterAPIProtos.MessageType.CLUSTER_TELEMETRY_SUBSCRIPTION_UPDATE_MESSAGE,
                             builder.build().toByteArray());
    }
    private void cleanupLocalWsSessionSubscriptions(String sessionId) {
        this.cleanupWsSessionSubscriptions(sessionId, true);
    }
    private void cleanupWsSessionSubscriptions(String sessionId, boolean localSession) {
        log.debug("[{}] Removing all subscriptions for particular session.", sessionId);
        Map<Integer, Subscription> sessionSubscriptions = this.subscriptionsByWsSessionId.get(sessionId);
        if (sessionSubscriptions != null) {
            int sessionSubscriptionSize = sessionSubscriptions.size();

            for (Subscription subscription : sessionSubscriptions.values()) {
                EntityId entityId = subscription.getEntityId();
                Set<Subscription> deviceSubscriptions = this.subscriptionsByEntityId.get(entityId);
                deviceSubscriptions.remove(subscription);
                if (deviceSubscriptions.isEmpty()) {
                    this.subscriptionsByEntityId.remove(entityId);
                }
            }
            this.subscriptionsByWsSessionId.remove(sessionId);
            log.debug("[{}] Removed {} subscriptions for particular session.", sessionId, sessionSubscriptionSize);

            if (localSession) {
                this.notifyWsSubscriptionClosed(sessionId, sessionSubscriptions);
            }
        }
        else {
            log.debug("[{}] No subscriptions found!", sessionId);
        }
    }
    private void notifyWsSubscriptionClosed(String sessionId, Map<Integer, Subscription> sessionSubscriptions) {
        Set<ServerAddress> affectedServers = new HashSet<>();
        for (Subscription subscription : sessionSubscriptions.values()) {
            if (subscription.getServer() != null) {
                affectedServers.add(subscription.getServer());
            }
        }
        for (ServerAddress address : affectedServers) {
            log.debug("[{}] Going to onSubscriptionUpdate [{}] server about session close event", sessionId, address);
            this.tellRemoteSessionClose(address, sessionId);
        }
    }
    private void tellRemoteSessionClose(ServerAddress address, String sessionId) {
        ClusterAPIProtos.SessionCloseProto proto = ClusterAPIProtos.SessionCloseProto.newBuilder().setSessionId(
                sessionId).build();
        this.rpcService.tell(address, ClusterAPIProtos.MessageType.CLUSTER_TELEMETRY_SESSION_CLOSE_MESSAGE,
                             proto.toByteArray());
    }
    @Override
    public void saveAndNotify(TenantId tenantId, EntityId entityId, List<TsKvEntry> ts, FutureCallback<Void> callback) {
        this.saveAndNotify(tenantId, entityId, ts, 0L, callback);
    }
    @Override
    public void saveAndNotify(TenantId tenantId,
                              EntityId entityId,
                              List<TsKvEntry> ts,
                              long ttl,
                              FutureCallback<Void> callback) {
        ListenableFuture<List<Void>> saveFuture = this.tsService.save(tenantId, entityId, ts, ttl);
        this.addMainCallback(saveFuture, callback);
        this.addWsCallback(saveFuture, success -> this.onTimeseriesUpdate(entityId, ts));
    }
    @Override
    public void saveAndNotify(TenantId tenantId,
                              EntityId entityId,
                              String scope,
                              List<AttributeKvEntry> attributes,
                              FutureCallback<Void> callback) {
        ListenableFuture<List<Void>> saveFuture = this.attrService.save(tenantId, entityId, scope, attributes);
        this.addMainCallback(saveFuture, callback);
        this.addWsCallback(saveFuture, success -> this.onAttributesUpdate(entityId, scope, attributes));
    }
    @Override
    public void saveAttrAndNotify(TenantId tenantId,
                                  EntityId entityId,
                                  String scope,
                                  String key,
                                  long value,
                                  FutureCallback<Void> callback) {
        this.saveAndNotify(tenantId, entityId, scope, Collections.singletonList(
                new BaseAttributeKvEntry(new LongDataEntry(key, value), System.currentTimeMillis())), callback);
    }
    @Override
    public void saveAttrAndNotify(TenantId tenantId,
                                  EntityId entityId,
                                  String scope,
                                  String key,
                                  String value,
                                  FutureCallback<Void> callback) {
        this.saveAndNotify(tenantId, entityId, scope, Collections.singletonList(
                new BaseAttributeKvEntry(new StringDataEntry(key, value), System.currentTimeMillis())), callback);
    }
    @Override
    public void saveAttrAndNotify(TenantId tenantId,
                                  EntityId entityId,
                                  String scope,
                                  String key,
                                  double value,
                                  FutureCallback<Void> callback) {
        this.saveAndNotify(tenantId, entityId, scope, Collections.singletonList(
                new BaseAttributeKvEntry(new DoubleDataEntry(key, value), System.currentTimeMillis())), callback);
    }
    @Override
    public void saveAttrAndNotify(TenantId tenantId,
                                  EntityId entityId,
                                  String scope,
                                  String key,
                                  boolean value,
                                  FutureCallback<Void> callback) {
        this.saveAndNotify(tenantId, entityId, scope, Collections.singletonList(
                new BaseAttributeKvEntry(new BooleanDataEntry(key, value), System.currentTimeMillis())), callback);
    }
    @Override
    public void onSharedAttributesUpdate(TenantId tenantId, DeviceId deviceId, Set<AttributeKvEntry> attributes) {
        DeviceAttributesEventNotificationMsg notificationMsg = DeviceAttributesEventNotificationMsg.onUpdate(tenantId,
                                                                                                             deviceId,
                                                                                                             DataConstants.SHARED_SCOPE,
                                                                                                             new ArrayList<>(
                                                                                                                     attributes));
        this.actorService.onMsg(new SendToClusterMsg(deviceId, notificationMsg));
    }
    private void addMainCallback(ListenableFuture<List<Void>> saveFuture, final FutureCallback<Void> callback) {
        Futures.addCallback(saveFuture, new FutureCallback<List<Void>>() {
            @Override
            public void onSuccess(@Nullable List<Void> result) {
                callback.onSuccess(null);
            }

            @Override
            public void onFailure(Throwable t) {
                callback.onFailure(t);
            }
        }, this.tsCallBackExecutor);
    }
    private void addWsCallback(ListenableFuture<List<Void>> saveFuture, Consumer<Void> callback) {
        Futures.addCallback(saveFuture, new FutureCallback<List<Void>>() {
            @Override
            public void onSuccess(@Nullable List<Void> result) {
                callback.accept(null);
            }

            @Override
            public void onFailure(Throwable t) {
            }
        }, this.wsCallBackExecutor);
    }
    private AttributeKvEntry toAttribute(ClusterAPIProtos.KeyValueProto proto) {
        return new BaseAttributeKvEntry(this.getKvEntry(proto), proto.getTs());
    }

    private KvEntry getKvEntry(ClusterAPIProtos.KeyValueProto proto) {
        KvEntry entry = null;
        DataType type = DataType.values()[proto.getValueType()];
        switch (type) {
            case BOOLEAN:
                entry = new BooleanDataEntry(proto.getKey(), proto.getBoolValue());
                break;
            case LONG:
                entry = new LongDataEntry(proto.getKey(), proto.getLongValue());
                break;
            case DOUBLE:
                entry = new DoubleDataEntry(proto.getKey(), proto.getDoubleValue());
                break;
            case STRING:
                entry = new StringDataEntry(proto.getKey(), proto.getStrValue());
                break;
        }
        return entry;
    }

    private TsKvEntry toTimeseries(ClusterAPIProtos.KeyValueProto proto) {
        return new BasicTsKvEntry(proto.getTs(), this.getKvEntry(proto));
    }

}
