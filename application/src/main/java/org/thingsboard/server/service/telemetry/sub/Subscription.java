/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.service.telemetry.sub;


import lombok.AllArgsConstructor;
import lombok.Data;
import org.thingsboard.server.common.data.id.EntityId;
import org.thingsboard.server.common.msg.cluster.ServerAddress;
import org.thingsboard.server.service.telemetry.TelemetryFeature;

import java.util.Map;


@Data
@AllArgsConstructor
public class Subscription {

    private final SubscriptionState sub;
    private final boolean local;
    private ServerAddress server;
    private long startTime;
    private long endTime;

    public Subscription(SubscriptionState sub, boolean local, ServerAddress server) {
        this(sub, local, server, 0L, 0L);
    }

    public String getWsSessionId() {
        return this.getSub().getWsSessionId();
    }

    public int getSubscriptionId() {
        return this.getSub().getSubscriptionId();
    }

    public EntityId getEntityId() {
        return this.getSub().getEntityId();
    }

    public TelemetryFeature getType() {
        return this.getSub().getType();
    }

    public String getScope() {
        return this.getSub().getScope();
    }

    public boolean isAllKeys() {
        return this.getSub().isAllKeys();
    }

    public Map<String, Long> getKeyStates() {
        return this.getSub().getKeyStates();
    }

    public void setKeyState(String key, long ts) {
        this.getSub().getKeyStates().put(key, ts);
    }

    @Override
    public String toString() {
        return "Subscription{" + "sub=" + this.sub + ", local=" + this.local + ", server=" + this.server + '}';
    }

}
