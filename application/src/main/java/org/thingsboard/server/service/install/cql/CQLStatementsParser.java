/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.service.install.cql;


import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;


@Slf4j
public class CQLStatementsParser {

    private String text;
    private State state;
    private int pos;
    private List<String> statements;
    public CQLStatementsParser(Path cql) throws IOException {
        try {
            List<String> lines = Files.readAllLines(cql);
            StringBuilder t = new StringBuilder();
            for (String l : lines) {
                t.append(l.trim());
                t.append('\n');
            }

            this.text = t.toString();
            this.pos = 0;
            this.state = State.DEFAULT;
            this.parseStatements();
        } catch (IOException e) {
            log.error("Unable to parse CQL file [{}]!", cql);
            log.error("Exception", e);
            throw e;
        }
    }

    private void parseStatements() {
        this.statements = new ArrayList<>();
        StringBuilder statementUnderConstruction = new StringBuilder();

        char c;
        while ((c = this.getChar()) != 0) {
            switch (this.state) {
                case DEFAULT:
                    this.processDefaultState(c, statementUnderConstruction);
                    break;
                case INSINGLELINECOMMENT:
                    if (c == '\n') {
                        this.state = State.DEFAULT;
                    }
                    break;

                case INMULTILINECOMMENT:
                    if (c == '*' && this.peekAhead() == '/') {
                        this.state = State.DEFAULT;
                        this.advance();
                    }
                    break;

                case INQUOTESTRING:
                    this.processInQuoteStringState(c, statementUnderConstruction);
                    break;
                case INSQUOTESTRING:
                    this.processInSQuoteStringState(c, statementUnderConstruction);
                    break;
            }

        }
        String tmp = statementUnderConstruction.toString().trim();
        if (tmp.length() > 0) {
            this.statements.add(tmp);
        }
    }

    private char getChar() {
        if (this.pos < this.text.length()) {
            return this.text.charAt(this.pos++);
        }
        else {
            return 0;
        }
    }

    private void processDefaultState(char c, StringBuilder statementUnderConstruction) {
        if ((c == '/' && this.peekAhead() == '/') || (c == '-' && this.peekAhead() == '-')) {
            this.state = State.INSINGLELINECOMMENT;
            this.advance();
        }
        else if (c == '/' && this.peekAhead() == '*') {
            this.state = State.INMULTILINECOMMENT;
            this.advance();
        }
        else if (c == '\n') {
            statementUnderConstruction.append(' ');
        }
        else {
            statementUnderConstruction.append(c);
            if (c == '\"') {
                this.state = State.INQUOTESTRING;
            }
            else if (c == '\'') {
                this.state = State.INSQUOTESTRING;
            }
            else if (c == ';') {
                this.statements.add(statementUnderConstruction.toString().trim());
                statementUnderConstruction.setLength(0);
            }
        }
    }

    private char peekAhead() {
        if (this.pos < this.text.length()) {
            return this.text.charAt(this.pos);  // don't advance
        }
        else {
            return 0;
        }
    }

    private void advance() {
        this.pos++;
    }

    private void processInQuoteStringState(char c, StringBuilder statementUnderConstruction) {
        statementUnderConstruction.append(c);
        if (c == '"') {
            if (this.peekAhead() == '"') {
                statementUnderConstruction.append(this.getChar());
            }
            else {
                this.state = State.DEFAULT;
            }
        }
    }

    private void processInSQuoteStringState(char c, StringBuilder statementUnderConstruction) {
        statementUnderConstruction.append(c);
        if (c == '\'') {
            if (this.peekAhead() == '\'') {
                statementUnderConstruction.append(this.getChar());
            }
            else {
                this.state = State.DEFAULT;
            }
        }
    }

    public List<String> getStatements() {
        return this.statements;
    }

    enum State {
        DEFAULT,
        INSINGLELINECOMMENT,
        INMULTILINECOMMENT,
        INQUOTESTRING,
        INSQUOTESTRING,

    }

}
