/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.service.telemetry.sub;


import lombok.AllArgsConstructor;
import lombok.Getter;
import org.thingsboard.server.common.data.id.EntityId;
import org.thingsboard.server.common.data.id.TenantId;
import org.thingsboard.server.service.telemetry.TelemetryFeature;

import java.util.Map;


/**
 * @author Andrew Shvayka
 */
@AllArgsConstructor
public class SubscriptionState {

    @Getter private final String wsSessionId;
    @Getter private final int subscriptionId;
    @Getter private final TenantId tenantId;
    @Getter private final EntityId entityId;
    @Getter private final TelemetryFeature type;
    @Getter private final boolean allKeys;
    @Getter private final Map<String, Long> keyStates;
    @Getter private final String scope;

    @Override
    public int hashCode() {
        int result = this.wsSessionId != null ? this.wsSessionId.hashCode() : 0;
        result = 31 * result + this.subscriptionId;
        result = 31 * result + (this.entityId != null ? this.entityId.hashCode() : 0);
        result = 31 * result + (this.type != null ? this.type.hashCode() : 0);
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }

        SubscriptionState that = (SubscriptionState) o;

        if (this.subscriptionId != that.subscriptionId) {
            return false;
        }
        if (this.wsSessionId != null ? !this.wsSessionId.equals(that.wsSessionId) : that.wsSessionId != null) {
            return false;
        }
        if (this.entityId != null ? !this.entityId.equals(that.entityId) : that.entityId != null) {
            return false;
        }
        return this.type == that.type;
    }

    @Override
    public String toString() {
        return "SubscriptionState{" + "type=" + this.type + ", entityId=" + this.entityId + ", subscriptionId=" +
               this.subscriptionId + ", wsSessionId='" + this.wsSessionId + '\'' + '}';
    }

}
