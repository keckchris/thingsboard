/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.service.security;


import com.google.common.base.Function;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.async.DeferredResult;
import org.thingsboard.server.common.data.Customer;
import org.thingsboard.server.common.data.Device;
import org.thingsboard.server.common.data.EntityView;
import org.thingsboard.server.common.data.Tenant;
import org.thingsboard.server.common.data.asset.Asset;
import org.thingsboard.server.common.data.exception.ThingsboardException;
import org.thingsboard.server.common.data.id.*;
import org.thingsboard.server.common.data.rule.RuleChain;
import org.thingsboard.server.common.data.rule.RuleNode;
import org.thingsboard.server.controller.HttpValidationCallback;
import org.thingsboard.server.dao.alarm.AlarmService;
import org.thingsboard.server.dao.asset.AssetService;
import org.thingsboard.server.dao.customer.CustomerService;
import org.thingsboard.server.dao.device.DeviceService;
import org.thingsboard.server.dao.entityview.EntityViewService;
import org.thingsboard.server.dao.rule.RuleChainService;
import org.thingsboard.server.dao.tenant.TenantService;
import org.thingsboard.server.dao.user.UserService;
import org.thingsboard.server.service.security.model.SecurityUser;
import org.thingsboard.server.service.telemetry.exception.ToErrorResponseEntity;

import javax.annotation.Nullable;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.BiConsumer;


/**
 * Created by ashvayka on 27.03.18.
 */
@Component
public class AccessValidator {

    public static final String CUSTOMER_USER_IS_NOT_ALLOWED_TO_PERFORM_THIS_OPERATION =
            "Customer user is not allowed to perform this operation!";
    public static final String SYSTEM_ADMINISTRATOR_IS_NOT_ALLOWED_TO_PERFORM_THIS_OPERATION =
            "System administrator is not allowed to perform this operation!";
    public static final String DEVICE_WITH_REQUESTED_ID_NOT_FOUND = "Device with requested id wasn't found!";
    public static final String ENTITY_VIEW_WITH_REQUESTED_ID_NOT_FOUND = "Entity-view with requested id wasn't found!";

    @Autowired protected TenantService tenantService;

    @Autowired protected CustomerService customerService;

    @Autowired protected UserService userService;

    @Autowired protected DeviceService deviceService;

    @Autowired protected AssetService assetService;

    @Autowired protected AlarmService alarmService;

    @Autowired protected RuleChainService ruleChainService;

    @Autowired protected EntityViewService entityViewService;

    private ExecutorService executor;

    @PostConstruct
    public void initExecutor() {
        this.executor = Executors.newSingleThreadExecutor();
    }

    @PreDestroy
    public void shutdownExecutor() {
        if (this.executor != null) {
            this.executor.shutdownNow();
        }
    }

    public DeferredResult<ResponseEntity> validateEntityAndCallback(SecurityUser currentUser,
                                                                    String entityType,
                                                                    String entityIdStr,
                                                                    ThreeConsumer<DeferredResult<ResponseEntity>,
                                                                                         TenantId, EntityId> onSuccess) throws
                                                                                                                                                 ThingsboardException {
        return this.validateEntityAndCallback(currentUser, entityType, entityIdStr, onSuccess,
                                              (result, t) -> handleError(t, result, HttpStatus.INTERNAL_SERVER_ERROR));
    }

    public DeferredResult<ResponseEntity> validateEntityAndCallback(SecurityUser currentUser,
                                                                    String entityType,
                                                                    String entityIdStr,
                                                                    ThreeConsumer<DeferredResult<ResponseEntity>,
                                                                                         TenantId, EntityId> onSuccess,
                                                                    BiConsumer<DeferredResult<ResponseEntity>,
                                                                                      Throwable> onFailure) throws
                                                                                                                                     ThingsboardException {
        return this.validateEntityAndCallback(currentUser, EntityIdFactory.getByTypeAndId(entityType, entityIdStr),
                                              onSuccess, onFailure);
    }

    public static void handleError(Throwable e,
                                   final DeferredResult<ResponseEntity> response,
                                   HttpStatus defaultErrorStatus) {
        ResponseEntity responseEntity;
        if (e != null && e instanceof ToErrorResponseEntity) {
            responseEntity = ((ToErrorResponseEntity) e).toErrorResponseEntity();
        }
        else if (e != null && e instanceof IllegalArgumentException) {
            responseEntity = new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
        else {
            responseEntity = new ResponseEntity<>(defaultErrorStatus);
        }
        response.setResult(responseEntity);
    }

    public DeferredResult<ResponseEntity> validateEntityAndCallback(SecurityUser currentUser,
                                                                    EntityId entityId,
                                                                    ThreeConsumer<DeferredResult<ResponseEntity>,
                                                                                         TenantId, EntityId> onSuccess,
                                                                    BiConsumer<DeferredResult<ResponseEntity>,
                                                                                      Throwable> onFailure) {

        final DeferredResult<ResponseEntity> response = new DeferredResult<>();

        this.validate(currentUser, entityId,
                      new HttpValidationCallback(response, new FutureCallback<DeferredResult<ResponseEntity>>() {
                          @Override
                          public void onSuccess(@Nullable DeferredResult<ResponseEntity> result) {
                              onSuccess.accept(response, currentUser.getTenantId(), entityId);
                          }

                          @Override
                          public void onFailure(Throwable t) {
                              onFailure.accept(response, t);
                          }
                      }));

        return response;
    }

    public void validate(SecurityUser currentUser, EntityId entityId, FutureCallback<ValidationResult> callback) {
        switch (entityId.getEntityType()) {
            case DEVICE:
                this.validateDevice(currentUser, entityId, callback);
                return;
            case ASSET:
                this.validateAsset(currentUser, entityId, callback);
                return;
            case RULE_CHAIN:
                this.validateRuleChain(currentUser, entityId, callback);
                return;
            case CUSTOMER:
                this.validateCustomer(currentUser, entityId, callback);
                return;
            case TENANT:
                this.validateTenant(currentUser, entityId, callback);
                return;
            case ENTITY_VIEW:
                this.validateEntityView(currentUser, entityId, callback);
                return;
            default:
                //TODO: add support of other entities
                throw new IllegalStateException("Not Implemented!");
        }
    }

    private void validateDevice(final SecurityUser currentUser,
                                EntityId entityId,
                                FutureCallback<ValidationResult> callback) {
        if (currentUser.isSystemAdmin()) {
            callback.onSuccess(
                    ValidationResult.accessDenied(SYSTEM_ADMINISTRATOR_IS_NOT_ALLOWED_TO_PERFORM_THIS_OPERATION));
        }
        else {
            ListenableFuture<Device> deviceFuture = this.deviceService.findDeviceByIdAsync(currentUser.getTenantId(),
                                                                                           new DeviceId(
                                                                                                   entityId.getId()));
            Futures.addCallback(deviceFuture, this.getCallback(callback, device -> {
                if (device == null) {
                    return ValidationResult.entityNotFound(DEVICE_WITH_REQUESTED_ID_NOT_FOUND);
                }
                else {
                    if (!device.getTenantId().equals(currentUser.getTenantId())) {
                        return ValidationResult.accessDenied("Device doesn't belong to the current Tenant!");
                    }
                    else if (currentUser.isCustomerUser() && !device.getCustomerId().equals(
                            currentUser.getCustomerId())) {
                        return ValidationResult.accessDenied("Device doesn't belong to the current Customer!");
                    }
                    else {
                        return ValidationResult.ok(device);
                    }
                }
            }), this.executor);
        }
    }

    private void validateAsset(final SecurityUser currentUser,
                               EntityId entityId,
                               FutureCallback<ValidationResult> callback) {
        if (currentUser.isSystemAdmin()) {
            callback.onSuccess(
                    ValidationResult.accessDenied(SYSTEM_ADMINISTRATOR_IS_NOT_ALLOWED_TO_PERFORM_THIS_OPERATION));
        }
        else {
            ListenableFuture<Asset> assetFuture = this.assetService.findAssetByIdAsync(currentUser.getTenantId(),
                                                                                       new AssetId(entityId.getId()));
            Futures.addCallback(assetFuture, this.getCallback(callback, asset -> {
                if (asset == null) {
                    return ValidationResult.entityNotFound("Asset with requested id wasn't found!");
                }
                else {
                    if (!asset.getTenantId().equals(currentUser.getTenantId())) {
                        return ValidationResult.accessDenied("Asset doesn't belong to the current Tenant!");
                    }
                    else if (currentUser.isCustomerUser() && !asset.getCustomerId().equals(
                            currentUser.getCustomerId())) {
                        return ValidationResult.accessDenied("Asset doesn't belong to the current Customer!");
                    }
                    else {
                        return ValidationResult.ok(asset);
                    }
                }
            }), this.executor);
        }
    }

    private void validateRuleChain(final SecurityUser currentUser,
                                   EntityId entityId,
                                   FutureCallback<ValidationResult> callback) {
        if (currentUser.isCustomerUser()) {
            callback.onSuccess(ValidationResult.accessDenied(CUSTOMER_USER_IS_NOT_ALLOWED_TO_PERFORM_THIS_OPERATION));
        }
        else {
            ListenableFuture<RuleChain> ruleChainFuture = this.ruleChainService.findRuleChainByIdAsync(
                    currentUser.getTenantId(), new RuleChainId(entityId.getId()));
            Futures.addCallback(ruleChainFuture, this.getCallback(callback, ruleChain -> {
                if (ruleChain == null) {
                    return ValidationResult.entityNotFound("Rule chain with requested id wasn't found!");
                }
                else {
                    if (currentUser.isTenantAdmin() && !ruleChain.getTenantId().equals(currentUser.getTenantId())) {
                        return ValidationResult.accessDenied("Rule chain doesn't belong to the current Tenant!");
                    }
                    else if (currentUser.isSystemAdmin() && !ruleChain.getTenantId().isNullUid()) {
                        return ValidationResult.accessDenied("Rule chain is not in system scope!");
                    }
                    else {
                        return ValidationResult.ok(ruleChain);
                    }
                }
            }), this.executor);
        }
    }

    private void validateCustomer(final SecurityUser currentUser,
                                  EntityId entityId,
                                  FutureCallback<ValidationResult> callback) {
        if (currentUser.isSystemAdmin()) {
            callback.onSuccess(
                    ValidationResult.accessDenied(SYSTEM_ADMINISTRATOR_IS_NOT_ALLOWED_TO_PERFORM_THIS_OPERATION));
        }
        else {
            ListenableFuture<Customer> customerFuture = this.customerService.findCustomerByIdAsync(
                    currentUser.getTenantId(), new CustomerId(entityId.getId()));
            Futures.addCallback(customerFuture, this.getCallback(callback, customer -> {
                if (customer == null) {
                    return ValidationResult.entityNotFound("Customer with requested id wasn't found!");
                }
                else {
                    if (!customer.getTenantId().equals(currentUser.getTenantId())) {
                        return ValidationResult.accessDenied("Customer doesn't belong to the current Tenant!");
                    }
                    else if (currentUser.isCustomerUser() && !customer.getId().equals(currentUser.getCustomerId())) {
                        return ValidationResult.accessDenied(
                                "Customer doesn't relate to the currently authorized customer user!");
                    }
                    else {
                        return ValidationResult.ok(customer);
                    }
                }
            }), this.executor);
        }
    }

    private void validateTenant(final SecurityUser currentUser,
                                EntityId entityId,
                                FutureCallback<ValidationResult> callback) {
        if (currentUser.isCustomerUser()) {
            callback.onSuccess(ValidationResult.accessDenied(CUSTOMER_USER_IS_NOT_ALLOWED_TO_PERFORM_THIS_OPERATION));
        }
        else if (currentUser.isSystemAdmin()) {
            callback.onSuccess(ValidationResult.ok(null));
        }
        else {
            ListenableFuture<Tenant> tenantFuture = this.tenantService.findTenantByIdAsync(currentUser.getTenantId(),
                                                                                           new TenantId(
                                                                                                   entityId.getId()));
            Futures.addCallback(tenantFuture, this.getCallback(callback, tenant -> {
                if (tenant == null) {
                    return ValidationResult.entityNotFound("Tenant with requested id wasn't found!");
                }
                else if (!tenant.getId().equals(currentUser.getTenantId())) {
                    return ValidationResult.accessDenied("Tenant doesn't relate to the currently authorized user!");
                }
                else {
                    return ValidationResult.ok(tenant);
                }
            }), this.executor);
        }
    }

    private void validateEntityView(final SecurityUser currentUser,
                                    EntityId entityId,
                                    FutureCallback<ValidationResult> callback) {
        if (currentUser.isSystemAdmin()) {
            callback.onSuccess(
                    ValidationResult.accessDenied(SYSTEM_ADMINISTRATOR_IS_NOT_ALLOWED_TO_PERFORM_THIS_OPERATION));
        }
        else {
            ListenableFuture<EntityView> entityViewFuture = this.entityViewService.findEntityViewByIdAsync(
                    currentUser.getTenantId(), new EntityViewId(entityId.getId()));
            Futures.addCallback(entityViewFuture, this.getCallback(callback, entityView -> {
                if (entityView == null) {
                    return ValidationResult.entityNotFound(ENTITY_VIEW_WITH_REQUESTED_ID_NOT_FOUND);
                }
                else {
                    if (!entityView.getTenantId().equals(currentUser.getTenantId())) {
                        return ValidationResult.accessDenied("Entity-view doesn't belong to the current Tenant!");
                    }
                    else if (currentUser.isCustomerUser() && !entityView.getCustomerId().equals(
                            currentUser.getCustomerId())) {
                        return ValidationResult.accessDenied("Entity-view doesn't belong to the current Customer!");
                    }
                    else {
                        return ValidationResult.ok(entityView);
                    }
                }
            }), this.executor);
        }
    }

    private <T, V> FutureCallback<T> getCallback(FutureCallback<ValidationResult> callback,
                                                 Function<T, ValidationResult<V>> transformer) {
        return new FutureCallback<T>() {
            @Override
            public void onSuccess(@Nullable T result) {
                callback.onSuccess(transformer.apply(result));
            }

            @Override
            public void onFailure(Throwable t) {
                callback.onFailure(t);
            }
        };
    }

    public DeferredResult<ResponseEntity> validateEntityAndCallback(SecurityUser currentUser,
                                                                    EntityId entityId,
                                                                    ThreeConsumer<DeferredResult<ResponseEntity>,
                                                                                         TenantId, EntityId> onSuccess) throws
                                                                                                                                                 ThingsboardException {
        return this.validateEntityAndCallback(currentUser, entityId, onSuccess,
                                              (result, t) -> handleError(t, result, HttpStatus.INTERNAL_SERVER_ERROR));
    }

    private void validateRule(final SecurityUser currentUser,
                              EntityId entityId,
                              FutureCallback<ValidationResult> callback) {
        if (currentUser.isCustomerUser()) {
            callback.onSuccess(ValidationResult.accessDenied(CUSTOMER_USER_IS_NOT_ALLOWED_TO_PERFORM_THIS_OPERATION));
        }
        else {
            ListenableFuture<RuleNode> ruleNodeFuture = this.ruleChainService.findRuleNodeByIdAsync(
                    currentUser.getTenantId(), new RuleNodeId(entityId.getId()));
            Futures.addCallback(ruleNodeFuture, this.getCallback(callback, ruleNodeTmp -> {
                RuleNode ruleNode = ruleNodeTmp;
                if (ruleNode == null) {
                    return ValidationResult.entityNotFound("Rule node with requested id wasn't found!");
                }
                else if (ruleNode.getRuleChainId() == null) {
                    return ValidationResult.entityNotFound("Rule chain with requested node id wasn't found!");
                }
                else {
                    //TODO: make async
                    RuleChain ruleChain = this.ruleChainService.findRuleChainById(currentUser.getTenantId(),
                                                                                  ruleNode.getRuleChainId());
                    if (currentUser.isTenantAdmin() && !ruleChain.getTenantId().equals(currentUser.getTenantId())) {
                        return ValidationResult.accessDenied("Rule chain doesn't belong to the current Tenant!");
                    }
                    else if (currentUser.isSystemAdmin() && !ruleChain.getTenantId().isNullUid()) {
                        return ValidationResult.accessDenied("Rule chain is not in system scope!");
                    }
                    else {
                        return ValidationResult.ok(ruleNode);
                    }
                }
            }), this.executor);
        }
    }

    public interface ThreeConsumer<A, B, C> {

        void accept(A a, B b, C c);

    }

}
