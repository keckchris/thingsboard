/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.service.script;


import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;


/**
 * Created by ashvayka on 26.09.18.
 */
@Slf4j
public abstract class AbstractJsInvokeService implements JsInvokeService {

    protected Map<UUID, String> scriptIdToNameMap = new ConcurrentHashMap<>();
    protected Map<UUID, AtomicInteger> blackListedFunctions = new ConcurrentHashMap<>();

    @Override
    public ListenableFuture<UUID> eval(JsScriptType scriptType, String scriptBody, String... argNames) {
        UUID scriptId = UUID.randomUUID();
        String functionName = "invokeInternal_" + scriptId.toString().replace('-', '_');
        String jsScript = this.generateJsScript(scriptType, functionName, scriptBody, argNames);
        return this.doEval(scriptId, functionName, jsScript);
    }

    @Override
    public ListenableFuture<Object> invokeFunction(UUID scriptId, Object... args) {
        String functionName = this.scriptIdToNameMap.get(scriptId);
        if (functionName == null) {
            return Futures.immediateFailedFuture(
                    new RuntimeException("No compiled script found for scriptId: [" + scriptId + "]!"));
        }
        if (!this.isBlackListed(scriptId)) {
            return this.doInvokeFunction(scriptId, functionName, args);
        }
        else {
            return Futures.immediateFailedFuture(new RuntimeException(
                    "Script is blacklisted due to maximum error count " + this.getMaxErrors() + "!"));
        }
    }

    @Override
    public ListenableFuture<Void> release(UUID scriptId) {
        String functionName = this.scriptIdToNameMap.get(scriptId);
        if (functionName != null) {
            try {
                this.scriptIdToNameMap.remove(scriptId);
                this.blackListedFunctions.remove(scriptId);
                this.doRelease(scriptId, functionName);
            } catch (Exception e) {
                return Futures.immediateFailedFuture(e);
            }
        }
        return Futures.immediateFuture(null);
    }

    protected abstract void doRelease(UUID scriptId, String functionName) throws Exception;

    private boolean isBlackListed(UUID scriptId) {
        if (this.blackListedFunctions.containsKey(scriptId)) {
            AtomicInteger errorCount = this.blackListedFunctions.get(scriptId);
            return errorCount.get() >= this.getMaxErrors();
        }
        else {
            return false;
        }
    }

    protected abstract ListenableFuture<Object> doInvokeFunction(UUID scriptId, String functionName, Object[] args);

    protected abstract int getMaxErrors();

    private String generateJsScript(JsScriptType scriptType,
                                    String functionName,
                                    String scriptBody,
                                    String... argNames) {
        switch (scriptType) {
            case RULE_NODE_SCRIPT:
                return RuleNodeScriptFactory.generateRuleNodeScript(functionName, scriptBody, argNames);
            default:
                throw new RuntimeException("No script factory implemented for scriptType: " + scriptType);
        }
    }

    protected abstract ListenableFuture<UUID> doEval(UUID scriptId, String functionName, String scriptBody);

    protected void onScriptExecutionError(UUID scriptId) {
        this.blackListedFunctions.computeIfAbsent(scriptId, key -> new AtomicInteger(0)).incrementAndGet();
    }

}
