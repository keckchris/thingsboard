/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.service.cluster.routing;


import com.google.common.hash.HashCode;
import com.google.common.hash.HashFunction;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.thingsboard.server.common.data.id.EntityId;
import org.thingsboard.server.common.msg.cluster.ServerAddress;
import org.thingsboard.server.common.msg.cluster.ServerType;
import org.thingsboard.server.service.cluster.discovery.DiscoveryService;
import org.thingsboard.server.service.cluster.discovery.ServerInstance;
import org.thingsboard.server.utils.MiscUtils;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentNavigableMap;


/**
 * Cluster service implementation based on consistent hash ring
 */

@Service
@Slf4j
public class ConsistentClusterRoutingService implements ClusterRoutingService {

    @Autowired private DiscoveryService discoveryService;

    @Value("${cluster.hash_function_name}") private String hashFunctionName;
    @Value("${cluster.vitrual_nodes_size}") private Integer virtualNodesSize;

    private ServerInstance currentServer;

    private HashFunction hashFunction;

    private ConsistentHashCircle[] circles;
    private ConsistentHashCircle rootCircle;

    @PostConstruct
    public void init() {
        log.info("Initializing Cluster routing service!");
        this.hashFunction = MiscUtils.forName(this.hashFunctionName);
        this.currentServer = this.discoveryService.getCurrentServer();
        this.circles = new ConsistentHashCircle[ServerType.values().length];
        for (ServerType serverType : ServerType.values()) {
            this.circles[serverType.ordinal()] = new ConsistentHashCircle();
        }
        this.rootCircle = this.circles[ServerType.CORE.ordinal()];
        this.addNode(this.discoveryService.getCurrentServer());
        for (ServerInstance instance : this.discoveryService.getOtherServers()) {
            this.addNode(instance);
        }
        this.logCircle();
        log.info("Cluster routing service initialized!");
    }

    private void addNode(ServerInstance instance) {
        for (int i = 0; i < this.virtualNodesSize; i++) {
            this.circles[instance.getServerAddress().getServerType().ordinal()].put(this.hash(instance, i).asLong(),
                                                                                    instance);
        }
    }

    private void logCircle() {
        log.trace("Consistent Hash Circle Start");
        Arrays.asList(this.circles).forEach(ConsistentHashCircle::log);
        log.trace("Consistent Hash Circle End");
    }

    private HashCode hash(ServerInstance instance, int i) {
        return this.hashFunction.newHasher()
                                .putString(instance.getHost(), MiscUtils.UTF8)
                                .putInt(instance.getPort())
                                .putInt(i)
                                .hash();
    }

    @Override
    public ServerAddress getCurrentServer() {
        return this.discoveryService.getCurrentServer().getServerAddress();
    }

    @Override
    public Optional<ServerAddress> resolveByUuid(UUID uuid) {
        return this.resolveByUuid(this.rootCircle, uuid);
    }

    @Override
    public Optional<ServerAddress> resolveById(EntityId entityId) {
        return this.resolveByUuid(this.rootCircle, entityId.getId());
    }

    @Override
    public Optional<ServerAddress> resolveByUuid(ServerType server, UUID uuid) {
        return this.resolveByUuid(this.circles[server.ordinal()], uuid);
    }

    @Override
    public Optional<ServerAddress> resolveById(ServerType server, EntityId entityId) {
        return this.resolveByUuid(this.circles[server.ordinal()], entityId.getId());
    }

    private Optional<ServerAddress> resolveByUuid(ConsistentHashCircle circle, UUID uuid) {
        Assert.notNull(uuid);
        if (circle.isEmpty()) {
            return Optional.empty();
        }
        Long hash = this.hashFunction.newHasher().putLong(uuid.getMostSignificantBits()).putLong(
                uuid.getLeastSignificantBits()).hash().asLong();
        if (!circle.containsKey(hash)) {
            ConcurrentNavigableMap<Long, ServerInstance> tailMap = circle.tailMap(hash);
            hash = tailMap.isEmpty() ? circle.firstKey() : tailMap.firstKey();
        }
        ServerInstance result = circle.get(hash);
        if (!this.currentServer.equals(result)) {
            return Optional.of(result.getServerAddress());
        }
        else {
            return Optional.empty();
        }
    }

    @Override
    public void onServerAdded(ServerInstance server) {
        log.info("On server added event: {}", server);
        this.addNode(server);
        this.logCircle();
    }

    @Override
    public void onServerUpdated(ServerInstance server) {
        log.debug("Ignoring server onUpdate event: {}", server);
    }

    @Override
    public void onServerRemoved(ServerInstance server) {
        log.info("On server removed event: {}", server);
        this.removeNode(server);
        this.logCircle();
    }

    private void removeNode(ServerInstance instance) {
        for (int i = 0; i < this.virtualNodesSize; i++) {
            this.circles[instance.getServerAddress().getServerType().ordinal()].remove(this.hash(instance, i).asLong());
        }
    }

}
