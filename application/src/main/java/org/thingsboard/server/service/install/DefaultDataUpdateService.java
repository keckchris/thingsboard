/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.service.install;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.thingsboard.server.common.data.SearchTextBased;
import org.thingsboard.server.common.data.Tenant;
import org.thingsboard.server.common.data.id.UUIDBased;
import org.thingsboard.server.common.data.page.TextPageData;
import org.thingsboard.server.common.data.page.TextPageLink;
import org.thingsboard.server.common.data.rule.RuleChain;
import org.thingsboard.server.dao.rule.RuleChainService;
import org.thingsboard.server.dao.tenant.TenantService;


@Service
@Profile("install")
@Slf4j
public class DefaultDataUpdateService implements DataUpdateService {

    @Autowired private TenantService tenantService;

    @Autowired private RuleChainService ruleChainService;

    @Autowired private InstallScripts installScripts;
    private PaginatedUpdater<String, Tenant> tenantsDefaultRuleChainUpdater = new PaginatedUpdater<String, Tenant>() {

        @Override
        protected TextPageData<Tenant> findEntities(String region, TextPageLink pageLink) {
            return DefaultDataUpdateService.this.tenantService.findTenants(pageLink);
        }

        @Override
        protected void updateEntity(Tenant tenant) {
            try {
                RuleChain ruleChain = DefaultDataUpdateService.this.ruleChainService.getRootTenantRuleChain(
                        tenant.getId());
                if (ruleChain == null) {
                    DefaultDataUpdateService.this.installScripts.createDefaultRuleChains(tenant.getId());
                }
            } catch (Exception e) {
                log.error("Unable to update Tenant", e);
            }
        }
    };

    @Override
    public void updateData(String fromVersion) {
        switch (fromVersion) {
            case "1.4.0":
                log.info("Updating data from version 1.4.0 to 2.0.0 ...");
                this.tenantsDefaultRuleChainUpdater.updateEntities(null);
                break;
            default:
                throw new RuntimeException("Unable to update data, unsupported fromVersion: " + fromVersion);
        }
    }

    public abstract class PaginatedUpdater<I, D extends SearchTextBased<? extends UUIDBased>> {

        private static final int DEFAULT_LIMIT = 100;

        public void updateEntities(I id) {
            TextPageLink pageLink = new TextPageLink(DEFAULT_LIMIT);
            boolean hasNext = true;
            while (hasNext) {
                TextPageData<D> entities = this.findEntities(id, pageLink);
                for (D entity : entities.getData()) {
                    this.updateEntity(entity);
                }
                hasNext = entities.hasNext();
                if (hasNext) {
                    pageLink = entities.getNextPageLink();
                }
            }
        }

        protected abstract TextPageData<D> findEntities(I id, TextPageLink pageLink);

        protected abstract void updateEntity(D entity);

    }

}
