/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.controller;


import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.thingsboard.server.common.data.EntityType;
import org.thingsboard.server.common.data.alarm.*;
import org.thingsboard.server.common.data.audit.ActionType;
import org.thingsboard.server.common.data.exception.ThingsboardErrorCode;
import org.thingsboard.server.common.data.exception.ThingsboardException;
import org.thingsboard.server.common.data.id.EntityId;
import org.thingsboard.server.common.data.id.EntityIdFactory;
import org.thingsboard.server.common.data.page.TimePageData;
import org.thingsboard.server.common.data.page.TimePageLink;


@RestController
@RequestMapping("/api")
public class AlarmController extends BaseController {

    public static final String ALARM_ID = "alarmId";

    @PreAuthorize("hasAnyAuthority('SYS_ADMIN', 'TENANT_ADMIN', 'CUSTOMER_USER')")
    @RequestMapping(value = "/alarm/{alarmId}", method = RequestMethod.GET)
    @ResponseBody
    public Alarm getAlarmById(@PathVariable(ALARM_ID) String strAlarmId) throws ThingsboardException {
        this.checkParameter(ALARM_ID, strAlarmId);
        try {
            AlarmId alarmId = new AlarmId(this.toUUID(strAlarmId));
            return this.checkAlarmId(alarmId);
        } catch (Exception e) {
            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAnyAuthority('SYS_ADMIN', 'TENANT_ADMIN', 'CUSTOMER_USER')")
    @RequestMapping(value = "/alarm/info/{alarmId}", method = RequestMethod.GET)
    @ResponseBody
    public AlarmInfo getAlarmInfoById(@PathVariable(ALARM_ID) String strAlarmId) throws ThingsboardException {
        this.checkParameter(ALARM_ID, strAlarmId);
        try {
            AlarmId alarmId = new AlarmId(this.toUUID(strAlarmId));
            return this.checkAlarmInfoId(alarmId);
        } catch (Exception e) {
            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAnyAuthority('SYS_ADMIN', 'TENANT_ADMIN', 'CUSTOMER_USER')")
    @RequestMapping(value = "/alarm", method = RequestMethod.POST)
    @ResponseBody
    public Alarm saveAlarm(@RequestBody Alarm alarm) throws ThingsboardException {
        try {
            alarm.setTenantId(this.getCurrentUser().getTenantId());
            Alarm savedAlarm = this.checkNotNull(this.alarmService.createOrUpdateAlarm(alarm));
            this.logEntityAction(savedAlarm.getId(), savedAlarm, this.getCurrentUser().getCustomerId(),
                                 alarm.getId() == null ? ActionType.ADDED : ActionType.UPDATED, null);
            return savedAlarm;
        } catch (Exception e) {
            this.logEntityAction(this.emptyId(EntityType.ALARM), alarm, null,
                                 alarm.getId() == null ? ActionType.ADDED : ActionType.UPDATED, e);
            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAnyAuthority('SYS_ADMIN', 'TENANT_ADMIN', 'CUSTOMER_USER')")
    @RequestMapping(value = "/alarm/{alarmId}/ack", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public void ackAlarm(@PathVariable(ALARM_ID) String strAlarmId) throws ThingsboardException {
        this.checkParameter(ALARM_ID, strAlarmId);
        try {
            AlarmId alarmId = new AlarmId(this.toUUID(strAlarmId));
            Alarm alarm = this.checkAlarmId(alarmId);
            this.alarmService.ackAlarm(this.getCurrentUser().getTenantId(), alarmId, System.currentTimeMillis()).get();
            this.logEntityAction(alarmId, alarm, this.getCurrentUser().getCustomerId(), ActionType.ALARM_ACK, null);
        } catch (Exception e) {
            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAnyAuthority('SYS_ADMIN', 'TENANT_ADMIN', 'CUSTOMER_USER')")
    @RequestMapping(value = "/alarm/{alarmId}/clear", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public void clearAlarm(@PathVariable(ALARM_ID) String strAlarmId) throws ThingsboardException {
        this.checkParameter(ALARM_ID, strAlarmId);
        try {
            AlarmId alarmId = new AlarmId(this.toUUID(strAlarmId));
            Alarm alarm = this.checkAlarmId(alarmId);
            this.alarmService.clearAlarm(this.getCurrentUser().getTenantId(), alarmId, null, System.currentTimeMillis())
                             .get();
            this.logEntityAction(alarmId, alarm, this.getCurrentUser().getCustomerId(), ActionType.ALARM_CLEAR, null);
        } catch (Exception e) {
            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAnyAuthority('SYS_ADMIN', 'TENANT_ADMIN', 'CUSTOMER_USER')")
    @RequestMapping(value = "/alarm/{entityType}/{entityId}", method = RequestMethod.GET)
    @ResponseBody
    public TimePageData<AlarmInfo> getAlarms(@PathVariable("entityType") String strEntityType,
                                             @PathVariable("entityId") String strEntityId,
                                             @RequestParam(required = false) String searchStatus,
                                             @RequestParam(required = false) String status,
                                             @RequestParam int limit,
                                             @RequestParam(required = false) Long startTime,
                                             @RequestParam(required = false) Long endTime,
                                             @RequestParam(required = false, defaultValue = "false") boolean ascOrder,
                                             @RequestParam(required = false) String offset,
                                             @RequestParam(required = false) Boolean fetchOriginator) throws
                                                                                                      ThingsboardException {
        this.checkParameter("EntityId", strEntityId);
        this.checkParameter("EntityType", strEntityType);
        EntityId entityId = EntityIdFactory.getByTypeAndId(strEntityType, strEntityId);
        AlarmSearchStatus alarmSearchStatus = StringUtils.isEmpty(searchStatus) ? null : AlarmSearchStatus.valueOf(
                searchStatus);
        AlarmStatus alarmStatus = StringUtils.isEmpty(status) ? null : AlarmStatus.valueOf(status);
        if (alarmSearchStatus != null && alarmStatus != null) {
            throw new ThingsboardException("Invalid alarms search query: Both parameters 'searchStatus' " +
                                           "and 'status' can't be specified at the same time!",
                                           ThingsboardErrorCode.BAD_REQUEST_PARAMS);
        }
        this.checkEntityId(entityId);
        try {
            TimePageLink pageLink = this.createPageLink(limit, startTime, endTime, ascOrder, offset);
            return this.checkNotNull(this.alarmService.findAlarms(this.getCurrentUser().getTenantId(),
                                                                  new AlarmQuery(entityId, pageLink, alarmSearchStatus,
                                                                                 alarmStatus, fetchOriginator)).get());
        } catch (Exception e) {
            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAnyAuthority('SYS_ADMIN', 'TENANT_ADMIN', 'CUSTOMER_USER')")
    @RequestMapping(value = "/alarm/highestSeverity/{entityType}/{entityId}", method = RequestMethod.GET)
    @ResponseBody
    public AlarmSeverity getHighestAlarmSeverity(@PathVariable("entityType") String strEntityType,
                                                 @PathVariable("entityId") String strEntityId,
                                                 @RequestParam(required = false) String searchStatus,
                                                 @RequestParam(required = false) String status) throws
                                                                                                ThingsboardException {
        this.checkParameter("EntityId", strEntityId);
        this.checkParameter("EntityType", strEntityType);
        EntityId entityId = EntityIdFactory.getByTypeAndId(strEntityType, strEntityId);
        AlarmSearchStatus alarmSearchStatus = StringUtils.isEmpty(searchStatus) ? null : AlarmSearchStatus.valueOf(
                searchStatus);
        AlarmStatus alarmStatus = StringUtils.isEmpty(status) ? null : AlarmStatus.valueOf(status);
        if (alarmSearchStatus != null && alarmStatus != null) {
            throw new ThingsboardException("Invalid alarms search query: Both parameters 'searchStatus' " +
                                           "and 'status' can't be specified at the same time!",
                                           ThingsboardErrorCode.BAD_REQUEST_PARAMS);
        }
        this.checkEntityId(entityId);
        try {
            return this.alarmService.findHighestAlarmSeverity(this.getCurrentUser().getTenantId(), entityId,
                                                              alarmSearchStatus, alarmStatus);
        } catch (Exception e) {
            throw this.handleException(e);
        }
    }

}
