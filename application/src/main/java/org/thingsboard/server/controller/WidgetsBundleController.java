/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.controller;


import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.thingsboard.server.common.data.exception.ThingsboardException;
import org.thingsboard.server.common.data.id.TenantId;
import org.thingsboard.server.common.data.id.WidgetsBundleId;
import org.thingsboard.server.common.data.page.TextPageData;
import org.thingsboard.server.common.data.page.TextPageLink;
import org.thingsboard.server.common.data.security.Authority;
import org.thingsboard.server.common.data.widget.WidgetsBundle;
import org.thingsboard.server.dao.model.ModelConstants;

import java.util.List;


@RestController
@RequestMapping("/api")
public class WidgetsBundleController extends BaseController {

    @PreAuthorize("hasAnyAuthority('SYS_ADMIN', 'TENANT_ADMIN', 'CUSTOMER_USER')")
    @RequestMapping(value = "/widgetsBundle/{widgetsBundleId}", method = RequestMethod.GET)
    @ResponseBody
    public WidgetsBundle getWidgetsBundleById(@PathVariable("widgetsBundleId") String strWidgetsBundleId) throws
                                                                                                          ThingsboardException {
        this.checkParameter("widgetsBundleId", strWidgetsBundleId);
        try {
            WidgetsBundleId widgetsBundleId = new WidgetsBundleId(this.toUUID(strWidgetsBundleId));
            return this.checkWidgetsBundleId(widgetsBundleId, false);
        } catch (Exception e) {
            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAnyAuthority('SYS_ADMIN', 'TENANT_ADMIN')")
    @RequestMapping(value = "/widgetsBundle", method = RequestMethod.POST)
    @ResponseBody
    public WidgetsBundle saveWidgetsBundle(@RequestBody WidgetsBundle widgetsBundle) throws ThingsboardException {
        try {
            if (this.getCurrentUser().getAuthority() == Authority.SYS_ADMIN) {
                widgetsBundle.setTenantId(new TenantId(ModelConstants.NULL_UUID));
            }
            else {
                widgetsBundle.setTenantId(this.getCurrentUser().getTenantId());
            }
            return this.checkNotNull(this.widgetsBundleService.saveWidgetsBundle(widgetsBundle));
        } catch (Exception e) {
            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAnyAuthority('SYS_ADMIN', 'TENANT_ADMIN')")
    @RequestMapping(value = "/widgetsBundle/{widgetsBundleId}", method = RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.OK)
    public void deleteWidgetsBundle(@PathVariable("widgetsBundleId") String strWidgetsBundleId) throws
                                                                                                ThingsboardException {
        this.checkParameter("widgetsBundleId", strWidgetsBundleId);
        try {
            WidgetsBundleId widgetsBundleId = new WidgetsBundleId(this.toUUID(strWidgetsBundleId));
            this.checkWidgetsBundleId(widgetsBundleId, true);
            this.widgetsBundleService.deleteWidgetsBundle(this.getTenantId(), widgetsBundleId);
        } catch (Exception e) {
            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAnyAuthority('SYS_ADMIN', 'TENANT_ADMIN', 'CUSTOMER_USER')")
    @RequestMapping(value = "/widgetsBundles", params = {"limit"}, method = RequestMethod.GET)
    @ResponseBody
    public TextPageData<WidgetsBundle> getWidgetsBundles(@RequestParam int limit,
                                                         @RequestParam(required = false) String textSearch,
                                                         @RequestParam(required = false) String idOffset,
                                                         @RequestParam(required = false) String textOffset) throws
                                                                                                            ThingsboardException {
        try {
            TextPageLink pageLink = this.createPageLink(limit, textSearch, idOffset, textOffset);
            if (this.getCurrentUser().getAuthority() == Authority.SYS_ADMIN) {
                return this.checkNotNull(
                        this.widgetsBundleService.findSystemWidgetsBundlesByPageLink(this.getTenantId(), pageLink));
            }
            else {
                TenantId tenantId = this.getCurrentUser().getTenantId();
                return this.checkNotNull(
                        this.widgetsBundleService.findAllTenantWidgetsBundlesByTenantIdAndPageLink(tenantId, pageLink));
            }
        } catch (Exception e) {
            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAnyAuthority('SYS_ADMIN', 'TENANT_ADMIN', 'CUSTOMER_USER')")
    @RequestMapping(value = "/widgetsBundles", method = RequestMethod.GET)
    @ResponseBody
    public List<WidgetsBundle> getWidgetsBundles() throws ThingsboardException {
        try {
            if (this.getCurrentUser().getAuthority() == Authority.SYS_ADMIN) {
                return this.checkNotNull(this.widgetsBundleService.findSystemWidgetsBundles(this.getTenantId()));
            }
            else {
                TenantId tenantId = this.getCurrentUser().getTenantId();
                return this.checkNotNull(this.widgetsBundleService.findAllTenantWidgetsBundlesByTenantId(tenantId));
            }
        } catch (Exception e) {
            throw this.handleException(e);
        }
    }

}
