/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.controller;


import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.thingsboard.server.common.data.audit.AuditLog;
import org.thingsboard.server.common.data.exception.ThingsboardException;
import org.thingsboard.server.common.data.id.CustomerId;
import org.thingsboard.server.common.data.id.EntityIdFactory;
import org.thingsboard.server.common.data.id.TenantId;
import org.thingsboard.server.common.data.id.UserId;
import org.thingsboard.server.common.data.page.TimePageData;
import org.thingsboard.server.common.data.page.TimePageLink;

import java.util.UUID;


@RestController
@RequestMapping("/api")
public class AuditLogController extends BaseController {

    @PreAuthorize("hasAuthority('TENANT_ADMIN')")
    @RequestMapping(value = "/audit/logs/customer/{customerId}", params = {"limit"}, method = RequestMethod.GET)
    @ResponseBody
    public TimePageData<AuditLog> getAuditLogsByCustomerId(@PathVariable("customerId") String strCustomerId,
                                                           @RequestParam int limit,
                                                           @RequestParam(required = false) Long startTime,
                                                           @RequestParam(required = false) Long endTime,
                                                           @RequestParam(required = false, defaultValue = "false")
                                                                   boolean ascOrder,
                                                           @RequestParam(required = false) String offset) throws
                                                                                                          ThingsboardException {
        try {
            this.checkParameter("CustomerId", strCustomerId);
            TenantId tenantId = this.getCurrentUser().getTenantId();
            TimePageLink pageLink = this.createPageLink(limit, startTime, endTime, ascOrder, offset);
            return this.checkNotNull(this.auditLogService.findAuditLogsByTenantIdAndCustomerId(tenantId, new CustomerId(
                    UUID.fromString(strCustomerId)), pageLink));
        } catch (Exception e) {
            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAuthority('TENANT_ADMIN')")
    @RequestMapping(value = "/audit/logs/user/{userId}", params = {"limit"}, method = RequestMethod.GET)
    @ResponseBody
    public TimePageData<AuditLog> getAuditLogsByUserId(@PathVariable("userId") String strUserId,
                                                       @RequestParam int limit,
                                                       @RequestParam(required = false) Long startTime,
                                                       @RequestParam(required = false) Long endTime,
                                                       @RequestParam(required = false, defaultValue = "false")
                                                               boolean ascOrder,
                                                       @RequestParam(required = false) String offset) throws
                                                                                                      ThingsboardException {
        try {
            this.checkParameter("UserId", strUserId);
            TenantId tenantId = this.getCurrentUser().getTenantId();
            TimePageLink pageLink = this.createPageLink(limit, startTime, endTime, ascOrder, offset);
            return this.checkNotNull(this.auditLogService.findAuditLogsByTenantIdAndUserId(tenantId, new UserId(
                    UUID.fromString(strUserId)), pageLink));
        } catch (Exception e) {
            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAuthority('TENANT_ADMIN')")
    @RequestMapping(value = "/audit/logs/entity/{entityType}/{entityId}",
                    params = {"limit"},
                    method = RequestMethod.GET)
    @ResponseBody
    public TimePageData<AuditLog> getAuditLogsByEntityId(@PathVariable("entityType") String strEntityType,
                                                         @PathVariable("entityId") String strEntityId,
                                                         @RequestParam int limit,
                                                         @RequestParam(required = false) Long startTime,
                                                         @RequestParam(required = false) Long endTime,
                                                         @RequestParam(required = false, defaultValue = "false")
                                                                 boolean ascOrder,
                                                         @RequestParam(required = false) String offset) throws
                                                                                                        ThingsboardException {
        try {
            this.checkParameter("EntityId", strEntityId);
            this.checkParameter("EntityType", strEntityType);
            TenantId tenantId = this.getCurrentUser().getTenantId();
            TimePageLink pageLink = this.createPageLink(limit, startTime, endTime, ascOrder, offset);
            return this.checkNotNull(this.auditLogService.findAuditLogsByTenantIdAndEntityId(tenantId,
                                                                                             EntityIdFactory.getByTypeAndId(
                                                                                                     strEntityType,
                                                                                                     strEntityId),
                                                                                             pageLink));
        } catch (Exception e) {
            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAuthority('TENANT_ADMIN')")
    @RequestMapping(value = "/audit/logs", params = {"limit"}, method = RequestMethod.GET)
    @ResponseBody
    public TimePageData<AuditLog> getAuditLogs(@RequestParam int limit,
                                               @RequestParam(required = false) Long startTime,
                                               @RequestParam(required = false) Long endTime,
                                               @RequestParam(required = false, defaultValue = "false") boolean ascOrder,
                                               @RequestParam(required = false) String offset) throws
                                                                                              ThingsboardException {
        try {
            TenantId tenantId = this.getCurrentUser().getTenantId();
            TimePageLink pageLink = this.createPageLink(limit, startTime, endTime, ascOrder, offset);
            return this.checkNotNull(this.auditLogService.findAuditLogsByTenantId(tenantId, pageLink));
        } catch (Exception e) {
            throw this.handleException(e);
        }
    }

}
