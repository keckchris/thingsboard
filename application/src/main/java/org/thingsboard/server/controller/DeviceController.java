/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.controller;


import com.google.common.util.concurrent.ListenableFuture;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.thingsboard.server.common.data.Customer;
import org.thingsboard.server.common.data.Device;
import org.thingsboard.server.common.data.EntitySubtype;
import org.thingsboard.server.common.data.EntityType;
import org.thingsboard.server.common.data.audit.ActionType;
import org.thingsboard.server.common.data.device.DeviceSearchQuery;
import org.thingsboard.server.common.data.exception.ThingsboardErrorCode;
import org.thingsboard.server.common.data.exception.ThingsboardException;
import org.thingsboard.server.common.data.id.CustomerId;
import org.thingsboard.server.common.data.id.DeviceId;
import org.thingsboard.server.common.data.id.TenantId;
import org.thingsboard.server.common.data.page.TextPageData;
import org.thingsboard.server.common.data.page.TextPageLink;
import org.thingsboard.server.common.data.security.Authority;
import org.thingsboard.server.common.data.security.DeviceCredentials;
import org.thingsboard.server.dao.exception.IncorrectParameterException;
import org.thingsboard.server.dao.model.ModelConstants;
import org.thingsboard.server.service.security.model.SecurityUser;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/api")
public class DeviceController extends BaseController {

    public static final String DEVICE_ID = "deviceId";

    @PreAuthorize("hasAnyAuthority('TENANT_ADMIN', 'CUSTOMER_USER')")
    @RequestMapping(value = "/device/{deviceId}", method = RequestMethod.GET)
    @ResponseBody
    public Device getDeviceById(@PathVariable(DEVICE_ID) String strDeviceId) throws ThingsboardException {
        this.checkParameter(DEVICE_ID, strDeviceId);
        try {
            DeviceId deviceId = new DeviceId(this.toUUID(strDeviceId));
            return this.checkDeviceId(deviceId);
        } catch (Exception e) {
            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAnyAuthority('TENANT_ADMIN', 'CUSTOMER_USER')")
    @RequestMapping(value = "/device", method = RequestMethod.POST)
    @ResponseBody
    public Device saveDevice(@RequestBody Device device) throws ThingsboardException {
        try {
            device.setTenantId(this.getCurrentUser().getTenantId());
            if (this.getCurrentUser().getAuthority() == Authority.CUSTOMER_USER) {
                if (device.getId() == null || device.getId().isNullUid() || device.getCustomerId() == null ||
                    device.getCustomerId().isNullUid()) {
                    throw new ThingsboardException("You don't have permission to perform this operation!",
                                                   ThingsboardErrorCode.PERMISSION_DENIED);
                }
                else {
                    this.checkCustomerId(device.getCustomerId());
                }
            }
            Device savedDevice = this.checkNotNull(this.deviceService.saveDevice(device));

            this.actorService.onDeviceNameOrTypeUpdate(savedDevice.getTenantId(), savedDevice.getId(),
                                                       savedDevice.getName(), savedDevice.getType());

            this.logEntityAction(savedDevice.getId(), savedDevice, savedDevice.getCustomerId(),
                                 device.getId() == null ? ActionType.ADDED : ActionType.UPDATED, null);

            if (device.getId() == null) {
                this.deviceStateService.onDeviceAdded(savedDevice);
            }
            else {
                this.deviceStateService.onDeviceUpdated(savedDevice);
            }
            return savedDevice;
        } catch (Exception e) {
            this.logEntityAction(this.emptyId(EntityType.DEVICE), device, null,
                                 device.getId() == null ? ActionType.ADDED : ActionType.UPDATED, e);
            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAuthority('TENANT_ADMIN')")
    @RequestMapping(value = "/device/{deviceId}", method = RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.OK)
    public void deleteDevice(@PathVariable(DEVICE_ID) String strDeviceId) throws ThingsboardException {
        this.checkParameter(DEVICE_ID, strDeviceId);
        try {
            DeviceId deviceId = new DeviceId(this.toUUID(strDeviceId));
            Device device = this.checkDeviceId(deviceId);
            this.deviceService.deleteDevice(this.getCurrentUser().getTenantId(), deviceId);

            this.logEntityAction(deviceId, device, device.getCustomerId(), ActionType.DELETED, null, strDeviceId);

            this.deviceStateService.onDeviceDeleted(device);
        } catch (Exception e) {
            this.logEntityAction(this.emptyId(EntityType.DEVICE), null, null, ActionType.DELETED, e, strDeviceId);
            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAuthority('TENANT_ADMIN')")
    @RequestMapping(value = "/customer/{customerId}/device/{deviceId}", method = RequestMethod.POST)
    @ResponseBody
    public Device assignDeviceToCustomer(@PathVariable("customerId") String strCustomerId,
                                         @PathVariable(DEVICE_ID) String strDeviceId) throws ThingsboardException {
        this.checkParameter("customerId", strCustomerId);
        this.checkParameter(DEVICE_ID, strDeviceId);
        try {
            CustomerId customerId = new CustomerId(this.toUUID(strCustomerId));
            Customer customer = this.checkCustomerId(customerId);

            DeviceId deviceId = new DeviceId(this.toUUID(strDeviceId));
            this.checkDeviceId(deviceId);

            Device savedDevice = this.checkNotNull(
                    this.deviceService.assignDeviceToCustomer(this.getCurrentUser().getTenantId(), deviceId,
                                                              customerId));

            this.logEntityAction(deviceId, savedDevice, savedDevice.getCustomerId(), ActionType.ASSIGNED_TO_CUSTOMER,
                                 null, strDeviceId, strCustomerId, customer.getName());

            return savedDevice;
        } catch (Exception e) {
            this.logEntityAction(this.emptyId(EntityType.DEVICE), null, null, ActionType.ASSIGNED_TO_CUSTOMER, e,
                                 strDeviceId, strCustomerId);
            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAuthority('TENANT_ADMIN')")
    @RequestMapping(value = "/customer/device/{deviceId}", method = RequestMethod.DELETE)
    @ResponseBody
    public Device unassignDeviceFromCustomer(@PathVariable(DEVICE_ID) String strDeviceId) throws ThingsboardException {
        this.checkParameter(DEVICE_ID, strDeviceId);
        try {
            DeviceId deviceId = new DeviceId(this.toUUID(strDeviceId));
            Device device = this.checkDeviceId(deviceId);
            if (device.getCustomerId() == null || device.getCustomerId().getId().equals(ModelConstants.NULL_UUID)) {
                throw new IncorrectParameterException("Device isn't assigned to any customer!");
            }
            Customer customer = this.checkCustomerId(device.getCustomerId());

            Device savedDevice = this.checkNotNull(
                    this.deviceService.unassignDeviceFromCustomer(this.getCurrentUser().getTenantId(), deviceId));

            this.logEntityAction(deviceId, device, device.getCustomerId(), ActionType.UNASSIGNED_FROM_CUSTOMER, null,
                                 strDeviceId, customer.getId().toString(), customer.getName());

            return savedDevice;
        } catch (Exception e) {
            this.logEntityAction(this.emptyId(EntityType.DEVICE), null, null, ActionType.UNASSIGNED_FROM_CUSTOMER, e,
                                 strDeviceId);
            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAuthority('TENANT_ADMIN')")
    @RequestMapping(value = "/customer/public/device/{deviceId}", method = RequestMethod.POST)
    @ResponseBody
    public Device assignDeviceToPublicCustomer(@PathVariable(DEVICE_ID) String strDeviceId) throws
                                                                                            ThingsboardException {
        this.checkParameter(DEVICE_ID, strDeviceId);
        try {
            DeviceId deviceId = new DeviceId(this.toUUID(strDeviceId));
            Device device = this.checkDeviceId(deviceId);
            Customer publicCustomer = this.customerService.findOrCreatePublicCustomer(device.getTenantId());
            Device savedDevice = this.checkNotNull(
                    this.deviceService.assignDeviceToCustomer(this.getCurrentUser().getTenantId(), deviceId,
                                                              publicCustomer.getId()));

            this.logEntityAction(deviceId, savedDevice, savedDevice.getCustomerId(), ActionType.ASSIGNED_TO_CUSTOMER,
                                 null, strDeviceId, publicCustomer.getId().toString(), publicCustomer.getName());

            return savedDevice;
        } catch (Exception e) {
            this.logEntityAction(this.emptyId(EntityType.DEVICE), null, null, ActionType.ASSIGNED_TO_CUSTOMER, e,
                                 strDeviceId);
            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAnyAuthority('TENANT_ADMIN', 'CUSTOMER_USER')")
    @RequestMapping(value = "/device/{deviceId}/credentials", method = RequestMethod.GET)
    @ResponseBody
    public DeviceCredentials getDeviceCredentialsByDeviceId(@PathVariable(DEVICE_ID) String strDeviceId) throws
                                                                                                         ThingsboardException {
        this.checkParameter(DEVICE_ID, strDeviceId);
        try {
            DeviceId deviceId = new DeviceId(this.toUUID(strDeviceId));
            Device device = this.checkDeviceId(deviceId);
            DeviceCredentials deviceCredentials = this.checkNotNull(
                    this.deviceCredentialsService.findDeviceCredentialsByDeviceId(this.getCurrentUser().getTenantId(),
                                                                                  deviceId));
            this.logEntityAction(deviceId, device, device.getCustomerId(), ActionType.CREDENTIALS_READ, null,
                                 strDeviceId);
            return deviceCredentials;
        } catch (Exception e) {
            this.logEntityAction(this.emptyId(EntityType.DEVICE), null, null, ActionType.CREDENTIALS_READ, e,
                                 strDeviceId);
            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAuthority('TENANT_ADMIN')")
    @RequestMapping(value = "/device/credentials", method = RequestMethod.POST)
    @ResponseBody
    public DeviceCredentials saveDeviceCredentials(@RequestBody DeviceCredentials deviceCredentials) throws
                                                                                                     ThingsboardException {
        this.checkNotNull(deviceCredentials);
        try {
            Device device = this.checkDeviceId(deviceCredentials.getDeviceId());
            DeviceCredentials result = this.checkNotNull(
                    this.deviceCredentialsService.updateDeviceCredentials(this.getCurrentUser().getTenantId(),
                                                                          deviceCredentials));
            this.actorService.onCredentialsUpdate(this.getCurrentUser().getTenantId(), deviceCredentials.getDeviceId());
            this.logEntityAction(device.getId(), device, device.getCustomerId(), ActionType.CREDENTIALS_UPDATED, null,
                                 deviceCredentials);
            return result;
        } catch (Exception e) {
            this.logEntityAction(this.emptyId(EntityType.DEVICE), null, null, ActionType.CREDENTIALS_UPDATED, e,
                                 deviceCredentials);
            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAuthority('TENANT_ADMIN')")
    @RequestMapping(value = "/tenant/devices", params = {"limit"}, method = RequestMethod.GET)
    @ResponseBody
    public TextPageData<Device> getTenantDevices(@RequestParam int limit,
                                                 @RequestParam(required = false) String type,
                                                 @RequestParam(required = false) String textSearch,
                                                 @RequestParam(required = false) String idOffset,
                                                 @RequestParam(required = false) String textOffset) throws
                                                                                                    ThingsboardException {
        try {
            TenantId tenantId = this.getCurrentUser().getTenantId();
            TextPageLink pageLink = this.createPageLink(limit, textSearch, idOffset, textOffset);
            if (type != null && type.trim().length() > 0) {
                return this.checkNotNull(this.deviceService.findDevicesByTenantIdAndType(tenantId, type, pageLink));
            }
            else {
                return this.checkNotNull(this.deviceService.findDevicesByTenantId(tenantId, pageLink));
            }
        } catch (Exception e) {
            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAuthority('TENANT_ADMIN')")
    @RequestMapping(value = "/tenant/devices", params = {"deviceName"}, method = RequestMethod.GET)
    @ResponseBody
    public Device getTenantDevice(@RequestParam String deviceName) throws ThingsboardException {
        try {
            TenantId tenantId = this.getCurrentUser().getTenantId();
            return this.checkNotNull(this.deviceService.findDeviceByTenantIdAndName(tenantId, deviceName));
        } catch (Exception e) {
            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAnyAuthority('TENANT_ADMIN', 'CUSTOMER_USER')")
    @RequestMapping(value = "/customer/{customerId}/devices", params = {"limit"}, method = RequestMethod.GET)
    @ResponseBody
    public TextPageData<Device> getCustomerDevices(@PathVariable("customerId") String strCustomerId,
                                                   @RequestParam int limit,
                                                   @RequestParam(required = false) String type,
                                                   @RequestParam(required = false) String textSearch,
                                                   @RequestParam(required = false) String idOffset,
                                                   @RequestParam(required = false) String textOffset) throws
                                                                                                      ThingsboardException {
        this.checkParameter("customerId", strCustomerId);
        try {
            TenantId tenantId = this.getCurrentUser().getTenantId();
            CustomerId customerId = new CustomerId(this.toUUID(strCustomerId));
            this.checkCustomerId(customerId);
            TextPageLink pageLink = this.createPageLink(limit, textSearch, idOffset, textOffset);
            if (type != null && type.trim().length() > 0) {
                return this.checkNotNull(
                        this.deviceService.findDevicesByTenantIdAndCustomerIdAndType(tenantId, customerId, type,
                                                                                     pageLink));
            }
            else {
                return this.checkNotNull(
                        this.deviceService.findDevicesByTenantIdAndCustomerId(tenantId, customerId, pageLink));
            }
        } catch (Exception e) {
            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAnyAuthority('TENANT_ADMIN', 'CUSTOMER_USER')")
    @RequestMapping(value = "/devices", params = {"deviceIds"}, method = RequestMethod.GET)
    @ResponseBody
    public List<Device> getDevicesByIds(@RequestParam("deviceIds") String[] strDeviceIds) throws ThingsboardException {
        this.checkArrayParameter("deviceIds", strDeviceIds);
        try {
            SecurityUser user = this.getCurrentUser();
            TenantId tenantId = user.getTenantId();
            CustomerId customerId = user.getCustomerId();
            List<DeviceId> deviceIds = new ArrayList<>();
            for (String strDeviceId : strDeviceIds) {
                deviceIds.add(new DeviceId(this.toUUID(strDeviceId)));
            }
            ListenableFuture<List<Device>> devices;
            if (customerId == null || customerId.isNullUid()) {
                devices = this.deviceService.findDevicesByTenantIdAndIdsAsync(tenantId, deviceIds);
            }
            else {
                devices = this.deviceService.findDevicesByTenantIdCustomerIdAndIdsAsync(tenantId, customerId,
                                                                                        deviceIds);
            }
            return this.checkNotNull(devices.get());
        } catch (Exception e) {
            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAnyAuthority('TENANT_ADMIN', 'CUSTOMER_USER')")
    @RequestMapping(value = "/devices", method = RequestMethod.POST)
    @ResponseBody
    public List<Device> findByQuery(@RequestBody DeviceSearchQuery query) throws ThingsboardException {
        this.checkNotNull(query);
        this.checkNotNull(query.getParameters());
        this.checkNotNull(query.getDeviceTypes());
        this.checkEntityId(query.getParameters().getEntityId());
        try {
            List<Device> devices = this.checkNotNull(
                    this.deviceService.findDevicesByQuery(this.getCurrentUser().getTenantId(), query).get());
            devices = devices.stream().filter(device -> {
                try {
                    this.checkDevice(device);
                    return true;
                } catch (ThingsboardException e) {
                    return false;
                }
            }).collect(Collectors.toList());
            return devices;
        } catch (Exception e) {
            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAnyAuthority('TENANT_ADMIN', 'CUSTOMER_USER')")
    @RequestMapping(value = "/device/types", method = RequestMethod.GET)
    @ResponseBody
    public List<EntitySubtype> getDeviceTypes() throws ThingsboardException {
        try {
            SecurityUser user = this.getCurrentUser();
            TenantId tenantId = user.getTenantId();
            ListenableFuture<List<EntitySubtype>> deviceTypes = this.deviceService.findDeviceTypesByTenantId(tenantId);
            return this.checkNotNull(deviceTypes.get());
        } catch (Exception e) {
            throw this.handleException(e);
        }
    }

}
