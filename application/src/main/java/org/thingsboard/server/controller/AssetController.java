/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.controller;


import com.google.common.util.concurrent.ListenableFuture;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.thingsboard.server.common.data.Customer;
import org.thingsboard.server.common.data.EntitySubtype;
import org.thingsboard.server.common.data.EntityType;
import org.thingsboard.server.common.data.asset.Asset;
import org.thingsboard.server.common.data.asset.AssetSearchQuery;
import org.thingsboard.server.common.data.audit.ActionType;
import org.thingsboard.server.common.data.exception.ThingsboardErrorCode;
import org.thingsboard.server.common.data.exception.ThingsboardException;
import org.thingsboard.server.common.data.id.AssetId;
import org.thingsboard.server.common.data.id.CustomerId;
import org.thingsboard.server.common.data.id.TenantId;
import org.thingsboard.server.common.data.page.TextPageData;
import org.thingsboard.server.common.data.page.TextPageLink;
import org.thingsboard.server.common.data.security.Authority;
import org.thingsboard.server.dao.exception.IncorrectParameterException;
import org.thingsboard.server.dao.model.ModelConstants;
import org.thingsboard.server.service.security.model.SecurityUser;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/api")
public class AssetController extends BaseController {

    public static final String ASSET_ID = "assetId";

    @PreAuthorize("hasAnyAuthority('TENANT_ADMIN', 'CUSTOMER_USER')")
    @RequestMapping(value = "/asset/{assetId}", method = RequestMethod.GET)
    @ResponseBody
    public Asset getAssetById(@PathVariable(ASSET_ID) String strAssetId) throws ThingsboardException {
        this.checkParameter(ASSET_ID, strAssetId);
        try {
            AssetId assetId = new AssetId(this.toUUID(strAssetId));
            return this.checkAssetId(assetId);
        } catch (Exception e) {
            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAnyAuthority('TENANT_ADMIN', 'CUSTOMER_USER')")
    @RequestMapping(value = "/asset", method = RequestMethod.POST)
    @ResponseBody
    public Asset saveAsset(@RequestBody Asset asset) throws ThingsboardException {
        try {
            asset.setTenantId(this.getCurrentUser().getTenantId());
            if (this.getCurrentUser().getAuthority() == Authority.CUSTOMER_USER) {
                if (asset.getId() == null || asset.getId().isNullUid() || asset.getCustomerId() == null ||
                    asset.getCustomerId().isNullUid()) {
                    throw new ThingsboardException("You don't have permission to perform this operation!",
                                                   ThingsboardErrorCode.PERMISSION_DENIED);
                }
                else {
                    this.checkCustomerId(asset.getCustomerId());
                }
            }
            Asset savedAsset = this.checkNotNull(this.assetService.saveAsset(asset));

            this.logEntityAction(savedAsset.getId(), savedAsset, savedAsset.getCustomerId(),
                                 asset.getId() == null ? ActionType.ADDED : ActionType.UPDATED, null);

            return savedAsset;
        } catch (Exception e) {
            this.logEntityAction(this.emptyId(EntityType.ASSET), asset, null,
                                 asset.getId() == null ? ActionType.ADDED : ActionType.UPDATED, e);
            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAuthority('TENANT_ADMIN')")
    @RequestMapping(value = "/asset/{assetId}", method = RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.OK)
    public void deleteAsset(@PathVariable(ASSET_ID) String strAssetId) throws ThingsboardException {
        this.checkParameter(ASSET_ID, strAssetId);
        try {
            AssetId assetId = new AssetId(this.toUUID(strAssetId));
            Asset asset = this.checkAssetId(assetId);
            this.assetService.deleteAsset(this.getTenantId(), assetId);

            this.logEntityAction(assetId, asset, asset.getCustomerId(), ActionType.DELETED, null, strAssetId);

        } catch (Exception e) {
            this.logEntityAction(this.emptyId(EntityType.ASSET), null, null, ActionType.DELETED, e, strAssetId);
            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAuthority('TENANT_ADMIN')")
    @RequestMapping(value = "/customer/{customerId}/asset/{assetId}", method = RequestMethod.POST)
    @ResponseBody
    public Asset assignAssetToCustomer(@PathVariable("customerId") String strCustomerId,
                                       @PathVariable(ASSET_ID) String strAssetId) throws ThingsboardException {
        this.checkParameter("customerId", strCustomerId);
        this.checkParameter(ASSET_ID, strAssetId);
        try {
            CustomerId customerId = new CustomerId(this.toUUID(strCustomerId));
            Customer customer = this.checkCustomerId(customerId);

            AssetId assetId = new AssetId(this.toUUID(strAssetId));
            this.checkAssetId(assetId);

            Asset savedAsset = this.checkNotNull(
                    this.assetService.assignAssetToCustomer(this.getTenantId(), assetId, customerId));

            this.logEntityAction(assetId, savedAsset, savedAsset.getCustomerId(), ActionType.ASSIGNED_TO_CUSTOMER, null,
                                 strAssetId, strCustomerId, customer.getName());

            return savedAsset;
        } catch (Exception e) {

            this.logEntityAction(this.emptyId(EntityType.ASSET), null, null, ActionType.ASSIGNED_TO_CUSTOMER, e,
                                 strAssetId, strCustomerId);

            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAuthority('TENANT_ADMIN')")
    @RequestMapping(value = "/customer/asset/{assetId}", method = RequestMethod.DELETE)
    @ResponseBody
    public Asset unassignAssetFromCustomer(@PathVariable(ASSET_ID) String strAssetId) throws ThingsboardException {
        this.checkParameter(ASSET_ID, strAssetId);
        try {
            AssetId assetId = new AssetId(this.toUUID(strAssetId));
            Asset asset = this.checkAssetId(assetId);
            if (asset.getCustomerId() == null || asset.getCustomerId().getId().equals(ModelConstants.NULL_UUID)) {
                throw new IncorrectParameterException("Asset isn't assigned to any customer!");
            }

            Customer customer = this.checkCustomerId(asset.getCustomerId());

            Asset savedAsset = this.checkNotNull(
                    this.assetService.unassignAssetFromCustomer(this.getTenantId(), assetId));

            this.logEntityAction(assetId, asset, asset.getCustomerId(), ActionType.UNASSIGNED_FROM_CUSTOMER, null,
                                 strAssetId, customer.getId().toString(), customer.getName());

            return savedAsset;
        } catch (Exception e) {

            this.logEntityAction(this.emptyId(EntityType.ASSET), null, null, ActionType.UNASSIGNED_FROM_CUSTOMER, e,
                                 strAssetId);

            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAuthority('TENANT_ADMIN')")
    @RequestMapping(value = "/customer/public/asset/{assetId}", method = RequestMethod.POST)
    @ResponseBody
    public Asset assignAssetToPublicCustomer(@PathVariable(ASSET_ID) String strAssetId) throws ThingsboardException {
        this.checkParameter(ASSET_ID, strAssetId);
        try {
            AssetId assetId = new AssetId(this.toUUID(strAssetId));
            Asset asset = this.checkAssetId(assetId);
            Customer publicCustomer = this.customerService.findOrCreatePublicCustomer(asset.getTenantId());
            Asset savedAsset = this.checkNotNull(
                    this.assetService.assignAssetToCustomer(this.getTenantId(), assetId, publicCustomer.getId()));

            this.logEntityAction(assetId, savedAsset, savedAsset.getCustomerId(), ActionType.ASSIGNED_TO_CUSTOMER, null,
                                 strAssetId, publicCustomer.getId().toString(), publicCustomer.getName());

            return savedAsset;
        } catch (Exception e) {

            this.logEntityAction(this.emptyId(EntityType.ASSET), null, null, ActionType.ASSIGNED_TO_CUSTOMER, e,
                                 strAssetId);

            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAuthority('TENANT_ADMIN')")
    @RequestMapping(value = "/tenant/assets", params = {"limit"}, method = RequestMethod.GET)
    @ResponseBody
    public TextPageData<Asset> getTenantAssets(@RequestParam int limit,
                                               @RequestParam(required = false) String type,
                                               @RequestParam(required = false) String textSearch,
                                               @RequestParam(required = false) String idOffset,
                                               @RequestParam(required = false) String textOffset) throws
                                                                                                  ThingsboardException {
        try {
            TenantId tenantId = this.getCurrentUser().getTenantId();
            TextPageLink pageLink = this.createPageLink(limit, textSearch, idOffset, textOffset);
            if (type != null && type.trim().length() > 0) {
                return this.checkNotNull(this.assetService.findAssetsByTenantIdAndType(tenantId, type, pageLink));
            }
            else {
                return this.checkNotNull(this.assetService.findAssetsByTenantId(tenantId, pageLink));
            }
        } catch (Exception e) {
            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAuthority('TENANT_ADMIN')")
    @RequestMapping(value = "/tenant/assets", params = {"assetName"}, method = RequestMethod.GET)
    @ResponseBody
    public Asset getTenantAsset(@RequestParam String assetName) throws ThingsboardException {
        try {
            TenantId tenantId = this.getCurrentUser().getTenantId();
            return this.checkNotNull(this.assetService.findAssetByTenantIdAndName(tenantId, assetName));
        } catch (Exception e) {
            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAnyAuthority('TENANT_ADMIN', 'CUSTOMER_USER')")
    @RequestMapping(value = "/customer/{customerId}/assets", params = {"limit"}, method = RequestMethod.GET)
    @ResponseBody
    public TextPageData<Asset> getCustomerAssets(@PathVariable("customerId") String strCustomerId,
                                                 @RequestParam int limit,
                                                 @RequestParam(required = false) String type,
                                                 @RequestParam(required = false) String textSearch,
                                                 @RequestParam(required = false) String idOffset,
                                                 @RequestParam(required = false) String textOffset) throws
                                                                                                    ThingsboardException {
        this.checkParameter("customerId", strCustomerId);
        try {
            TenantId tenantId = this.getCurrentUser().getTenantId();
            CustomerId customerId = new CustomerId(this.toUUID(strCustomerId));
            this.checkCustomerId(customerId);
            TextPageLink pageLink = this.createPageLink(limit, textSearch, idOffset, textOffset);
            if (type != null && type.trim().length() > 0) {
                return this.checkNotNull(
                        this.assetService.findAssetsByTenantIdAndCustomerIdAndType(tenantId, customerId, type,
                                                                                   pageLink));
            }
            else {
                return this.checkNotNull(
                        this.assetService.findAssetsByTenantIdAndCustomerId(tenantId, customerId, pageLink));
            }
        } catch (Exception e) {
            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAnyAuthority('TENANT_ADMIN', 'CUSTOMER_USER')")
    @RequestMapping(value = "/assets", params = {"assetIds"}, method = RequestMethod.GET)
    @ResponseBody
    public List<Asset> getAssetsByIds(@RequestParam("assetIds") String[] strAssetIds) throws ThingsboardException {
        this.checkArrayParameter("assetIds", strAssetIds);
        try {
            SecurityUser user = this.getCurrentUser();
            TenantId tenantId = user.getTenantId();
            CustomerId customerId = user.getCustomerId();
            List<AssetId> assetIds = new ArrayList<>();
            for (String strAssetId : strAssetIds) {
                assetIds.add(new AssetId(this.toUUID(strAssetId)));
            }
            ListenableFuture<List<Asset>> assets;
            if (customerId == null || customerId.isNullUid()) {
                assets = this.assetService.findAssetsByTenantIdAndIdsAsync(tenantId, assetIds);
            }
            else {
                assets = this.assetService.findAssetsByTenantIdCustomerIdAndIdsAsync(tenantId, customerId, assetIds);
            }
            return this.checkNotNull(assets.get());
        } catch (Exception e) {
            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAnyAuthority('TENANT_ADMIN', 'CUSTOMER_USER')")
    @RequestMapping(value = "/assets", method = RequestMethod.POST)
    @ResponseBody
    public List<Asset> findByQuery(@RequestBody AssetSearchQuery query) throws ThingsboardException {
        this.checkNotNull(query);
        this.checkNotNull(query.getParameters());
        this.checkNotNull(query.getAssetTypes());
        this.checkEntityId(query.getParameters().getEntityId());
        try {
            List<Asset> assets = this.checkNotNull(
                    this.assetService.findAssetsByQuery(this.getTenantId(), query).get());
            assets = assets.stream().filter(asset -> {
                try {
                    this.checkAsset(asset);
                    return true;
                } catch (ThingsboardException e) {
                    return false;
                }
            }).collect(Collectors.toList());
            return assets;
        } catch (Exception e) {
            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAnyAuthority('TENANT_ADMIN', 'CUSTOMER_USER')")
    @RequestMapping(value = "/asset/types", method = RequestMethod.GET)
    @ResponseBody
    public List<EntitySubtype> getAssetTypes() throws ThingsboardException {
        try {
            SecurityUser user = this.getCurrentUser();
            TenantId tenantId = user.getTenantId();
            ListenableFuture<List<EntitySubtype>> assetTypes = this.assetService.findAssetTypesByTenantId(tenantId);
            return this.checkNotNull(assetTypes.get());
        } catch (Exception e) {
            throw this.handleException(e);
        }
    }

}
