/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.controller;


import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.thingsboard.server.common.data.audit.ActionType;
import org.thingsboard.server.common.data.exception.ThingsboardErrorCode;
import org.thingsboard.server.common.data.exception.ThingsboardException;
import org.thingsboard.server.common.data.id.EntityId;
import org.thingsboard.server.common.data.id.EntityIdFactory;
import org.thingsboard.server.common.data.relation.EntityRelation;
import org.thingsboard.server.common.data.relation.EntityRelationInfo;
import org.thingsboard.server.common.data.relation.EntityRelationsQuery;
import org.thingsboard.server.common.data.relation.RelationTypeGroup;

import java.util.List;


@RestController
@RequestMapping("/api")
public class EntityRelationController extends BaseController {

    public static final String TO_TYPE = "toType";
    public static final String FROM_ID = "fromId";
    public static final String FROM_TYPE = "fromType";
    public static final String RELATION_TYPE = "relationType";
    public static final String TO_ID = "toId";

    @PreAuthorize("hasAnyAuthority('SYS_ADMIN', 'TENANT_ADMIN', 'CUSTOMER_USER')")
    @RequestMapping(value = "/relation", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public void saveRelation(@RequestBody EntityRelation relation) throws ThingsboardException {
        try {
            this.checkNotNull(relation);
            this.checkEntityId(relation.getFrom());
            this.checkEntityId(relation.getTo());
            if (relation.getTypeGroup() == null) {
                relation.setTypeGroup(RelationTypeGroup.COMMON);
            }
            this.relationService.saveRelation(this.getTenantId(), relation);
            this.logEntityAction(relation.getFrom(), null, this.getCurrentUser().getCustomerId(),
                                 ActionType.RELATION_ADD_OR_UPDATE, null, relation);
            this.logEntityAction(relation.getTo(), null, this.getCurrentUser().getCustomerId(),
                                 ActionType.RELATION_ADD_OR_UPDATE, null, relation);
        } catch (Exception e) {
            this.logEntityAction(relation.getFrom(), null, this.getCurrentUser().getCustomerId(),
                                 ActionType.RELATION_ADD_OR_UPDATE, e, relation);
            this.logEntityAction(relation.getTo(), null, this.getCurrentUser().getCustomerId(),
                                 ActionType.RELATION_ADD_OR_UPDATE, e, relation);
            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAnyAuthority('SYS_ADMIN', 'TENANT_ADMIN', 'CUSTOMER_USER')")
    @RequestMapping(value = "/relation",
                    method = RequestMethod.DELETE,
                    params = {FROM_ID, FROM_TYPE, RELATION_TYPE, TO_ID, TO_TYPE})
    @ResponseStatus(value = HttpStatus.OK)
    public void deleteRelation(@RequestParam(FROM_ID) String strFromId,
                               @RequestParam(FROM_TYPE) String strFromType,
                               @RequestParam(RELATION_TYPE) String strRelationType,
                               @RequestParam(value = "relationTypeGroup", required = false) String strRelationTypeGroup,
                               @RequestParam(TO_ID) String strToId,
                               @RequestParam(TO_TYPE) String strToType) throws ThingsboardException {
        this.checkParameter(FROM_ID, strFromId);
        this.checkParameter(FROM_TYPE, strFromType);
        this.checkParameter(RELATION_TYPE, strRelationType);
        this.checkParameter(TO_ID, strToId);
        this.checkParameter(TO_TYPE, strToType);
        EntityId fromId = EntityIdFactory.getByTypeAndId(strFromType, strFromId);
        EntityId toId = EntityIdFactory.getByTypeAndId(strToType, strToId);
        this.checkEntityId(fromId);
        this.checkEntityId(toId);
        RelationTypeGroup relationTypeGroup = this.parseRelationTypeGroup(strRelationTypeGroup,
                                                                          RelationTypeGroup.COMMON);
        EntityRelation relation = new EntityRelation(fromId, toId, strRelationType, relationTypeGroup);
        try {
            Boolean found = this.relationService.deleteRelation(this.getTenantId(), fromId, toId, strRelationType,
                                                                relationTypeGroup);
            if (!found) {
                throw new ThingsboardException("Requested item wasn't found!", ThingsboardErrorCode.ITEM_NOT_FOUND);
            }
            this.logEntityAction(relation.getFrom(), null, this.getCurrentUser().getCustomerId(),
                                 ActionType.RELATION_DELETED, null, relation);
            this.logEntityAction(relation.getTo(), null, this.getCurrentUser().getCustomerId(),
                                 ActionType.RELATION_DELETED, null, relation);
        } catch (Exception e) {
            this.logEntityAction(relation.getFrom(), null, this.getCurrentUser().getCustomerId(),
                                 ActionType.RELATION_DELETED, e, relation);
            this.logEntityAction(relation.getTo(), null, this.getCurrentUser().getCustomerId(),
                                 ActionType.RELATION_DELETED, e, relation);
            throw this.handleException(e);
        }
    }

    private RelationTypeGroup parseRelationTypeGroup(String strRelationTypeGroup, RelationTypeGroup defaultValue) {
        RelationTypeGroup result = defaultValue;
        if (strRelationTypeGroup != null && strRelationTypeGroup.trim().length() > 0) {
            try {
                result = RelationTypeGroup.valueOf(strRelationTypeGroup);
            } catch (IllegalArgumentException e) {
            }
        }
        return result;
    }

    @PreAuthorize("hasAnyAuthority('SYS_ADMIN','TENANT_ADMIN', 'CUSTOMER_USER')")
    @RequestMapping(value = "/relations", method = RequestMethod.DELETE, params = {"id", "type"})
    @ResponseStatus(value = HttpStatus.OK)
    public void deleteRelations(@RequestParam("entityId") String strId,
                                @RequestParam("entityType") String strType) throws ThingsboardException {
        this.checkParameter("entityId", strId);
        this.checkParameter("entityType", strType);
        EntityId entityId = EntityIdFactory.getByTypeAndId(strType, strId);
        this.checkEntityId(entityId);
        try {
            this.relationService.deleteEntityRelations(this.getTenantId(), entityId);
            this.logEntityAction(entityId, null, this.getCurrentUser().getCustomerId(), ActionType.RELATIONS_DELETED,
                                 null);
        } catch (Exception e) {
            this.logEntityAction(entityId, null, this.getCurrentUser().getCustomerId(), ActionType.RELATIONS_DELETED,
                                 e);
            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAnyAuthority('SYS_ADMIN', 'TENANT_ADMIN', 'CUSTOMER_USER')")
    @RequestMapping(value = "/relation",
                    method = RequestMethod.GET,
                    params = {FROM_ID, FROM_TYPE, RELATION_TYPE, TO_ID, TO_TYPE})
    @ResponseBody
    public EntityRelation getRelation(@RequestParam(FROM_ID) String strFromId,
                                      @RequestParam(FROM_TYPE) String strFromType,
                                      @RequestParam(RELATION_TYPE) String strRelationType,
                                      @RequestParam(value = "relationTypeGroup", required = false)
                                              String strRelationTypeGroup,
                                      @RequestParam(TO_ID) String strToId,
                                      @RequestParam(TO_TYPE) String strToType) throws ThingsboardException {
        try {
            this.checkParameter(FROM_ID, strFromId);
            this.checkParameter(FROM_TYPE, strFromType);
            this.checkParameter(RELATION_TYPE, strRelationType);
            this.checkParameter(TO_ID, strToId);
            this.checkParameter(TO_TYPE, strToType);
            EntityId fromId = EntityIdFactory.getByTypeAndId(strFromType, strFromId);
            EntityId toId = EntityIdFactory.getByTypeAndId(strToType, strToId);
            this.checkEntityId(fromId);
            this.checkEntityId(toId);
            RelationTypeGroup typeGroup = this.parseRelationTypeGroup(strRelationTypeGroup, RelationTypeGroup.COMMON);
            return this.checkNotNull(
                    this.relationService.getRelation(this.getTenantId(), fromId, toId, strRelationType, typeGroup));
        } catch (Exception e) {
            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAnyAuthority('SYS_ADMIN', 'TENANT_ADMIN', 'CUSTOMER_USER')")
    @RequestMapping(value = "/relations", method = RequestMethod.GET, params = {FROM_ID, FROM_TYPE})
    @ResponseBody
    public List<EntityRelation> findByFrom(@RequestParam(FROM_ID) String strFromId,
                                           @RequestParam(FROM_TYPE) String strFromType,
                                           @RequestParam(value = "relationTypeGroup", required = false)
                                                   String strRelationTypeGroup) throws ThingsboardException {
        this.checkParameter(FROM_ID, strFromId);
        this.checkParameter(FROM_TYPE, strFromType);
        EntityId entityId = EntityIdFactory.getByTypeAndId(strFromType, strFromId);
        this.checkEntityId(entityId);
        RelationTypeGroup typeGroup = this.parseRelationTypeGroup(strRelationTypeGroup, RelationTypeGroup.COMMON);
        try {
            return this.checkNotNull(this.relationService.findByFrom(this.getTenantId(), entityId, typeGroup));
        } catch (Exception e) {
            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAnyAuthority('SYS_ADMIN', 'TENANT_ADMIN', 'CUSTOMER_USER')")
    @RequestMapping(value = "/relations/info", method = RequestMethod.GET, params = {FROM_ID, FROM_TYPE})
    @ResponseBody
    public List<EntityRelationInfo> findInfoByFrom(@RequestParam(FROM_ID) String strFromId,
                                                   @RequestParam(FROM_TYPE) String strFromType,
                                                   @RequestParam(value = "relationTypeGroup", required = false)
                                                           String strRelationTypeGroup) throws ThingsboardException {
        this.checkParameter(FROM_ID, strFromId);
        this.checkParameter(FROM_TYPE, strFromType);
        EntityId entityId = EntityIdFactory.getByTypeAndId(strFromType, strFromId);
        this.checkEntityId(entityId);
        RelationTypeGroup typeGroup = this.parseRelationTypeGroup(strRelationTypeGroup, RelationTypeGroup.COMMON);
        try {
            return this.checkNotNull(
                    this.relationService.findInfoByFrom(this.getTenantId(), entityId, typeGroup).get());
        } catch (Exception e) {
            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAnyAuthority('SYS_ADMIN', 'TENANT_ADMIN', 'CUSTOMER_USER')")
    @RequestMapping(value = "/relations", method = RequestMethod.GET, params = {FROM_ID, FROM_TYPE, RELATION_TYPE})
    @ResponseBody
    public List<EntityRelation> findByFrom(@RequestParam(FROM_ID) String strFromId,
                                           @RequestParam(FROM_TYPE) String strFromType,
                                           @RequestParam(RELATION_TYPE) String strRelationType,
                                           @RequestParam(value = "relationTypeGroup", required = false)
                                                   String strRelationTypeGroup) throws ThingsboardException {
        this.checkParameter(FROM_ID, strFromId);
        this.checkParameter(FROM_TYPE, strFromType);
        this.checkParameter(RELATION_TYPE, strRelationType);
        EntityId entityId = EntityIdFactory.getByTypeAndId(strFromType, strFromId);
        this.checkEntityId(entityId);
        RelationTypeGroup typeGroup = this.parseRelationTypeGroup(strRelationTypeGroup, RelationTypeGroup.COMMON);
        try {
            return this.checkNotNull(
                    this.relationService.findByFromAndType(this.getTenantId(), entityId, strRelationType, typeGroup));
        } catch (Exception e) {
            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAnyAuthority('SYS_ADMIN', 'TENANT_ADMIN', 'CUSTOMER_USER')")
    @RequestMapping(value = "/relations", method = RequestMethod.GET, params = {TO_ID, TO_TYPE})
    @ResponseBody
    public List<EntityRelation> findByTo(@RequestParam(TO_ID) String strToId,
                                         @RequestParam(TO_TYPE) String strToType,
                                         @RequestParam(value = "relationTypeGroup", required = false)
                                                 String strRelationTypeGroup) throws ThingsboardException {
        this.checkParameter(TO_ID, strToId);
        this.checkParameter(TO_TYPE, strToType);
        EntityId entityId = EntityIdFactory.getByTypeAndId(strToType, strToId);
        this.checkEntityId(entityId);
        RelationTypeGroup typeGroup = this.parseRelationTypeGroup(strRelationTypeGroup, RelationTypeGroup.COMMON);
        try {
            return this.checkNotNull(this.relationService.findByTo(this.getTenantId(), entityId, typeGroup));
        } catch (Exception e) {
            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAnyAuthority('SYS_ADMIN', 'TENANT_ADMIN', 'CUSTOMER_USER')")
    @RequestMapping(value = "/relations/info", method = RequestMethod.GET, params = {TO_ID, TO_TYPE})
    @ResponseBody
    public List<EntityRelationInfo> findInfoByTo(@RequestParam(TO_ID) String strToId,
                                                 @RequestParam(TO_TYPE) String strToType,
                                                 @RequestParam(value = "relationTypeGroup", required = false)
                                                         String strRelationTypeGroup) throws ThingsboardException {
        this.checkParameter(TO_ID, strToId);
        this.checkParameter(TO_TYPE, strToType);
        EntityId entityId = EntityIdFactory.getByTypeAndId(strToType, strToId);
        this.checkEntityId(entityId);
        RelationTypeGroup typeGroup = this.parseRelationTypeGroup(strRelationTypeGroup, RelationTypeGroup.COMMON);
        try {
            return this.checkNotNull(this.relationService.findInfoByTo(this.getTenantId(), entityId, typeGroup).get());
        } catch (Exception e) {
            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAnyAuthority('SYS_ADMIN', 'TENANT_ADMIN', 'CUSTOMER_USER')")
    @RequestMapping(value = "/relations", method = RequestMethod.GET, params = {TO_ID, TO_TYPE, RELATION_TYPE})
    @ResponseBody
    public List<EntityRelation> findByTo(@RequestParam(TO_ID) String strToId,
                                         @RequestParam(TO_TYPE) String strToType,
                                         @RequestParam(RELATION_TYPE) String strRelationType,
                                         @RequestParam(value = "relationTypeGroup", required = false)
                                                 String strRelationTypeGroup) throws ThingsboardException {
        this.checkParameter(TO_ID, strToId);
        this.checkParameter(TO_TYPE, strToType);
        this.checkParameter(RELATION_TYPE, strRelationType);
        EntityId entityId = EntityIdFactory.getByTypeAndId(strToType, strToId);
        this.checkEntityId(entityId);
        RelationTypeGroup typeGroup = this.parseRelationTypeGroup(strRelationTypeGroup, RelationTypeGroup.COMMON);
        try {
            return this.checkNotNull(
                    this.relationService.findByToAndType(this.getTenantId(), entityId, strRelationType, typeGroup));
        } catch (Exception e) {
            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAnyAuthority('SYS_ADMIN', 'TENANT_ADMIN', 'CUSTOMER_USER')")
    @RequestMapping(value = "/relations", method = RequestMethod.POST)
    @ResponseBody
    public List<EntityRelation> findByQuery(@RequestBody EntityRelationsQuery query) throws ThingsboardException {
        this.checkNotNull(query);
        this.checkNotNull(query.getParameters());
        this.checkNotNull(query.getFilters());
        this.checkEntityId(query.getParameters().getEntityId());
        try {
            return this.checkNotNull(this.relationService.findByQuery(this.getTenantId(), query).get());
        } catch (Exception e) {
            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAnyAuthority('SYS_ADMIN', 'TENANT_ADMIN', 'CUSTOMER_USER')")
    @RequestMapping(value = "/relations/info", method = RequestMethod.POST)
    @ResponseBody
    public List<EntityRelationInfo> findInfoByQuery(@RequestBody EntityRelationsQuery query) throws
                                                                                             ThingsboardException {
        this.checkNotNull(query);
        this.checkNotNull(query.getParameters());
        this.checkNotNull(query.getFilters());
        this.checkEntityId(query.getParameters().getEntityId());
        try {
            return this.checkNotNull(this.relationService.findInfoByQuery(this.getTenantId(), query).get());
        } catch (Exception e) {
            throw this.handleException(e);
        }
    }

}
