/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.controller;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.thingsboard.server.common.data.*;
import org.thingsboard.server.common.data.audit.ActionType;
import org.thingsboard.server.common.data.exception.ThingsboardException;
import org.thingsboard.server.common.data.id.CustomerId;
import org.thingsboard.server.common.data.id.DashboardId;
import org.thingsboard.server.common.data.id.TenantId;
import org.thingsboard.server.common.data.page.TextPageData;
import org.thingsboard.server.common.data.page.TextPageLink;
import org.thingsboard.server.common.data.page.TimePageData;
import org.thingsboard.server.common.data.page.TimePageLink;

import java.util.HashSet;
import java.util.Set;


@RestController
@RequestMapping("/api")
public class DashboardController extends BaseController {

    public static final String DASHBOARD_ID = "dashboardId";

    @Value("${dashboard.max_datapoints_limit}") private long maxDatapointsLimit;


    @PreAuthorize("hasAnyAuthority('SYS_ADMIN', 'TENANT_ADMIN', 'CUSTOMER_USER')")
    @RequestMapping(value = "/dashboard/serverTime", method = RequestMethod.GET)
    @ResponseBody
    public long getServerTime() {
        return System.currentTimeMillis();
    }

    @PreAuthorize("hasAnyAuthority('SYS_ADMIN', 'TENANT_ADMIN', 'CUSTOMER_USER')")
    @RequestMapping(value = "/dashboard/maxDatapointsLimit", method = RequestMethod.GET)
    @ResponseBody
    public long getMaxDatapointsLimit() {
        return this.maxDatapointsLimit;
    }

    @PreAuthorize("hasAnyAuthority('SYS_ADMIN', 'TENANT_ADMIN', 'CUSTOMER_USER')")
    @RequestMapping(value = "/dashboard/info/{dashboardId}", method = RequestMethod.GET)
    @ResponseBody
    public DashboardInfo getDashboardInfoById(@PathVariable(DASHBOARD_ID) String strDashboardId) throws
                                                                                                 ThingsboardException {
        this.checkParameter(DASHBOARD_ID, strDashboardId);
        try {
            DashboardId dashboardId = new DashboardId(this.toUUID(strDashboardId));
            return this.checkDashboardInfoId(dashboardId);
        } catch (Exception e) {
            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAnyAuthority('TENANT_ADMIN', 'CUSTOMER_USER')")
    @RequestMapping(value = "/dashboard/{dashboardId}", method = RequestMethod.GET)
    @ResponseBody
    public Dashboard getDashboardById(@PathVariable(DASHBOARD_ID) String strDashboardId) throws ThingsboardException {
        this.checkParameter(DASHBOARD_ID, strDashboardId);
        try {
            DashboardId dashboardId = new DashboardId(this.toUUID(strDashboardId));
            return this.checkDashboardId(dashboardId);
        } catch (Exception e) {
            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAuthority('TENANT_ADMIN')")
    @RequestMapping(value = "/dashboard", method = RequestMethod.POST)
    @ResponseBody
    public Dashboard saveDashboard(@RequestBody Dashboard dashboard) throws ThingsboardException {
        try {
            dashboard.setTenantId(this.getCurrentUser().getTenantId());
            Dashboard savedDashboard = this.checkNotNull(this.dashboardService.saveDashboard(dashboard));

            this.logEntityAction(savedDashboard.getId(), savedDashboard, null,
                                 dashboard.getId() == null ? ActionType.ADDED : ActionType.UPDATED, null);

            return savedDashboard;
        } catch (Exception e) {
            this.logEntityAction(this.emptyId(EntityType.DASHBOARD), dashboard, null,
                                 dashboard.getId() == null ? ActionType.ADDED : ActionType.UPDATED, e);

            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAuthority('TENANT_ADMIN')")
    @RequestMapping(value = "/dashboard/{dashboardId}", method = RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.OK)
    public void deleteDashboard(@PathVariable(DASHBOARD_ID) String strDashboardId) throws ThingsboardException {
        this.checkParameter(DASHBOARD_ID, strDashboardId);
        try {
            DashboardId dashboardId = new DashboardId(this.toUUID(strDashboardId));
            Dashboard dashboard = this.checkDashboardId(dashboardId);
            this.dashboardService.deleteDashboard(this.getCurrentUser().getTenantId(), dashboardId);

            this.logEntityAction(dashboardId, dashboard, null, ActionType.DELETED, null, strDashboardId);

        } catch (Exception e) {

            this.logEntityAction(this.emptyId(EntityType.DASHBOARD), null, null, ActionType.DELETED, e, strDashboardId);

            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAuthority('TENANT_ADMIN')")
    @RequestMapping(value = "/customer/{customerId}/dashboard/{dashboardId}", method = RequestMethod.POST)
    @ResponseBody
    public Dashboard assignDashboardToCustomer(@PathVariable("customerId") String strCustomerId,
                                               @PathVariable(DASHBOARD_ID) String strDashboardId) throws
                                                                                                  ThingsboardException {
        this.checkParameter("customerId", strCustomerId);
        this.checkParameter(DASHBOARD_ID, strDashboardId);
        try {
            CustomerId customerId = new CustomerId(this.toUUID(strCustomerId));
            Customer customer = this.checkCustomerId(customerId);

            DashboardId dashboardId = new DashboardId(this.toUUID(strDashboardId));
            this.checkDashboardId(dashboardId);

            Dashboard savedDashboard = this.checkNotNull(
                    this.dashboardService.assignDashboardToCustomer(this.getCurrentUser().getTenantId(), dashboardId,
                                                                    customerId));

            this.logEntityAction(dashboardId, savedDashboard, customerId, ActionType.ASSIGNED_TO_CUSTOMER, null,
                                 strDashboardId, strCustomerId, customer.getName());


            return savedDashboard;
        } catch (Exception e) {

            this.logEntityAction(this.emptyId(EntityType.DASHBOARD), null, null, ActionType.ASSIGNED_TO_CUSTOMER, e,
                                 strDashboardId, strCustomerId);

            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAuthority('TENANT_ADMIN')")
    @RequestMapping(value = "/customer/{customerId}/dashboard/{dashboardId}", method = RequestMethod.DELETE)
    @ResponseBody
    public Dashboard unassignDashboardFromCustomer(@PathVariable("customerId") String strCustomerId,
                                                   @PathVariable(DASHBOARD_ID) String strDashboardId) throws
                                                                                                      ThingsboardException {
        this.checkParameter("customerId", strCustomerId);
        this.checkParameter(DASHBOARD_ID, strDashboardId);
        try {
            CustomerId customerId = new CustomerId(this.toUUID(strCustomerId));
            Customer customer = this.checkCustomerId(customerId);
            DashboardId dashboardId = new DashboardId(this.toUUID(strDashboardId));
            Dashboard dashboard = this.checkDashboardId(dashboardId);

            Dashboard savedDashboard = this.checkNotNull(
                    this.dashboardService.unassignDashboardFromCustomer(this.getCurrentUser().getTenantId(),
                                                                        dashboardId, customerId));

            this.logEntityAction(dashboardId, dashboard, customerId, ActionType.UNASSIGNED_FROM_CUSTOMER, null,
                                 strDashboardId, customer.getId().toString(), customer.getName());

            return savedDashboard;
        } catch (Exception e) {

            this.logEntityAction(this.emptyId(EntityType.DASHBOARD), null, null, ActionType.UNASSIGNED_FROM_CUSTOMER, e,
                                 strDashboardId);

            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAuthority('TENANT_ADMIN')")
    @RequestMapping(value = "/dashboard/{dashboardId}/customers", method = RequestMethod.POST)
    @ResponseBody
    public Dashboard updateDashboardCustomers(@PathVariable(DASHBOARD_ID) String strDashboardId,
                                              @RequestBody String[] strCustomerIds) throws ThingsboardException {
        this.checkParameter(DASHBOARD_ID, strDashboardId);
        try {
            DashboardId dashboardId = new DashboardId(this.toUUID(strDashboardId));
            Dashboard dashboard = this.checkDashboardId(dashboardId);

            Set<CustomerId> customerIds = new HashSet<>();
            if (strCustomerIds != null) {
                for (String strCustomerId : strCustomerIds) {
                    customerIds.add(new CustomerId(this.toUUID(strCustomerId)));
                }
            }

            Set<CustomerId> addedCustomerIds = new HashSet<>();
            Set<CustomerId> removedCustomerIds = new HashSet<>();
            for (CustomerId customerId : customerIds) {
                if (!dashboard.isAssignedToCustomer(customerId)) {
                    addedCustomerIds.add(customerId);
                }
            }

            Set<ShortCustomerInfo> assignedCustomers = dashboard.getAssignedCustomers();
            if (assignedCustomers != null) {
                for (ShortCustomerInfo customerInfo : assignedCustomers) {
                    if (!customerIds.contains(customerInfo.getCustomerId())) {
                        removedCustomerIds.add(customerInfo.getCustomerId());
                    }
                }
            }

            if (addedCustomerIds.isEmpty() && removedCustomerIds.isEmpty()) {
                return dashboard;
            }
            else {
                Dashboard savedDashboard = null;
                for (CustomerId customerId : addedCustomerIds) {
                    savedDashboard = this.checkNotNull(
                            this.dashboardService.assignDashboardToCustomer(this.getCurrentUser().getTenantId(),
                                                                            dashboardId, customerId));
                    ShortCustomerInfo customerInfo = savedDashboard.getAssignedCustomerInfo(customerId);
                    this.logEntityAction(dashboardId, savedDashboard, customerId, ActionType.ASSIGNED_TO_CUSTOMER, null,
                                         strDashboardId, customerId.toString(), customerInfo.getTitle());
                }
                for (CustomerId customerId : removedCustomerIds) {
                    ShortCustomerInfo customerInfo = dashboard.getAssignedCustomerInfo(customerId);
                    savedDashboard = this.checkNotNull(
                            this.dashboardService.unassignDashboardFromCustomer(this.getCurrentUser().getTenantId(),
                                                                                dashboardId, customerId));
                    this.logEntityAction(dashboardId, dashboard, customerId, ActionType.UNASSIGNED_FROM_CUSTOMER, null,
                                         strDashboardId, customerId.toString(), customerInfo.getTitle());

                }
                return savedDashboard;
            }
        } catch (Exception e) {

            this.logEntityAction(this.emptyId(EntityType.DASHBOARD), null, null, ActionType.ASSIGNED_TO_CUSTOMER, e,
                                 strDashboardId);

            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAuthority('TENANT_ADMIN')")
    @RequestMapping(value = "/dashboard/{dashboardId}/customers/add", method = RequestMethod.POST)
    @ResponseBody
    public Dashboard addDashboardCustomers(@PathVariable(DASHBOARD_ID) String strDashboardId,
                                           @RequestBody String[] strCustomerIds) throws ThingsboardException {
        this.checkParameter(DASHBOARD_ID, strDashboardId);
        try {
            DashboardId dashboardId = new DashboardId(this.toUUID(strDashboardId));
            Dashboard dashboard = this.checkDashboardId(dashboardId);

            Set<CustomerId> customerIds = new HashSet<>();
            if (strCustomerIds != null) {
                for (String strCustomerId : strCustomerIds) {
                    CustomerId customerId = new CustomerId(this.toUUID(strCustomerId));
                    if (!dashboard.isAssignedToCustomer(customerId)) {
                        customerIds.add(customerId);
                    }
                }
            }

            if (customerIds.isEmpty()) {
                return dashboard;
            }
            else {
                Dashboard savedDashboard = null;
                for (CustomerId customerId : customerIds) {
                    savedDashboard = this.checkNotNull(
                            this.dashboardService.assignDashboardToCustomer(this.getCurrentUser().getTenantId(),
                                                                            dashboardId, customerId));
                    ShortCustomerInfo customerInfo = savedDashboard.getAssignedCustomerInfo(customerId);
                    this.logEntityAction(dashboardId, savedDashboard, customerId, ActionType.ASSIGNED_TO_CUSTOMER, null,
                                         strDashboardId, customerId.toString(), customerInfo.getTitle());
                }
                return savedDashboard;
            }
        } catch (Exception e) {

            this.logEntityAction(this.emptyId(EntityType.DASHBOARD), null, null, ActionType.ASSIGNED_TO_CUSTOMER, e,
                                 strDashboardId);

            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAuthority('TENANT_ADMIN')")
    @RequestMapping(value = "/dashboard/{dashboardId}/customers/remove", method = RequestMethod.POST)
    @ResponseBody
    public Dashboard removeDashboardCustomers(@PathVariable(DASHBOARD_ID) String strDashboardId,
                                              @RequestBody String[] strCustomerIds) throws ThingsboardException {
        this.checkParameter(DASHBOARD_ID, strDashboardId);
        try {
            DashboardId dashboardId = new DashboardId(this.toUUID(strDashboardId));
            Dashboard dashboard = this.checkDashboardId(dashboardId);

            Set<CustomerId> customerIds = new HashSet<>();
            if (strCustomerIds != null) {
                for (String strCustomerId : strCustomerIds) {
                    CustomerId customerId = new CustomerId(this.toUUID(strCustomerId));
                    if (dashboard.isAssignedToCustomer(customerId)) {
                        customerIds.add(customerId);
                    }
                }
            }

            if (customerIds.isEmpty()) {
                return dashboard;
            }
            else {
                Dashboard savedDashboard = null;
                for (CustomerId customerId : customerIds) {
                    ShortCustomerInfo customerInfo = dashboard.getAssignedCustomerInfo(customerId);
                    savedDashboard = this.checkNotNull(
                            this.dashboardService.unassignDashboardFromCustomer(this.getCurrentUser().getTenantId(),
                                                                                dashboardId, customerId));
                    this.logEntityAction(dashboardId, dashboard, customerId, ActionType.UNASSIGNED_FROM_CUSTOMER, null,
                                         strDashboardId, customerId.toString(), customerInfo.getTitle());

                }
                return savedDashboard;
            }
        } catch (Exception e) {

            this.logEntityAction(this.emptyId(EntityType.DASHBOARD), null, null, ActionType.UNASSIGNED_FROM_CUSTOMER, e,
                                 strDashboardId);

            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAuthority('TENANT_ADMIN')")
    @RequestMapping(value = "/customer/public/dashboard/{dashboardId}", method = RequestMethod.POST)
    @ResponseBody
    public Dashboard assignDashboardToPublicCustomer(@PathVariable(DASHBOARD_ID) String strDashboardId) throws
                                                                                                        ThingsboardException {
        this.checkParameter(DASHBOARD_ID, strDashboardId);
        try {
            DashboardId dashboardId = new DashboardId(this.toUUID(strDashboardId));
            Dashboard dashboard = this.checkDashboardId(dashboardId);
            Customer publicCustomer = this.customerService.findOrCreatePublicCustomer(dashboard.getTenantId());
            Dashboard savedDashboard = this.checkNotNull(
                    this.dashboardService.assignDashboardToCustomer(this.getCurrentUser().getTenantId(), dashboardId,
                                                                    publicCustomer.getId()));

            this.logEntityAction(dashboardId, savedDashboard, publicCustomer.getId(), ActionType.ASSIGNED_TO_CUSTOMER,
                                 null, strDashboardId, publicCustomer.getId().toString(), publicCustomer.getName());

            return savedDashboard;
        } catch (Exception e) {

            this.logEntityAction(this.emptyId(EntityType.DASHBOARD), null, null, ActionType.ASSIGNED_TO_CUSTOMER, e,
                                 strDashboardId);

            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAuthority('TENANT_ADMIN')")
    @RequestMapping(value = "/customer/public/dashboard/{dashboardId}", method = RequestMethod.DELETE)
    @ResponseBody
    public Dashboard unassignDashboardFromPublicCustomer(@PathVariable(DASHBOARD_ID) String strDashboardId) throws
                                                                                                            ThingsboardException {
        this.checkParameter(DASHBOARD_ID, strDashboardId);
        try {
            DashboardId dashboardId = new DashboardId(this.toUUID(strDashboardId));
            Dashboard dashboard = this.checkDashboardId(dashboardId);
            Customer publicCustomer = this.customerService.findOrCreatePublicCustomer(dashboard.getTenantId());

            Dashboard savedDashboard = this.checkNotNull(
                    this.dashboardService.unassignDashboardFromCustomer(this.getCurrentUser().getTenantId(),
                                                                        dashboardId, publicCustomer.getId()));

            this.logEntityAction(dashboardId, dashboard, publicCustomer.getId(), ActionType.UNASSIGNED_FROM_CUSTOMER,
                                 null, strDashboardId, publicCustomer.getId().toString(), publicCustomer.getName());

            return savedDashboard;
        } catch (Exception e) {

            this.logEntityAction(this.emptyId(EntityType.DASHBOARD), null, null, ActionType.UNASSIGNED_FROM_CUSTOMER, e,
                                 strDashboardId);

            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAuthority('SYS_ADMIN')")
    @RequestMapping(value = "/tenant/{tenantId}/dashboards", params = {"limit"}, method = RequestMethod.GET)
    @ResponseBody
    public TextPageData<DashboardInfo> getTenantDashboards(@PathVariable("tenantId") String strTenantId,
                                                           @RequestParam int limit,
                                                           @RequestParam(required = false) String textSearch,
                                                           @RequestParam(required = false) String idOffset,
                                                           @RequestParam(required = false) String textOffset) throws
                                                                                                              ThingsboardException {
        try {
            TenantId tenantId = new TenantId(this.toUUID(strTenantId));
            this.checkTenantId(tenantId);
            TextPageLink pageLink = this.createPageLink(limit, textSearch, idOffset, textOffset);
            return this.checkNotNull(this.dashboardService.findDashboardsByTenantId(tenantId, pageLink));
        } catch (Exception e) {
            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAuthority('TENANT_ADMIN')")
    @RequestMapping(value = "/tenant/dashboards", params = {"limit"}, method = RequestMethod.GET)
    @ResponseBody
    public TextPageData<DashboardInfo> getTenantDashboards(@RequestParam int limit,
                                                           @RequestParam(required = false) String textSearch,
                                                           @RequestParam(required = false) String idOffset,
                                                           @RequestParam(required = false) String textOffset) throws
                                                                                                              ThingsboardException {
        try {
            TenantId tenantId = this.getCurrentUser().getTenantId();
            TextPageLink pageLink = this.createPageLink(limit, textSearch, idOffset, textOffset);
            return this.checkNotNull(this.dashboardService.findDashboardsByTenantId(tenantId, pageLink));
        } catch (Exception e) {
            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAnyAuthority('TENANT_ADMIN', 'CUSTOMER_USER')")
    @RequestMapping(value = "/customer/{customerId}/dashboards", params = {"limit"}, method = RequestMethod.GET)
    @ResponseBody
    public TimePageData<DashboardInfo> getCustomerDashboards(@PathVariable("customerId") String strCustomerId,
                                                             @RequestParam int limit,
                                                             @RequestParam(required = false) Long startTime,
                                                             @RequestParam(required = false) Long endTime,
                                                             @RequestParam(required = false, defaultValue = "false")
                                                                     boolean ascOrder,
                                                             @RequestParam(required = false) String offset) throws
                                                                                                            ThingsboardException {
        this.checkParameter("customerId", strCustomerId);
        try {
            TenantId tenantId = this.getCurrentUser().getTenantId();
            CustomerId customerId = new CustomerId(this.toUUID(strCustomerId));
            this.checkCustomerId(customerId);
            TimePageLink pageLink = this.createPageLink(limit, startTime, endTime, ascOrder, offset);
            return this.checkNotNull(
                    this.dashboardService.findDashboardsByTenantIdAndCustomerId(tenantId, customerId, pageLink).get());
        } catch (Exception e) {
            throw this.handleException(e);
        }
    }

}
