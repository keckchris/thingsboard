/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.controller;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.thingsboard.rule.engine.api.MailService;
import org.thingsboard.server.common.data.EntityType;
import org.thingsboard.server.common.data.User;
import org.thingsboard.server.common.data.audit.ActionType;
import org.thingsboard.server.common.data.exception.ThingsboardErrorCode;
import org.thingsboard.server.common.data.exception.ThingsboardException;
import org.thingsboard.server.common.data.id.CustomerId;
import org.thingsboard.server.common.data.id.TenantId;
import org.thingsboard.server.common.data.id.UserId;
import org.thingsboard.server.common.data.page.TextPageData;
import org.thingsboard.server.common.data.page.TextPageLink;
import org.thingsboard.server.common.data.security.Authority;
import org.thingsboard.server.common.data.security.UserCredentials;
import org.thingsboard.server.service.security.auth.jwt.RefreshTokenRepository;
import org.thingsboard.server.service.security.model.SecurityUser;
import org.thingsboard.server.service.security.model.UserPrincipal;
import org.thingsboard.server.service.security.model.token.JwtToken;
import org.thingsboard.server.service.security.model.token.JwtTokenFactory;

import javax.servlet.http.HttpServletRequest;


@RestController
@RequestMapping("/api")
public class UserController extends BaseController {

    public static final String USER_ID = "userId";
    public static final String YOU_DON_T_HAVE_PERMISSION_TO_PERFORM_THIS_OPERATION =
            "You don't have permission to perform this operation!";
    public static final String ACTIVATE_URL_PATTERN = "%s/api/noauth/activate?activateToken=%s";

    @Value("${security.user_token_access_enabled}")
    @Getter
    private boolean userTokenAccessEnabled;

    @Autowired private MailService mailService;

    @Autowired private JwtTokenFactory tokenFactory;

    @Autowired private RefreshTokenRepository refreshTokenRepository;


    @PreAuthorize("hasAnyAuthority('SYS_ADMIN', 'TENANT_ADMIN', 'CUSTOMER_USER')")
    @RequestMapping(value = "/user/{userId}", method = RequestMethod.GET)
    @ResponseBody
    public User getUserById(@PathVariable(USER_ID) String strUserId) throws ThingsboardException {
        this.checkParameter(USER_ID, strUserId);
        try {
            UserId userId = new UserId(this.toUUID(strUserId));
            SecurityUser authUser = this.getCurrentUser();
            if (authUser.getAuthority() == Authority.CUSTOMER_USER && !authUser.getId().equals(userId)) {
                throw new ThingsboardException(YOU_DON_T_HAVE_PERMISSION_TO_PERFORM_THIS_OPERATION,
                                               ThingsboardErrorCode.PERMISSION_DENIED);
            }
            return this.checkUserId(userId);
        } catch (Exception e) {
            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAnyAuthority('SYS_ADMIN', 'TENANT_ADMIN')")
    @RequestMapping(value = "/user/tokenAccessEnabled", method = RequestMethod.GET)
    @ResponseBody
    public boolean isUserTokenAccessEnabled() {
        return this.userTokenAccessEnabled;
    }

    @PreAuthorize("hasAnyAuthority('SYS_ADMIN', 'TENANT_ADMIN')")
    @RequestMapping(value = "/user/{userId}/token", method = RequestMethod.GET)
    @ResponseBody
    public JsonNode getUserToken(@PathVariable(USER_ID) String strUserId) throws ThingsboardException {
        this.checkParameter(USER_ID, strUserId);
        try {
            UserId userId = new UserId(this.toUUID(strUserId));
            SecurityUser authUser = this.getCurrentUser();
            User user = this.userService.findUserById(authUser.getTenantId(), userId);
            if (!this.userTokenAccessEnabled ||
                (authUser.getAuthority() == Authority.SYS_ADMIN && user.getAuthority() != Authority.TENANT_ADMIN) ||
                (authUser.getAuthority() == Authority.TENANT_ADMIN && !authUser.getTenantId().equals(
                        user.getTenantId()))) {
                throw new ThingsboardException(YOU_DON_T_HAVE_PERMISSION_TO_PERFORM_THIS_OPERATION,
                                               ThingsboardErrorCode.PERMISSION_DENIED);
            }
            UserPrincipal principal = new UserPrincipal(UserPrincipal.Type.USER_NAME, user.getEmail());
            UserCredentials credentials = this.userService.findUserCredentialsByUserId(authUser.getTenantId(), userId);
            SecurityUser securityUser = new SecurityUser(user, credentials.isEnabled(), principal);
            JwtToken accessToken = this.tokenFactory.createAccessJwtToken(securityUser);
            JwtToken refreshToken = this.refreshTokenRepository.requestRefreshToken(securityUser);
            ObjectMapper objectMapper = new ObjectMapper();
            ObjectNode tokenObject = objectMapper.createObjectNode();
            tokenObject.put("token", accessToken.getToken());
            tokenObject.put("refreshToken", refreshToken.getToken());
            return tokenObject;
        } catch (Exception e) {
            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAnyAuthority('SYS_ADMIN', 'TENANT_ADMIN', 'CUSTOMER_USER')")
    @RequestMapping(value = "/user", method = RequestMethod.POST)
    @ResponseBody
    public User saveUser(@RequestBody User user,
                         @RequestParam(required = false, defaultValue = "true") boolean sendActivationMail,
                         HttpServletRequest request) throws ThingsboardException {
        try {
            SecurityUser authUser = this.getCurrentUser();
            if (authUser.getAuthority() == Authority.CUSTOMER_USER && !authUser.getId().equals(user.getId())) {
                throw new ThingsboardException(YOU_DON_T_HAVE_PERMISSION_TO_PERFORM_THIS_OPERATION,
                                               ThingsboardErrorCode.PERMISSION_DENIED);
            }
            boolean sendEmail = user.getId() == null && sendActivationMail;
            if (this.getCurrentUser().getAuthority() == Authority.TENANT_ADMIN) {
                user.setTenantId(this.getCurrentUser().getTenantId());
            }
            User savedUser = this.checkNotNull(this.userService.saveUser(user));
            if (sendEmail) {
                UserCredentials userCredentials = this.userService.findUserCredentialsByUserId(authUser.getTenantId(),
                                                                                               savedUser.getId());
                String baseUrl = this.constructBaseUrl(request);
                String activateUrl = String.format(ACTIVATE_URL_PATTERN, baseUrl, userCredentials.getActivateToken());
                String email = savedUser.getEmail();
                try {
                    this.mailService.sendActivationEmail(activateUrl, email);
                } catch (ThingsboardException e) {
                    this.userService.deleteUser(authUser.getTenantId(), savedUser.getId());
                    throw e;
                }
            }

            this.logEntityAction(savedUser.getId(), savedUser, savedUser.getCustomerId(),
                                 user.getId() == null ? ActionType.ADDED : ActionType.UPDATED, null);

            return savedUser;
        } catch (Exception e) {

            this.logEntityAction(this.emptyId(EntityType.USER), user, null,
                                 user.getId() == null ? ActionType.ADDED : ActionType.UPDATED, e);

            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAnyAuthority('SYS_ADMIN', 'TENANT_ADMIN')")
    @RequestMapping(value = "/user/sendActivationMail", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public void sendActivationEmail(@RequestParam(value = "email") String email, HttpServletRequest request) throws
                                                                                                             ThingsboardException {
        try {
            User user = this.checkNotNull(this.userService.findUserByEmail(this.getCurrentUser().getTenantId(), email));
            UserCredentials userCredentials = this.userService.findUserCredentialsByUserId(
                    this.getCurrentUser().getTenantId(), user.getId());
            if (!userCredentials.isEnabled()) {
                String baseUrl = this.constructBaseUrl(request);
                String activateUrl = String.format(ACTIVATE_URL_PATTERN, baseUrl, userCredentials.getActivateToken());
                this.mailService.sendActivationEmail(activateUrl, email);
            }
            else {
                throw new ThingsboardException("User is already active!", ThingsboardErrorCode.BAD_REQUEST_PARAMS);
            }
        } catch (Exception e) {
            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAnyAuthority('SYS_ADMIN', 'TENANT_ADMIN')")
    @RequestMapping(value = "/user/{userId}/activationLink", method = RequestMethod.GET, produces = "text/plain")
    @ResponseBody
    public String getActivationLink(@PathVariable(USER_ID) String strUserId, HttpServletRequest request) throws
                                                                                                         ThingsboardException {
        this.checkParameter(USER_ID, strUserId);
        try {
            UserId userId = new UserId(this.toUUID(strUserId));
            SecurityUser authUser = this.getCurrentUser();
            if (authUser.getAuthority() == Authority.CUSTOMER_USER && !authUser.getId().equals(userId)) {
                throw new ThingsboardException(YOU_DON_T_HAVE_PERMISSION_TO_PERFORM_THIS_OPERATION,
                                               ThingsboardErrorCode.PERMISSION_DENIED);
            }
            User user = this.checkUserId(userId);
            UserCredentials userCredentials = this.userService.findUserCredentialsByUserId(
                    this.getCurrentUser().getTenantId(), user.getId());
            if (!userCredentials.isEnabled()) {
                String baseUrl = this.constructBaseUrl(request);
                String activateUrl = String.format(ACTIVATE_URL_PATTERN, baseUrl, userCredentials.getActivateToken());
                return activateUrl;
            }
            else {
                throw new ThingsboardException("User is already active!", ThingsboardErrorCode.BAD_REQUEST_PARAMS);
            }
        } catch (Exception e) {
            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAnyAuthority('SYS_ADMIN', 'TENANT_ADMIN')")
    @RequestMapping(value = "/user/{userId}", method = RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.OK)
    public void deleteUser(@PathVariable(USER_ID) String strUserId) throws ThingsboardException {
        this.checkParameter(USER_ID, strUserId);
        try {
            UserId userId = new UserId(this.toUUID(strUserId));
            User user = this.checkUserId(userId);
            this.userService.deleteUser(this.getCurrentUser().getTenantId(), userId);

            this.logEntityAction(userId, user, user.getCustomerId(), ActionType.DELETED, null, strUserId);

        } catch (Exception e) {
            this.logEntityAction(this.emptyId(EntityType.USER), null, null, ActionType.DELETED, e, strUserId);
            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAuthority('SYS_ADMIN')")
    @RequestMapping(value = "/tenant/{tenantId}/users", params = {"limit"}, method = RequestMethod.GET)
    @ResponseBody
    public TextPageData<User> getTenantAdmins(@PathVariable("tenantId") String strTenantId,
                                              @RequestParam int limit,
                                              @RequestParam(required = false) String textSearch,
                                              @RequestParam(required = false) String idOffset,
                                              @RequestParam(required = false) String textOffset) throws
                                                                                                 ThingsboardException {
        this.checkParameter("tenantId", strTenantId);
        try {
            TenantId tenantId = new TenantId(this.toUUID(strTenantId));
            TextPageLink pageLink = this.createPageLink(limit, textSearch, idOffset, textOffset);
            return this.checkNotNull(this.userService.findTenantAdmins(tenantId, pageLink));
        } catch (Exception e) {
            throw this.handleException(e);
        }
    }

    @PreAuthorize("hasAuthority('TENANT_ADMIN')")
    @RequestMapping(value = "/customer/{customerId}/users", params = {"limit"}, method = RequestMethod.GET)
    @ResponseBody
    public TextPageData<User> getCustomerUsers(@PathVariable("customerId") String strCustomerId,
                                               @RequestParam int limit,
                                               @RequestParam(required = false) String textSearch,
                                               @RequestParam(required = false) String idOffset,
                                               @RequestParam(required = false) String textOffset) throws
                                                                                                  ThingsboardException {
        this.checkParameter("customerId", strCustomerId);
        try {
            CustomerId customerId = new CustomerId(this.toUUID(strCustomerId));
            this.checkCustomerId(customerId);
            TextPageLink pageLink = this.createPageLink(limit, textSearch, idOffset, textOffset);
            TenantId tenantId = this.getCurrentUser().getTenantId();
            return this.checkNotNull(this.userService.findCustomerUsers(tenantId, customerId, pageLink));
        } catch (Exception e) {
            throw this.handleException(e);
        }
    }

}
