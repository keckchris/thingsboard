/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.controller;


import com.fasterxml.jackson.core.type.TypeReference;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.thingsboard.server.common.data.Tenant;
import org.thingsboard.server.common.data.User;
import org.thingsboard.server.common.data.page.TextPageData;
import org.thingsboard.server.common.data.page.TextPageLink;
import org.thingsboard.server.common.data.security.Authority;
import org.thingsboard.server.common.data.widget.WidgetsBundle;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public abstract class BaseWidgetsBundleControllerTest extends AbstractControllerTest {

    private IdComparator<WidgetsBundle> idComparator = new IdComparator<>();

    private Tenant savedTenant;
    private User tenantAdmin;

    @Before
    public void beforeTest() throws Exception {
        this.loginSysAdmin();

        Tenant tenant = new Tenant();
        tenant.setTitle("My tenant");
        this.savedTenant = this.doPost("/api/tenant", tenant, Tenant.class);
        Assert.assertNotNull(this.savedTenant);

        this.tenantAdmin = new User();
        this.tenantAdmin.setAuthority(Authority.TENANT_ADMIN);
        this.tenantAdmin.setTenantId(this.savedTenant.getId());
        this.tenantAdmin.setEmail("tenant2@thingsboard.org");
        this.tenantAdmin.setFirstName("Joe");
        this.tenantAdmin.setLastName("Downs");

        this.tenantAdmin = this.createUserAndLogin(this.tenantAdmin, "testPassword1");
    }

    @After
    public void afterTest() throws Exception {
        this.loginSysAdmin();

        this.doDelete("/api/tenant/" + this.savedTenant.getId().getId().toString()).andExpect(status().isOk());
    }

    @Test
    public void testSaveWidgetsBundle() throws Exception {
        WidgetsBundle widgetsBundle = new WidgetsBundle();
        widgetsBundle.setTitle("My widgets bundle");
        WidgetsBundle savedWidgetsBundle = this.doPost("/api/widgetsBundle", widgetsBundle, WidgetsBundle.class);

        Assert.assertNotNull(savedWidgetsBundle);
        Assert.assertNotNull(savedWidgetsBundle.getId());
        Assert.assertNotNull(savedWidgetsBundle.getAlias());
        Assert.assertTrue(savedWidgetsBundle.getCreatedTime() > 0);
        Assert.assertEquals(this.savedTenant.getId(), savedWidgetsBundle.getTenantId());
        Assert.assertEquals(widgetsBundle.getTitle(), savedWidgetsBundle.getTitle());

        savedWidgetsBundle.setTitle("My new widgets bundle");
        this.doPost("/api/widgetsBundle", savedWidgetsBundle, WidgetsBundle.class);

        WidgetsBundle foundWidgetsBundle = this.doGet(
                "/api/widgetsBundle/" + savedWidgetsBundle.getId().getId().toString(), WidgetsBundle.class);
        Assert.assertEquals(foundWidgetsBundle.getTitle(), savedWidgetsBundle.getTitle());
    }

    @Test
    public void testFindWidgetsBundleById() throws Exception {
        WidgetsBundle widgetsBundle = new WidgetsBundle();
        widgetsBundle.setTitle("My widgets bundle");
        WidgetsBundle savedWidgetsBundle = this.doPost("/api/widgetsBundle", widgetsBundle, WidgetsBundle.class);
        WidgetsBundle foundWidgetsBundle = this.doGet(
                "/api/widgetsBundle/" + savedWidgetsBundle.getId().getId().toString(), WidgetsBundle.class);
        Assert.assertNotNull(foundWidgetsBundle);
        Assert.assertEquals(savedWidgetsBundle, foundWidgetsBundle);
    }

    @Test
    public void testDeleteWidgetsBundle() throws Exception {
        WidgetsBundle widgetsBundle = new WidgetsBundle();
        widgetsBundle.setTitle("My widgets bundle");
        WidgetsBundle savedWidgetsBundle = this.doPost("/api/widgetsBundle", widgetsBundle, WidgetsBundle.class);

        this.doDelete("/api/widgetsBundle/" + savedWidgetsBundle.getId().getId().toString()).andExpect(status().isOk());

        this.doGet("/api/widgetsBundle/" + savedWidgetsBundle.getId().getId().toString()).andExpect(
                status().isNotFound());
    }

    @Test
    public void testSaveWidgetsBundleWithEmptyTitle() throws Exception {
        WidgetsBundle widgetsBundle = new WidgetsBundle();
        this.doPost("/api/widgetsBundle", widgetsBundle).andExpect(status().isBadRequest()).andExpect(
                statusReason(containsString("Widgets bundle title should be specified")));
    }

    @Test
    public void testUpdateWidgetsBundleAlias() throws Exception {
        WidgetsBundle widgetsBundle = new WidgetsBundle();
        widgetsBundle.setTitle("My widgets bundle");
        WidgetsBundle savedWidgetsBundle = this.doPost("/api/widgetsBundle", widgetsBundle, WidgetsBundle.class);
        savedWidgetsBundle.setAlias("new_alias");
        this.doPost("/api/widgetsBundle", savedWidgetsBundle).andExpect(status().isBadRequest()).andExpect(
                statusReason(containsString("Update of widgets bundle alias is prohibited")));

    }

    @Test
    public void testFindTenantWidgetsBundlesByPageLink() throws Exception {

        this.login(this.tenantAdmin.getEmail(), "testPassword1");

        List<WidgetsBundle> sysWidgetsBundles = this.doGetTyped("/api/widgetsBundles?",
                                                                new TypeReference<List<WidgetsBundle>>() {});


        List<WidgetsBundle> widgetsBundles = new ArrayList<>();
        for (int i = 0; i < 73; i++) {
            WidgetsBundle widgetsBundle = new WidgetsBundle();
            widgetsBundle.setTitle("Widgets bundle" + i);
            widgetsBundles.add(this.doPost("/api/widgetsBundle", widgetsBundle, WidgetsBundle.class));
        }

        widgetsBundles.addAll(sysWidgetsBundles);

        List<WidgetsBundle> loadedWidgetsBundles = new ArrayList<>();
        TextPageLink pageLink = new TextPageLink(14);
        TextPageData<WidgetsBundle> pageData;
        do {
            pageData = this.doGetTypedWithPageLink("/api/widgetsBundles?",
                                                   new TypeReference<TextPageData<WidgetsBundle>>() {}, pageLink);
            loadedWidgetsBundles.addAll(pageData.getData());
            if (pageData.hasNext()) {
                pageLink = pageData.getNextPageLink();
            }
        } while (pageData.hasNext());

        Collections.sort(widgetsBundles, this.idComparator);
        Collections.sort(loadedWidgetsBundles, this.idComparator);

        Assert.assertEquals(widgetsBundles, loadedWidgetsBundles);
    }

    @Test
    public void testFindSystemWidgetsBundlesByPageLink() throws Exception {

        this.loginSysAdmin();

        List<WidgetsBundle> sysWidgetsBundles = this.doGetTyped("/api/widgetsBundles?",
                                                                new TypeReference<List<WidgetsBundle>>() {});

        List<WidgetsBundle> createdWidgetsBundles = new ArrayList<>();
        for (int i = 0; i < 120; i++) {
            WidgetsBundle widgetsBundle = new WidgetsBundle();
            widgetsBundle.setTitle("Widgets bundle" + i);
            createdWidgetsBundles.add(this.doPost("/api/widgetsBundle", widgetsBundle, WidgetsBundle.class));
        }

        List<WidgetsBundle> widgetsBundles = new ArrayList<>(createdWidgetsBundles);
        widgetsBundles.addAll(sysWidgetsBundles);

        List<WidgetsBundle> loadedWidgetsBundles = new ArrayList<>();
        TextPageLink pageLink = new TextPageLink(14);
        TextPageData<WidgetsBundle> pageData;
        do {
            pageData = this.doGetTypedWithPageLink("/api/widgetsBundles?",
                                                   new TypeReference<TextPageData<WidgetsBundle>>() {}, pageLink);
            loadedWidgetsBundles.addAll(pageData.getData());
            if (pageData.hasNext()) {
                pageLink = pageData.getNextPageLink();
            }
        } while (pageData.hasNext());

        Collections.sort(widgetsBundles, this.idComparator);
        Collections.sort(loadedWidgetsBundles, this.idComparator);

        Assert.assertEquals(widgetsBundles, loadedWidgetsBundles);

        for (WidgetsBundle widgetsBundle : createdWidgetsBundles) {
            this.doDelete("/api/widgetsBundle/" + widgetsBundle.getId().getId().toString()).andExpect(status().isOk());
        }

        pageLink = new TextPageLink(17);
        loadedWidgetsBundles.clear();
        do {
            pageData = this.doGetTypedWithPageLink("/api/widgetsBundles?",
                                                   new TypeReference<TextPageData<WidgetsBundle>>() {}, pageLink);
            loadedWidgetsBundles.addAll(pageData.getData());
            if (pageData.hasNext()) {
                pageLink = pageData.getNextPageLink();
            }
        } while (pageData.hasNext());

        Collections.sort(sysWidgetsBundles, this.idComparator);
        Collections.sort(loadedWidgetsBundles, this.idComparator);

        Assert.assertEquals(sysWidgetsBundles, loadedWidgetsBundles);
    }


    @Test
    public void testFindTenantWidgetsBundles() throws Exception {

        this.login(this.tenantAdmin.getEmail(), "testPassword1");

        List<WidgetsBundle> sysWidgetsBundles = this.doGetTyped("/api/widgetsBundles?",
                                                                new TypeReference<List<WidgetsBundle>>() {});

        List<WidgetsBundle> widgetsBundles = new ArrayList<>();
        for (int i = 0; i < 73; i++) {
            WidgetsBundle widgetsBundle = new WidgetsBundle();
            widgetsBundle.setTitle("Widgets bundle" + i);
            widgetsBundles.add(this.doPost("/api/widgetsBundle", widgetsBundle, WidgetsBundle.class));
        }

        widgetsBundles.addAll(sysWidgetsBundles);

        List<WidgetsBundle> loadedWidgetsBundles = this.doGetTyped("/api/widgetsBundles?",
                                                                   new TypeReference<List<WidgetsBundle>>() {});

        Collections.sort(widgetsBundles, this.idComparator);
        Collections.sort(loadedWidgetsBundles, this.idComparator);

        Assert.assertEquals(widgetsBundles, loadedWidgetsBundles);
    }

    @Test
    public void testFindSystemAndTenantWidgetsBundles() throws Exception {

        this.loginSysAdmin();


        List<WidgetsBundle> sysWidgetsBundles = this.doGetTyped("/api/widgetsBundles?",
                                                                new TypeReference<List<WidgetsBundle>>() {});

        List<WidgetsBundle> createdSystemWidgetsBundles = new ArrayList<>();
        for (int i = 0; i < 82; i++) {
            WidgetsBundle widgetsBundle = new WidgetsBundle();
            widgetsBundle.setTitle("Sys widgets bundle" + i);
            createdSystemWidgetsBundles.add(this.doPost("/api/widgetsBundle", widgetsBundle, WidgetsBundle.class));
        }

        List<WidgetsBundle> systemWidgetsBundles = new ArrayList<>(createdSystemWidgetsBundles);
        systemWidgetsBundles.addAll(sysWidgetsBundles);

        List<WidgetsBundle> widgetsBundles = new ArrayList<>();
        widgetsBundles.addAll(systemWidgetsBundles);

        this.login(this.tenantAdmin.getEmail(), "testPassword1");

        for (int i = 0; i < 127; i++) {
            WidgetsBundle widgetsBundle = new WidgetsBundle();
            widgetsBundle.setTitle("Tenant widgets bundle" + i);
            widgetsBundles.add(this.doPost("/api/widgetsBundle", widgetsBundle, WidgetsBundle.class));
        }

        List<WidgetsBundle> loadedWidgetsBundles = this.doGetTyped("/api/widgetsBundles?",
                                                                   new TypeReference<List<WidgetsBundle>>() {});

        Collections.sort(widgetsBundles, this.idComparator);
        Collections.sort(loadedWidgetsBundles, this.idComparator);

        Assert.assertEquals(widgetsBundles, loadedWidgetsBundles);

        this.loginSysAdmin();

        loadedWidgetsBundles = this.doGetTyped("/api/widgetsBundles?", new TypeReference<List<WidgetsBundle>>() {});

        Collections.sort(systemWidgetsBundles, this.idComparator);
        Collections.sort(loadedWidgetsBundles, this.idComparator);

        Assert.assertEquals(systemWidgetsBundles, loadedWidgetsBundles);

        for (WidgetsBundle widgetsBundle : createdSystemWidgetsBundles) {
            this.doDelete("/api/widgetsBundle/" + widgetsBundle.getId().getId().toString()).andExpect(status().isOk());
        }

        loadedWidgetsBundles = this.doGetTyped("/api/widgetsBundles?", new TypeReference<List<WidgetsBundle>>() {});

        Collections.sort(sysWidgetsBundles, this.idComparator);
        Collections.sort(loadedWidgetsBundles, this.idComparator);

        Assert.assertEquals(sysWidgetsBundles, loadedWidgetsBundles);
    }

}
