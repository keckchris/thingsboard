/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.controller;


import org.junit.Test;
import org.thingsboard.server.common.data.security.Authority;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public abstract class BaseAuthControllerTest extends AbstractControllerTest {

    @Test
    public void testGetUser() throws Exception {

        this.doGet("/api/auth/user").andExpect(status().isUnauthorized());

        this.loginSysAdmin();
        this.doGet("/api/auth/user").andExpect(status().isOk()).andExpect(
                jsonPath("$.authority", is(Authority.SYS_ADMIN.name()))).andExpect(
                jsonPath("$.email", is(SYS_ADMIN_EMAIL)));

        this.loginTenantAdmin();
        this.doGet("/api/auth/user").andExpect(status().isOk()).andExpect(
                jsonPath("$.authority", is(Authority.TENANT_ADMIN.name()))).andExpect(
                jsonPath("$.email", is(TENANT_ADMIN_EMAIL)));

        this.loginCustomerUser();
        this.doGet("/api/auth/user").andExpect(status().isOk()).andExpect(
                jsonPath("$.authority", is(Authority.CUSTOMER_USER.name()))).andExpect(
                jsonPath("$.email", is(CUSTOMER_USER_EMAIL)));
    }

    @Test
    public void testLoginLogout() throws Exception {
        this.loginSysAdmin();
        this.doGet("/api/auth/user").andExpect(status().isOk()).andExpect(
                jsonPath("$.authority", is(Authority.SYS_ADMIN.name()))).andExpect(
                jsonPath("$.email", is(SYS_ADMIN_EMAIL)));

        this.logout();
        this.doGet("/api/auth/user").andExpect(status().isUnauthorized());
    }

    @Test
    public void testRefreshToken() throws Exception {
        this.loginSysAdmin();
        this.doGet("/api/auth/user").andExpect(status().isOk()).andExpect(
                jsonPath("$.authority", is(Authority.SYS_ADMIN.name()))).andExpect(
                jsonPath("$.email", is(SYS_ADMIN_EMAIL)));

        this.refreshToken();
        this.doGet("/api/auth/user").andExpect(status().isOk()).andExpect(
                jsonPath("$.authority", is(Authority.SYS_ADMIN.name()))).andExpect(
                jsonPath("$.email", is(SYS_ADMIN_EMAIL)));
    }

}
