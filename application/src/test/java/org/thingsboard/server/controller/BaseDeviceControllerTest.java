/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.controller;


import com.datastax.driver.core.utils.UUIDs;
import com.fasterxml.jackson.core.type.TypeReference;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.thingsboard.server.common.data.*;
import org.thingsboard.server.common.data.id.CustomerId;
import org.thingsboard.server.common.data.id.DeviceCredentialsId;
import org.thingsboard.server.common.data.id.DeviceId;
import org.thingsboard.server.common.data.page.TextPageData;
import org.thingsboard.server.common.data.page.TextPageLink;
import org.thingsboard.server.common.data.security.Authority;
import org.thingsboard.server.common.data.security.DeviceCredentials;
import org.thingsboard.server.common.data.security.DeviceCredentialsType;
import org.thingsboard.server.dao.model.ModelConstants;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.thingsboard.server.dao.model.ModelConstants.NULL_UUID;


public abstract class BaseDeviceControllerTest extends AbstractControllerTest {

    private IdComparator<Device> idComparator = new IdComparator<>();

    private Tenant savedTenant;
    private User tenantAdmin;

    @Before
    public void beforeTest() throws Exception {
        this.loginSysAdmin();

        Tenant tenant = new Tenant();
        tenant.setTitle("My tenant");
        this.savedTenant = this.doPost("/api/tenant", tenant, Tenant.class);
        Assert.assertNotNull(this.savedTenant);

        this.tenantAdmin = new User();
        this.tenantAdmin.setAuthority(Authority.TENANT_ADMIN);
        this.tenantAdmin.setTenantId(this.savedTenant.getId());
        this.tenantAdmin.setEmail("tenant2@thingsboard.org");
        this.tenantAdmin.setFirstName("Joe");
        this.tenantAdmin.setLastName("Downs");

        this.tenantAdmin = this.createUserAndLogin(this.tenantAdmin, "testPassword1");
    }

    @After
    public void afterTest() throws Exception {
        this.loginSysAdmin();

        this.doDelete("/api/tenant/" + this.savedTenant.getId().getId().toString()).andExpect(status().isOk());
    }

    @Test
    public void testSaveDevice() throws Exception {
        Device device = new Device();
        device.setName("My device");
        device.setType("default");
        Device savedDevice = this.doPost("/api/device", device, Device.class);

        Assert.assertNotNull(savedDevice);
        Assert.assertNotNull(savedDevice.getId());
        Assert.assertTrue(savedDevice.getCreatedTime() > 0);
        Assert.assertEquals(this.savedTenant.getId(), savedDevice.getTenantId());
        Assert.assertNotNull(savedDevice.getCustomerId());
        Assert.assertEquals(NULL_UUID, savedDevice.getCustomerId().getId());
        Assert.assertEquals(device.getName(), savedDevice.getName());

        DeviceCredentials deviceCredentials = this.doGet(
                "/api/device/" + savedDevice.getId().getId().toString() + "/credentials", DeviceCredentials.class);

        Assert.assertNotNull(deviceCredentials);
        Assert.assertNotNull(deviceCredentials.getId());
        Assert.assertEquals(savedDevice.getId(), deviceCredentials.getDeviceId());
        Assert.assertEquals(DeviceCredentialsType.ACCESS_TOKEN, deviceCredentials.getCredentialsType());
        Assert.assertNotNull(deviceCredentials.getCredentialsId());
        Assert.assertEquals(20, deviceCredentials.getCredentialsId().length());

        savedDevice.setName("My new device");
        this.doPost("/api/device", savedDevice, Device.class);

        Device foundDevice = this.doGet("/api/device/" + savedDevice.getId().getId().toString(), Device.class);
        Assert.assertEquals(foundDevice.getName(), savedDevice.getName());
    }

    @Test
    public void testFindDeviceById() throws Exception {
        Device device = new Device();
        device.setName("My device");
        device.setType("default");
        Device savedDevice = this.doPost("/api/device", device, Device.class);
        Device foundDevice = this.doGet("/api/device/" + savedDevice.getId().getId().toString(), Device.class);
        Assert.assertNotNull(foundDevice);
        Assert.assertEquals(savedDevice, foundDevice);
    }

    @Test
    public void testFindDeviceTypesByTenantId() throws Exception {
        List<Device> devices = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            Device device = new Device();
            device.setName("My device B" + i);
            device.setType("typeB");
            devices.add(this.doPost("/api/device", device, Device.class));
        }
        for (int i = 0; i < 7; i++) {
            Device device = new Device();
            device.setName("My device C" + i);
            device.setType("typeC");
            devices.add(this.doPost("/api/device", device, Device.class));
        }
        for (int i = 0; i < 9; i++) {
            Device device = new Device();
            device.setName("My device A" + i);
            device.setType("typeA");
            devices.add(this.doPost("/api/device", device, Device.class));
        }
        List<EntitySubtype> deviceTypes = this.doGetTyped("/api/device/types",
                                                          new TypeReference<List<EntitySubtype>>() {});

        Assert.assertNotNull(deviceTypes);
        Assert.assertEquals(3, deviceTypes.size());
        Assert.assertEquals("typeA", deviceTypes.get(0).getType());
        Assert.assertEquals("typeB", deviceTypes.get(1).getType());
        Assert.assertEquals("typeC", deviceTypes.get(2).getType());
    }

    @Test
    public void testDeleteDevice() throws Exception {
        Device device = new Device();
        device.setName("My device");
        device.setType("default");
        Device savedDevice = this.doPost("/api/device", device, Device.class);

        this.doDelete("/api/device/" + savedDevice.getId().getId().toString()).andExpect(status().isOk());

        this.doGet("/api/device/" + savedDevice.getId().getId().toString()).andExpect(status().isNotFound());
    }

    @Test
    public void testSaveDeviceWithEmptyType() throws Exception {
        Device device = new Device();
        device.setName("My device");
        this.doPost("/api/device", device).andExpect(status().isBadRequest()).andExpect(
                statusReason(containsString("Device type should be specified")));
    }

    @Test
    public void testSaveDeviceWithEmptyName() throws Exception {
        Device device = new Device();
        device.setType("default");
        this.doPost("/api/device", device).andExpect(status().isBadRequest()).andExpect(
                statusReason(containsString("Device name should be specified")));
    }

    @Test
    public void testAssignUnassignDeviceToCustomer() throws Exception {
        Device device = new Device();
        device.setName("My device");
        device.setType("default");
        Device savedDevice = this.doPost("/api/device", device, Device.class);

        Customer customer = new Customer();
        customer.setTitle("My customer");
        Customer savedCustomer = this.doPost("/api/customer", customer, Customer.class);

        Device assignedDevice = this.doPost("/api/customer/" + savedCustomer.getId().getId().toString() + "/device/" +
                                            savedDevice.getId().getId().toString(), Device.class);
        Assert.assertEquals(savedCustomer.getId(), assignedDevice.getCustomerId());

        Device foundDevice = this.doGet("/api/device/" + savedDevice.getId().getId().toString(), Device.class);
        Assert.assertEquals(savedCustomer.getId(), foundDevice.getCustomerId());

        Device unassignedDevice = this.doDelete("/api/customer/device/" + savedDevice.getId().getId().toString(),
                                                Device.class);
        Assert.assertEquals(ModelConstants.NULL_UUID, unassignedDevice.getCustomerId().getId());

        foundDevice = this.doGet("/api/device/" + savedDevice.getId().getId().toString(), Device.class);
        Assert.assertEquals(ModelConstants.NULL_UUID, foundDevice.getCustomerId().getId());
    }

    @Test
    public void testAssignDeviceToNonExistentCustomer() throws Exception {
        Device device = new Device();
        device.setName("My device");
        device.setType("default");
        Device savedDevice = this.doPost("/api/device", device, Device.class);

        this.doPost(
                "/api/customer/" + UUIDs.timeBased().toString() + "/device/" + savedDevice.getId().getId().toString())
            .andExpect(status().isNotFound());
    }

    @Test
    public void testAssignDeviceToCustomerFromDifferentTenant() throws Exception {
        this.loginSysAdmin();

        Tenant tenant2 = new Tenant();
        tenant2.setTitle("Different tenant");
        Tenant savedTenant2 = this.doPost("/api/tenant", tenant2, Tenant.class);
        Assert.assertNotNull(savedTenant2);

        User tenantAdmin2 = new User();
        tenantAdmin2.setAuthority(Authority.TENANT_ADMIN);
        tenantAdmin2.setTenantId(savedTenant2.getId());
        tenantAdmin2.setEmail("tenant3@thingsboard.org");
        tenantAdmin2.setFirstName("Joe");
        tenantAdmin2.setLastName("Downs");

        tenantAdmin2 = this.createUserAndLogin(tenantAdmin2, "testPassword1");

        Customer customer = new Customer();
        customer.setTitle("Different customer");
        Customer savedCustomer = this.doPost("/api/customer", customer, Customer.class);

        this.login(this.tenantAdmin.getEmail(), "testPassword1");

        Device device = new Device();
        device.setName("My device");
        device.setType("default");
        Device savedDevice = this.doPost("/api/device", device, Device.class);

        this.doPost("/api/customer/" + savedCustomer.getId().getId().toString() + "/device/" +
                    savedDevice.getId().getId().toString()).andExpect(status().isForbidden());

        this.loginSysAdmin();

        this.doDelete("/api/tenant/" + savedTenant2.getId().getId().toString()).andExpect(status().isOk());
    }

    @Test
    public void testFindDeviceCredentialsByDeviceId() throws Exception {
        Device device = new Device();
        device.setName("My device");
        device.setType("default");
        Device savedDevice = this.doPost("/api/device", device, Device.class);
        DeviceCredentials deviceCredentials = this.doGet(
                "/api/device/" + savedDevice.getId().getId().toString() + "/credentials", DeviceCredentials.class);
        Assert.assertEquals(savedDevice.getId(), deviceCredentials.getDeviceId());
    }

    @Test
    public void testSaveDeviceCredentials() throws Exception {
        Device device = new Device();
        device.setName("My device");
        device.setType("default");
        Device savedDevice = this.doPost("/api/device", device, Device.class);
        DeviceCredentials deviceCredentials = this.doGet(
                "/api/device/" + savedDevice.getId().getId().toString() + "/credentials", DeviceCredentials.class);
        Assert.assertEquals(savedDevice.getId(), deviceCredentials.getDeviceId());
        deviceCredentials.setCredentialsType(DeviceCredentialsType.ACCESS_TOKEN);
        deviceCredentials.setCredentialsId("access_token");
        this.doPost("/api/device/credentials", deviceCredentials).andExpect(status().isOk());

        DeviceCredentials foundDeviceCredentials = this.doGet(
                "/api/device/" + savedDevice.getId().getId().toString() + "/credentials", DeviceCredentials.class);

        Assert.assertEquals(deviceCredentials, foundDeviceCredentials);
    }

    @Test
    public void testSaveDeviceCredentialsWithEmptyDevice() throws Exception {
        DeviceCredentials deviceCredentials = new DeviceCredentials();
        this.doPost("/api/device/credentials", deviceCredentials).andExpect(status().isBadRequest());
    }

    @Test
    public void testSaveDeviceCredentialsWithEmptyCredentialsType() throws Exception {
        Device device = new Device();
        device.setName("My device");
        device.setType("default");
        Device savedDevice = this.doPost("/api/device", device, Device.class);
        DeviceCredentials deviceCredentials = this.doGet(
                "/api/device/" + savedDevice.getId().getId().toString() + "/credentials", DeviceCredentials.class);
        deviceCredentials.setCredentialsType(null);
        this.doPost("/api/device/credentials", deviceCredentials).andExpect(status().isBadRequest()).andExpect(
                statusReason(containsString("Device credentials type should be specified")));
    }

    @Test
    public void testSaveDeviceCredentialsWithEmptyCredentialsId() throws Exception {
        Device device = new Device();
        device.setName("My device");
        device.setType("default");
        Device savedDevice = this.doPost("/api/device", device, Device.class);
        DeviceCredentials deviceCredentials = this.doGet(
                "/api/device/" + savedDevice.getId().getId().toString() + "/credentials", DeviceCredentials.class);
        deviceCredentials.setCredentialsId(null);
        this.doPost("/api/device/credentials", deviceCredentials).andExpect(status().isBadRequest()).andExpect(
                statusReason(containsString("Device credentials id should be specified")));
    }

    @Test
    public void testSaveNonExistentDeviceCredentials() throws Exception {
        Device device = new Device();
        device.setName("My device");
        device.setType("default");
        Device savedDevice = this.doPost("/api/device", device, Device.class);
        DeviceCredentials deviceCredentials = this.doGet(
                "/api/device/" + savedDevice.getId().getId().toString() + "/credentials", DeviceCredentials.class);
        DeviceCredentials newDeviceCredentials = new DeviceCredentials(new DeviceCredentialsId(UUIDs.timeBased()));
        newDeviceCredentials.setCreatedTime(deviceCredentials.getCreatedTime());
        newDeviceCredentials.setDeviceId(deviceCredentials.getDeviceId());
        newDeviceCredentials.setCredentialsType(deviceCredentials.getCredentialsType());
        newDeviceCredentials.setCredentialsId(deviceCredentials.getCredentialsId());
        this.doPost("/api/device/credentials", newDeviceCredentials).andExpect(status().isBadRequest()).andExpect(
                statusReason(containsString("Unable to update non-existent device credentials")));
    }

    @Test
    public void testSaveDeviceCredentialsWithNonExistentDevice() throws Exception {
        Device device = new Device();
        device.setName("My device");
        device.setType("default");
        Device savedDevice = this.doPost("/api/device", device, Device.class);
        DeviceCredentials deviceCredentials = this.doGet(
                "/api/device/" + savedDevice.getId().getId().toString() + "/credentials", DeviceCredentials.class);
        deviceCredentials.setDeviceId(new DeviceId(UUIDs.timeBased()));
        this.doPost("/api/device/credentials", deviceCredentials).andExpect(status().isNotFound());
    }

    @Test
    public void testFindTenantDevices() throws Exception {
        List<Device> devices = new ArrayList<>();
        for (int i = 0; i < 178; i++) {
            Device device = new Device();
            device.setName("Device" + i);
            device.setType("default");
            devices.add(this.doPost("/api/device", device, Device.class));
        }
        List<Device> loadedDevices = new ArrayList<>();
        TextPageLink pageLink = new TextPageLink(23);
        TextPageData<Device> pageData = null;
        do {
            pageData = this.doGetTypedWithPageLink("/api/tenant/devices?", new TypeReference<TextPageData<Device>>() {},
                                                   pageLink);
            loadedDevices.addAll(pageData.getData());
            if (pageData.hasNext()) {
                pageLink = pageData.getNextPageLink();
            }
        } while (pageData.hasNext());

        Collections.sort(devices, this.idComparator);
        Collections.sort(loadedDevices, this.idComparator);

        Assert.assertEquals(devices, loadedDevices);
    }

    @Test
    public void testFindTenantDevicesByName() throws Exception {
        String title1 = "Device title 1";
        List<Device> devicesTitle1 = new ArrayList<>();
        for (int i = 0; i < 143; i++) {
            Device device = new Device();
            String suffix = RandomStringUtils.randomAlphanumeric(15);
            String name = title1 + suffix;
            name = i % 2 == 0 ? name.toLowerCase() : name.toUpperCase();
            device.setName(name);
            device.setType("default");
            devicesTitle1.add(this.doPost("/api/device", device, Device.class));
        }
        String title2 = "Device title 2";
        List<Device> devicesTitle2 = new ArrayList<>();
        for (int i = 0; i < 75; i++) {
            Device device = new Device();
            String suffix = RandomStringUtils.randomAlphanumeric(15);
            String name = title2 + suffix;
            name = i % 2 == 0 ? name.toLowerCase() : name.toUpperCase();
            device.setName(name);
            device.setType("default");
            devicesTitle2.add(this.doPost("/api/device", device, Device.class));
        }

        List<Device> loadedDevicesTitle1 = new ArrayList<>();
        TextPageLink pageLink = new TextPageLink(15, title1);
        TextPageData<Device> pageData = null;
        do {
            pageData = this.doGetTypedWithPageLink("/api/tenant/devices?", new TypeReference<TextPageData<Device>>() {},
                                                   pageLink);
            loadedDevicesTitle1.addAll(pageData.getData());
            if (pageData.hasNext()) {
                pageLink = pageData.getNextPageLink();
            }
        } while (pageData.hasNext());

        Collections.sort(devicesTitle1, this.idComparator);
        Collections.sort(loadedDevicesTitle1, this.idComparator);

        Assert.assertEquals(devicesTitle1, loadedDevicesTitle1);

        List<Device> loadedDevicesTitle2 = new ArrayList<>();
        pageLink = new TextPageLink(4, title2);
        do {
            pageData = this.doGetTypedWithPageLink("/api/tenant/devices?", new TypeReference<TextPageData<Device>>() {},
                                                   pageLink);
            loadedDevicesTitle2.addAll(pageData.getData());
            if (pageData.hasNext()) {
                pageLink = pageData.getNextPageLink();
            }
        } while (pageData.hasNext());

        Collections.sort(devicesTitle2, this.idComparator);
        Collections.sort(loadedDevicesTitle2, this.idComparator);

        Assert.assertEquals(devicesTitle2, loadedDevicesTitle2);

        for (Device device : loadedDevicesTitle1) {
            this.doDelete("/api/device/" + device.getId().getId().toString()).andExpect(status().isOk());
        }

        pageLink = new TextPageLink(4, title1);
        pageData = this.doGetTypedWithPageLink("/api/tenant/devices?", new TypeReference<TextPageData<Device>>() {},
                                               pageLink);
        Assert.assertFalse(pageData.hasNext());
        Assert.assertEquals(0, pageData.getData().size());

        for (Device device : loadedDevicesTitle2) {
            this.doDelete("/api/device/" + device.getId().getId().toString()).andExpect(status().isOk());
        }

        pageLink = new TextPageLink(4, title2);
        pageData = this.doGetTypedWithPageLink("/api/tenant/devices?", new TypeReference<TextPageData<Device>>() {},
                                               pageLink);
        Assert.assertFalse(pageData.hasNext());
        Assert.assertEquals(0, pageData.getData().size());
    }

    @Test
    public void testFindTenantDevicesByType() throws Exception {
        String title1 = "Device title 1";
        String type1 = "typeA";
        List<Device> devicesType1 = new ArrayList<>();
        for (int i = 0; i < 143; i++) {
            Device device = new Device();
            String suffix = RandomStringUtils.randomAlphanumeric(15);
            String name = title1 + suffix;
            name = i % 2 == 0 ? name.toLowerCase() : name.toUpperCase();
            device.setName(name);
            device.setType(type1);
            devicesType1.add(this.doPost("/api/device", device, Device.class));
        }
        String title2 = "Device title 2";
        String type2 = "typeB";
        List<Device> devicesType2 = new ArrayList<>();
        for (int i = 0; i < 75; i++) {
            Device device = new Device();
            String suffix = RandomStringUtils.randomAlphanumeric(15);
            String name = title2 + suffix;
            name = i % 2 == 0 ? name.toLowerCase() : name.toUpperCase();
            device.setName(name);
            device.setType(type2);
            devicesType2.add(this.doPost("/api/device", device, Device.class));
        }

        List<Device> loadedDevicesType1 = new ArrayList<>();
        TextPageLink pageLink = new TextPageLink(15);
        TextPageData<Device> pageData = null;
        do {
            pageData = this.doGetTypedWithPageLink("/api/tenant/devices?type={type}&",
                                                   new TypeReference<TextPageData<Device>>() {}, pageLink, type1);
            loadedDevicesType1.addAll(pageData.getData());
            if (pageData.hasNext()) {
                pageLink = pageData.getNextPageLink();
            }
        } while (pageData.hasNext());

        Collections.sort(devicesType1, this.idComparator);
        Collections.sort(loadedDevicesType1, this.idComparator);

        Assert.assertEquals(devicesType1, loadedDevicesType1);

        List<Device> loadedDevicesType2 = new ArrayList<>();
        pageLink = new TextPageLink(4);
        do {
            pageData = this.doGetTypedWithPageLink("/api/tenant/devices?type={type}&",
                                                   new TypeReference<TextPageData<Device>>() {}, pageLink, type2);
            loadedDevicesType2.addAll(pageData.getData());
            if (pageData.hasNext()) {
                pageLink = pageData.getNextPageLink();
            }
        } while (pageData.hasNext());

        Collections.sort(devicesType2, this.idComparator);
        Collections.sort(loadedDevicesType2, this.idComparator);

        Assert.assertEquals(devicesType2, loadedDevicesType2);

        for (Device device : loadedDevicesType1) {
            this.doDelete("/api/device/" + device.getId().getId().toString()).andExpect(status().isOk());
        }

        pageLink = new TextPageLink(4);
        pageData = this.doGetTypedWithPageLink("/api/tenant/devices?type={type}&",
                                               new TypeReference<TextPageData<Device>>() {}, pageLink, type1);
        Assert.assertFalse(pageData.hasNext());
        Assert.assertEquals(0, pageData.getData().size());

        for (Device device : loadedDevicesType2) {
            this.doDelete("/api/device/" + device.getId().getId().toString()).andExpect(status().isOk());
        }

        pageLink = new TextPageLink(4);
        pageData = this.doGetTypedWithPageLink("/api/tenant/devices?type={type}&",
                                               new TypeReference<TextPageData<Device>>() {}, pageLink, type2);
        Assert.assertFalse(pageData.hasNext());
        Assert.assertEquals(0, pageData.getData().size());
    }

    @Test
    public void testFindCustomerDevices() throws Exception {
        Customer customer = new Customer();
        customer.setTitle("Test customer");
        customer = this.doPost("/api/customer", customer, Customer.class);
        CustomerId customerId = customer.getId();

        List<Device> devices = new ArrayList<>();
        for (int i = 0; i < 128; i++) {
            Device device = new Device();
            device.setName("Device" + i);
            device.setType("default");
            device = this.doPost("/api/device", device, Device.class);
            devices.add(this.doPost(
                    "/api/customer/" + customerId.getId().toString() + "/device/" + device.getId().getId().toString(),
                    Device.class));
        }

        List<Device> loadedDevices = new ArrayList<>();
        TextPageLink pageLink = new TextPageLink(23);
        TextPageData<Device> pageData = null;
        do {
            pageData = this.doGetTypedWithPageLink("/api/customer/" + customerId.getId().toString() + "/devices?",
                                                   new TypeReference<TextPageData<Device>>() {}, pageLink);
            loadedDevices.addAll(pageData.getData());
            if (pageData.hasNext()) {
                pageLink = pageData.getNextPageLink();
            }
        } while (pageData.hasNext());

        Collections.sort(devices, this.idComparator);
        Collections.sort(loadedDevices, this.idComparator);

        Assert.assertEquals(devices, loadedDevices);
    }

    @Test
    public void testFindCustomerDevicesByName() throws Exception {
        Customer customer = new Customer();
        customer.setTitle("Test customer");
        customer = this.doPost("/api/customer", customer, Customer.class);
        CustomerId customerId = customer.getId();

        String title1 = "Device title 1";
        List<Device> devicesTitle1 = new ArrayList<>();
        for (int i = 0; i < 125; i++) {
            Device device = new Device();
            String suffix = RandomStringUtils.randomAlphanumeric(15);
            String name = title1 + suffix;
            name = i % 2 == 0 ? name.toLowerCase() : name.toUpperCase();
            device.setName(name);
            device.setType("default");
            device = this.doPost("/api/device", device, Device.class);
            devicesTitle1.add(this.doPost(
                    "/api/customer/" + customerId.getId().toString() + "/device/" + device.getId().getId().toString(),
                    Device.class));
        }
        String title2 = "Device title 2";
        List<Device> devicesTitle2 = new ArrayList<>();
        for (int i = 0; i < 143; i++) {
            Device device = new Device();
            String suffix = RandomStringUtils.randomAlphanumeric(15);
            String name = title2 + suffix;
            name = i % 2 == 0 ? name.toLowerCase() : name.toUpperCase();
            device.setName(name);
            device.setType("default");
            device = this.doPost("/api/device", device, Device.class);
            devicesTitle2.add(this.doPost(
                    "/api/customer/" + customerId.getId().toString() + "/device/" + device.getId().getId().toString(),
                    Device.class));
        }

        List<Device> loadedDevicesTitle1 = new ArrayList<>();
        TextPageLink pageLink = new TextPageLink(15, title1);
        TextPageData<Device> pageData = null;
        do {
            pageData = this.doGetTypedWithPageLink("/api/customer/" + customerId.getId().toString() + "/devices?",
                                                   new TypeReference<TextPageData<Device>>() {}, pageLink);
            loadedDevicesTitle1.addAll(pageData.getData());
            if (pageData.hasNext()) {
                pageLink = pageData.getNextPageLink();
            }
        } while (pageData.hasNext());

        Collections.sort(devicesTitle1, this.idComparator);
        Collections.sort(loadedDevicesTitle1, this.idComparator);

        Assert.assertEquals(devicesTitle1, loadedDevicesTitle1);

        List<Device> loadedDevicesTitle2 = new ArrayList<>();
        pageLink = new TextPageLink(4, title2);
        do {
            pageData = this.doGetTypedWithPageLink("/api/customer/" + customerId.getId().toString() + "/devices?",
                                                   new TypeReference<TextPageData<Device>>() {}, pageLink);
            loadedDevicesTitle2.addAll(pageData.getData());
            if (pageData.hasNext()) {
                pageLink = pageData.getNextPageLink();
            }
        } while (pageData.hasNext());

        Collections.sort(devicesTitle2, this.idComparator);
        Collections.sort(loadedDevicesTitle2, this.idComparator);

        Assert.assertEquals(devicesTitle2, loadedDevicesTitle2);

        for (Device device : loadedDevicesTitle1) {
            this.doDelete("/api/customer/device/" + device.getId().getId().toString()).andExpect(status().isOk());
        }

        pageLink = new TextPageLink(4, title1);
        pageData = this.doGetTypedWithPageLink("/api/customer/" + customerId.getId().toString() + "/devices?",
                                               new TypeReference<TextPageData<Device>>() {}, pageLink);
        Assert.assertFalse(pageData.hasNext());
        Assert.assertEquals(0, pageData.getData().size());

        for (Device device : loadedDevicesTitle2) {
            this.doDelete("/api/customer/device/" + device.getId().getId().toString()).andExpect(status().isOk());
        }

        pageLink = new TextPageLink(4, title2);
        pageData = this.doGetTypedWithPageLink("/api/customer/" + customerId.getId().toString() + "/devices?",
                                               new TypeReference<TextPageData<Device>>() {}, pageLink);
        Assert.assertFalse(pageData.hasNext());
        Assert.assertEquals(0, pageData.getData().size());
    }

    @Test
    public void testFindCustomerDevicesByType() throws Exception {
        Customer customer = new Customer();
        customer.setTitle("Test customer");
        customer = this.doPost("/api/customer", customer, Customer.class);
        CustomerId customerId = customer.getId();

        String title1 = "Device title 1";
        String type1 = "typeC";
        List<Device> devicesType1 = new ArrayList<>();
        for (int i = 0; i < 125; i++) {
            Device device = new Device();
            String suffix = RandomStringUtils.randomAlphanumeric(15);
            String name = title1 + suffix;
            name = i % 2 == 0 ? name.toLowerCase() : name.toUpperCase();
            device.setName(name);
            device.setType(type1);
            device = this.doPost("/api/device", device, Device.class);
            devicesType1.add(this.doPost(
                    "/api/customer/" + customerId.getId().toString() + "/device/" + device.getId().getId().toString(),
                    Device.class));
        }
        String title2 = "Device title 2";
        String type2 = "typeD";
        List<Device> devicesType2 = new ArrayList<>();
        for (int i = 0; i < 143; i++) {
            Device device = new Device();
            String suffix = RandomStringUtils.randomAlphanumeric(15);
            String name = title2 + suffix;
            name = i % 2 == 0 ? name.toLowerCase() : name.toUpperCase();
            device.setName(name);
            device.setType(type2);
            device = this.doPost("/api/device", device, Device.class);
            devicesType2.add(this.doPost(
                    "/api/customer/" + customerId.getId().toString() + "/device/" + device.getId().getId().toString(),
                    Device.class));
        }

        List<Device> loadedDevicesType1 = new ArrayList<>();
        TextPageLink pageLink = new TextPageLink(15);
        TextPageData<Device> pageData = null;
        do {
            pageData = this.doGetTypedWithPageLink(
                    "/api/customer/" + customerId.getId().toString() + "/devices?type={type}&",
                    new TypeReference<TextPageData<Device>>() {}, pageLink, type1);
            loadedDevicesType1.addAll(pageData.getData());
            if (pageData.hasNext()) {
                pageLink = pageData.getNextPageLink();
            }
        } while (pageData.hasNext());

        Collections.sort(devicesType1, this.idComparator);
        Collections.sort(loadedDevicesType1, this.idComparator);

        Assert.assertEquals(devicesType1, loadedDevicesType1);

        List<Device> loadedDevicesType2 = new ArrayList<>();
        pageLink = new TextPageLink(4);
        do {
            pageData = this.doGetTypedWithPageLink(
                    "/api/customer/" + customerId.getId().toString() + "/devices?type={type}&",
                    new TypeReference<TextPageData<Device>>() {}, pageLink, type2);
            loadedDevicesType2.addAll(pageData.getData());
            if (pageData.hasNext()) {
                pageLink = pageData.getNextPageLink();
            }
        } while (pageData.hasNext());

        Collections.sort(devicesType2, this.idComparator);
        Collections.sort(loadedDevicesType2, this.idComparator);

        Assert.assertEquals(devicesType2, loadedDevicesType2);

        for (Device device : loadedDevicesType1) {
            this.doDelete("/api/customer/device/" + device.getId().getId().toString()).andExpect(status().isOk());
        }

        pageLink = new TextPageLink(4);
        pageData = this.doGetTypedWithPageLink(
                "/api/customer/" + customerId.getId().toString() + "/devices?type={type}&",
                new TypeReference<TextPageData<Device>>() {}, pageLink, type1);
        Assert.assertFalse(pageData.hasNext());
        Assert.assertEquals(0, pageData.getData().size());

        for (Device device : loadedDevicesType2) {
            this.doDelete("/api/customer/device/" + device.getId().getId().toString()).andExpect(status().isOk());
        }

        pageLink = new TextPageLink(4);
        pageData = this.doGetTypedWithPageLink(
                "/api/customer/" + customerId.getId().toString() + "/devices?type={type}&",
                new TypeReference<TextPageData<Device>>() {}, pageLink, type2);
        Assert.assertFalse(pageData.hasNext());
        Assert.assertEquals(0, pageData.getData().size());
    }

}
