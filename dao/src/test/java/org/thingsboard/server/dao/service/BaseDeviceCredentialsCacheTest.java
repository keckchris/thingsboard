/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.dao.service;


import org.apache.commons.lang3.RandomStringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.aop.framework.Advised;
import org.springframework.aop.support.AopUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.test.util.ReflectionTestUtils;
import org.thingsboard.server.common.data.CacheConstants;
import org.thingsboard.server.common.data.Device;
import org.thingsboard.server.common.data.id.DeviceCredentialsId;
import org.thingsboard.server.common.data.id.DeviceId;
import org.thingsboard.server.common.data.security.DeviceCredentials;
import org.thingsboard.server.common.data.security.DeviceCredentialsType;
import org.thingsboard.server.dao.device.DeviceCredentialsDao;
import org.thingsboard.server.dao.device.DeviceCredentialsService;
import org.thingsboard.server.dao.device.DeviceService;

import java.util.UUID;

import static org.mockito.Mockito.*;


public abstract class BaseDeviceCredentialsCacheTest extends AbstractServiceTest {

    private static final String CREDENTIALS_ID_1 = RandomStringUtils.randomAlphanumeric(20);
    private static final String CREDENTIALS_ID_2 = RandomStringUtils.randomAlphanumeric(20);

    @Autowired private DeviceCredentialsService deviceCredentialsService;

    private DeviceCredentialsDao deviceCredentialsDao;
    private DeviceService deviceService;

    @Autowired private CacheManager cacheManager;

    private UUID deviceId = UUID.randomUUID();

    @Before
    public void setup() throws Exception {
        this.deviceCredentialsDao = mock(DeviceCredentialsDao.class);
        this.deviceService = mock(DeviceService.class);
        ReflectionTestUtils.setField(this.unwrapDeviceCredentialsService(), "deviceCredentialsDao",
                                     this.deviceCredentialsDao);
        ReflectionTestUtils.setField(this.unwrapDeviceCredentialsService(), "deviceService", this.deviceService);
    }

    private DeviceCredentialsService unwrapDeviceCredentialsService() throws Exception {
        if (AopUtils.isAopProxy(this.deviceCredentialsService) && this.deviceCredentialsService instanceof Advised) {
            Object target = ((Advised) this.deviceCredentialsService).getTargetSource().getTarget();
            return (DeviceCredentialsService) target;
        }
        return null;
    }

    @After
    public void cleanup() {
        this.cacheManager.getCache(CacheConstants.DEVICE_CREDENTIALS_CACHE).clear();
    }

    @Test
    public void testFindDeviceCredentialsByCredentialsId_Cached() {
        when(this.deviceCredentialsDao.findByCredentialsId(SYSTEM_TENANT_ID, CREDENTIALS_ID_1)).thenReturn(
                this.createDummyDeviceCredentialsEntity(CREDENTIALS_ID_1));

        this.deviceCredentialsService.findDeviceCredentialsByCredentialsId(CREDENTIALS_ID_1);
        this.deviceCredentialsService.findDeviceCredentialsByCredentialsId(CREDENTIALS_ID_1);

        verify(this.deviceCredentialsDao, times(1)).findByCredentialsId(SYSTEM_TENANT_ID, CREDENTIALS_ID_1);
    }

    private DeviceCredentials createDummyDeviceCredentialsEntity(String deviceCredentialsId) {
        DeviceCredentials result = new DeviceCredentials(new DeviceCredentialsId(UUID.randomUUID()));
        result.setCredentialsId(deviceCredentialsId);
        return result;
    }

    @Test
    public void testDeleteDeviceCredentials_EvictsCache() {
        when(this.deviceCredentialsDao.findByCredentialsId(SYSTEM_TENANT_ID, CREDENTIALS_ID_1)).thenReturn(
                this.createDummyDeviceCredentialsEntity(CREDENTIALS_ID_1));

        this.deviceCredentialsService.findDeviceCredentialsByCredentialsId(CREDENTIALS_ID_1);
        this.deviceCredentialsService.findDeviceCredentialsByCredentialsId(CREDENTIALS_ID_1);

        verify(this.deviceCredentialsDao, times(1)).findByCredentialsId(SYSTEM_TENANT_ID, CREDENTIALS_ID_1);

        this.deviceCredentialsService.deleteDeviceCredentials(SYSTEM_TENANT_ID,
                                                              this.createDummyDeviceCredentials(CREDENTIALS_ID_1,
                                                                                                this.deviceId));

        this.deviceCredentialsService.findDeviceCredentialsByCredentialsId(CREDENTIALS_ID_1);
        this.deviceCredentialsService.findDeviceCredentialsByCredentialsId(CREDENTIALS_ID_1);

        verify(this.deviceCredentialsDao, times(2)).findByCredentialsId(SYSTEM_TENANT_ID, CREDENTIALS_ID_1);
    }

    private DeviceCredentials createDummyDeviceCredentials(String deviceCredentialsId, UUID deviceId) {
        return this.createDummyDeviceCredentials(null, deviceCredentialsId, deviceId);
    }

    private DeviceCredentials createDummyDeviceCredentials(UUID id, String deviceCredentialsId, UUID deviceId) {
        DeviceCredentials result = new DeviceCredentials();
        result.setId(new DeviceCredentialsId(id));
        result.setDeviceId(new DeviceId(deviceId));
        result.setCredentialsId(deviceCredentialsId);
        result.setCredentialsType(DeviceCredentialsType.ACCESS_TOKEN);
        return result;
    }

    @Test
    public void testSaveDeviceCredentials_EvictsPreviousCache() {
        when(this.deviceCredentialsDao.findByCredentialsId(SYSTEM_TENANT_ID, CREDENTIALS_ID_1)).thenReturn(
                this.createDummyDeviceCredentialsEntity(CREDENTIALS_ID_1));

        this.deviceCredentialsService.findDeviceCredentialsByCredentialsId(CREDENTIALS_ID_1);
        this.deviceCredentialsService.findDeviceCredentialsByCredentialsId(CREDENTIALS_ID_1);

        verify(this.deviceCredentialsDao, times(1)).findByCredentialsId(SYSTEM_TENANT_ID, CREDENTIALS_ID_1);

        when(this.deviceCredentialsDao.findByDeviceId(SYSTEM_TENANT_ID, this.deviceId)).thenReturn(
                this.createDummyDeviceCredentialsEntity(CREDENTIALS_ID_1));

        UUID deviceCredentialsId = UUID.randomUUID();
        when(this.deviceCredentialsDao.findById(SYSTEM_TENANT_ID, deviceCredentialsId)).thenReturn(
                this.createDummyDeviceCredentialsEntity(CREDENTIALS_ID_1));
        when(this.deviceService.findDeviceById(SYSTEM_TENANT_ID, new DeviceId(this.deviceId))).thenReturn(new Device());

        this.deviceCredentialsService.updateDeviceCredentials(SYSTEM_TENANT_ID,
                                                              this.createDummyDeviceCredentials(deviceCredentialsId,
                                                                                                CREDENTIALS_ID_2,
                                                                                                this.deviceId));

        when(this.deviceCredentialsDao.findByCredentialsId(SYSTEM_TENANT_ID, CREDENTIALS_ID_1)).thenReturn(null);

        this.deviceCredentialsService.findDeviceCredentialsByCredentialsId(CREDENTIALS_ID_1);
        this.deviceCredentialsService.findDeviceCredentialsByCredentialsId(CREDENTIALS_ID_1);

        verify(this.deviceCredentialsDao, times(3)).findByCredentialsId(SYSTEM_TENANT_ID, CREDENTIALS_ID_1);
    }

}

