/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.dao.service;


import com.datastax.driver.core.utils.UUIDs;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.thingsboard.server.common.data.Customer;
import org.thingsboard.server.common.data.Dashboard;
import org.thingsboard.server.common.data.DashboardInfo;
import org.thingsboard.server.common.data.Tenant;
import org.thingsboard.server.common.data.id.CustomerId;
import org.thingsboard.server.common.data.id.TenantId;
import org.thingsboard.server.common.data.page.TextPageData;
import org.thingsboard.server.common.data.page.TextPageLink;
import org.thingsboard.server.common.data.page.TimePageData;
import org.thingsboard.server.common.data.page.TimePageLink;
import org.thingsboard.server.dao.exception.DataValidationException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;


public abstract class BaseDashboardServiceTest extends AbstractServiceTest {

    private IdComparator<DashboardInfo> idComparator = new IdComparator<>();

    private TenantId tenantId;

    @Before
    public void before() {
        Tenant tenant = new Tenant();
        tenant.setTitle("My tenant");
        Tenant savedTenant = this.tenantService.saveTenant(tenant);
        Assert.assertNotNull(savedTenant);
        this.tenantId = savedTenant.getId();
    }

    @After
    public void after() {
        this.tenantService.deleteTenant(this.tenantId);
    }

    @Test
    public void testSaveDashboard() {
        Dashboard dashboard = new Dashboard();
        dashboard.setTenantId(this.tenantId);
        dashboard.setTitle("My dashboard");
        Dashboard savedDashboard = this.dashboardService.saveDashboard(dashboard);

        Assert.assertNotNull(savedDashboard);
        Assert.assertNotNull(savedDashboard.getId());
        Assert.assertTrue(savedDashboard.getCreatedTime() > 0);
        Assert.assertEquals(dashboard.getTenantId(), savedDashboard.getTenantId());
        Assert.assertEquals(dashboard.getTitle(), savedDashboard.getTitle());

        savedDashboard.setTitle("My new dashboard");

        this.dashboardService.saveDashboard(savedDashboard);
        Dashboard foundDashboard = this.dashboardService.findDashboardById(this.tenantId, savedDashboard.getId());
        Assert.assertEquals(foundDashboard.getTitle(), savedDashboard.getTitle());

        this.dashboardService.deleteDashboard(this.tenantId, savedDashboard.getId());
    }

    @Test(expected = DataValidationException.class)
    public void testSaveDashboardWithEmptyTitle() {
        Dashboard dashboard = new Dashboard();
        dashboard.setTenantId(this.tenantId);
        this.dashboardService.saveDashboard(dashboard);
    }

    @Test(expected = DataValidationException.class)
    public void testSaveDashboardWithEmptyTenant() {
        Dashboard dashboard = new Dashboard();
        dashboard.setTitle("My dashboard");
        this.dashboardService.saveDashboard(dashboard);
    }

    @Test(expected = DataValidationException.class)
    public void testSaveDashboardWithInvalidTenant() {
        Dashboard dashboard = new Dashboard();
        dashboard.setTitle("My dashboard");
        dashboard.setTenantId(new TenantId(UUIDs.timeBased()));
        this.dashboardService.saveDashboard(dashboard);
    }

    @Test(expected = DataValidationException.class)
    public void testAssignDashboardToNonExistentCustomer() {
        Dashboard dashboard = new Dashboard();
        dashboard.setTitle("My dashboard");
        dashboard.setTenantId(this.tenantId);
        dashboard = this.dashboardService.saveDashboard(dashboard);
        try {
            this.dashboardService.assignDashboardToCustomer(this.tenantId, dashboard.getId(),
                                                            new CustomerId(UUIDs.timeBased()));
        } finally {
            this.dashboardService.deleteDashboard(this.tenantId, dashboard.getId());
        }
    }

    @Test(expected = DataValidationException.class)
    public void testAssignDashboardToCustomerFromDifferentTenant() {
        Dashboard dashboard = new Dashboard();
        dashboard.setTitle("My dashboard");
        dashboard.setTenantId(this.tenantId);
        dashboard = this.dashboardService.saveDashboard(dashboard);
        Tenant tenant = new Tenant();
        tenant.setTitle("Test different tenant");
        tenant = this.tenantService.saveTenant(tenant);
        Customer customer = new Customer();
        customer.setTenantId(tenant.getId());
        customer.setTitle("Test different customer");
        customer = this.customerService.saveCustomer(customer);
        try {
            this.dashboardService.assignDashboardToCustomer(this.tenantId, dashboard.getId(), customer.getId());
        } finally {
            this.dashboardService.deleteDashboard(this.tenantId, dashboard.getId());
            this.tenantService.deleteTenant(tenant.getId());
        }
    }

    @Test
    public void testFindDashboardById() {
        Dashboard dashboard = new Dashboard();
        dashboard.setTenantId(this.tenantId);
        dashboard.setTitle("My dashboard");
        Dashboard savedDashboard = this.dashboardService.saveDashboard(dashboard);
        Dashboard foundDashboard = this.dashboardService.findDashboardById(this.tenantId, savedDashboard.getId());
        Assert.assertNotNull(foundDashboard);
        Assert.assertEquals(savedDashboard, foundDashboard);
        this.dashboardService.deleteDashboard(this.tenantId, savedDashboard.getId());
    }

    @Test
    public void testDeleteDashboard() {
        Dashboard dashboard = new Dashboard();
        dashboard.setTenantId(this.tenantId);
        dashboard.setTitle("My dashboard");
        Dashboard savedDashboard = this.dashboardService.saveDashboard(dashboard);
        Dashboard foundDashboard = this.dashboardService.findDashboardById(this.tenantId, savedDashboard.getId());
        Assert.assertNotNull(foundDashboard);
        this.dashboardService.deleteDashboard(this.tenantId, savedDashboard.getId());
        foundDashboard = this.dashboardService.findDashboardById(this.tenantId, savedDashboard.getId());
        Assert.assertNull(foundDashboard);
    }

    @Test
    public void testFindDashboardsByTenantId() {
        Tenant tenant = new Tenant();
        tenant.setTitle("Test tenant");
        tenant = this.tenantService.saveTenant(tenant);

        TenantId tenantId = tenant.getId();

        List<DashboardInfo> dashboards = new ArrayList<>();
        for (int i = 0; i < 165; i++) {
            Dashboard dashboard = new Dashboard();
            dashboard.setTenantId(tenantId);
            dashboard.setTitle("Dashboard" + i);
            dashboards.add(new DashboardInfo(this.dashboardService.saveDashboard(dashboard)));
        }

        List<DashboardInfo> loadedDashboards = new ArrayList<>();
        TextPageLink pageLink = new TextPageLink(16);
        TextPageData<DashboardInfo> pageData = null;
        do {
            pageData = this.dashboardService.findDashboardsByTenantId(tenantId, pageLink);
            loadedDashboards.addAll(pageData.getData());
            if (pageData.hasNext()) {
                pageLink = pageData.getNextPageLink();
            }
        } while (pageData.hasNext());

        Collections.sort(dashboards, this.idComparator);
        Collections.sort(loadedDashboards, this.idComparator);

        Assert.assertEquals(dashboards, loadedDashboards);

        this.dashboardService.deleteDashboardsByTenantId(tenantId);

        pageLink = new TextPageLink(31);
        pageData = this.dashboardService.findDashboardsByTenantId(tenantId, pageLink);
        Assert.assertFalse(pageData.hasNext());
        Assert.assertTrue(pageData.getData().isEmpty());

        this.tenantService.deleteTenant(tenantId);
    }

    @Test
    public void testFindDashboardsByTenantIdAndTitle() {
        String title1 = "Dashboard title 1";
        List<DashboardInfo> dashboardsTitle1 = new ArrayList<>();
        for (int i = 0; i < 123; i++) {
            Dashboard dashboard = new Dashboard();
            dashboard.setTenantId(this.tenantId);
            String suffix = RandomStringUtils.randomAlphanumeric((int) (Math.random() * 17));
            String title = title1 + suffix;
            title = i % 2 == 0 ? title.toLowerCase() : title.toUpperCase();
            dashboard.setTitle(title);
            dashboardsTitle1.add(new DashboardInfo(this.dashboardService.saveDashboard(dashboard)));
        }
        String title2 = "Dashboard title 2";
        List<DashboardInfo> dashboardsTitle2 = new ArrayList<>();
        for (int i = 0; i < 193; i++) {
            Dashboard dashboard = new Dashboard();
            dashboard.setTenantId(this.tenantId);
            String suffix = RandomStringUtils.randomAlphanumeric((int) (Math.random() * 15));
            String title = title2 + suffix;
            title = i % 2 == 0 ? title.toLowerCase() : title.toUpperCase();
            dashboard.setTitle(title);
            dashboardsTitle2.add(new DashboardInfo(this.dashboardService.saveDashboard(dashboard)));
        }

        List<DashboardInfo> loadedDashboardsTitle1 = new ArrayList<>();
        TextPageLink pageLink = new TextPageLink(19, title1);
        TextPageData<DashboardInfo> pageData = null;
        do {
            pageData = this.dashboardService.findDashboardsByTenantId(this.tenantId, pageLink);
            loadedDashboardsTitle1.addAll(pageData.getData());
            if (pageData.hasNext()) {
                pageLink = pageData.getNextPageLink();
            }
        } while (pageData.hasNext());

        Collections.sort(dashboardsTitle1, this.idComparator);
        Collections.sort(loadedDashboardsTitle1, this.idComparator);

        Assert.assertEquals(dashboardsTitle1, loadedDashboardsTitle1);

        List<DashboardInfo> loadedDashboardsTitle2 = new ArrayList<>();
        pageLink = new TextPageLink(4, title2);
        do {
            pageData = this.dashboardService.findDashboardsByTenantId(this.tenantId, pageLink);
            loadedDashboardsTitle2.addAll(pageData.getData());
            if (pageData.hasNext()) {
                pageLink = pageData.getNextPageLink();
            }
        } while (pageData.hasNext());

        Collections.sort(dashboardsTitle2, this.idComparator);
        Collections.sort(loadedDashboardsTitle2, this.idComparator);

        Assert.assertEquals(dashboardsTitle2, loadedDashboardsTitle2);

        for (DashboardInfo dashboard : loadedDashboardsTitle1) {
            this.dashboardService.deleteDashboard(this.tenantId, dashboard.getId());
        }

        pageLink = new TextPageLink(4, title1);
        pageData = this.dashboardService.findDashboardsByTenantId(this.tenantId, pageLink);
        Assert.assertFalse(pageData.hasNext());
        Assert.assertEquals(0, pageData.getData().size());

        for (DashboardInfo dashboard : loadedDashboardsTitle2) {
            this.dashboardService.deleteDashboard(this.tenantId, dashboard.getId());
        }

        pageLink = new TextPageLink(4, title2);
        pageData = this.dashboardService.findDashboardsByTenantId(this.tenantId, pageLink);
        Assert.assertFalse(pageData.hasNext());
        Assert.assertEquals(0, pageData.getData().size());
    }

    @Test
    public void testFindDashboardsByTenantIdAndCustomerId() throws ExecutionException, InterruptedException {
        Tenant tenant = new Tenant();
        tenant.setTitle("Test tenant");
        tenant = this.tenantService.saveTenant(tenant);

        TenantId tenantId = tenant.getId();

        Customer customer = new Customer();
        customer.setTitle("Test customer");
        customer.setTenantId(tenantId);
        customer = this.customerService.saveCustomer(customer);
        CustomerId customerId = customer.getId();

        List<DashboardInfo> dashboards = new ArrayList<>();
        for (int i = 0; i < 223; i++) {
            Dashboard dashboard = new Dashboard();
            dashboard.setTenantId(tenantId);
            dashboard.setTitle("Dashboard" + i);
            dashboard = this.dashboardService.saveDashboard(dashboard);
            dashboards.add(new DashboardInfo(
                    this.dashboardService.assignDashboardToCustomer(tenantId, dashboard.getId(), customerId)));
        }

        List<DashboardInfo> loadedDashboards = new ArrayList<>();
        TimePageLink pageLink = new TimePageLink(23);
        TimePageData<DashboardInfo> pageData = null;
        do {
            pageData = this.dashboardService.findDashboardsByTenantIdAndCustomerId(tenantId, customerId, pageLink)
                                            .get();
            loadedDashboards.addAll(pageData.getData());
            if (pageData.hasNext()) {
                pageLink = pageData.getNextPageLink();
            }
        } while (pageData.hasNext());

        Collections.sort(dashboards, this.idComparator);
        Collections.sort(loadedDashboards, this.idComparator);

        Assert.assertEquals(dashboards, loadedDashboards);

        this.dashboardService.unassignCustomerDashboards(tenantId, customerId);

        pageLink = new TimePageLink(42);
        pageData = this.dashboardService.findDashboardsByTenantIdAndCustomerId(tenantId, customerId, pageLink).get();
        Assert.assertFalse(pageData.hasNext());
        Assert.assertTrue(pageData.getData().isEmpty());

        this.tenantService.deleteTenant(tenantId);
    }

}
