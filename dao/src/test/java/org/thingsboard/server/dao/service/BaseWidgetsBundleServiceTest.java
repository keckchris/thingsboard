/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.dao.service;


import com.datastax.driver.core.utils.UUIDs;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.thingsboard.server.common.data.Tenant;
import org.thingsboard.server.common.data.id.TenantId;
import org.thingsboard.server.common.data.page.TextPageData;
import org.thingsboard.server.common.data.page.TextPageLink;
import org.thingsboard.server.common.data.widget.WidgetsBundle;
import org.thingsboard.server.dao.exception.DataValidationException;
import org.thingsboard.server.dao.model.ModelConstants;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public abstract class BaseWidgetsBundleServiceTest extends AbstractServiceTest {

    private IdComparator<WidgetsBundle> idComparator = new IdComparator<>();

    private TenantId tenantId;

    @Before
    public void before() {
        Tenant tenant = new Tenant();
        tenant.setTitle("My tenant");
        Tenant savedTenant = this.tenantService.saveTenant(tenant);
        Assert.assertNotNull(savedTenant);
        this.tenantId = savedTenant.getId();
    }

    @After
    public void after() {
        this.tenantService.deleteTenant(this.tenantId);
    }

    @Test
    public void testSaveWidgetsBundle() {
        WidgetsBundle widgetsBundle = new WidgetsBundle();
        widgetsBundle.setTenantId(this.tenantId);
        widgetsBundle.setTitle("My first widgets bundle");

        WidgetsBundle savedWidgetsBundle = this.widgetsBundleService.saveWidgetsBundle(widgetsBundle);

        Assert.assertNotNull(savedWidgetsBundle);
        Assert.assertNotNull(savedWidgetsBundle.getId());
        Assert.assertNotNull(savedWidgetsBundle.getAlias());
        Assert.assertTrue(savedWidgetsBundle.getCreatedTime() > 0);
        Assert.assertEquals(widgetsBundle.getTenantId(), savedWidgetsBundle.getTenantId());
        Assert.assertEquals(widgetsBundle.getTitle(), savedWidgetsBundle.getTitle());

        savedWidgetsBundle.setTitle("My new widgets bundle");

        this.widgetsBundleService.saveWidgetsBundle(savedWidgetsBundle);
        WidgetsBundle foundWidgetsBundle = this.widgetsBundleService.findWidgetsBundleById(this.tenantId,
                                                                                           savedWidgetsBundle.getId());
        Assert.assertEquals(foundWidgetsBundle.getTitle(), savedWidgetsBundle.getTitle());

        this.widgetsBundleService.deleteWidgetsBundle(this.tenantId, savedWidgetsBundle.getId());
    }

    @Test(expected = DataValidationException.class)
    public void testSaveWidgetsBundleWithEmptyTitle() {
        WidgetsBundle widgetsBundle = new WidgetsBundle();
        widgetsBundle.setTenantId(this.tenantId);
        this.widgetsBundleService.saveWidgetsBundle(widgetsBundle);
    }

    @Test(expected = DataValidationException.class)
    public void testSaveWidgetsBundleWithInvalidTenant() {
        WidgetsBundle widgetsBundle = new WidgetsBundle();
        widgetsBundle.setTitle("My widgets bundle");
        widgetsBundle.setTenantId(new TenantId(UUIDs.timeBased()));
        this.widgetsBundleService.saveWidgetsBundle(widgetsBundle);
    }

    @Test(expected = DataValidationException.class)
    public void testUpdateWidgetsBundleTenant() {
        WidgetsBundle widgetsBundle = new WidgetsBundle();
        widgetsBundle.setTitle("My widgets bundle");
        widgetsBundle.setTenantId(this.tenantId);
        WidgetsBundle savedWidgetsBundle = this.widgetsBundleService.saveWidgetsBundle(widgetsBundle);
        savedWidgetsBundle.setTenantId(new TenantId(ModelConstants.NULL_UUID));
        try {
            this.widgetsBundleService.saveWidgetsBundle(savedWidgetsBundle);
        } finally {
            this.widgetsBundleService.deleteWidgetsBundle(this.tenantId, savedWidgetsBundle.getId());
        }
    }

    @Test(expected = DataValidationException.class)
    public void testUpdateWidgetsBundleAlias() {
        WidgetsBundle widgetsBundle = new WidgetsBundle();
        widgetsBundle.setTitle("My widgets bundle");
        widgetsBundle.setTenantId(this.tenantId);
        WidgetsBundle savedWidgetsBundle = this.widgetsBundleService.saveWidgetsBundle(widgetsBundle);
        savedWidgetsBundle.setAlias("new_alias");
        try {
            this.widgetsBundleService.saveWidgetsBundle(savedWidgetsBundle);
        } finally {
            this.widgetsBundleService.deleteWidgetsBundle(this.tenantId, savedWidgetsBundle.getId());
        }
    }

    @Test
    public void testFindWidgetsBundleById() {
        WidgetsBundle widgetsBundle = new WidgetsBundle();
        widgetsBundle.setTenantId(this.tenantId);
        widgetsBundle.setTitle("My widgets bundle");
        WidgetsBundle savedWidgetsBundle = this.widgetsBundleService.saveWidgetsBundle(widgetsBundle);
        WidgetsBundle foundWidgetsBundle = this.widgetsBundleService.findWidgetsBundleById(this.tenantId,
                                                                                           savedWidgetsBundle.getId());
        Assert.assertNotNull(foundWidgetsBundle);
        Assert.assertEquals(savedWidgetsBundle, foundWidgetsBundle);
        this.widgetsBundleService.deleteWidgetsBundle(this.tenantId, savedWidgetsBundle.getId());
    }

    @Test
    public void testFindWidgetsBundleByTenantIdAndAlias() {
        WidgetsBundle widgetsBundle = new WidgetsBundle();
        widgetsBundle.setTenantId(this.tenantId);
        widgetsBundle.setTitle("My widgets bundle");
        WidgetsBundle savedWidgetsBundle = this.widgetsBundleService.saveWidgetsBundle(widgetsBundle);
        WidgetsBundle foundWidgetsBundle = this.widgetsBundleService.findWidgetsBundleByTenantIdAndAlias(this.tenantId,
                                                                                                         savedWidgetsBundle
                                                                                                                 .getAlias());
        Assert.assertNotNull(foundWidgetsBundle);
        Assert.assertEquals(savedWidgetsBundle, foundWidgetsBundle);
        this.widgetsBundleService.deleteWidgetsBundle(this.tenantId, savedWidgetsBundle.getId());
    }

    @Test
    public void testDeleteWidgetsBundle() {
        WidgetsBundle widgetsBundle = new WidgetsBundle();
        widgetsBundle.setTenantId(this.tenantId);
        widgetsBundle.setTitle("My widgets bundle");
        WidgetsBundle savedWidgetsBundle = this.widgetsBundleService.saveWidgetsBundle(widgetsBundle);
        WidgetsBundle foundWidgetsBundle = this.widgetsBundleService.findWidgetsBundleById(this.tenantId,
                                                                                           savedWidgetsBundle.getId());
        Assert.assertNotNull(foundWidgetsBundle);
        this.widgetsBundleService.deleteWidgetsBundle(this.tenantId, savedWidgetsBundle.getId());
        foundWidgetsBundle = this.widgetsBundleService.findWidgetsBundleById(this.tenantId, savedWidgetsBundle.getId());
        Assert.assertNull(foundWidgetsBundle);
    }

    @Test
    public void testFindSystemWidgetsBundlesByPageLink() {

        TenantId tenantId = new TenantId(ModelConstants.NULL_UUID);

        List<WidgetsBundle> systemWidgetsBundles = this.widgetsBundleService.findSystemWidgetsBundles(tenantId);
        List<WidgetsBundle> createdWidgetsBundles = new ArrayList<>();
        for (int i = 0; i < 235; i++) {
            WidgetsBundle widgetsBundle = new WidgetsBundle();
            widgetsBundle.setTenantId(tenantId);
            widgetsBundle.setTitle("Widgets bundle " + i);
            createdWidgetsBundles.add(this.widgetsBundleService.saveWidgetsBundle(widgetsBundle));
        }

        List<WidgetsBundle> widgetsBundles = new ArrayList<>(createdWidgetsBundles);
        widgetsBundles.addAll(systemWidgetsBundles);

        List<WidgetsBundle> loadedWidgetsBundles = new ArrayList<>();
        TextPageLink pageLink = new TextPageLink(19);
        TextPageData<WidgetsBundle> pageData = null;
        do {
            pageData = this.widgetsBundleService.findSystemWidgetsBundlesByPageLink(tenantId, pageLink);
            loadedWidgetsBundles.addAll(pageData.getData());
            if (pageData.hasNext()) {
                pageLink = pageData.getNextPageLink();
            }
        } while (pageData.hasNext());

        Collections.sort(widgetsBundles, this.idComparator);
        Collections.sort(loadedWidgetsBundles, this.idComparator);

        Assert.assertEquals(widgetsBundles, loadedWidgetsBundles);

        for (WidgetsBundle widgetsBundle : createdWidgetsBundles) {
            this.widgetsBundleService.deleteWidgetsBundle(tenantId, widgetsBundle.getId());
        }

        loadedWidgetsBundles = this.widgetsBundleService.findSystemWidgetsBundles(tenantId);

        Collections.sort(systemWidgetsBundles, this.idComparator);
        Collections.sort(loadedWidgetsBundles, this.idComparator);

        Assert.assertEquals(systemWidgetsBundles, loadedWidgetsBundles);
    }

    @Test
    public void testFindSystemWidgetsBundles() {
        TenantId tenantId = new TenantId(ModelConstants.NULL_UUID);

        List<WidgetsBundle> systemWidgetsBundles = this.widgetsBundleService.findSystemWidgetsBundles(tenantId);

        List<WidgetsBundle> createdWidgetsBundles = new ArrayList<>();
        for (int i = 0; i < 135; i++) {
            WidgetsBundle widgetsBundle = new WidgetsBundle();
            widgetsBundle.setTenantId(tenantId);
            widgetsBundle.setTitle("Widgets bundle " + i);
            createdWidgetsBundles.add(this.widgetsBundleService.saveWidgetsBundle(widgetsBundle));
        }

        List<WidgetsBundle> widgetsBundles = new ArrayList<>(createdWidgetsBundles);
        widgetsBundles.addAll(systemWidgetsBundles);

        List<WidgetsBundle> loadedWidgetsBundles = this.widgetsBundleService.findSystemWidgetsBundles(tenantId);

        Collections.sort(widgetsBundles, this.idComparator);
        Collections.sort(loadedWidgetsBundles, this.idComparator);

        Assert.assertEquals(widgetsBundles, loadedWidgetsBundles);

        for (WidgetsBundle widgetsBundle : createdWidgetsBundles) {
            this.widgetsBundleService.deleteWidgetsBundle(tenantId, widgetsBundle.getId());
        }

        loadedWidgetsBundles = this.widgetsBundleService.findSystemWidgetsBundles(tenantId);

        Collections.sort(systemWidgetsBundles, this.idComparator);
        Collections.sort(loadedWidgetsBundles, this.idComparator);

        Assert.assertEquals(systemWidgetsBundles, loadedWidgetsBundles);
    }

    @Test
    public void testFindTenantWidgetsBundlesByTenantId() {
        Tenant tenant = new Tenant();
        tenant.setTitle("Test tenant");
        tenant = this.tenantService.saveTenant(tenant);

        TenantId tenantId = tenant.getId();

        List<WidgetsBundle> widgetsBundles = new ArrayList<>();
        for (int i = 0; i < 127; i++) {
            WidgetsBundle widgetsBundle = new WidgetsBundle();
            widgetsBundle.setTenantId(tenantId);
            widgetsBundle.setTitle("Widgets bundle " + i);
            widgetsBundles.add(this.widgetsBundleService.saveWidgetsBundle(widgetsBundle));
        }

        List<WidgetsBundle> loadedWidgetsBundles = new ArrayList<>();
        TextPageLink pageLink = new TextPageLink(11);
        TextPageData<WidgetsBundle> pageData = null;
        do {
            pageData = this.widgetsBundleService.findTenantWidgetsBundlesByTenantId(tenantId, pageLink);
            loadedWidgetsBundles.addAll(pageData.getData());
            if (pageData.hasNext()) {
                pageLink = pageData.getNextPageLink();
            }
        } while (pageData.hasNext());

        Collections.sort(widgetsBundles, this.idComparator);
        Collections.sort(loadedWidgetsBundles, this.idComparator);

        Assert.assertEquals(widgetsBundles, loadedWidgetsBundles);

        this.widgetsBundleService.deleteWidgetsBundlesByTenantId(tenantId);

        pageLink = new TextPageLink(15);
        pageData = this.widgetsBundleService.findTenantWidgetsBundlesByTenantId(tenantId, pageLink);
        Assert.assertFalse(pageData.hasNext());
        Assert.assertTrue(pageData.getData().isEmpty());

        this.tenantService.deleteTenant(tenantId);
    }

    @Test
    public void testFindAllWidgetsBundlesByTenantIdAndPageLink() {

        List<WidgetsBundle> systemWidgetsBundles = this.widgetsBundleService.findSystemWidgetsBundles(this.tenantId);

        Tenant tenant = new Tenant();
        tenant.setTitle("Test tenant");
        tenant = this.tenantService.saveTenant(tenant);

        TenantId tenantId = tenant.getId();
        TenantId systemTenantId = new TenantId(ModelConstants.NULL_UUID);

        List<WidgetsBundle> createdWidgetsBundles = new ArrayList<>();
        List<WidgetsBundle> createdSystemWidgetsBundles = new ArrayList<>();
        for (int i = 0; i < 177; i++) {
            WidgetsBundle widgetsBundle = new WidgetsBundle();
            widgetsBundle.setTenantId(i % 2 == 0 ? tenantId : systemTenantId);
            widgetsBundle.setTitle((i % 2 == 0 ? "Widgets bundle " : "System widget bundle ") + i);
            WidgetsBundle savedWidgetsBundle = this.widgetsBundleService.saveWidgetsBundle(widgetsBundle);
            createdWidgetsBundles.add(savedWidgetsBundle);
            if (i % 2 == 1) {
                createdSystemWidgetsBundles.add(savedWidgetsBundle);
            }
        }

        List<WidgetsBundle> widgetsBundles = new ArrayList<>(createdWidgetsBundles);
        widgetsBundles.addAll(systemWidgetsBundles);

        List<WidgetsBundle> loadedWidgetsBundles = new ArrayList<>();
        TextPageLink pageLink = new TextPageLink(17);
        TextPageData<WidgetsBundle> pageData = null;
        do {
            pageData = this.widgetsBundleService.findAllTenantWidgetsBundlesByTenantIdAndPageLink(tenantId, pageLink);
            loadedWidgetsBundles.addAll(pageData.getData());
            if (pageData.hasNext()) {
                pageLink = pageData.getNextPageLink();
            }
        } while (pageData.hasNext());

        Collections.sort(widgetsBundles, this.idComparator);
        Collections.sort(loadedWidgetsBundles, this.idComparator);

        Assert.assertEquals(widgetsBundles, loadedWidgetsBundles);

        this.widgetsBundleService.deleteWidgetsBundlesByTenantId(tenantId);

        loadedWidgetsBundles.clear();
        pageLink = new TextPageLink(14);
        do {
            pageData = this.widgetsBundleService.findAllTenantWidgetsBundlesByTenantIdAndPageLink(tenantId, pageLink);
            loadedWidgetsBundles.addAll(pageData.getData());
            if (pageData.hasNext()) {
                pageLink = pageData.getNextPageLink();
            }
        } while (pageData.hasNext());

        List<WidgetsBundle> allSystemWidgetsBundles = new ArrayList<>(systemWidgetsBundles);
        allSystemWidgetsBundles.addAll(createdSystemWidgetsBundles);

        Collections.sort(allSystemWidgetsBundles, this.idComparator);
        Collections.sort(loadedWidgetsBundles, this.idComparator);

        Assert.assertEquals(allSystemWidgetsBundles, loadedWidgetsBundles);

        for (WidgetsBundle widgetsBundle : createdSystemWidgetsBundles) {
            this.widgetsBundleService.deleteWidgetsBundle(tenantId, widgetsBundle.getId());
        }

        loadedWidgetsBundles.clear();
        pageLink = new TextPageLink(18);
        do {
            pageData = this.widgetsBundleService.findAllTenantWidgetsBundlesByTenantIdAndPageLink(tenantId, pageLink);
            loadedWidgetsBundles.addAll(pageData.getData());
            if (pageData.hasNext()) {
                pageLink = pageData.getNextPageLink();
            }
        } while (pageData.hasNext());

        Collections.sort(systemWidgetsBundles, this.idComparator);
        Collections.sort(loadedWidgetsBundles, this.idComparator);

        Assert.assertEquals(systemWidgetsBundles, loadedWidgetsBundles);

        this.tenantService.deleteTenant(tenantId);
    }

    @Test
    public void testFindAllWidgetsBundlesByTenantId() {

        List<WidgetsBundle> systemWidgetsBundles = this.widgetsBundleService.findSystemWidgetsBundles(this.tenantId);

        Tenant tenant = new Tenant();
        tenant.setTitle("Test tenant");
        tenant = this.tenantService.saveTenant(tenant);

        TenantId tenantId = tenant.getId();
        TenantId systemTenantId = new TenantId(ModelConstants.NULL_UUID);

        List<WidgetsBundle> createdWidgetsBundles = new ArrayList<>();
        List<WidgetsBundle> createdSystemWidgetsBundles = new ArrayList<>();
        for (int i = 0; i < 277; i++) {
            WidgetsBundle widgetsBundle = new WidgetsBundle();
            widgetsBundle.setTenantId(i % 2 == 0 ? tenantId : systemTenantId);
            widgetsBundle.setTitle((i % 2 == 0 ? "Widgets bundle " : "System widget bundle ") + i);
            WidgetsBundle savedWidgetsBundle = this.widgetsBundleService.saveWidgetsBundle(widgetsBundle);
            createdWidgetsBundles.add(savedWidgetsBundle);
            if (i % 2 == 1) {
                createdSystemWidgetsBundles.add(savedWidgetsBundle);
            }
        }

        List<WidgetsBundle> widgetsBundles = new ArrayList<>(createdWidgetsBundles);
        widgetsBundles.addAll(systemWidgetsBundles);

        List<WidgetsBundle> loadedWidgetsBundles = this.widgetsBundleService.findAllTenantWidgetsBundlesByTenantId(
                tenantId);

        Collections.sort(widgetsBundles, this.idComparator);
        Collections.sort(loadedWidgetsBundles, this.idComparator);

        Assert.assertEquals(widgetsBundles, loadedWidgetsBundles);

        this.widgetsBundleService.deleteWidgetsBundlesByTenantId(tenantId);

        loadedWidgetsBundles = this.widgetsBundleService.findAllTenantWidgetsBundlesByTenantId(tenantId);

        List<WidgetsBundle> allSystemWidgetsBundles = new ArrayList<>(systemWidgetsBundles);
        allSystemWidgetsBundles.addAll(createdSystemWidgetsBundles);

        Collections.sort(allSystemWidgetsBundles, this.idComparator);
        Collections.sort(loadedWidgetsBundles, this.idComparator);

        Assert.assertEquals(allSystemWidgetsBundles, loadedWidgetsBundles);

        for (WidgetsBundle widgetsBundle : createdSystemWidgetsBundles) {
            this.widgetsBundleService.deleteWidgetsBundle(tenantId, widgetsBundle.getId());
        }

        loadedWidgetsBundles = this.widgetsBundleService.findAllTenantWidgetsBundlesByTenantId(tenantId);

        Collections.sort(systemWidgetsBundles, this.idComparator);
        Collections.sort(loadedWidgetsBundles, this.idComparator);

        Assert.assertEquals(systemWidgetsBundles, loadedWidgetsBundles);

        this.tenantService.deleteTenant(tenantId);
    }

}
