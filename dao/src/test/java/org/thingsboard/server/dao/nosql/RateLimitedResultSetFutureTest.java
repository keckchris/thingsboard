/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.dao.nosql;


import com.datastax.driver.core.*;
import com.datastax.driver.core.exceptions.UnsupportedFeatureException;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.MoreExecutors;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import org.thingsboard.server.dao.exception.BufferLimitException;
import org.thingsboard.server.dao.util.AsyncRateLimiter;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeoutException;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
public class RateLimitedResultSetFutureTest {

    private RateLimitedResultSetFuture resultSetFuture;

    @Mock private AsyncRateLimiter rateLimiter;
    @Mock private Session session;
    @Mock private Statement statement;
    @Mock private ResultSetFuture realFuture;
    @Mock private ResultSet rows;
    @Mock private Row row;

    @Test
    public void doNotReleasePermissionIfRateLimitFutureFailed() throws InterruptedException {
        when(this.rateLimiter.acquireAsync()).thenReturn(Futures.immediateFailedFuture(new BufferLimitException()));
        this.resultSetFuture = new RateLimitedResultSetFuture(this.session, this.rateLimiter, this.statement);
        Thread.sleep(1000L);
        verify(this.rateLimiter).acquireAsync();
        try {
            assertTrue(this.resultSetFuture.isDone());
            fail();
        } catch (Exception e) {
            assertTrue(e instanceof IllegalStateException);
            Throwable actualCause = e.getCause();
            assertTrue(actualCause instanceof ExecutionException);
        }
        verifyNoMoreInteractions(this.session, this.rateLimiter, this.statement);

    }

    @Test
    public void getUninterruptiblyDelegateToCassandra() {
        when(this.rateLimiter.acquireAsync()).thenReturn(Futures.immediateFuture(null));
        when(this.session.executeAsync(this.statement)).thenReturn(this.realFuture);
        Mockito.doAnswer((Answer<Void>) invocation -> {
            Object[] args = invocation.getArguments();
            Runnable task = (Runnable) args[0];
            task.run();
            return null;
        }).when(this.realFuture).addListener(Mockito.any(), Mockito.any());

        when(this.realFuture.getUninterruptibly()).thenReturn(this.rows);

        this.resultSetFuture = new RateLimitedResultSetFuture(this.session, this.rateLimiter, this.statement);
        ResultSet actual = this.resultSetFuture.getUninterruptibly();
        assertSame(this.rows, actual);
        verify(this.rateLimiter, times(1)).acquireAsync();
        verify(this.rateLimiter, times(1)).release();
    }

    @Test
    public void addListenerAllowsFutureTransformation() throws InterruptedException, ExecutionException {
        when(this.rateLimiter.acquireAsync()).thenReturn(Futures.immediateFuture(null));
        when(this.session.executeAsync(this.statement)).thenReturn(this.realFuture);
        Mockito.doAnswer((Answer<Void>) invocation -> {
            Object[] args = invocation.getArguments();
            Runnable task = (Runnable) args[0];
            task.run();
            return null;
        }).when(this.realFuture).addListener(Mockito.any(), Mockito.any());

        when(this.realFuture.get()).thenReturn(this.rows);
        when(this.rows.one()).thenReturn(this.row);

        this.resultSetFuture = new RateLimitedResultSetFuture(this.session, this.rateLimiter, this.statement);

        ListenableFuture<Row> transform = Futures.transform(this.resultSetFuture, ResultSet::one);
        Row actualRow = transform.get();

        assertSame(this.row, actualRow);
        verify(this.rateLimiter, times(1)).acquireAsync();
        verify(this.rateLimiter, times(1)).release();
    }

    @Test
    public void immidiateCassandraExceptionReturnsPermit() {
        when(this.rateLimiter.acquireAsync()).thenReturn(Futures.immediateFuture(null));
        when(this.session.executeAsync(this.statement)).thenThrow(
                new UnsupportedFeatureException(ProtocolVersion.V3, "hjg"));
        this.resultSetFuture = new RateLimitedResultSetFuture(this.session, this.rateLimiter, this.statement);
        ListenableFuture<Row> transform = Futures.transform(this.resultSetFuture, ResultSet::one);
        try {
            transform.get();
            fail();
        } catch (Exception e) {
            assertTrue(e instanceof ExecutionException);
        }
        verify(this.rateLimiter, times(1)).acquireAsync();
        verify(this.rateLimiter, times(1)).release();
    }

    @Test
    public void queryTimeoutReturnsPermit() throws InterruptedException, ExecutionException {
        when(this.rateLimiter.acquireAsync()).thenReturn(Futures.immediateFuture(null));
        when(this.session.executeAsync(this.statement)).thenReturn(this.realFuture);
        Mockito.doAnswer((Answer<Void>) invocation -> {
            Object[] args = invocation.getArguments();
            Runnable task = (Runnable) args[0];
            task.run();
            return null;
        }).when(this.realFuture).addListener(Mockito.any(), Mockito.any());

        when(this.realFuture.get()).thenThrow(new ExecutionException("Fail", new TimeoutException("timeout")));
        this.resultSetFuture = new RateLimitedResultSetFuture(this.session, this.rateLimiter, this.statement);
        ListenableFuture<Row> transform = Futures.transform(this.resultSetFuture, ResultSet::one);
        try {
            transform.get();
            fail();
        } catch (Exception e) {
            assertTrue(e instanceof ExecutionException);
        }
        verify(this.rateLimiter, times(1)).acquireAsync();
        verify(this.rateLimiter, times(1)).release();
    }

    @Test
    public void expiredQueryReturnPermit() {
        CountDownLatch latch = new CountDownLatch(1);
        ListenableFuture<Void> future = MoreExecutors.listeningDecorator(Executors.newFixedThreadPool(1)).submit(() -> {
            latch.await();
            return null;
        });
        when(this.rateLimiter.acquireAsync()).thenReturn(future);
        this.resultSetFuture = new RateLimitedResultSetFuture(this.session, this.rateLimiter, this.statement);

        ListenableFuture<Row> transform = Futures.transform(this.resultSetFuture, ResultSet::one);
        //        TimeUnit.MILLISECONDS.sleep(200);
        future.cancel(false);
        latch.countDown();

        try {
            transform.get();
            fail();
        } catch (Exception e) {
            assertTrue(e instanceof ExecutionException);
        }
        verify(this.rateLimiter, times(1)).acquireAsync();
        verify(this.rateLimiter, times(1)).release();
    }

}
