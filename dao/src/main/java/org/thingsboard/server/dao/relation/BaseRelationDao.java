/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.dao.relation;


import com.datastax.driver.core.*;
import com.datastax.driver.core.querybuilder.QueryBuilder;
import com.datastax.driver.core.querybuilder.Select;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.common.util.concurrent.ListenableFuture;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.thingsboard.server.common.data.EntityType;
import org.thingsboard.server.common.data.id.EntityId;
import org.thingsboard.server.common.data.id.EntityIdFactory;
import org.thingsboard.server.common.data.id.TenantId;
import org.thingsboard.server.common.data.page.TimePageLink;
import org.thingsboard.server.common.data.relation.EntityRelation;
import org.thingsboard.server.common.data.relation.RelationTypeGroup;
import org.thingsboard.server.dao.model.ModelConstants;
import org.thingsboard.server.dao.model.type.RelationTypeGroupCodec;
import org.thingsboard.server.dao.nosql.CassandraAbstractAsyncDao;
import org.thingsboard.server.dao.nosql.CassandraAbstractSearchTimeDao;
import org.thingsboard.server.dao.util.NoSqlDao;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.datastax.driver.core.querybuilder.QueryBuilder.eq;


/**
 * Created by ashvayka on 25.04.17.
 */
@Component
@Slf4j
@NoSqlDao
public class BaseRelationDao extends CassandraAbstractAsyncDao implements RelationDao {

    public static final String FROM = " FROM ";
    public static final String WHERE = " WHERE ";
    public static final String AND = " AND ";
    public static final String EQUAL_TO_PARAM = " = ? ";
    private static final String SELECT_COLUMNS =
            "SELECT " + ModelConstants.RELATION_FROM_ID_PROPERTY + "," + ModelConstants.RELATION_FROM_TYPE_PROPERTY +
            "," + ModelConstants.RELATION_TO_ID_PROPERTY + "," + ModelConstants.RELATION_TO_TYPE_PROPERTY + "," +
            ModelConstants.RELATION_TYPE_GROUP_PROPERTY + "," + ModelConstants.RELATION_TYPE_PROPERTY + "," +
            ModelConstants.ADDITIONAL_INFO_PROPERTY;
    private static final RelationTypeGroupCodec relationTypeGroupCodec = new RelationTypeGroupCodec();
    private PreparedStatement saveStmt;
    private PreparedStatement findAllByFromStmt;
    private PreparedStatement findAllByFromAndTypeStmt;
    private PreparedStatement findAllByToStmt;
    private PreparedStatement findAllByToAndTypeStmt;
    private PreparedStatement checkRelationStmt;
    private PreparedStatement deleteStmt;
    private PreparedStatement deleteAllByEntityStmt;

    @PostConstruct
    public void init() {
        super.startExecutor();
    }

    @Override
    public ListenableFuture<List<EntityRelation>> findAllByFrom(TenantId tenantId,
                                                                EntityId from,
                                                                RelationTypeGroup typeGroup) {
        BoundStatement stmt = this.getFindAllByFromStmt().bind().setUUID(0, from.getId()).setString(1,
                                                                                                    from.getEntityType()
                                                                                                        .name()).set(2,
                                                                                                                     typeGroup,
                                                                                                                     relationTypeGroupCodec);
        return this.executeAsyncRead(tenantId, from, stmt);
    }

    @Override
    public ListenableFuture<List<EntityRelation>> findAllByFromAndType(TenantId tenantId,
                                                                       EntityId from,
                                                                       String relationType,
                                                                       RelationTypeGroup typeGroup) {
        BoundStatement stmt = this.getFindAllByFromAndTypeStmt()
                                  .bind()
                                  .setUUID(0, from.getId())
                                  .setString(1, from.getEntityType()
                                                    .name())
                                  .set(2, typeGroup, relationTypeGroupCodec)
                                  .setString(3, relationType);
        return this.executeAsyncRead(tenantId, from, stmt);
    }

    @Override
    public ListenableFuture<List<EntityRelation>> findAllByTo(TenantId tenantId,
                                                              EntityId to,
                                                              RelationTypeGroup typeGroup) {
        BoundStatement stmt = this.getFindAllByToStmt().bind().setUUID(0, to.getId()).setString(1, to.getEntityType()
                                                                                                     .name()).set(2,
                                                                                                                  typeGroup,
                                                                                                                  relationTypeGroupCodec);
        return this.executeAsyncRead(tenantId, to, stmt);
    }

    @Override
    public ListenableFuture<List<EntityRelation>> findAllByToAndType(TenantId tenantId,
                                                                     EntityId to,
                                                                     String relationType,
                                                                     RelationTypeGroup typeGroup) {
        BoundStatement stmt = this.getFindAllByToAndTypeStmt()
                                  .bind()
                                  .setUUID(0, to.getId())
                                  .setString(1, to.getEntityType()
                                                  .name())
                                  .set(2, typeGroup, relationTypeGroupCodec)
                                  .setString(3, relationType);
        return this.executeAsyncRead(tenantId, to, stmt);
    }

    @Override
    public ListenableFuture<Boolean> checkRelation(TenantId tenantId,
                                                   EntityId from,
                                                   EntityId to,
                                                   String relationType,
                                                   RelationTypeGroup typeGroup) {
        BoundStatement stmt = this.getCheckRelationStmt()
                                  .bind()
                                  .setUUID(0, from.getId())
                                  .setString(1, from.getEntityType()
                                                    .name())
                                  .setUUID(2, to.getId())
                                  .setString(3, to.getEntityType().name())
                                  .set(4, typeGroup, relationTypeGroupCodec)
                                  .setString(5, relationType);
        return this.getFuture(this.executeAsyncRead(tenantId, stmt), rs -> rs != null && rs.one() != null);
    }

    @Override
    public ListenableFuture<EntityRelation> getRelation(TenantId tenantId,
                                                        EntityId from,
                                                        EntityId to,
                                                        String relationType,
                                                        RelationTypeGroup typeGroup) {
        BoundStatement stmt = this.getCheckRelationStmt()
                                  .bind()
                                  .setUUID(0, from.getId())
                                  .setString(1, from.getEntityType()
                                                    .name())
                                  .setUUID(2, to.getId())
                                  .setString(3, to.getEntityType().name())
                                  .set(4, typeGroup, relationTypeGroupCodec)
                                  .setString(5, relationType);
        return this.getFuture(this.executeAsyncRead(tenantId, stmt),
                              rs -> rs != null ? this.getEntityRelation(rs.one()) : null);
    }

    @Override
    public boolean saveRelation(TenantId tenantId, EntityRelation relation) {
        BoundStatement stmt = this.getSaveRelationStatement(tenantId, relation);
        ResultSet rs = this.executeWrite(tenantId, stmt);
        return rs.wasApplied();
    }

    @Override
    public ListenableFuture<Boolean> saveRelationAsync(TenantId tenantId, EntityRelation relation) {
        BoundStatement stmt = this.getSaveRelationStatement(tenantId, relation);
        ResultSetFuture future = this.executeAsyncWrite(tenantId, stmt);
        return this.getBooleanListenableFuture(future);
    }

    @Override
    public boolean deleteRelation(TenantId tenantId, EntityRelation relation) {
        return this.deleteRelation(tenantId, relation.getFrom(), relation.getTo(), relation.getType(),
                                   relation.getTypeGroup());
    }

    @Override
    public ListenableFuture<Boolean> deleteRelationAsync(TenantId tenantId, EntityRelation relation) {
        return this.deleteRelationAsync(tenantId, relation.getFrom(), relation.getTo(), relation.getType(),
                                        relation.getTypeGroup());
    }

    @Override
    public boolean deleteRelation(TenantId tenantId,
                                  EntityId from,
                                  EntityId to,
                                  String relationType,
                                  RelationTypeGroup typeGroup) {
        BoundStatement stmt = this.getDeleteRelationStatement(tenantId, from, to, relationType, typeGroup);
        ResultSet rs = this.executeWrite(tenantId, stmt);
        return rs.wasApplied();
    }

    @Override
    public ListenableFuture<Boolean> deleteRelationAsync(TenantId tenantId,
                                                         EntityId from,
                                                         EntityId to,
                                                         String relationType,
                                                         RelationTypeGroup typeGroup) {
        BoundStatement stmt = this.getDeleteRelationStatement(tenantId, from, to, relationType, typeGroup);
        ResultSetFuture future = this.executeAsyncWrite(tenantId, stmt);
        return this.getBooleanListenableFuture(future);
    }

    @Override
    public boolean deleteOutboundRelations(TenantId tenantId, EntityId entity) {
        BoundStatement stmt = this.getDeleteAllByEntityStmt().bind().setUUID(0, entity.getId()).setString(1,
                                                                                                          entity.getEntityType()
                                                                                                                .name());
        ResultSet rs = this.executeWrite(tenantId, stmt);
        return rs.wasApplied();
    }

    @Override
    public ListenableFuture<Boolean> deleteOutboundRelationsAsync(TenantId tenantId, EntityId entity) {
        BoundStatement stmt = this.getDeleteAllByEntityStmt().bind().setUUID(0, entity.getId()).setString(1,
                                                                                                          entity.getEntityType()
                                                                                                                .name());
        ResultSetFuture future = this.executeAsyncWrite(tenantId, stmt);
        return this.getBooleanListenableFuture(future);
    }

    @Override
    public ListenableFuture<List<EntityRelation>> findRelations(TenantId tenantId,
                                                                EntityId from,
                                                                String relationType,
                                                                RelationTypeGroup typeGroup,
                                                                EntityType childType,
                                                                TimePageLink pageLink) {
        Select.Where query = CassandraAbstractSearchTimeDao.buildQuery(
                ModelConstants.RELATION_BY_TYPE_AND_CHILD_TYPE_VIEW_NAME,
                Arrays.asList(eq(ModelConstants.RELATION_FROM_ID_PROPERTY, from.getId()),
                              eq(ModelConstants.RELATION_FROM_TYPE_PROPERTY, from.getEntityType().name()),
                              eq(ModelConstants.RELATION_TYPE_GROUP_PROPERTY, typeGroup.name()),
                              eq(ModelConstants.RELATION_TYPE_PROPERTY, relationType),
                              eq(ModelConstants.RELATION_TO_TYPE_PROPERTY, childType.name())), Arrays.asList(
                        pageLink.isAscOrder() ? QueryBuilder.desc(ModelConstants.RELATION_TYPE_GROUP_PROPERTY)
                                              : QueryBuilder.asc(ModelConstants.RELATION_TYPE_GROUP_PROPERTY),
                        pageLink.isAscOrder() ? QueryBuilder.desc(ModelConstants.RELATION_TYPE_PROPERTY)
                                              : QueryBuilder.asc(ModelConstants.RELATION_TYPE_PROPERTY),
                        pageLink.isAscOrder() ? QueryBuilder.desc(ModelConstants.RELATION_TO_TYPE_PROPERTY)
                                              : QueryBuilder.asc(ModelConstants.RELATION_TO_TYPE_PROPERTY)), pageLink,
                ModelConstants.RELATION_TO_ID_PROPERTY);
        return this.getFuture(this.executeAsyncRead(tenantId, query), this::getEntityRelations);
    }

    private PreparedStatement getDeleteAllByEntityStmt() {
        if (this.deleteAllByEntityStmt == null) {
            this.deleteAllByEntityStmt = this.prepare(
                    "DELETE FROM " + ModelConstants.RELATION_COLUMN_FAMILY_NAME + WHERE +
                    ModelConstants.RELATION_FROM_ID_PROPERTY + " = ?" + AND +
                    ModelConstants.RELATION_FROM_TYPE_PROPERTY + " = ?");
        }
        return this.deleteAllByEntityStmt;
    }

    private BoundStatement getDeleteRelationStatement(TenantId tenantId,
                                                      EntityId from,
                                                      EntityId to,
                                                      String relationType,
                                                      RelationTypeGroup typeGroup) {
        BoundStatement stmt = this.getDeleteStmt()
                                  .bind()
                                  .setUUID(0, from.getId())
                                  .setString(1, from.getEntityType()
                                                    .name())
                                  .setUUID(2, to.getId())
                                  .setString(3, to.getEntityType().name())
                                  .set(4, typeGroup, relationTypeGroupCodec)
                                  .setString(5, relationType);
        return stmt;
    }

    private PreparedStatement getDeleteStmt() {
        if (this.deleteStmt == null) {
            this.deleteStmt = this.prepare("DELETE FROM " + ModelConstants.RELATION_COLUMN_FAMILY_NAME + WHERE +
                                           ModelConstants.RELATION_FROM_ID_PROPERTY + " = ?" + AND +
                                           ModelConstants.RELATION_FROM_TYPE_PROPERTY + " = ?" + AND +
                                           ModelConstants.RELATION_TO_ID_PROPERTY + " = ?" + AND +
                                           ModelConstants.RELATION_TO_TYPE_PROPERTY + " = ?" + AND +
                                           ModelConstants.RELATION_TYPE_GROUP_PROPERTY + " = ?" + AND +
                                           ModelConstants.RELATION_TYPE_PROPERTY + " = ?");
        }
        return this.deleteStmt;
    }

    private ListenableFuture<Boolean> getBooleanListenableFuture(ResultSetFuture rsFuture) {
        return this.getFuture(rsFuture, rs -> rs != null && rs.wasApplied());
    }

    private BoundStatement getSaveRelationStatement(TenantId tenantId, EntityRelation relation) {
        BoundStatement stmt = this.getSaveStmt()
                                  .bind()
                                  .setUUID(0, relation.getFrom().getId())
                                  .setString(1, relation.getFrom()
                                                        .getEntityType()
                                                        .name())
                                  .setUUID(2, relation.getTo().getId())
                                  .setString(3, relation.getTo().getEntityType().name())
                                  .set(4, relation.getTypeGroup(), relationTypeGroupCodec)
                                  .setString(5, relation.getType())
                                  .set(6, relation.getAdditionalInfo(), JsonNode.class);
        return stmt;
    }

    private PreparedStatement getSaveStmt() {
        if (this.saveStmt == null) {
            this.saveStmt = this.prepare("INSERT INTO " + ModelConstants.RELATION_COLUMN_FAMILY_NAME + " " + "(" +
                                         ModelConstants.RELATION_FROM_ID_PROPERTY + "," +
                                         ModelConstants.RELATION_FROM_TYPE_PROPERTY + "," +
                                         ModelConstants.RELATION_TO_ID_PROPERTY + "," +
                                         ModelConstants.RELATION_TO_TYPE_PROPERTY + "," +
                                         ModelConstants.RELATION_TYPE_GROUP_PROPERTY + "," +
                                         ModelConstants.RELATION_TYPE_PROPERTY + "," +
                                         ModelConstants.ADDITIONAL_INFO_PROPERTY + ")" +
                                         " VALUES(?, ?, ?, ?, ?, ?, ?)");
        }
        return this.saveStmt;
    }

    private PreparedStatement getCheckRelationStmt() {
        if (this.checkRelationStmt == null) {
            this.checkRelationStmt = this.prepare(
                    SELECT_COLUMNS + " " + FROM + ModelConstants.RELATION_COLUMN_FAMILY_NAME + " " + WHERE +
                    ModelConstants.RELATION_FROM_ID_PROPERTY + EQUAL_TO_PARAM + AND +
                    ModelConstants.RELATION_FROM_TYPE_PROPERTY + EQUAL_TO_PARAM + AND +
                    ModelConstants.RELATION_TO_ID_PROPERTY + EQUAL_TO_PARAM + AND +
                    ModelConstants.RELATION_TO_TYPE_PROPERTY + EQUAL_TO_PARAM + AND +
                    ModelConstants.RELATION_TYPE_GROUP_PROPERTY + EQUAL_TO_PARAM + AND +
                    ModelConstants.RELATION_TYPE_PROPERTY + EQUAL_TO_PARAM);
        }
        return this.checkRelationStmt;
    }

    private PreparedStatement getFindAllByToAndTypeStmt() {
        if (this.findAllByToAndTypeStmt == null) {
            this.findAllByToAndTypeStmt = this.prepare(
                    SELECT_COLUMNS + " " + FROM + ModelConstants.RELATION_REVERSE_VIEW_NAME + " " + WHERE +
                    ModelConstants.RELATION_TO_ID_PROPERTY + EQUAL_TO_PARAM + AND +
                    ModelConstants.RELATION_TO_TYPE_PROPERTY + EQUAL_TO_PARAM + AND +
                    ModelConstants.RELATION_TYPE_GROUP_PROPERTY + EQUAL_TO_PARAM + AND +
                    ModelConstants.RELATION_TYPE_PROPERTY + EQUAL_TO_PARAM);
        }
        return this.findAllByToAndTypeStmt;
    }

    private PreparedStatement getFindAllByToStmt() {
        if (this.findAllByToStmt == null) {
            this.findAllByToStmt = this.prepare(
                    SELECT_COLUMNS + " " + FROM + ModelConstants.RELATION_REVERSE_VIEW_NAME + " " + WHERE +
                    ModelConstants.RELATION_TO_ID_PROPERTY + EQUAL_TO_PARAM + AND +
                    ModelConstants.RELATION_TO_TYPE_PROPERTY + EQUAL_TO_PARAM + AND +
                    ModelConstants.RELATION_TYPE_GROUP_PROPERTY + EQUAL_TO_PARAM);
        }
        return this.findAllByToStmt;
    }

    private PreparedStatement getFindAllByFromAndTypeStmt() {
        if (this.findAllByFromAndTypeStmt == null) {
            this.findAllByFromAndTypeStmt = this.prepare(
                    SELECT_COLUMNS + " " + FROM + ModelConstants.RELATION_COLUMN_FAMILY_NAME + " " + WHERE +
                    ModelConstants.RELATION_FROM_ID_PROPERTY + EQUAL_TO_PARAM + AND +
                    ModelConstants.RELATION_FROM_TYPE_PROPERTY + EQUAL_TO_PARAM + AND +
                    ModelConstants.RELATION_TYPE_GROUP_PROPERTY + EQUAL_TO_PARAM + AND +
                    ModelConstants.RELATION_TYPE_PROPERTY + EQUAL_TO_PARAM);
        }
        return this.findAllByFromAndTypeStmt;
    }

    private PreparedStatement getFindAllByFromStmt() {
        if (this.findAllByFromStmt == null) {
            this.findAllByFromStmt = this.prepare(
                    SELECT_COLUMNS + " " + FROM + ModelConstants.RELATION_COLUMN_FAMILY_NAME + " " + WHERE +
                    ModelConstants.RELATION_FROM_ID_PROPERTY + EQUAL_TO_PARAM + AND +
                    ModelConstants.RELATION_FROM_TYPE_PROPERTY + EQUAL_TO_PARAM + AND +
                    ModelConstants.RELATION_TYPE_GROUP_PROPERTY + EQUAL_TO_PARAM);
        }
        return this.findAllByFromStmt;
    }

    private ListenableFuture<List<EntityRelation>> executeAsyncRead(TenantId tenantId,
                                                                    EntityId from,
                                                                    BoundStatement stmt) {
        log.debug("Generated query [{}] for entity {}", stmt, from);
        return this.getFuture(this.executeAsyncRead(tenantId, stmt), rs -> this.getEntityRelations(rs));
    }

    private List<EntityRelation> getEntityRelations(ResultSet rs) {
        List<Row> rows = rs.all();
        List<EntityRelation> entries = new ArrayList<>(rows.size());
        if (!rows.isEmpty()) {
            rows.forEach(row -> {
                entries.add(this.getEntityRelation(row));
            });
        }
        return entries;
    }

    private EntityRelation getEntityRelation(Row row) {
        EntityRelation relation = new EntityRelation();
        relation.setTypeGroup(row.get(ModelConstants.RELATION_TYPE_GROUP_PROPERTY, relationTypeGroupCodec));
        relation.setType(row.getString(ModelConstants.RELATION_TYPE_PROPERTY));
        relation.setAdditionalInfo(row.get(ModelConstants.ADDITIONAL_INFO_PROPERTY, JsonNode.class));
        relation.setFrom(this.toEntity(row, ModelConstants.RELATION_FROM_ID_PROPERTY,
                                       ModelConstants.RELATION_FROM_TYPE_PROPERTY));
        relation.setTo(
                this.toEntity(row, ModelConstants.RELATION_TO_ID_PROPERTY, ModelConstants.RELATION_TO_TYPE_PROPERTY));
        return relation;
    }

    private EntityId toEntity(Row row, String uuidColumn, String typeColumn) {
        return EntityIdFactory.getByTypeAndUuid(row.getString(typeColumn), row.getUUID(uuidColumn));
    }

}
