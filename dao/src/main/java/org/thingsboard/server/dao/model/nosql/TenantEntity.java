/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.dao.model.nosql;


import com.datastax.driver.core.utils.UUIDs;
import com.datastax.driver.mapping.annotations.Column;
import com.datastax.driver.mapping.annotations.PartitionKey;
import com.datastax.driver.mapping.annotations.Table;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.thingsboard.server.common.data.Tenant;
import org.thingsboard.server.common.data.id.TenantId;
import org.thingsboard.server.dao.model.SearchTextEntity;
import org.thingsboard.server.dao.model.type.JsonCodec;

import java.util.UUID;

import static org.thingsboard.server.dao.model.ModelConstants.*;


@Table(name = TENANT_COLUMN_FAMILY_NAME)
@EqualsAndHashCode
@ToString
public final class TenantEntity implements SearchTextEntity<Tenant> {

    @PartitionKey(value = 0)
    @Column(name = ID_PROPERTY)
    private UUID id;

    @Column(name = TENANT_TITLE_PROPERTY) private String title;

    @Column(name = SEARCH_TEXT_PROPERTY) private String searchText;

    @Column(name = TENANT_REGION_PROPERTY) private String region;

    @Column(name = COUNTRY_PROPERTY) private String country;

    @Column(name = STATE_PROPERTY) private String state;

    @Column(name = CITY_PROPERTY) private String city;

    @Column(name = ADDRESS_PROPERTY) private String address;

    @Column(name = ADDRESS2_PROPERTY) private String address2;

    @Column(name = ZIP_PROPERTY) private String zip;

    @Column(name = PHONE_PROPERTY) private String phone;

    @Column(name = EMAIL_PROPERTY) private String email;

    @Column(name = TENANT_ADDITIONAL_INFO_PROPERTY, codec = JsonCodec.class) private JsonNode additionalInfo;

    public TenantEntity() {
        super();
    }

    public TenantEntity(Tenant tenant) {
        if (tenant.getId() != null) {
            this.id = tenant.getId().getId();
        }
        this.title = tenant.getTitle();
        this.region = tenant.getRegion();
        this.country = tenant.getCountry();
        this.state = tenant.getState();
        this.city = tenant.getCity();
        this.address = tenant.getAddress();
        this.address2 = tenant.getAddress2();
        this.zip = tenant.getZip();
        this.phone = tenant.getPhone();
        this.email = tenant.getEmail();
        this.additionalInfo = tenant.getAdditionalInfo();
    }

    public UUID getId() {
        return this.id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getRegion() {
        return this.region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCountry() {
        return this.country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return this.state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return this.city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress2() {
        return this.address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getZip() {
        return this.zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getPhone() {
        return this.phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public JsonNode getAdditionalInfo() {
        return this.additionalInfo;
    }

    public void setAdditionalInfo(JsonNode additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    @Override
    public String getSearchTextSource() {
        return this.getTitle();
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSearchText() {
        return this.searchText;
    }

    @Override
    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    @Override
    public Tenant toData() {
        Tenant tenant = new Tenant(new TenantId(this.id));
        tenant.setCreatedTime(UUIDs.unixTimestamp(this.id));
        tenant.setTitle(this.title);
        tenant.setRegion(this.region);
        tenant.setCountry(this.country);
        tenant.setState(this.state);
        tenant.setCity(this.city);
        tenant.setAddress(this.address);
        tenant.setAddress2(this.address2);
        tenant.setZip(this.zip);
        tenant.setPhone(this.phone);
        tenant.setEmail(this.email);
        tenant.setAdditionalInfo(this.additionalInfo);
        return tenant;
    }


}
