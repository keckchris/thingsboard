/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.dao.timeseries;


import lombok.Getter;
import org.thingsboard.server.common.data.kv.ReadTsKvQuery;
import org.thingsboard.server.common.data.kv.TsKvEntry;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.thingsboard.server.dao.timeseries.CassandraBaseTimeseriesDao.DESC_ORDER;


/**
 * Created by ashvayka on 21.02.17.
 */
public class TsKvQueryCursor extends QueryCursor {

    @Getter private final List<TsKvEntry> data;
    @Getter private String orderBy;

    private int partitionIndex;
    private int currentLimit;

    public TsKvQueryCursor(String entityType, UUID entityId, ReadTsKvQuery baseQuery, List<Long> partitions) {
        super(entityType, entityId, baseQuery, partitions);
        this.orderBy = baseQuery.getOrderBy();
        this.partitionIndex = this.isDesc() ? partitions.size() - 1 : 0;
        this.data = new ArrayList<>();
        this.currentLimit = baseQuery.getLimit();
    }

    private boolean isDesc() {
        return this.orderBy.equals(DESC_ORDER);
    }

    @Override
    public boolean hasNextPartition() {
        return this.isDesc() ? this.partitionIndex >= 0 : this.partitionIndex <= this.partitions.size() - 1;
    }

    @Override
    public long getNextPartition() {
        long partition = this.partitions.get(this.partitionIndex);
        if (this.isDesc()) {
            this.partitionIndex--;
        }
        else {
            this.partitionIndex++;
        }
        return partition;
    }

    public boolean isFull() {
        return this.currentLimit <= 0;
    }

    public int getCurrentLimit() {
        return this.currentLimit;
    }

    public void addData(List<TsKvEntry> newData) {
        this.currentLimit -= newData.size();
        this.data.addAll(newData);
    }

}
