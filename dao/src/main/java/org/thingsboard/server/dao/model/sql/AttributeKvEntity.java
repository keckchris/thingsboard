/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.dao.model.sql;


import lombok.Data;
import org.thingsboard.server.common.data.kv.*;
import org.thingsboard.server.dao.model.ToData;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

import static org.thingsboard.server.dao.model.ModelConstants.*;


@Data
@Entity
@Table(name = "attribute_kv")
public class AttributeKvEntity implements ToData<AttributeKvEntry>, Serializable {

    @EmbeddedId private AttributeKvCompositeKey id;

    @Column(name = BOOLEAN_VALUE_COLUMN) private Boolean booleanValue;

    @Column(name = STRING_VALUE_COLUMN) private String strValue;

    @Column(name = LONG_VALUE_COLUMN) private Long longValue;

    @Column(name = DOUBLE_VALUE_COLUMN) private Double doubleValue;

    @Column(name = LAST_UPDATE_TS_COLUMN) private Long lastUpdateTs;

    @Override
    public AttributeKvEntry toData() {
        KvEntry kvEntry = null;
        if (this.strValue != null) {
            kvEntry = new StringDataEntry(this.id.getAttributeKey(), this.strValue);
        }
        else if (this.booleanValue != null) {
            kvEntry = new BooleanDataEntry(this.id.getAttributeKey(), this.booleanValue);
        }
        else if (this.doubleValue != null) {
            kvEntry = new DoubleDataEntry(this.id.getAttributeKey(), this.doubleValue);
        }
        else if (this.longValue != null) {
            kvEntry = new LongDataEntry(this.id.getAttributeKey(), this.longValue);
        }
        return new BaseAttributeKvEntry(kvEntry, this.lastUpdateTs);
    }

}
