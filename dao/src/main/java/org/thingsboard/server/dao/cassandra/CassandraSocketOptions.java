/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.dao.cassandra;


import com.datastax.driver.core.SocketOptions;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.thingsboard.server.dao.util.NoSqlAnyDao;

import javax.annotation.PostConstruct;


@Component
@Configuration
@Data
@NoSqlAnyDao
public class CassandraSocketOptions {

    @Value("${cassandra.socket.connect_timeout}") private int connectTimeoutMillis;
    @Value("${cassandra.socket.read_timeout}") private int readTimeoutMillis;
    @Value("${cassandra.socket.keep_alive}") private Boolean keepAlive;
    @Value("${cassandra.socket.reuse_address}") private Boolean reuseAddress;
    @Value("${cassandra.socket.so_linger}") private Integer soLinger;
    @Value("${cassandra.socket.tcp_no_delay}") private Boolean tcpNoDelay;
    @Value("${cassandra.socket.receive_buffer_size}") private Integer receiveBufferSize;
    @Value("${cassandra.socket.send_buffer_size}") private Integer sendBufferSize;

    private SocketOptions opts;

    @PostConstruct
    public void initOpts() {
        this.opts = new SocketOptions();
        this.opts.setConnectTimeoutMillis(this.connectTimeoutMillis);
        this.opts.setReadTimeoutMillis(this.readTimeoutMillis);
        if (this.keepAlive != null) {
            this.opts.setKeepAlive(this.keepAlive);
        }
        if (this.reuseAddress != null) {
            this.opts.setReuseAddress(this.reuseAddress);
        }
        if (this.soLinger != null) {
            this.opts.setSoLinger(this.soLinger);
        }
        if (this.tcpNoDelay != null) {
            this.opts.setTcpNoDelay(this.tcpNoDelay);
        }
        if (this.receiveBufferSize != null) {
            this.opts.setReceiveBufferSize(this.receiveBufferSize);
        }
        if (this.sendBufferSize != null) {
            this.opts.setSendBufferSize(this.sendBufferSize);
        }
    }

}
