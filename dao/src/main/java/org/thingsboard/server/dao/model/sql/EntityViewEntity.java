/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.dao.model.sql;


import com.datastax.driver.core.utils.UUIDs;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.thingsboard.server.common.data.EntityType;
import org.thingsboard.server.common.data.EntityView;
import org.thingsboard.server.common.data.id.CustomerId;
import org.thingsboard.server.common.data.id.EntityIdFactory;
import org.thingsboard.server.common.data.id.EntityViewId;
import org.thingsboard.server.common.data.id.TenantId;
import org.thingsboard.server.common.data.objects.TelemetryEntityView;
import org.thingsboard.server.dao.model.BaseSqlEntity;
import org.thingsboard.server.dao.model.ModelConstants;
import org.thingsboard.server.dao.model.SearchTextEntity;
import org.thingsboard.server.dao.util.mapping.JsonStringType;

import javax.persistence.*;
import java.io.IOException;

import static org.thingsboard.server.dao.model.ModelConstants.ENTITY_TYPE_PROPERTY;


/**
 * Created by Victor Basanets on 8/30/2017.
 */

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@TypeDef(name = "json", typeClass = JsonStringType.class)
@Table(name = ModelConstants.ENTITY_VIEW_TABLE_FAMILY_NAME)
@Slf4j
public class EntityViewEntity extends BaseSqlEntity<EntityView> implements SearchTextEntity<EntityView> {

    private static final ObjectMapper mapper = new ObjectMapper();
    @Column(name = ModelConstants.ENTITY_VIEW_ENTITY_ID_PROPERTY) private String entityId;
    @Enumerated(EnumType.STRING)
    @Column(name = ENTITY_TYPE_PROPERTY)
    private EntityType entityType;
    @Column(name = ModelConstants.ENTITY_VIEW_TENANT_ID_PROPERTY) private String tenantId;
    @Column(name = ModelConstants.ENTITY_VIEW_CUSTOMER_ID_PROPERTY) private String customerId;
    @Column(name = ModelConstants.DEVICE_TYPE_PROPERTY) private String type;
    @Column(name = ModelConstants.ENTITY_VIEW_NAME_PROPERTY) private String name;
    @Column(name = ModelConstants.ENTITY_VIEW_KEYS_PROPERTY) private String keys;
    @Column(name = ModelConstants.ENTITY_VIEW_START_TS_PROPERTY) private long startTs;
    @Column(name = ModelConstants.ENTITY_VIEW_END_TS_PROPERTY) private long endTs;
    @Column(name = ModelConstants.SEARCH_TEXT_PROPERTY) private String searchText;
    @Type(type = "json")
    @Column(name = ModelConstants.ENTITY_VIEW_ADDITIONAL_INFO_PROPERTY)
    private JsonNode additionalInfo;

    public EntityViewEntity() {
        super();
    }

    public EntityViewEntity(EntityView entityView) {
        if (entityView.getId() != null) {
            this.setId(entityView.getId().getId());
        }
        if (entityView.getEntityId() != null) {
            this.entityId = this.toString(entityView.getEntityId().getId());
            this.entityType = entityView.getEntityId().getEntityType();
        }
        if (entityView.getTenantId() != null) {
            this.tenantId = this.toString(entityView.getTenantId().getId());
        }
        if (entityView.getCustomerId() != null) {
            this.customerId = this.toString(entityView.getCustomerId().getId());
        }
        this.type = entityView.getType();
        this.name = entityView.getName();
        try {
            this.keys = mapper.writeValueAsString(entityView.getKeys());
        } catch (IOException e) {
            log.error("Unable to serialize entity view keys!", e);
        }
        this.startTs = entityView.getStartTimeMs();
        this.endTs = entityView.getEndTimeMs();
        this.searchText = entityView.getSearchText();
        this.additionalInfo = entityView.getAdditionalInfo();
    }

    @Override
    public String getSearchTextSource() {
        return this.name;
    }

    @Override
    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    @Override
    public EntityView toData() {
        EntityView entityView = new EntityView(new EntityViewId(this.getId()));
        entityView.setCreatedTime(UUIDs.unixTimestamp(this.getId()));

        if (this.entityId != null) {
            entityView.setEntityId(
                    EntityIdFactory.getByTypeAndId(this.entityType.name(), this.toUUID(this.entityId).toString()));
        }
        if (this.tenantId != null) {
            entityView.setTenantId(new TenantId(this.toUUID(this.tenantId)));
        }
        if (this.customerId != null) {
            entityView.setCustomerId(new CustomerId(this.toUUID(this.customerId)));
        }
        entityView.setType(this.type);
        entityView.setName(this.name);
        try {
            entityView.setKeys(mapper.readValue(this.keys, TelemetryEntityView.class));
        } catch (IOException e) {
            log.error("Unable to read entity view keys!", e);
        }
        entityView.setStartTimeMs(this.startTs);
        entityView.setEndTimeMs(this.endTs);
        entityView.setAdditionalInfo(this.additionalInfo);
        return entityView;
    }

}
