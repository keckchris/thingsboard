/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.dao.nosql;


import com.datastax.driver.core.*;
import com.datastax.driver.core.exceptions.CodecNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.thingsboard.server.common.data.id.TenantId;
import org.thingsboard.server.dao.cassandra.CassandraCluster;
import org.thingsboard.server.dao.model.type.*;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;


@Slf4j
public abstract class CassandraAbstractDao {

    @Autowired protected CassandraCluster cluster;

    private ConcurrentMap<String, PreparedStatement> preparedStatementMap = new ConcurrentHashMap<>();

    @Autowired private CassandraBufferedRateExecutor rateLimiter;

    private Session session;

    private ConsistencyLevel defaultReadLevel;
    private ConsistencyLevel defaultWriteLevel;

    protected PreparedStatement prepare(String query) {
        return this.preparedStatementMap.computeIfAbsent(query, i -> this.getSession().prepare(i));
    }

    private Session getSession() {
        if (this.session == null) {
            this.session = this.cluster.getSession();
            this.defaultReadLevel = this.cluster.getDefaultReadConsistencyLevel();
            this.defaultWriteLevel = this.cluster.getDefaultWriteConsistencyLevel();
            CodecRegistry registry = this.session.getCluster().getConfiguration().getCodecRegistry();
            this.registerCodecIfNotFound(registry, new JsonCodec());
            this.registerCodecIfNotFound(registry, new DeviceCredentialsTypeCodec());
            this.registerCodecIfNotFound(registry, new AuthorityCodec());
            this.registerCodecIfNotFound(registry, new ComponentLifecycleStateCodec());
            this.registerCodecIfNotFound(registry, new ComponentTypeCodec());
            this.registerCodecIfNotFound(registry, new ComponentScopeCodec());
            this.registerCodecIfNotFound(registry, new EntityTypeCodec());
        }
        return this.session;
    }

    private void registerCodecIfNotFound(CodecRegistry registry, TypeCodec<?> codec) {
        try {
            registry.codecFor(codec.getCqlType(), codec.getJavaType());
        } catch (CodecNotFoundException e) {
            registry.register(codec);
        }
    }

    protected ResultSet executeRead(TenantId tenantId, Statement statement) {
        return this.execute(tenantId, statement, this.defaultReadLevel);
    }

    private ResultSet execute(TenantId tenantId, Statement statement, ConsistencyLevel level) {
        if (log.isDebugEnabled()) {
            log.debug("Execute cassandra statement {}", statementToString(statement));
        }
        return this.executeAsync(tenantId, statement, level).getUninterruptibly();
    }

    private static String statementToString(Statement statement) {
        if (statement instanceof BoundStatement) {
            return ((BoundStatement) statement).preparedStatement().getQueryString();
        }
        else {
            return statement.toString();
        }
    }

    private ResultSetFuture executeAsync(TenantId tenantId, Statement statement, ConsistencyLevel level) {
        if (log.isDebugEnabled()) {
            log.debug("Execute cassandra async statement {}", statementToString(statement));
        }
        if (statement.getConsistencyLevel() == null) {
            statement.setConsistencyLevel(level);
        }
        return this.rateLimiter.submit(new CassandraStatementTask(tenantId, this.getSession(), statement));
    }

    protected ResultSet executeWrite(TenantId tenantId, Statement statement) {
        return this.execute(tenantId, statement, this.defaultWriteLevel);
    }

    protected ResultSetFuture executeAsyncRead(TenantId tenantId, Statement statement) {
        return this.executeAsync(tenantId, statement, this.defaultReadLevel);
    }

    protected ResultSetFuture executeAsyncWrite(TenantId tenantId, Statement statement) {
        return this.executeAsync(tenantId, statement, this.defaultWriteLevel);
    }

}
