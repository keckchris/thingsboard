/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.dao.util;


import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.SettableFuture;
import lombok.extern.slf4j.Slf4j;
import org.thingsboard.server.common.data.id.TenantId;
import org.thingsboard.server.common.msg.tools.TbRateLimits;

import javax.annotation.Nullable;
import java.util.UUID;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;


/**
 * Created by ashvayka on 24.10.18.
 */
@Slf4j
public abstract class AbstractBufferedRateExecutor<T extends AsyncTask, F extends ListenableFuture<V>, V>
        implements BufferedRateExecutor<T, F> {

    protected final ConcurrentMap<TenantId, AtomicInteger> rateLimitedTenants = new ConcurrentHashMap<>();
    protected final AtomicInteger concurrencyLevel = new AtomicInteger();
    protected final AtomicInteger totalAdded = new AtomicInteger();
    protected final AtomicInteger totalLaunched = new AtomicInteger();
    protected final AtomicInteger totalReleased = new AtomicInteger();
    protected final AtomicInteger totalFailed = new AtomicInteger();
    protected final AtomicInteger totalExpired = new AtomicInteger();
    protected final AtomicInteger totalRejected = new AtomicInteger();
    protected final AtomicInteger totalRateLimited = new AtomicInteger();
    private final long maxWaitTime;
    private final long pollMs;
    private final BlockingQueue<AsyncTaskContext<T, V>> queue;
    private final ExecutorService dispatcherExecutor;
    private final ExecutorService callbackExecutor;
    private final ScheduledExecutorService timeoutExecutor;
    private final int concurrencyLimit;
    private final boolean perTenantLimitsEnabled;
    private final String perTenantLimitsConfiguration;
    private final ConcurrentMap<TenantId, TbRateLimits> perTenantLimits = new ConcurrentHashMap<>();

    public AbstractBufferedRateExecutor(int queueLimit,
                                        int concurrencyLimit,
                                        long maxWaitTime,
                                        int dispatcherThreads,
                                        int callbackThreads,
                                        long pollMs,
                                        boolean perTenantLimitsEnabled,
                                        String perTenantLimitsConfiguration) {
        this.maxWaitTime = maxWaitTime;
        this.pollMs = pollMs;
        this.concurrencyLimit = concurrencyLimit;
        this.queue = new LinkedBlockingDeque<>(queueLimit);
        this.dispatcherExecutor = Executors.newFixedThreadPool(dispatcherThreads);
        this.callbackExecutor = new ThreadPoolExecutor(callbackThreads, 50, 60L, TimeUnit.SECONDS,
                                                       new LinkedBlockingQueue<>());
        this.timeoutExecutor = Executors.newSingleThreadScheduledExecutor();
        this.perTenantLimitsEnabled = perTenantLimitsEnabled;
        this.perTenantLimitsConfiguration = perTenantLimitsConfiguration;
        for (int i = 0; i < dispatcherThreads; i++) {
            this.dispatcherExecutor.submit(this::dispatch);
        }
    }

    @Override
    public F submit(T task) {
        SettableFuture<V> settableFuture = this.create();
        F result = this.wrap(task, settableFuture);
        boolean perTenantLimitReached = false;
        if (this.perTenantLimitsEnabled) {
            if (task.getTenantId() == null) {
                log.info("Invalid task received: {}", task);
            }
            else if (!task.getTenantId().isNullUid()) {
                TbRateLimits rateLimits = this.perTenantLimits.computeIfAbsent(task.getTenantId(),
                                                                               id -> new TbRateLimits(
                                                                                       this.perTenantLimitsConfiguration));
                if (!rateLimits.tryConsume()) {
                    this.rateLimitedTenants.computeIfAbsent(task.getTenantId(), tId -> new AtomicInteger(0))
                                           .incrementAndGet();
                    this.totalRateLimited.incrementAndGet();
                    settableFuture.setException(new TenantRateLimitException());
                    perTenantLimitReached = true;
                }
            }
        }
        if (!perTenantLimitReached) {
            try {
                this.totalAdded.incrementAndGet();
                this.queue.add(
                        new AsyncTaskContext<>(UUID.randomUUID(), task, settableFuture, System.currentTimeMillis()));
            } catch (IllegalStateException e) {
                this.totalRejected.incrementAndGet();
                settableFuture.setException(e);
            }
        }
        return result;
    }

    protected abstract SettableFuture<V> create();

    protected abstract F wrap(T task, SettableFuture<V> future);

    public void stop() {
        if (this.dispatcherExecutor != null) {
            this.dispatcherExecutor.shutdownNow();
        }
        if (this.callbackExecutor != null) {
            this.callbackExecutor.shutdownNow();
        }
        if (this.timeoutExecutor != null) {
            this.timeoutExecutor.shutdownNow();
        }
    }

    private void dispatch() {
        log.info("Buffered rate executor thread started");
        while (!Thread.interrupted()) {
            int curLvl = this.concurrencyLevel.get();
            AsyncTaskContext<T, V> taskCtx = null;
            try {
                if (curLvl <= this.concurrencyLimit) {
                    taskCtx = this.queue.take();
                    final AsyncTaskContext<T, V> finalTaskCtx = taskCtx;
                    this.logTask("Processing", finalTaskCtx);
                    this.concurrencyLevel.incrementAndGet();
                    long timeout = finalTaskCtx.getCreateTime() + this.maxWaitTime - System.currentTimeMillis();
                    if (timeout > 0) {
                        this.totalLaunched.incrementAndGet();
                        ListenableFuture<V> result = this.execute(finalTaskCtx);
                        result = Futures.withTimeout(result, timeout, TimeUnit.MILLISECONDS, this.timeoutExecutor);
                        Futures.addCallback(result, new FutureCallback<V>() {
                            @Override
                            public void onSuccess(@Nullable V result) {
                                AbstractBufferedRateExecutor.this.logTask("Releasing", finalTaskCtx);
                                AbstractBufferedRateExecutor.this.totalReleased.incrementAndGet();
                                AbstractBufferedRateExecutor.this.concurrencyLevel.decrementAndGet();
                                finalTaskCtx.getFuture().set(result);
                            }

                            @Override
                            public void onFailure(Throwable t) {
                                if (t instanceof TimeoutException) {
                                    AbstractBufferedRateExecutor.this.logTask("Expired During Execution", finalTaskCtx);
                                }
                                else {
                                    AbstractBufferedRateExecutor.this.logTask("Failed", finalTaskCtx);
                                }
                                AbstractBufferedRateExecutor.this.totalFailed.incrementAndGet();
                                AbstractBufferedRateExecutor.this.concurrencyLevel.decrementAndGet();
                                finalTaskCtx.getFuture().setException(t);
                                log.debug("[{}] Failed to execute task: {}", finalTaskCtx.getId(),
                                          finalTaskCtx.getTask(), t);
                            }
                        }, this.callbackExecutor);
                    }
                    else {
                        this.logTask("Expired Before Execution", finalTaskCtx);
                        this.totalExpired.incrementAndGet();
                        this.concurrencyLevel.decrementAndGet();
                        taskCtx.getFuture().setException(new TimeoutException());
                    }
                }
                else {
                    Thread.sleep(this.pollMs);
                }
            } catch (InterruptedException e) {
                break;
            } catch (Throwable e) {
                if (taskCtx != null) {
                    log.debug("[{}] Failed to execute task: {}", taskCtx.getId(), taskCtx, e);
                    this.totalFailed.incrementAndGet();
                    this.concurrencyLevel.decrementAndGet();
                }
                else {
                    log.debug("Failed to queue task:", e);
                }
            }
        }
        log.info("Buffered rate executor thread stopped");
    }

    private void logTask(String action, AsyncTaskContext<T, V> taskCtx) {
        if (log.isTraceEnabled()) {
            log.trace("[{}] {} task: {}", taskCtx.getId(), action, taskCtx);
        }
        else {
            log.debug("[{}] {} task", taskCtx.getId(), action);
        }
    }

    protected abstract ListenableFuture<V> execute(AsyncTaskContext<T, V> taskCtx);

    protected int getQueueSize() {
        return this.queue.size();
    }

}
