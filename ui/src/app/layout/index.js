import "angular-breadcrumb";
import ngSanitize from "angular-sanitize";

import uiRouter from "angular-ui-router";
import thingsboardAdmin from "../admin";
import thingsboardAlarm from "../alarm";
import thingsboardApiDevice from "../api/device.service";
import thingsboardApiLogin from "../api/login.service";
import thingsboardApiUser from "../api/user.service";
import thingsboardAsset from "../asset";
import thingsboardAuditLog from "../audit";
import thingsboardDashboardAutocomplete from "../components/dashboard-autocomplete.directive";
import thingsboardOnFinishRender from "../components/finish-render.directive";
import thingsboardJsonContent from "../components/json-content.directive";
import thingsboardJsonObjectEdit from "../components/json-object-edit.directive";
import thingsboardKvMap from "../components/kv-map.directive";

import thingsboardNoAnimate from "../components/no-animate.directive";
import thingsboardSideMenu from "../components/side-menu.directive";
import thingsboardCustomer from "../customer";
import thingsboardDashboard from "../dashboard";
import thingsboardDevice from "../device";

import thingsboardEntity from "../entity";
import thingsboardEntityView from "../entity-view";
import thingsboardEvent from "../event";
import thingsboardExtension from "../extension";
import thingsboardHomeLinks from "../home";

import thingsboardJsonForm from "../jsonform";
import thingsboardProfile from "../profile";
import thingsboardRuleChain from "../rulechain";

import thingsboardMenu from "../services/menu.service";
import thingsboardTenant from "../tenant";
import thingsboardUser from "../user";
import thingsboardWidgetLibrary from "../widget";
import BreadcrumbIcon from "./breadcrumb-icon.filter";
import BreadcrumbLabel from "./breadcrumb-label.filter";
import HomeController from "./home.controller";

import HomeRoutes from "./home.routes";
/*
 * Copyright © 2016-2018 The Thingsboard Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import "./home.scss";

import thingsboardUserMenu from "./user-menu.directive";

export default angular.module("thingsboard.home", [
    uiRouter,
    ngSanitize,
    "ncy-angular-breadcrumb",
    thingsboardMenu,
    thingsboardHomeLinks,
    thingsboardUserMenu,
    thingsboardEntity,
    thingsboardEvent,
    thingsboardAlarm,
    thingsboardAuditLog,
    thingsboardExtension,
    thingsboardTenant,
    thingsboardCustomer,
    thingsboardUser,
    thingsboardAdmin,
    thingsboardProfile,
    thingsboardAsset,
    thingsboardDevice,
    thingsboardEntityView,
    thingsboardWidgetLibrary,
    thingsboardDashboard,
    thingsboardRuleChain,
    thingsboardJsonForm,
    thingsboardApiDevice,
    thingsboardApiLogin,
    thingsboardApiUser,
    thingsboardNoAnimate,
    thingsboardOnFinishRender,
    thingsboardSideMenu,
    thingsboardDashboardAutocomplete,
    thingsboardKvMap,
    thingsboardJsonObjectEdit,
    thingsboardJsonContent
])
                      .config(HomeRoutes)
                      .controller("HomeController", HomeController)
                      .filter("breadcrumbLabel", BreadcrumbLabel)
                      .filter("breadcrumbIcon", BreadcrumbIcon)
    .name;
