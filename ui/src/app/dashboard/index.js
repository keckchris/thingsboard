import uiRouter from "angular-ui-router";
import thingsboardApiCustomer from "../api/customer.service";
import thingsboardApiDashboard from "../api/dashboard.service";
import thingsboardApiUser from "../api/user.service";
import thingsboardApiWidget from "../api/widget.service";
import thingsboardTypes from "../common/types.constant";
import thingsboardDashboardSelect from "../components/dashboard-select.directive";
import thingsboardDashboard from "../components/dashboard.directive";
import thingsboardDetailsSidenav from "../components/details-sidenav.directive";
import thingsboardExpandFullscreen from "../components/expand-fullscreen.directive";

import thingsboardGrid from "../components/grid.directive";
import thingsboardRelatedEntityAutocomplete from "../components/related-entity-autocomplete.directive";
import thingsboardSocialsharePanel from "../components/socialshare-panel.directive";
import thingsboardWidgetConfig from "../components/widget/widget-config.directive";
import thingsboardWidgetsBundleSelect from "../components/widgets-bundle-select.directive";
import thingsboardImportExport from "../import-export";
import thingsboardItemBuffer from "../services/item-buffer.service";
import AddDashboardsToCustomerController from "./add-dashboards-to-customer.controller";
import AddWidgetController from "./add-widget.controller";
import DashboardSettingsController from "./dashboard-settings.controller";
import DashboardToolbar from "./dashboard-toolbar.directive";
import DashboardController from "./dashboard.controller";
import DashboardDirective from "./dashboard.directive";

import DashboardRoutes from "./dashboard.routes";
/*
 * Copyright © 2016-2018 The Thingsboard Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import "./dashboard.scss";
import {
    DashboardCardController,
    DashboardsController,
    MakeDashboardPublicDialogController
} from "./dashboards.controller";
import EditWidgetDirective from "./edit-widget.directive";
import dashboardLayouts from "./layouts";
import ManageAssignedCustomersController from "./manage-assigned-customers.controller";
import dashboardStates from "./states";

export default angular.module("thingsboard.dashboard", [
    uiRouter,
    thingsboardTypes,
    thingsboardItemBuffer,
    thingsboardImportExport,
    thingsboardGrid,
    thingsboardApiWidget,
    thingsboardApiUser,
    thingsboardApiDashboard,
    thingsboardApiCustomer,
    thingsboardDetailsSidenav,
    thingsboardWidgetConfig,
    thingsboardDashboardSelect,
    thingsboardRelatedEntityAutocomplete,
    thingsboardDashboard,
    thingsboardExpandFullscreen,
    thingsboardWidgetsBundleSelect,
    thingsboardSocialsharePanel,
    dashboardLayouts,
    dashboardStates
])
                      .config(DashboardRoutes)
                      .controller("DashboardsController", DashboardsController)
                      .controller("DashboardCardController", DashboardCardController)
                      .controller("MakeDashboardPublicDialogController", MakeDashboardPublicDialogController)
                      .controller("DashboardController", DashboardController)
                      .controller("DashboardSettingsController", DashboardSettingsController)
                      .controller("AddDashboardsToCustomerController", AddDashboardsToCustomerController)
                      .controller("ManageAssignedCustomersController", ManageAssignedCustomersController)
                      .controller("AddWidgetController", AddWidgetController)
                      .directive("tbDashboardDetails", DashboardDirective)
                      .directive("tbEditWidget", EditWidgetDirective)
                      .directive("tbDashboardToolbar", DashboardToolbar)
    .name;
