import "@flowjs/ng-flow/dist/ng-flow-standalone.min";

import angular from "angular";
import ngAnimate from "angular-animate";
import "angular-carousel";
import "angular-carousel/dist/angular-carousel.min.css";
import ngCookies from "angular-cookies";
import "angular-drag-and-drop-lists";
import FBAngular from "angular-fullscreen";
import "angular-gridster/dist/angular-gridster.min.css";
import "angular-hotkeys/build/hotkeys.min.css";
import angularJwt from "angular-jwt";
import ngMaterial from "angular-material";
import mdDataTable from "angular-material-data-table";
import "angular-material-expansion-panel";
import "angular-material-expansion-panel/dist/md-expansion-panel.min.css";
import ngMdIcons from "angular-material-icons";
import "angular-material-icons/angular-material-icons.css";
import "angular-material/angular-material.min.css";
import ngSanitize from "angular-sanitize";
import angularSocialshare from "angular-socialshare";
import ngTouch from "angular-touch";
import "angular-translate";
import "angular-translate-handler-log";
import "angular-translate-interpolation-messageformat";
import "angular-translate-loader-static-files";
import "angular-translate-storage-cookie";
import "angular-translate-storage-local";
import uiRouter from "angular-ui-router";
import "angular-websocket";
import "clipboard";

import "event-source-polyfill";
import "font-awesome/css/font-awesome.min.css";
import "material-ui";
import "md-color-picker";
import "md-color-picker/dist/mdColorPicker.min.css";
import mdPickers from "mdPickers";
import "mdPickers/dist/mdPickers.min.css";
import "ngclipboard";
import "ngFlowchart/dist/flowchart.css";
import "ngFlowchart/dist/ngFlowchart";
import react from "ngreact";
import "react";
import "react-dom";
import "react-schema-form";

import "typeface-roboto";
import vAccordion from "v-accordion";
import "v-accordion/dist/v-accordion.min.css";
import "../scss/main.scss";
import thingsboardApiAlarm from "./api/alarm.service";
import thingsboardApiAsset from "./api/asset.service";
import thingsboardApiAttribute from "./api/attribute.service";
import thingsboardApiAuditLog from "./api/audit-log.service";
import thingsboardApiComponentDescriptor from "./api/component-descriptor.service";
import thingsboardApiDevice from "./api/device.service";
import thingsboardApiEntityRelation from "./api/entity-relation.service";
import thingsboardApiEntityView from "./api/entity-view.service";
import thingsboardApiEntity from "./api/entity.service";
import thingsboardApiLogin from "./api/login.service";
import thingsboardApiRuleChain from "./api/rule-chain.service";
import thingsboardApiTime from "./api/time.service";
import thingsboardApiUser from "./api/user.service";

import AppConfig from "./app.config";
import AppRun from "./app.run";
import thingsboardDashboardUtils from "./common/dashboard-utils.service";
import thingsboardRaf from "./common/raf.provider";

import thingsboardThirdpartyFix from "./common/thirdparty-fix";
import thingsboardTypes from "./common/types.constant";
import thingsboardUtils from "./common/utils.service";
import thingsboardDialogs from "./components/datakey-config-dialog.controller";
import thingsboardKeyboardShortcut from "./components/keyboard-shortcut.filter";
import GlobalInterceptor from "./global-interceptor.service";
import thingsboardHelp from "./help/help.directive";
/*
 * Copyright © 2016-2018 The Thingsboard Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import "./ie.support";
import thingsboardHome from "./layout";
import thingsboardTranslateHandler from "./locale/translate-handler";
import thingsboardLogin from "./login";
import thingsboardClipboard from "./services/clipboard.service";
import thingsboardMenu from "./services/menu.service";
import thingsboardToast from "./services/toast";

angular.module("thingsboard", [
    ngMaterial,
    ngMdIcons,
    ngCookies,
    angularSocialshare,
    "pascalprecht.translate",
    "mdColorPicker",
    mdPickers,
    ngSanitize,
    FBAngular.name,
    vAccordion,
    ngAnimate,
    "ngWebSocket",
    angularJwt,
    "dndLists",
    mdDataTable,
    "material.components.expansionPanels",
    ngTouch,
    "angular-carousel",
    "ngclipboard",
    react.name,
    "flow",
    "flowchart",
    thingsboardThirdpartyFix,
    thingsboardTranslateHandler,
    thingsboardLogin,
    thingsboardDialogs,
    thingsboardMenu,
    thingsboardRaf,
    thingsboardUtils,
    thingsboardDashboardUtils,
    thingsboardTypes,
    thingsboardApiTime,
    thingsboardKeyboardShortcut,
    thingsboardHelp,
    thingsboardToast,
    thingsboardClipboard,
    thingsboardHome,
    thingsboardApiLogin,
    thingsboardApiDevice,
    thingsboardApiEntityView,
    thingsboardApiUser,
    thingsboardApiEntityRelation,
    thingsboardApiAsset,
    thingsboardApiAttribute,
    thingsboardApiEntity,
    thingsboardApiAlarm,
    thingsboardApiAuditLog,
    thingsboardApiComponentDescriptor,
    thingsboardApiRuleChain,
    uiRouter])
       .config(AppConfig)
       .factory("globalInterceptor", GlobalInterceptor)
       .run(AppRun);
