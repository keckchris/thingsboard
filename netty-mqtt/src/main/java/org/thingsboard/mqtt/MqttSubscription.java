/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.mqtt;


import java.util.regex.Pattern;


final class MqttSubscription {

    private final String topic;
    private final Pattern topicRegex;
    private final MqttHandler handler;

    private final boolean once;

    private boolean called;

    MqttSubscription(String topic, MqttHandler handler, boolean once) {
        if (topic == null) {
            throw new NullPointerException("topic");
        }
        if (handler == null) {
            throw new NullPointerException("handler");
        }
        this.topic = topic;
        this.handler = handler;
        this.once = once;
        this.topicRegex = Pattern.compile(topic.replace("+", "[^/]+").replace("#", ".+") + "$");
    }

    String getTopic() {
        return this.topic;
    }

    public MqttHandler getHandler() {
        return this.handler;
    }

    boolean isOnce() {
        return this.once;
    }

    boolean isCalled() {
        return this.called;
    }
    void setCalled(boolean called) {
        this.called = called;
    }
    boolean matches(String topic) {
        return this.topicRegex.matcher(topic).matches();
    }
    @Override
    public int hashCode() {
        int result = this.topic.hashCode();
        result = 31 * result + this.handler.hashCode();
        result = 31 * result + (this.once ? 1 : 0);
        return result;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }

        MqttSubscription that = (MqttSubscription) o;

        return this.once == that.once && this.topic.equals(that.topic) && this.handler.equals(that.handler);
    }

}
