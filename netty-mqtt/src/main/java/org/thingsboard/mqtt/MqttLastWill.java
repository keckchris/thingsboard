/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.mqtt;


import io.netty.handler.codec.mqtt.MqttQoS;


public final class MqttLastWill {

    private final String topic;
    private final String message;
    private final boolean retain;
    private final MqttQoS qos;

    public MqttLastWill(String topic, String message, boolean retain, MqttQoS qos) {
        if (topic == null) {
            throw new NullPointerException("topic");
        }
        if (message == null) {
            throw new NullPointerException("message");
        }
        if (qos == null) {
            throw new NullPointerException("qos");
        }
        this.topic = topic;
        this.message = message;
        this.retain = retain;
        this.qos = qos;
    }
    public static MqttLastWill.Builder builder() {
        return new MqttLastWill.Builder();
    }
    public String getTopic() {
        return this.topic;
    }
    public String getMessage() {
        return this.message;
    }
    public boolean isRetain() {
        return this.retain;
    }
    public MqttQoS getQos() {
        return this.qos;
    }
    @Override
    public int hashCode() {
        int result = this.topic.hashCode();
        result = 31 * result + this.message.hashCode();
        result = 31 * result + (this.retain ? 1 : 0);
        result = 31 * result + this.qos.hashCode();
        return result;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }

        MqttLastWill that = (MqttLastWill) o;

        if (this.retain != that.retain) {
            return false;
        }
        if (!this.topic.equals(that.topic)) {
            return false;
        }
        if (!this.message.equals(that.message)) {
            return false;
        }
        return this.qos == that.qos;

    }
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("MqttLastWill{");
        sb.append("topic='").append(this.topic).append('\'');
        sb.append(", message='").append(this.message).append('\'');
        sb.append(", retain=").append(this.retain);
        sb.append(", qos=").append(this.qos.name());
        sb.append('}');
        return sb.toString();
    }


    public static final class Builder {

        private String topic;
        private String message;
        private boolean retain;
        private MqttQoS qos;

        public String getTopic() {
            return this.topic;
        }

        public Builder setTopic(String topic) {
            if (topic == null) {
                throw new NullPointerException("topic");
            }
            this.topic = topic;
            return this;
        }

        public String getMessage() {
            return this.message;
        }

        public Builder setMessage(String message) {
            if (message == null) {
                throw new NullPointerException("message");
            }
            this.message = message;
            return this;
        }

        public boolean isRetain() {
            return this.retain;
        }

        public Builder setRetain(boolean retain) {
            this.retain = retain;
            return this;
        }

        public MqttQoS getQos() {
            return this.qos;
        }

        public Builder setQos(MqttQoS qos) {
            if (qos == null) {
                throw new NullPointerException("qos");
            }
            this.qos = qos;
            return this;
        }

        public MqttLastWill build() {
            return new MqttLastWill(this.topic, this.message, this.retain, this.qos);
        }

    }

}
