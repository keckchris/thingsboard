/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.rule.engine.api.msg;


import org.thingsboard.server.common.data.DataConstants;
import org.thingsboard.server.common.data.kv.AttributeKey;
import org.thingsboard.server.common.data.kv.AttributeKvEntry;

import java.util.*;


/**
 * @author Andrew Shvayka
 */
public class DeviceAttributes {

    private final Map<String, AttributeKvEntry> clientSideAttributesMap;
    private final Map<String, AttributeKvEntry> serverPrivateAttributesMap;
    private final Map<String, AttributeKvEntry> serverPublicAttributesMap;

    public DeviceAttributes(List<AttributeKvEntry> clientSideAttributes,
                            List<AttributeKvEntry> serverPrivateAttributes,
                            List<AttributeKvEntry> serverPublicAttributes) {
        this.clientSideAttributesMap = mapAttributes(clientSideAttributes);
        this.serverPrivateAttributesMap = mapAttributes(serverPrivateAttributes);
        this.serverPublicAttributesMap = mapAttributes(serverPublicAttributes);
    }

    private static Map<String, AttributeKvEntry> mapAttributes(List<AttributeKvEntry> attributes) {
        Map<String, AttributeKvEntry> result = new HashMap<>();
        for (AttributeKvEntry attribute : attributes) {
            result.put(attribute.getKey(), attribute);
        }
        return result;
    }

    public Collection<AttributeKvEntry> getClientSideAttributes() {
        return this.clientSideAttributesMap.values();
    }

    public Collection<AttributeKvEntry> getServerSideAttributes() {
        return this.serverPrivateAttributesMap.values();
    }

    public Collection<AttributeKvEntry> getServerSidePublicAttributes() {
        return this.serverPublicAttributesMap.values();
    }

    public Optional<AttributeKvEntry> getClientSideAttribute(String attribute) {
        return Optional.ofNullable(this.clientSideAttributesMap.get(attribute));
    }

    public Optional<AttributeKvEntry> getServerPrivateAttribute(String attribute) {
        return Optional.ofNullable(this.serverPrivateAttributesMap.get(attribute));
    }

    public Optional<AttributeKvEntry> getServerPublicAttribute(String attribute) {
        return Optional.ofNullable(this.serverPublicAttributesMap.get(attribute));
    }

    public void remove(AttributeKey key) {
        Map<String, AttributeKvEntry> map = this.getMapByScope(key.getScope());
        if (map != null) {
            map.remove(key);
        }
    }
    private Map<String, AttributeKvEntry> getMapByScope(String scope) {
        Map<String, AttributeKvEntry> map = null;
        if (scope.equalsIgnoreCase(DataConstants.CLIENT_SCOPE)) {
            map = this.clientSideAttributesMap;
        }
        else if (scope.equalsIgnoreCase(DataConstants.SHARED_SCOPE)) {
            map = this.serverPublicAttributesMap;
        }
        else if (scope.equalsIgnoreCase(DataConstants.SERVER_SCOPE)) {
            map = this.serverPrivateAttributesMap;
        }
        return map;
    }
    public void update(String scope, List<AttributeKvEntry> values) {
        Map<String, AttributeKvEntry> map = this.getMapByScope(scope);
        values.forEach(v -> map.put(v.getKey(), v));
    }
    @Override
    public String toString() {
        return "DeviceAttributes{" + "clientSideAttributesMap=" + this.clientSideAttributesMap +
               ", serverPrivateAttributesMap=" + this.serverPrivateAttributesMap + ", serverPublicAttributesMap=" +
               this.serverPublicAttributesMap + '}';
    }

}
