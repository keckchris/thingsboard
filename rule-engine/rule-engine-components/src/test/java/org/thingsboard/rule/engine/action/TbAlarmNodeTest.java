/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.rule.engine.action;


import com.datastax.driver.core.utils.UUIDs;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import org.apache.commons.lang3.NotImplementedException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import org.thingsboard.rule.engine.api.*;
import org.thingsboard.server.common.data.alarm.Alarm;
import org.thingsboard.server.common.data.id.*;
import org.thingsboard.server.common.msg.TbMsg;
import org.thingsboard.server.common.msg.TbMsgMetaData;
import org.thingsboard.server.dao.alarm.AlarmService;

import javax.script.ScriptException;
import java.io.IOException;
import java.util.concurrent.Callable;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.thingsboard.rule.engine.action.TbAbstractAlarmNode.*;
import static org.thingsboard.server.common.data.alarm.AlarmSeverity.CRITICAL;
import static org.thingsboard.server.common.data.alarm.AlarmSeverity.WARNING;
import static org.thingsboard.server.common.data.alarm.AlarmStatus.*;


@RunWith(MockitoJUnitRunner.class)
public class TbAlarmNodeTest {

    private TbAbstractAlarmNode node;

    @Mock private TbContext ctx;
    @Mock private ListeningExecutor executor;
    @Mock private AlarmService alarmService;

    @Mock private ScriptEngine detailsJs;

    private RuleChainId ruleChainId = new RuleChainId(UUIDs.timeBased());
    private RuleNodeId ruleNodeId = new RuleNodeId(UUIDs.timeBased());

    private ListeningExecutor dbExecutor;

    private EntityId originator = new DeviceId(UUIDs.timeBased());
    private TenantId tenantId = new TenantId(UUIDs.timeBased());
    private TbMsgMetaData metaData = new TbMsgMetaData();
    private String rawJson = "{\"name\": \"Vit\", \"passed\": 5}";

    @Before
    public void before() {
        this.dbExecutor = new ListeningExecutor() {
            @Override
            public <T> ListenableFuture<T> executeAsync(Callable<T> task) {
                try {
                    return Futures.immediateFuture(task.call());
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }

            @Override
            public void execute(Runnable command) {
                command.run();
            }
        };
    }

    @Test
    public void newAlarmCanBeCreated() throws ScriptException, IOException {
        this.initWithCreateAlarmScript();
        this.metaData.putValue("key", "value");
        TbMsg msg = new TbMsg(UUIDs.timeBased(), "USER", this.originator, this.metaData, this.rawJson, this.ruleChainId,
                              this.ruleNodeId, 0L);

        when(this.detailsJs.executeJson(msg)).thenReturn(null);
        when(this.alarmService.findLatestByOriginatorAndType(this.tenantId, this.originator, "SomeType")).thenReturn(
                Futures.immediateFuture(null));

        doAnswer((Answer<Alarm>) invocationOnMock -> (Alarm) (invocationOnMock.getArguments())[0]).when(
                this.alarmService).createOrUpdateAlarm(any(Alarm.class));

        this.node.onMsg(this.ctx, msg);

        verify(this.ctx).tellNext(any(), eq("Created"));

        ArgumentCaptor<TbMsg> msgCaptor = ArgumentCaptor.forClass(TbMsg.class);
        ArgumentCaptor<String> typeCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<EntityId> originatorCaptor = ArgumentCaptor.forClass(EntityId.class);
        ArgumentCaptor<TbMsgMetaData> metadataCaptor = ArgumentCaptor.forClass(TbMsgMetaData.class);
        ArgumentCaptor<String> dataCaptor = ArgumentCaptor.forClass(String.class);
        verify(this.ctx).transformMsg(msgCaptor.capture(), typeCaptor.capture(), originatorCaptor.capture(),
                                      metadataCaptor.capture(), dataCaptor.capture());

        assertEquals("ALARM", typeCaptor.getValue());
        assertEquals(this.originator, originatorCaptor.getValue());
        assertEquals("value", metadataCaptor.getValue().getValue("key"));
        assertEquals(Boolean.TRUE.toString(), metadataCaptor.getValue().getValue(IS_NEW_ALARM));
        assertNotSame(this.metaData, metadataCaptor.getValue());

        Alarm actualAlarm = new ObjectMapper().readValue(dataCaptor.getValue().getBytes(), Alarm.class);
        Alarm expectedAlarm = Alarm.builder()
                                   .tenantId(this.tenantId)
                                   .originator(this.originator)
                                   .status(ACTIVE_UNACK)
                                   .severity(CRITICAL)
                                   .propagate(true)
                                   .type("SomeType")
                                   .details(null)
                                   .build();

        assertEquals(expectedAlarm, actualAlarm);

        verify(this.executor, times(1)).executeAsync(any(Callable.class));
    }
    private void initWithCreateAlarmScript() {
        try {
            TbCreateAlarmNodeConfiguration config = new TbCreateAlarmNodeConfiguration();
            config.setPropagate(true);
            config.setSeverity(CRITICAL);
            config.setAlarmType("SomeType");
            config.setAlarmDetailsBuildJs("DETAILS");
            ObjectMapper mapper = new ObjectMapper();
            TbNodeConfiguration nodeConfiguration = new TbNodeConfiguration(mapper.valueToTree(config));

            when(this.ctx.createJsScriptEngine("DETAILS")).thenReturn(this.detailsJs);

            when(this.ctx.getTenantId()).thenReturn(this.tenantId);
            when(this.ctx.getJsExecutor()).thenReturn(this.executor);
            when(this.ctx.getAlarmService()).thenReturn(this.alarmService);
            when(this.ctx.getDbCallbackExecutor()).thenReturn(this.dbExecutor);

            this.mockJsExecutor();

            this.node = new TbCreateAlarmNode();
            this.node.init(this.ctx, nodeConfiguration);
        } catch (TbNodeException ex) {
            throw new IllegalStateException(ex);
        }
    }
    private void mockJsExecutor() {
        when(this.ctx.getJsExecutor()).thenReturn(this.executor);
        doAnswer((Answer<ListenableFuture<Boolean>>) invocationOnMock -> {
            try {
                Callable task = (Callable) (invocationOnMock.getArguments())[0];
                return Futures.immediateFuture((Boolean) task.call());
            } catch (Throwable th) {
                return Futures.immediateFailedFuture(th);
            }
        }).when(this.executor).executeAsync(any(Callable.class));
    }
    @Test
    public void buildDetailsThrowsException() throws ScriptException {
        this.initWithCreateAlarmScript();
        this.metaData.putValue("key", "value");
        TbMsg msg = new TbMsg(UUIDs.timeBased(), "USER", this.originator, this.metaData, this.rawJson, this.ruleChainId,
                              this.ruleNodeId, 0L);

        when(this.detailsJs.executeJson(msg)).thenThrow(new NotImplementedException("message"));
        when(this.alarmService.findLatestByOriginatorAndType(this.tenantId, this.originator, "SomeType")).thenReturn(
                Futures.immediateFuture(null));

        this.node.onMsg(this.ctx, msg);

        this.verifyError(msg, "message", NotImplementedException.class);

        verify(this.ctx).createJsScriptEngine("DETAILS");
        verify(this.ctx, times(1)).getJsExecutor();
        verify(this.ctx).getAlarmService();
        verify(this.ctx, times(3)).getDbCallbackExecutor();
        verify(this.ctx).getTenantId();
        verify(this.alarmService).findLatestByOriginatorAndType(this.tenantId, this.originator, "SomeType");

        verifyNoMoreInteractions(this.ctx, this.alarmService);
    }
    private void verifyError(TbMsg msg, String message, Class expectedClass) {
        ArgumentCaptor<Throwable> captor = ArgumentCaptor.forClass(Throwable.class);
        verify(this.ctx).tellFailure(same(msg), captor.capture());

        Throwable value = captor.getValue();
        assertEquals(expectedClass, value.getClass());
        assertEquals(message, value.getMessage());
    }
    @Test
    public void ifAlarmClearedCreateNew() throws ScriptException, IOException {
        this.initWithCreateAlarmScript();
        this.metaData.putValue("key", "value");
        TbMsg msg = new TbMsg(UUIDs.timeBased(), "USER", this.originator, this.metaData, this.rawJson, this.ruleChainId,
                              this.ruleNodeId, 0L);

        Alarm clearedAlarm = Alarm.builder().status(CLEARED_ACK).build();

        when(this.detailsJs.executeJson(msg)).thenReturn(null);
        when(this.alarmService.findLatestByOriginatorAndType(this.tenantId, this.originator, "SomeType")).thenReturn(
                Futures.immediateFuture(clearedAlarm));

        doAnswer((Answer<Alarm>) invocationOnMock -> (Alarm) (invocationOnMock.getArguments())[0]).when(
                this.alarmService).createOrUpdateAlarm(any(Alarm.class));

        this.node.onMsg(this.ctx, msg);

        verify(this.ctx).tellNext(any(), eq("Created"));

        ArgumentCaptor<TbMsg> msgCaptor = ArgumentCaptor.forClass(TbMsg.class);
        ArgumentCaptor<String> typeCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<EntityId> originatorCaptor = ArgumentCaptor.forClass(EntityId.class);
        ArgumentCaptor<TbMsgMetaData> metadataCaptor = ArgumentCaptor.forClass(TbMsgMetaData.class);
        ArgumentCaptor<String> dataCaptor = ArgumentCaptor.forClass(String.class);
        verify(this.ctx).transformMsg(msgCaptor.capture(), typeCaptor.capture(), originatorCaptor.capture(),
                                      metadataCaptor.capture(), dataCaptor.capture());

        assertEquals("ALARM", typeCaptor.getValue());
        assertEquals(this.originator, originatorCaptor.getValue());
        assertEquals("value", metadataCaptor.getValue().getValue("key"));
        assertEquals(Boolean.TRUE.toString(), metadataCaptor.getValue().getValue(IS_NEW_ALARM));
        assertNotSame(this.metaData, metadataCaptor.getValue());


        Alarm actualAlarm = new ObjectMapper().readValue(dataCaptor.getValue().getBytes(), Alarm.class);
        Alarm expectedAlarm = Alarm.builder()
                                   .tenantId(this.tenantId)
                                   .originator(this.originator)
                                   .status(ACTIVE_UNACK)
                                   .severity(CRITICAL)
                                   .propagate(true)
                                   .type("SomeType")
                                   .details(null)
                                   .build();

        assertEquals(expectedAlarm, actualAlarm);

        verify(this.executor, times(1)).executeAsync(any(Callable.class));
    }
    @Test
    public void alarmCanBeUpdated() throws ScriptException, IOException {
        this.initWithCreateAlarmScript();
        this.metaData.putValue("key", "value");
        TbMsg msg = new TbMsg(UUIDs.timeBased(), "USER", this.originator, this.metaData, this.rawJson, this.ruleChainId,
                              this.ruleNodeId, 0L);

        long oldEndDate = System.currentTimeMillis();
        Alarm activeAlarm = Alarm.builder().type("SomeType").tenantId(this.tenantId).originator(this.originator).status(
                ACTIVE_UNACK).severity(WARNING).endTs(oldEndDate).build();

        when(this.detailsJs.executeJson(msg)).thenReturn(null);
        when(this.alarmService.findLatestByOriginatorAndType(this.tenantId, this.originator, "SomeType")).thenReturn(
                Futures.immediateFuture(activeAlarm));

        doAnswer((Answer<Alarm>) invocationOnMock -> (Alarm) (invocationOnMock.getArguments())[0]).when(
                this.alarmService).createOrUpdateAlarm(activeAlarm);

        this.node.onMsg(this.ctx, msg);

        verify(this.ctx).tellNext(any(), eq("Updated"));

        ArgumentCaptor<TbMsg> msgCaptor = ArgumentCaptor.forClass(TbMsg.class);
        ArgumentCaptor<String> typeCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<EntityId> originatorCaptor = ArgumentCaptor.forClass(EntityId.class);
        ArgumentCaptor<TbMsgMetaData> metadataCaptor = ArgumentCaptor.forClass(TbMsgMetaData.class);
        ArgumentCaptor<String> dataCaptor = ArgumentCaptor.forClass(String.class);
        verify(this.ctx).transformMsg(msgCaptor.capture(), typeCaptor.capture(), originatorCaptor.capture(),
                                      metadataCaptor.capture(), dataCaptor.capture());

        assertEquals("ALARM", typeCaptor.getValue());
        assertEquals(this.originator, originatorCaptor.getValue());
        assertEquals("value", metadataCaptor.getValue().getValue("key"));
        assertEquals(Boolean.TRUE.toString(), metadataCaptor.getValue().getValue(IS_EXISTING_ALARM));
        assertNotSame(this.metaData, metadataCaptor.getValue());

        Alarm actualAlarm = new ObjectMapper().readValue(dataCaptor.getValue().getBytes(), Alarm.class);
        assertTrue(activeAlarm.getEndTs() > oldEndDate);
        Alarm expectedAlarm = Alarm.builder()
                                   .tenantId(this.tenantId)
                                   .originator(this.originator)
                                   .status(ACTIVE_UNACK)
                                   .severity(CRITICAL)
                                   .propagate(true)
                                   .type("SomeType")
                                   .details(null)
                                   .endTs(activeAlarm.getEndTs())
                                   .build();

        assertEquals(expectedAlarm, actualAlarm);

        verify(this.executor, times(1)).executeAsync(any(Callable.class));
    }
    @Test
    public void alarmCanBeCleared() throws IOException {
        this.initWithClearAlarmScript();
        this.metaData.putValue("key", "value");
        TbMsg msg = new TbMsg(UUIDs.timeBased(), "USER", this.originator, this.metaData, this.rawJson, this.ruleChainId,
                              this.ruleNodeId, 0L);

        long oldEndDate = System.currentTimeMillis();
        Alarm activeAlarm = Alarm.builder().type("SomeType").tenantId(this.tenantId).originator(this.originator).status(
                ACTIVE_UNACK).severity(WARNING).endTs(oldEndDate).build();

        //        when(detailsJs.executeJson(msg)).thenReturn(null);
        when(this.alarmService.findLatestByOriginatorAndType(this.tenantId, this.originator, "SomeType")).thenReturn(
                Futures.immediateFuture(activeAlarm));
        when(this.alarmService.clearAlarm(eq(activeAlarm.getTenantId()), eq(activeAlarm.getId()),
                                          org.mockito.Mockito.any(JsonNode.class), anyLong())).thenReturn(
                Futures.immediateFuture(true));
        //        doAnswer((Answer<Alarm>) invocationOnMock -> (Alarm) (invocationOnMock.getArguments())[0]).when
        // (alarmService).createOrUpdateAlarm(activeAlarm);

        this.node.onMsg(this.ctx, msg);

        verify(this.ctx).tellNext(any(), eq("Cleared"));

        ArgumentCaptor<TbMsg> msgCaptor = ArgumentCaptor.forClass(TbMsg.class);
        ArgumentCaptor<String> typeCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<EntityId> originatorCaptor = ArgumentCaptor.forClass(EntityId.class);
        ArgumentCaptor<TbMsgMetaData> metadataCaptor = ArgumentCaptor.forClass(TbMsgMetaData.class);
        ArgumentCaptor<String> dataCaptor = ArgumentCaptor.forClass(String.class);
        verify(this.ctx).transformMsg(msgCaptor.capture(), typeCaptor.capture(), originatorCaptor.capture(),
                                      metadataCaptor.capture(), dataCaptor.capture());

        assertEquals("ALARM", typeCaptor.getValue());
        assertEquals(this.originator, originatorCaptor.getValue());
        assertEquals("value", metadataCaptor.getValue().getValue("key"));
        assertEquals(Boolean.TRUE.toString(), metadataCaptor.getValue().getValue(IS_CLEARED_ALARM));
        assertNotSame(this.metaData, metadataCaptor.getValue());

        Alarm actualAlarm = new ObjectMapper().readValue(dataCaptor.getValue().getBytes(), Alarm.class);
        Alarm expectedAlarm = Alarm.builder()
                                   .tenantId(this.tenantId)
                                   .originator(this.originator)
                                   .status(CLEARED_UNACK)
                                   .severity(WARNING)
                                   .propagate(false)
                                   .type("SomeType")
                                   .details(null)
                                   .endTs(oldEndDate)
                                   .build();

        assertEquals(expectedAlarm, actualAlarm);
    }
    private void initWithClearAlarmScript() {
        try {
            TbClearAlarmNodeConfiguration config = new TbClearAlarmNodeConfiguration();
            config.setAlarmType("SomeType");
            config.setAlarmDetailsBuildJs("DETAILS");
            ObjectMapper mapper = new ObjectMapper();
            TbNodeConfiguration nodeConfiguration = new TbNodeConfiguration(mapper.valueToTree(config));

            when(this.ctx.createJsScriptEngine("DETAILS")).thenReturn(this.detailsJs);

            when(this.ctx.getTenantId()).thenReturn(this.tenantId);
            when(this.ctx.getJsExecutor()).thenReturn(this.executor);
            when(this.ctx.getAlarmService()).thenReturn(this.alarmService);
            when(this.ctx.getDbCallbackExecutor()).thenReturn(this.dbExecutor);

            this.mockJsExecutor();

            this.node = new TbClearAlarmNode();
            this.node.init(this.ctx, nodeConfiguration);
        } catch (TbNodeException ex) {
            throw new IllegalStateException(ex);
        }
    }

}
