/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.rule.engine.mail;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;
import org.thingsboard.rule.engine.api.*;
import org.thingsboard.rule.engine.api.util.TbNodeUtils;
import org.thingsboard.server.common.data.plugin.ComponentType;
import org.thingsboard.server.common.msg.TbMsg;
import org.thingsboard.server.common.msg.TbMsgMetaData;

import static org.thingsboard.rule.engine.api.TbRelationTypes.SUCCESS;
import static org.thingsboard.rule.engine.mail.TbSendEmailNode.SEND_EMAIL_TYPE;


@Slf4j
@RuleNode(type = ComponentType.TRANSFORMATION,
          name = "to email",
          configClazz = TbMsgToEmailNodeConfiguration.class,
          nodeDescription = "Transforms message to email message",
          nodeDetails =
                  "Transforms message to email message by populating email fields using values derived from message " +
                  "metadata. " + "Set 'SEND_EMAIL' output message type.",
          uiResources = {"static/rulenode/rulenode-core-config.js"},
          configDirective = "tbTransformationNodeToEmailConfig",
          icon = "email")
public class TbMsgToEmailNode implements TbNode {

    private static final ObjectMapper MAPPER = new ObjectMapper();

    private TbMsgToEmailNodeConfiguration config;

    @Override
    public void init(TbContext ctx, TbNodeConfiguration configuration) throws TbNodeException {
        this.config = TbNodeUtils.convert(configuration, TbMsgToEmailNodeConfiguration.class);
    }

    @Override
    public void onMsg(TbContext ctx, TbMsg msg) {
        try {
            EmailPojo email = this.convert(msg);
            TbMsg emailMsg = this.buildEmailMsg(ctx, msg, email);
            ctx.tellNext(emailMsg, SUCCESS);
        } catch (Exception ex) {
            log.warn("Can not convert message to email " + ex.getMessage());
            ctx.tellFailure(msg, ex);
        }
    }
    private EmailPojo convert(TbMsg msg) {
        EmailPojo.EmailPojoBuilder builder = EmailPojo.builder();
        builder.from(this.fromTemplate(this.config.getFromTemplate(), msg.getMetaData()));
        builder.to(this.fromTemplate(this.config.getToTemplate(), msg.getMetaData()));
        builder.cc(this.fromTemplate(this.config.getCcTemplate(), msg.getMetaData()));
        builder.bcc(this.fromTemplate(this.config.getBccTemplate(), msg.getMetaData()));
        builder.subject(this.fromTemplate(this.config.getSubjectTemplate(), msg.getMetaData()));
        builder.body(this.fromTemplate(this.config.getBodyTemplate(), msg.getMetaData()));
        return builder.build();
    }
    private TbMsg buildEmailMsg(TbContext ctx, TbMsg msg, EmailPojo email) throws JsonProcessingException {
        String emailJson = MAPPER.writeValueAsString(email);
        return ctx.transformMsg(msg, SEND_EMAIL_TYPE, msg.getOriginator(), msg.getMetaData().copy(), emailJson);
    }
    private String fromTemplate(String template, TbMsgMetaData metaData) {
        if (!StringUtils.isEmpty(template)) {
            return TbNodeUtils.processPattern(template, metaData);
        }
        else {
            return null;
        }
    }

    @Override
    public void destroy() {

    }

}
