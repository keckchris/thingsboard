/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.common.data;


import lombok.EqualsAndHashCode;
import org.thingsboard.server.common.data.id.CustomerId;
import org.thingsboard.server.common.data.id.DeviceId;
import org.thingsboard.server.common.data.id.TenantId;


@EqualsAndHashCode(callSuper = true)
public class Device extends SearchTextBasedWithAdditionalInfo<DeviceId> implements HasName, HasTenantId, HasCustomerId {

    private static final long serialVersionUID = 2807343040519543363L;

    private TenantId tenantId;
    private CustomerId customerId;
    private String name;
    private String type;

    public Device() {
        super();
    }

    public Device(DeviceId id) {
        super(id);
    }

    public Device(Device device) {
        super(device);
        this.tenantId = device.getTenantId();
        this.customerId = device.getCustomerId();
        this.name = device.getName();
        this.type = device.getType();
    }

    public TenantId getTenantId() {
        return this.tenantId;
    }

    public void setTenantId(TenantId tenantId) {
        this.tenantId = tenantId;
    }

    public CustomerId getCustomerId() {
        return this.customerId;
    }

    public void setCustomerId(CustomerId customerId) {
        this.customerId = customerId;
    }
    public String getType() {
        return this.type;
    }

    public void setName(String name) {
        this.name = name;
    }
    @Override
    public String getSearchText() {
        return this.getName();
    }

    public void setType(String type) {
        this.type = type;
    }
    @Override
    public String getName() {
        return this.name;
    }
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Device [tenantId=");
        builder.append(this.tenantId);
        builder.append(", customerId=");
        builder.append(this.customerId);
        builder.append(", name=");
        builder.append(this.name);
        builder.append(", type=");
        builder.append(this.type);
        builder.append(", additionalInfo=");
        builder.append(this.getAdditionalInfo());
        builder.append(", createdTime=");
        builder.append(this.createdTime);
        builder.append(", id=");
        builder.append(this.id);
        builder.append("]");
        return builder.toString();
    }

}
