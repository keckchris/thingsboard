/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.common.data;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import org.thingsboard.server.common.data.id.CustomerId;
import org.thingsboard.server.common.data.id.EntityId;
import org.thingsboard.server.common.data.id.TenantId;
import org.thingsboard.server.common.data.id.UserId;
import org.thingsboard.server.common.data.security.Authority;


@EqualsAndHashCode(callSuper = true)
public class User extends SearchTextBasedWithAdditionalInfo<UserId> implements HasName, HasTenantId, HasCustomerId {

    private static final long serialVersionUID = 8250339805336035966L;

    private TenantId tenantId;
    private CustomerId customerId;
    private String email;
    private Authority authority;
    private String firstName;
    private String lastName;

    public User() {
        super();
    }

    public User(UserId id) {
        super(id);
    }

    public User(User user) {
        super(user);
        this.tenantId = user.getTenantId();
        this.customerId = user.getCustomerId();
        this.email = user.getEmail();
        this.authority = user.getAuthority();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
    }

    public TenantId getTenantId() {
        return this.tenantId;
    }

    public void setTenantId(TenantId tenantId) {
        this.tenantId = tenantId;
    }

    public CustomerId getCustomerId() {
        return this.customerId;
    }

    public void setCustomerId(CustomerId customerId) {
        this.customerId = customerId;
    }
    public Authority getAuthority() {
        return this.authority;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    public String getFirstName() {
        return this.firstName;
    }

    public void setAuthority(Authority authority) {
        this.authority = authority;
    }
    public String getLastName() {
        return this.lastName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    @Override
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    public String getName() {
        return this.email;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    @Override
    public String getSearchText() {
        return this.getEmail();
    }
    public String getEmail() {
        return this.email;
    }
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("User [tenantId=");
        builder.append(this.tenantId);
        builder.append(", customerId=");
        builder.append(this.customerId);
        builder.append(", email=");
        builder.append(this.email);
        builder.append(", authority=");
        builder.append(this.authority);
        builder.append(", firstName=");
        builder.append(this.firstName);
        builder.append(", lastName=");
        builder.append(this.lastName);
        builder.append(", additionalInfo=");
        builder.append(this.getAdditionalInfo());
        builder.append(", createdTime=");
        builder.append(this.createdTime);
        builder.append(", id=");
        builder.append(this.id);
        builder.append("]");
        return builder.toString();
    }

    @JsonIgnore
    public boolean isCustomerUser() {
        return !this.isSystemAdmin() && !this.isTenantAdmin();
    }

    @JsonIgnore
    public boolean isSystemAdmin() {
        return this.tenantId == null || EntityId.NULL_UUID.equals(this.tenantId.getId());
    }

    @JsonIgnore
    public boolean isTenantAdmin() {
        return !this.isSystemAdmin() && (this.customerId == null || EntityId.NULL_UUID.equals(this.customerId.getId()));
    }

}
