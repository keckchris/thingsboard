/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.common.data.kv;


import java.util.Objects;
import java.util.Optional;


public class StringDataEntry extends BasicKvEntry {

    private static final long serialVersionUID = 1L;
    private final String value;

    public StringDataEntry(String key, String value) {
        super(key);
        this.value = value;
    }

    @Override
    public DataType getDataType() {
        return DataType.STRING;
    }

    @Override
    public String getValueAsString() {
        return this.value;
    }

    @Override
    public Object getValue() {
        return this.value;
    }

    @Override
    public Optional<String> getStrValue() {
        return Optional.ofNullable(this.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), this.value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof StringDataEntry)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        StringDataEntry that = (StringDataEntry) o;
        return Objects.equals(this.value, that.value);
    }

    @Override
    public String toString() {
        return "StringDataEntry{" + "value='" + this.value + '\'' + "} " + super.toString();
    }

}
