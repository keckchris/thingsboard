/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.common.data;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import org.thingsboard.server.common.data.id.CustomerId;
import org.thingsboard.server.common.data.id.TenantId;


public class Customer extends ContactBased<CustomerId> implements HasName, HasTenantId {

    private static final long serialVersionUID = -1599722990298929275L;

    private String title;
    private TenantId tenantId;

    public Customer() {
        super();
    }

    public Customer(CustomerId id) {
        super(id);
    }

    public Customer(Customer customer) {
        super(customer);
        this.tenantId = customer.getTenantId();
        this.title = customer.getTitle();
    }

    public TenantId getTenantId() {
        return this.tenantId;
    }

    public void setTenantId(TenantId tenantId) {
        this.tenantId = tenantId;
    }
    @JsonIgnore
    public ShortCustomerInfo toShortCustomerInfo() {
        return new ShortCustomerInfo(this.id, this.title, this.isPublic());
    }

    public void setTitle(String title) {
        this.title = title;
    }
    @JsonIgnore
    public boolean isPublic() {
        if (this.getAdditionalInfo() != null && this.getAdditionalInfo().has("isPublic")) {
            return this.getAdditionalInfo().get("isPublic").asBoolean();
        }

        return false;
    }
    @Override
    @JsonProperty(access = Access.READ_ONLY)
    public String getName() {
        return this.title;
    }
    @Override
    public String getSearchText() {
        return this.getTitle();
    }
    public String getTitle() {
        return this.title;
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        Customer other = (Customer) obj;
        if (this.tenantId == null) {
            if (other.tenantId != null) {
                return false;
            }
        }
        else if (!this.tenantId.equals(other.tenantId)) {
            return false;
        }
        if (this.title == null) {
            return other.title == null;
        }
        else {
            return this.title.equals(other.title);
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((this.tenantId == null) ? 0 : this.tenantId.hashCode());
        result = prime * result + ((this.title == null) ? 0 : this.title.hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Customer [title=");
        builder.append(this.title);
        builder.append(", tenantId=");
        builder.append(this.tenantId);
        builder.append(", additionalInfo=");
        builder.append(this.getAdditionalInfo());
        builder.append(", country=");
        builder.append(this.country);
        builder.append(", state=");
        builder.append(this.state);
        builder.append(", city=");
        builder.append(this.city);
        builder.append(", address=");
        builder.append(this.address);
        builder.append(", address2=");
        builder.append(this.address2);
        builder.append(", zip=");
        builder.append(this.zip);
        builder.append(", phone=");
        builder.append(this.phone);
        builder.append(", email=");
        builder.append(this.email);
        builder.append(", createdTime=");
        builder.append(this.createdTime);
        builder.append(", id=");
        builder.append(this.id);
        builder.append("]");
        return builder.toString();
    }

}
