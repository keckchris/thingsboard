/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.common.data.kv;


import java.util.Objects;
import java.util.Optional;


public class BasicTsKvEntry implements TsKvEntry {

    private final long ts;
    private final KvEntry kv;

    public BasicTsKvEntry(long ts, KvEntry kv) {
        this.ts = ts;
        this.kv = kv;
    }

    @Override
    public String getKey() {
        return this.kv.getKey();
    }

    @Override
    public DataType getDataType() {
        return this.kv.getDataType();
    }

    @Override
    public Optional<String> getStrValue() {
        return this.kv.getStrValue();
    }

    @Override
    public Optional<Long> getLongValue() {
        return this.kv.getLongValue();
    }

    @Override
    public Optional<Boolean> getBooleanValue() {
        return this.kv.getBooleanValue();
    }

    @Override
    public Optional<Double> getDoubleValue() {
        return this.kv.getDoubleValue();
    }

    @Override
    public String getValueAsString() {
        return this.kv.getValueAsString();
    }

    @Override
    public Object getValue() {
        return this.kv.getValue();
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.getTs(), this.kv);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BasicTsKvEntry)) {
            return false;
        }
        BasicTsKvEntry that = (BasicTsKvEntry) o;
        return this.getTs() == that.getTs() && Objects.equals(this.kv, that.kv);
    }
    @Override
    public String toString() {
        return "BasicTsKvEntry{" + "ts=" + this.ts + ", kv=" + this.kv + '}';
    }
    @Override
    public long getTs() {
        return this.ts;
    }

}
