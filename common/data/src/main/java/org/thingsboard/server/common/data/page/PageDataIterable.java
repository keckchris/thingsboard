/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.common.data.page;


import org.thingsboard.server.common.data.SearchTextBased;
import org.thingsboard.server.common.data.id.UUIDBased;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;


public class PageDataIterable<T extends SearchTextBased<? extends UUIDBased>> implements Iterable<T>, Iterator<T> {

    private final FetchFunction<T> function;
    private final int fetchSize;

    private List<T> currentItems;
    private int currentIdx;
    private boolean hasNextPack;
    private TextPageLink nextPackLink;
    private boolean initialized;

    public PageDataIterable(FetchFunction<T> function, int fetchSize) {
        super();
        this.function = function;
        this.fetchSize = fetchSize;
    }

    @Override
    public Iterator<T> iterator() {
        return this;
    }

    public interface FetchFunction<T extends SearchTextBased<? extends UUIDBased>> {

        TextPageData<T> fetch(TextPageLink link);

    }

    @Override
    public boolean hasNext() {
        if (!this.initialized) {
            this.fetch(new TextPageLink(this.fetchSize));
            this.initialized = true;
        }
        if (this.currentIdx == this.currentItems.size()) {
            if (this.hasNextPack) {
                this.fetch(this.nextPackLink);
            }
        }
        return this.currentIdx < this.currentItems.size();
    }

    private void fetch(TextPageLink link) {
        TextPageData<T> pageData = this.function.fetch(link);
        this.currentIdx = 0;
        this.currentItems = pageData.getData();
        this.hasNextPack = pageData.hasNext();
        this.nextPackLink = pageData.getNextPageLink();
    }

    @Override
    public T next() {
        if (!this.hasNext()) {
            throw new NoSuchElementException();
        }
        return this.currentItems.get(this.currentIdx++);
    }


}
