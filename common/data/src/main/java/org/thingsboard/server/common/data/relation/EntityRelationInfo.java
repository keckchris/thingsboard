/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.common.data.relation;


public class EntityRelationInfo extends EntityRelation {

    private static final long serialVersionUID = 2807343097519543363L;

    private String fromName;
    private String toName;

    public EntityRelationInfo() {
        super();
    }

    public EntityRelationInfo(EntityRelation entityRelation) {
        super(entityRelation);
    }

    public String getFromName() {
        return this.fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    public String getToName() {
        return this.toName;
    }

    public void setToName(String toName) {
        this.toName = toName;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (this.toName != null ? this.toName.hashCode() : 0);
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        EntityRelationInfo that = (EntityRelationInfo) o;

        return this.toName != null ? this.toName.equals(that.toName) : that.toName == null;

    }

}
