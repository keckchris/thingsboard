/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.common.data.plugin;


import com.fasterxml.jackson.databind.JsonNode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.thingsboard.server.common.data.SearchTextBased;
import org.thingsboard.server.common.data.id.ComponentDescriptorId;


/**
 * @author Andrew Shvayka
 */
@ToString
public class ComponentDescriptor extends SearchTextBased<ComponentDescriptorId> {

    private static final long serialVersionUID = 1L;

    @Getter
    @Setter
    private ComponentType type;
    @Getter
    @Setter
    private ComponentScope scope;
    @Getter
    @Setter
    private String name;
    @Getter
    @Setter
    private String clazz;
    @Getter
    @Setter
    private transient JsonNode configurationDescriptor;
    @Getter
    @Setter
    private String actions;

    public ComponentDescriptor() {
        super();
    }

    public ComponentDescriptor(ComponentDescriptorId id) {
        super(id);
    }

    public ComponentDescriptor(ComponentDescriptor plugin) {
        super(plugin);
        this.type = plugin.getType();
        this.scope = plugin.getScope();
        this.name = plugin.getName();
        this.clazz = plugin.getClazz();
        this.configurationDescriptor = plugin.getConfigurationDescriptor();
        this.actions = plugin.getActions();
    }

    @Override
    public String getSearchText() {
        return this.name;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (this.type != null ? this.type.hashCode() : 0);
        result = 31 * result + (this.scope != null ? this.scope.hashCode() : 0);
        result = 31 * result + (this.name != null ? this.name.hashCode() : 0);
        result = 31 * result + (this.clazz != null ? this.clazz.hashCode() : 0);
        result = 31 * result + (this.actions != null ? this.actions.hashCode() : 0);
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }

        ComponentDescriptor that = (ComponentDescriptor) o;

        if (this.type != that.type) {
            return false;
        }
        if (this.scope != that.scope) {
            return false;
        }
        if (this.name != null ? !this.name.equals(that.name) : that.name != null) {
            return false;
        }
        if (this.actions != null ? !this.actions.equals(that.actions) : that.actions != null) {
            return false;
        }
        if (this.configurationDescriptor != null ? !this.configurationDescriptor.equals(that.configurationDescriptor)
                                                 : that.configurationDescriptor != null) {
            return false;
        }
        return this.clazz != null ? this.clazz.equals(that.clazz) : that.clazz == null;
    }

}
