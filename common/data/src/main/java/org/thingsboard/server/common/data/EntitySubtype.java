/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.common.data;


import org.thingsboard.server.common.data.id.TenantId;


public class EntitySubtype {

    private static final long serialVersionUID = 8057240243059922101L;

    private TenantId tenantId;
    private EntityType entityType;
    private String type;

    public EntitySubtype() {
        super();
    }

    public EntitySubtype(TenantId tenantId, EntityType entityType, String type) {
        this.tenantId = tenantId;
        this.entityType = entityType;
        this.type = type;
    }

    public TenantId getTenantId() {
        return this.tenantId;
    }

    public void setTenantId(TenantId tenantId) {
        this.tenantId = tenantId;
    }

    public EntityType getEntityType() {
        return this.entityType;
    }

    public void setEntityType(EntityType entityType) {
        this.entityType = entityType;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public int hashCode() {
        int result = this.tenantId != null ? this.tenantId.hashCode() : 0;
        result = 31 * result + (this.entityType != null ? this.entityType.hashCode() : 0);
        result = 31 * result + (this.type != null ? this.type.hashCode() : 0);
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }

        EntitySubtype that = (EntitySubtype) o;

        if (this.tenantId != null ? !this.tenantId.equals(that.tenantId) : that.tenantId != null) {
            return false;
        }
        if (this.entityType != that.entityType) {
            return false;
        }
        return this.type != null ? this.type.equals(that.type) : that.type == null;

    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("EntitySubtype{");
        sb.append("tenantId=").append(this.tenantId);
        sb.append(", entityType=").append(this.entityType);
        sb.append(", type='").append(this.type).append('\'');
        sb.append('}');
        return sb.toString();
    }

}
