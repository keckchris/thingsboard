/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.common.data.widget;


import org.thingsboard.server.common.data.SearchTextBased;
import org.thingsboard.server.common.data.id.TenantId;
import org.thingsboard.server.common.data.id.WidgetsBundleId;

import java.util.Arrays;


public class WidgetsBundle extends SearchTextBased<WidgetsBundleId> {

    private static final long serialVersionUID = -7627368878362410489L;

    private TenantId tenantId;
    private String alias;
    private String title;
    private byte[] image;

    public WidgetsBundle() {
        super();
    }

    public WidgetsBundle(WidgetsBundleId id) {
        super(id);
    }

    public WidgetsBundle(WidgetsBundle widgetsBundle) {
        super(widgetsBundle);
        this.tenantId = widgetsBundle.getTenantId();
        this.alias = widgetsBundle.getAlias();
        this.title = widgetsBundle.getTitle();
        this.image = widgetsBundle.getImage();
    }

    public TenantId getTenantId() {
        return this.tenantId;
    }

    public void setTenantId(TenantId tenantId) {
        this.tenantId = tenantId;
    }

    public String getAlias() {
        return this.alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }
    public byte[] getImage() {
        return this.image;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    @Override
    public String getSearchText() {
        return this.getTitle();
    }

    public void setImage(byte[] image) {
        this.image = image;
    }
    public String getTitle() {
        return this.title;
    }
    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (this.tenantId != null ? this.tenantId.hashCode() : 0);
        result = 31 * result + (this.alias != null ? this.alias.hashCode() : 0);
        result = 31 * result + (this.title != null ? this.title.hashCode() : 0);
        result = 31 * result + Arrays.hashCode(this.image);
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        WidgetsBundle that = (WidgetsBundle) o;

        if (this.tenantId != null ? !this.tenantId.equals(that.tenantId) : that.tenantId != null) {
            return false;
        }
        if (this.alias != null ? !this.alias.equals(that.alias) : that.alias != null) {
            return false;
        }
        if (this.title != null ? !this.title.equals(that.title) : that.title != null) {
            return false;
        }
        return Arrays.equals(this.image, that.image);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("WidgetsBundle{");
        sb.append("tenantId=").append(this.tenantId);
        sb.append(", alias='").append(this.alias).append('\'');
        sb.append(", title='").append(this.title).append('\'');
        sb.append(", image=").append(Arrays.toString(this.image));
        sb.append('}');
        return sb.toString();
    }

}
