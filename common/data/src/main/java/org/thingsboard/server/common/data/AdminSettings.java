/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.common.data;


import com.fasterxml.jackson.databind.JsonNode;
import org.thingsboard.server.common.data.id.AdminSettingsId;


public class AdminSettings extends BaseData<AdminSettingsId> {

    private static final long serialVersionUID = -7670322981725511892L;

    private String key;
    private transient JsonNode jsonValue;

    public AdminSettings() {
        super();
    }

    public AdminSettings(AdminSettingsId id) {
        super(id);
    }

    public AdminSettings(AdminSettings adminSettings) {
        super(adminSettings);
        this.key = adminSettings.getKey();
        this.jsonValue = adminSettings.getJsonValue();
    }

    public String getKey() {
        return this.key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public JsonNode getJsonValue() {
        return this.jsonValue;
    }

    public void setJsonValue(JsonNode jsonValue) {
        this.jsonValue = jsonValue;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((this.jsonValue == null) ? 0 : this.jsonValue.hashCode());
        result = prime * result + ((this.key == null) ? 0 : this.key.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        AdminSettings other = (AdminSettings) obj;
        if (this.jsonValue == null) {
            if (other.jsonValue != null) {
                return false;
            }
        }
        else if (!this.jsonValue.equals(other.jsonValue)) {
            return false;
        }
        if (this.key == null) {
            return other.key == null;
        }
        else {
            return this.key.equals(other.key);
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("AdminSettings [key=");
        builder.append(this.key);
        builder.append(", jsonValue=");
        builder.append(this.jsonValue);
        builder.append(", createdTime=");
        builder.append(this.createdTime);
        builder.append(", id=");
        builder.append(this.id);
        builder.append("]");
        return builder.toString();
    }

}
