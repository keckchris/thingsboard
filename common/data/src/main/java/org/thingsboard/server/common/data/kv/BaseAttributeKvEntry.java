/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.common.data.kv;


import java.util.Optional;


/**
 * @author Andrew Shvayka
 */
public class BaseAttributeKvEntry implements AttributeKvEntry {

    private final long lastUpdateTs;
    private final KvEntry kv;

    public BaseAttributeKvEntry(KvEntry kv, long lastUpdateTs) {
        this.kv = kv;
        this.lastUpdateTs = lastUpdateTs;
    }

    @Override
    public long getLastUpdateTs() {
        return this.lastUpdateTs;
    }

    @Override
    public String getKey() {
        return this.kv.getKey();
    }

    @Override
    public DataType getDataType() {
        return this.kv.getDataType();
    }

    @Override
    public Optional<String> getStrValue() {
        return this.kv.getStrValue();
    }

    @Override
    public Optional<Long> getLongValue() {
        return this.kv.getLongValue();
    }

    @Override
    public Optional<Boolean> getBooleanValue() {
        return this.kv.getBooleanValue();
    }

    @Override
    public Optional<Double> getDoubleValue() {
        return this.kv.getDoubleValue();
    }

    @Override
    public String getValueAsString() {
        return this.kv.getValueAsString();
    }

    @Override
    public Object getValue() {
        return this.kv.getValue();
    }

    @Override
    public int hashCode() {
        int result = (int) (this.lastUpdateTs ^ (this.lastUpdateTs >>> 32));
        result = 31 * result + this.kv.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }

        BaseAttributeKvEntry that = (BaseAttributeKvEntry) o;

        if (this.lastUpdateTs != that.lastUpdateTs) {
            return false;
        }
        return this.kv.equals(that.kv);

    }

    @Override
    public String toString() {
        return "BaseAttributeKvEntry{" + "lastUpdateTs=" + this.lastUpdateTs + ", kv=" + this.kv + '}';
    }

}
