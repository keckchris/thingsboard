/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.common.data;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import org.thingsboard.server.common.data.id.TenantId;


@EqualsAndHashCode(callSuper = true)
public class Tenant extends ContactBased<TenantId> implements HasName {

    private static final long serialVersionUID = 8057243243859922101L;

    private String title;
    private String region;

    public Tenant() {
        super();
    }

    public Tenant(TenantId id) {
        super(id);
    }

    public Tenant(Tenant tenant) {
        super(tenant);
        this.title = tenant.getTitle();
        this.region = tenant.getRegion();
    }

    public String getRegion() {
        return this.region;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    @Override
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    public String getName() {
        return this.title;
    }

    public void setRegion(String region) {
        this.region = region;
    }
    @Override
    public String getSearchText() {
        return this.getTitle();
    }
    public String getTitle() {
        return this.title;
    }
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Tenant [title=");
        builder.append(this.title);
        builder.append(", region=");
        builder.append(this.region);
        builder.append(", additionalInfo=");
        builder.append(this.getAdditionalInfo());
        builder.append(", country=");
        builder.append(this.country);
        builder.append(", state=");
        builder.append(this.state);
        builder.append(", city=");
        builder.append(this.city);
        builder.append(", address=");
        builder.append(this.address);
        builder.append(", address2=");
        builder.append(this.address2);
        builder.append(", zip=");
        builder.append(this.zip);
        builder.append(", phone=");
        builder.append(this.phone);
        builder.append(", email=");
        builder.append(this.email);
        builder.append(", createdTime=");
        builder.append(this.createdTime);
        builder.append(", id=");
        builder.append(this.id);
        builder.append("]");
        return builder.toString();
    }

}
