/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.transport.mqtt;


import com.fasterxml.jackson.databind.JsonNode;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.codec.mqtt.*;
import io.netty.handler.ssl.SslHandler;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;
import org.thingsboard.server.common.msg.EncryptionUtil;
import org.thingsboard.server.common.transport.SessionMsgListener;
import org.thingsboard.server.common.transport.TransportService;
import org.thingsboard.server.common.transport.TransportServiceCallback;
import org.thingsboard.server.common.transport.adaptor.AdaptorException;
import org.thingsboard.server.common.transport.service.AbstractTransportService;
import org.thingsboard.server.gen.transport.TransportProtos;
import org.thingsboard.server.gen.transport.TransportProtos.*;
import org.thingsboard.server.transport.mqtt.adaptors.MqttTransportAdaptor;
import org.thingsboard.server.transport.mqtt.session.DeviceSessionCtx;
import org.thingsboard.server.transport.mqtt.session.GatewaySessionHandler;
import org.thingsboard.server.transport.mqtt.session.MqttTopicMatcher;
import org.thingsboard.server.transport.mqtt.util.SslUtil;

import javax.net.ssl.SSLPeerUnverifiedException;
import javax.security.cert.X509Certificate;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import static io.netty.handler.codec.mqtt.MqttConnectReturnCode.*;
import static io.netty.handler.codec.mqtt.MqttMessageType.*;
import static io.netty.handler.codec.mqtt.MqttQoS.*;


/**
 * @author Andrew Shvayka
 */
@Slf4j
public class MqttTransportHandler extends ChannelInboundHandlerAdapter
        implements GenericFutureListener<Future<? super Void>>, SessionMsgListener {

    private static final MqttQoS MAX_SUPPORTED_QOS_LVL = AT_LEAST_ONCE;

    private final UUID sessionId;
    private final MqttTransportContext context;
    private final MqttTransportAdaptor adaptor;
    private final TransportService transportService;
    private final SslHandler sslHandler;
    private final ConcurrentMap<MqttTopicMatcher, Integer> mqttQoSMap;

    private volatile SessionInfoProto sessionInfo;
    private volatile InetSocketAddress address;
    private volatile DeviceSessionCtx deviceSessionCtx;
    private volatile GatewaySessionHandler gatewaySessionHandler;

    MqttTransportHandler(MqttTransportContext context) {
        this.sessionId = UUID.randomUUID();
        this.context = context;
        this.transportService = context.getTransportService();
        this.adaptor = context.getAdaptor();
        this.sslHandler = context.getSslHandler();
        this.mqttQoSMap = new ConcurrentHashMap<>();
        this.deviceSessionCtx = new DeviceSessionCtx(this.sessionId, this.mqttQoSMap);
    }

    private static MqttSubAckMessage createSubAckMessage(Integer msgId, List<Integer> grantedQoSList) {
        MqttFixedHeader mqttFixedHeader = new MqttFixedHeader(SUBACK, false, AT_LEAST_ONCE, false, 0);
        MqttMessageIdVariableHeader mqttMessageIdVariableHeader = MqttMessageIdVariableHeader.from(msgId);
        MqttSubAckPayload mqttSubAckPayload = new MqttSubAckPayload(grantedQoSList);
        return new MqttSubAckMessage(mqttFixedHeader, mqttMessageIdVariableHeader, mqttSubAckPayload);
    }

    private static int getMinSupportedQos(MqttQoS reqQoS) {
        return Math.min(reqQoS.value(), MAX_SUPPORTED_QOS_LVL.value());
    }

    public static MqttPubAckMessage createMqttPubAckMsg(int requestId) {
        MqttFixedHeader mqttFixedHeader = new MqttFixedHeader(PUBACK, false, AT_LEAST_ONCE, false, 0);
        MqttMessageIdVariableHeader mqttMsgIdVariableHeader = MqttMessageIdVariableHeader.from(requestId);
        return new MqttPubAckMessage(mqttFixedHeader, mqttMsgIdVariableHeader);
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {
        log.trace("[{}] Processing msg: {}", this.sessionId, msg);
        if (msg instanceof MqttMessage) {
            this.processMqttMsg(ctx, (MqttMessage) msg);
        }
        else {
            ctx.close();
        }
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) {
        ctx.flush();
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        log.error("[{}] Unexpected Exception", this.sessionId, cause);
        ctx.close();
    }

    private void processMqttMsg(ChannelHandlerContext ctx, MqttMessage msg) {
        this.address = (InetSocketAddress) ctx.channel().remoteAddress();
        if (msg.fixedHeader() == null) {
            log.info("[{}:{}] Invalid message received", this.address.getHostName(), this.address.getPort());
            this.processDisconnect(ctx);
            return;
        }
        this.deviceSessionCtx.setChannel(ctx);
        switch (msg.fixedHeader().messageType()) {
            case CONNECT:
                this.processConnect(ctx, (MqttConnectMessage) msg);
                break;
            case PUBLISH:
                this.processPublish(ctx, (MqttPublishMessage) msg);
                break;
            case SUBSCRIBE:
                this.processSubscribe(ctx, (MqttSubscribeMessage) msg);
                break;
            case UNSUBSCRIBE:
                this.processUnsubscribe(ctx, (MqttUnsubscribeMessage) msg);
                break;
            case PINGREQ:
                if (this.checkConnected(ctx, msg)) {
                    ctx.writeAndFlush(new MqttMessage(new MqttFixedHeader(PINGRESP, false, AT_MOST_ONCE, false, 0)));
                    this.transportService.reportActivity(this.sessionInfo);
                    if (this.gatewaySessionHandler != null) {
                        this.gatewaySessionHandler.reportActivity();
                    }
                }
                break;
            case DISCONNECT:
                if (this.checkConnected(ctx, msg)) {
                    this.processDisconnect(ctx);
                }
                break;
            default:
                break;
        }

    }

    private void processPublish(ChannelHandlerContext ctx, MqttPublishMessage mqttMsg) {
        if (!this.checkConnected(ctx, mqttMsg)) {
            return;
        }
        String topicName = mqttMsg.variableHeader().topicName();
        int msgId = mqttMsg.variableHeader().packetId();
        log.trace("[{}][{}] Processing publish msg [{}][{}]!", this.sessionId, this.deviceSessionCtx.getDeviceId(),
                  topicName, msgId);

        if (topicName.startsWith(MqttTopics.BASE_GATEWAY_API_TOPIC)) {
            if (this.gatewaySessionHandler != null) {
                this.handleGatewayPublishMsg(topicName, msgId, mqttMsg);
            }
        }
        else {
            this.processDevicePublish(ctx, mqttMsg, topicName, msgId);
        }
    }

    private void handleGatewayPublishMsg(String topicName, int msgId, MqttPublishMessage mqttMsg) {
        try {
            switch (topicName) {
                case MqttTopics.GATEWAY_TELEMETRY_TOPIC:
                    this.gatewaySessionHandler.onDeviceTelemetry(mqttMsg);
                    break;
                case MqttTopics.GATEWAY_ATTRIBUTES_TOPIC:
                    this.gatewaySessionHandler.onDeviceAttributes(mqttMsg);
                    break;
                case MqttTopics.GATEWAY_ATTRIBUTES_REQUEST_TOPIC:
                    this.gatewaySessionHandler.onDeviceAttributesRequest(mqttMsg);
                    break;
                case MqttTopics.GATEWAY_RPC_TOPIC:
                    this.gatewaySessionHandler.onDeviceRpcResponse(mqttMsg);
                    break;
                case MqttTopics.GATEWAY_CONNECT_TOPIC:
                    this.gatewaySessionHandler.onDeviceConnect(mqttMsg);
                    break;
                case MqttTopics.GATEWAY_DISCONNECT_TOPIC:
                    this.gatewaySessionHandler.onDeviceDisconnect(mqttMsg);
                    break;
            }
        } catch (RuntimeException | AdaptorException e) {
            log.warn("[{}] Failed to process publish msg [{}][{}]", this.sessionId, topicName, msgId, e);
        }
    }

    private void processDevicePublish(ChannelHandlerContext ctx,
                                      MqttPublishMessage mqttMsg,
                                      String topicName,
                                      int msgId) {
        try {
            if (topicName.equals(MqttTopics.DEVICE_TELEMETRY_TOPIC)) {
                TransportProtos.PostTelemetryMsg postTelemetryMsg = this.adaptor.convertToPostTelemetry(
                        this.deviceSessionCtx, mqttMsg);
                this.transportService.process(this.sessionInfo, postTelemetryMsg,
                                              this.getPubAckCallback(ctx, msgId, postTelemetryMsg));
            }
            else if (topicName.equals(MqttTopics.DEVICE_ATTRIBUTES_TOPIC)) {
                TransportProtos.PostAttributeMsg postAttributeMsg = this.adaptor.convertToPostAttributes(
                        this.deviceSessionCtx, mqttMsg);
                this.transportService.process(this.sessionInfo, postAttributeMsg,
                                              this.getPubAckCallback(ctx, msgId, postAttributeMsg));
            }
            else if (topicName.startsWith(MqttTopics.DEVICE_ATTRIBUTES_REQUEST_TOPIC_PREFIX)) {
                TransportProtos.GetAttributeRequestMsg getAttributeMsg = this.adaptor.convertToGetAttributes(
                        this.deviceSessionCtx, mqttMsg);
                this.transportService.process(this.sessionInfo, getAttributeMsg,
                                              this.getPubAckCallback(ctx, msgId, getAttributeMsg));
            }
            else if (topicName.startsWith(MqttTopics.DEVICE_RPC_RESPONSE_TOPIC)) {
                TransportProtos.ToDeviceRpcResponseMsg rpcResponseMsg = this.adaptor.convertToDeviceRpcResponse(
                        this.deviceSessionCtx, mqttMsg);
                this.transportService.process(this.sessionInfo, rpcResponseMsg,
                                              this.getPubAckCallback(ctx, msgId, rpcResponseMsg));
            }
            else if (topicName.startsWith(MqttTopics.DEVICE_RPC_REQUESTS_TOPIC)) {
                TransportProtos.ToServerRpcRequestMsg rpcRequestMsg = this.adaptor.convertToServerRpcRequest(
                        this.deviceSessionCtx, mqttMsg);
                this.transportService.process(this.sessionInfo, rpcRequestMsg,
                                              this.getPubAckCallback(ctx, msgId, rpcRequestMsg));
            }
        } catch (AdaptorException e) {
            log.warn("[{}] Failed to process publish msg [{}][{}]", this.sessionId, topicName, msgId, e);
            log.info("[{}] Closing current session due to invalid publish msg [{}][{}]", this.sessionId, topicName,
                     msgId);
            ctx.close();
        }
    }

    private <T> TransportServiceCallback<Void> getPubAckCallback(final ChannelHandlerContext ctx,
                                                                 final int msgId,
                                                                 final T msg) {
        return new TransportServiceCallback<Void>() {
            @Override
            public void onSuccess(Void dummy) {
                log.trace("[{}] Published msg: {}", MqttTransportHandler.this.sessionId, msg);
                if (msgId > 0) {
                    ctx.writeAndFlush(createMqttPubAckMsg(msgId));
                }
            }

            @Override
            public void onError(Throwable e) {
                log.trace("[{}] Failed to publish msg: {}", MqttTransportHandler.this.sessionId, msg, e);
                MqttTransportHandler.this.processDisconnect(ctx);
            }
        };
    }

    private void processSubscribe(ChannelHandlerContext ctx, MqttSubscribeMessage mqttMsg) {
        if (!this.checkConnected(ctx, mqttMsg)) {
            return;
        }
        log.trace("[{}] Processing subscription [{}]!", this.sessionId, mqttMsg.variableHeader().messageId());
        List<Integer> grantedQoSList = new ArrayList<>();
        for (MqttTopicSubscription subscription : mqttMsg.payload().topicSubscriptions()) {
            String topic = subscription.topicName();
            MqttQoS reqQoS = subscription.qualityOfService();
            try {
                switch (topic) {
                    case MqttTopics.DEVICE_ATTRIBUTES_TOPIC: {
                        this.transportService.process(this.sessionInfo,
                                                      TransportProtos.SubscribeToAttributeUpdatesMsg.newBuilder()
                                                                                                    .build(), null);
                        this.registerSubQoS(topic, grantedQoSList, reqQoS);
                        break;
                    }
                    case MqttTopics.DEVICE_RPC_REQUESTS_SUB_TOPIC: {
                        this.transportService.process(this.sessionInfo,
                                                      TransportProtos.SubscribeToRPCMsg.newBuilder().build(), null);
                        this.registerSubQoS(topic, grantedQoSList, reqQoS);
                        break;
                    }
                    case MqttTopics.DEVICE_RPC_RESPONSE_SUB_TOPIC:
                    case MqttTopics.GATEWAY_ATTRIBUTES_TOPIC:
                    case MqttTopics.GATEWAY_RPC_TOPIC:
                    case MqttTopics.DEVICE_ATTRIBUTES_RESPONSES_TOPIC:
                        this.registerSubQoS(topic, grantedQoSList, reqQoS);
                        break;
                    default:
                        log.warn("[{}] Failed to subscribe to [{}][{}]", this.sessionId, topic, reqQoS);
                        grantedQoSList.add(FAILURE.value());
                        break;
                }
            } catch (Exception e) {
                log.warn("[{}] Failed to subscribe to [{}][{}]", this.sessionId, topic, reqQoS);
                grantedQoSList.add(FAILURE.value());
            }
        }
        ctx.writeAndFlush(createSubAckMessage(mqttMsg.variableHeader().messageId(), grantedQoSList));
    }

    private void registerSubQoS(String topic, List<Integer> grantedQoSList, MqttQoS reqQoS) {
        grantedQoSList.add(getMinSupportedQos(reqQoS));
        this.mqttQoSMap.put(new MqttTopicMatcher(topic), getMinSupportedQos(reqQoS));
    }

    private void processUnsubscribe(ChannelHandlerContext ctx, MqttUnsubscribeMessage mqttMsg) {
        if (!this.checkConnected(ctx, mqttMsg)) {
            return;
        }
        log.trace("[{}] Processing subscription [{}]!", this.sessionId, mqttMsg.variableHeader().messageId());
        for (String topicName : mqttMsg.payload().topics()) {
            this.mqttQoSMap.remove(new MqttTopicMatcher(topicName));
            try {
                switch (topicName) {
                    case MqttTopics.DEVICE_ATTRIBUTES_TOPIC: {
                        this.transportService.process(this.sessionInfo,
                                                      TransportProtos.SubscribeToAttributeUpdatesMsg.newBuilder()
                                                                                                    .setUnsubscribe(
                                                                                                            true)
                                                                                                    .build(), null);
                        break;
                    }
                    case MqttTopics.DEVICE_RPC_REQUESTS_SUB_TOPIC: {
                        this.transportService.process(this.sessionInfo, TransportProtos.SubscribeToRPCMsg.newBuilder()
                                                                                                         .setUnsubscribe(
                                                                                                                 true)
                                                                                                         .build(),
                                                      null);
                        break;
                    }
                }
            } catch (Exception e) {
                log.warn("[{}] Failed to process unsubscription [{}] to [{}]", this.sessionId,
                         mqttMsg.variableHeader().messageId(), topicName);
            }
        }
        ctx.writeAndFlush(this.createUnSubAckMessage(mqttMsg.variableHeader().messageId()));
    }

    private MqttMessage createUnSubAckMessage(int msgId) {
        MqttFixedHeader mqttFixedHeader = new MqttFixedHeader(UNSUBACK, false, AT_LEAST_ONCE, false, 0);
        MqttMessageIdVariableHeader mqttMessageIdVariableHeader = MqttMessageIdVariableHeader.from(msgId);
        return new MqttMessage(mqttFixedHeader, mqttMessageIdVariableHeader);
    }

    private void processConnect(ChannelHandlerContext ctx, MqttConnectMessage msg) {
        log.info("[{}] Processing connect msg for client: {}!", this.sessionId, msg.payload().clientIdentifier());
        X509Certificate cert;
        if (this.sslHandler != null && (cert = this.getX509Certificate()) != null) {
            this.processX509CertConnect(ctx, cert);
        }
        else {
            this.processAuthTokenConnect(ctx, msg);
        }
    }

    private void processAuthTokenConnect(ChannelHandlerContext ctx, MqttConnectMessage msg) {
        String userName = msg.payload().userName();
        log.info("[{}] Processing connect msg for client with user name: {}!", this.sessionId, userName);
        if (StringUtils.isEmpty(userName)) {
            ctx.writeAndFlush(this.createMqttConnAckMsg(CONNECTION_REFUSED_BAD_USER_NAME_OR_PASSWORD));
            ctx.close();
        }
        else {
            this.transportService.process(ValidateDeviceTokenRequestMsg.newBuilder().setToken(userName).build(),
                                          new TransportServiceCallback<ValidateDeviceCredentialsResponseMsg>() {
                                              @Override
                                              public void onSuccess(ValidateDeviceCredentialsResponseMsg msg) {
                                                  MqttTransportHandler.this.onValidateDeviceResponse(msg, ctx);
                                              }

                                              @Override
                                              public void onError(Throwable e) {
                                                  log.trace("[{}] Failed to process credentials: {}",
                                                            MqttTransportHandler.this.address, userName, e);
                                                  ctx.writeAndFlush(MqttTransportHandler.this.createMqttConnAckMsg(
                                                          MqttConnectReturnCode.CONNECTION_REFUSED_SERVER_UNAVAILABLE));
                                                  ctx.close();
                                              }
                                          });
        }
    }

    private void processX509CertConnect(ChannelHandlerContext ctx, X509Certificate cert) {
        try {
            String strCert = SslUtil.getX509CertificateString(cert);
            String sha3Hash = EncryptionUtil.getSha3Hash(strCert);
            this.transportService.process(ValidateDeviceX509CertRequestMsg.newBuilder().setHash(sha3Hash).build(),
                                          new TransportServiceCallback<ValidateDeviceCredentialsResponseMsg>() {
                                              @Override
                                              public void onSuccess(ValidateDeviceCredentialsResponseMsg msg) {
                                                  MqttTransportHandler.this.onValidateDeviceResponse(msg, ctx);
                                              }

                                              @Override
                                              public void onError(Throwable e) {
                                                  log.trace("[{}] Failed to process credentials: {}",
                                                            MqttTransportHandler.this.address, sha3Hash, e);
                                                  ctx.writeAndFlush(MqttTransportHandler.this.createMqttConnAckMsg(
                                                          MqttConnectReturnCode.CONNECTION_REFUSED_SERVER_UNAVAILABLE));
                                                  ctx.close();
                                              }
                                          });
        } catch (Exception e) {
            ctx.writeAndFlush(this.createMqttConnAckMsg(CONNECTION_REFUSED_NOT_AUTHORIZED));
            ctx.close();
        }
    }

    private X509Certificate getX509Certificate() {
        try {
            X509Certificate[] certChain = this.sslHandler.engine().getSession().getPeerCertificateChain();
            if (certChain.length > 0) {
                return certChain[0];
            }
        } catch (SSLPeerUnverifiedException e) {
            log.warn(e.getMessage());
            return null;
        }
        return null;
    }

    private MqttConnAckMessage createMqttConnAckMsg(MqttConnectReturnCode returnCode) {
        MqttFixedHeader mqttFixedHeader = new MqttFixedHeader(CONNACK, false, AT_MOST_ONCE, false, 0);
        MqttConnAckVariableHeader mqttConnAckVariableHeader = new MqttConnAckVariableHeader(returnCode, true);
        return new MqttConnAckMessage(mqttFixedHeader, mqttConnAckVariableHeader);
    }

    private boolean checkConnected(ChannelHandlerContext ctx, MqttMessage msg) {
        if (this.deviceSessionCtx.isConnected()) {
            return true;
        }
        else {
            log.info("[{}] Closing current session due to invalid msg order: {}", this.sessionId, msg);
            ctx.close();
            return false;
        }
    }

    private void checkGatewaySession() {
        DeviceInfoProto device = this.deviceSessionCtx.getDeviceInfo();
        try {
            JsonNode infoNode = this.context.getMapper().readTree(device.getAdditionalInfo());
            if (infoNode != null) {
                JsonNode gatewayNode = infoNode.get("gateway");
                if (gatewayNode != null && gatewayNode.asBoolean()) {
                    this.gatewaySessionHandler = new GatewaySessionHandler(this.context, this.deviceSessionCtx,
                                                                           this.sessionId);
                }
            }
        } catch (IOException e) {
            log.trace("[{}][{}] Failed to fetch device additional info", this.sessionId, device.getDeviceName(), e);
        }
    }

    @Override
    public void operationComplete(Future<? super Void> future) {
        if (this.deviceSessionCtx.isConnected()) {
            this.transportService.process(this.sessionInfo,
                                          AbstractTransportService.getSessionEventMsg(SessionEvent.CLOSED), null);
            this.transportService.deregisterSession(this.sessionInfo);
        }
    }

    private void onValidateDeviceResponse(ValidateDeviceCredentialsResponseMsg msg, ChannelHandlerContext ctx) {
        if (!msg.hasDeviceInfo()) {
            ctx.writeAndFlush(this.createMqttConnAckMsg(CONNECTION_REFUSED_NOT_AUTHORIZED));
            ctx.close();
        }
        else {
            this.deviceSessionCtx.setDeviceInfo(msg.getDeviceInfo());
            this.sessionInfo = SessionInfoProto.newBuilder()
                                               .setNodeId(this.context.getNodeId())
                                               .setSessionIdMSB(this.sessionId.getMostSignificantBits())
                                               .setSessionIdLSB(this.sessionId.getLeastSignificantBits())
                                               .setDeviceIdMSB(msg.getDeviceInfo().getDeviceIdMSB())
                                               .setDeviceIdLSB(msg.getDeviceInfo().getDeviceIdLSB())
                                               .setTenantIdMSB(msg.getDeviceInfo().getTenantIdMSB())
                                               .setTenantIdLSB(msg.getDeviceInfo().getTenantIdLSB())
                                               .build();
            this.transportService.process(this.sessionInfo,
                                          AbstractTransportService.getSessionEventMsg(SessionEvent.OPEN), null);
            this.transportService.registerAsyncSession(this.sessionInfo, this);
            this.checkGatewaySession();
            ctx.writeAndFlush(this.createMqttConnAckMsg(CONNECTION_ACCEPTED));
            log.info("[{}] Client connected!", this.sessionId);
        }
    }

    @Override
    public void onGetAttributesResponse(TransportProtos.GetAttributeResponseMsg response) {
        try {
            this.adaptor.convertToPublish(this.deviceSessionCtx, response).ifPresent(
                    this.deviceSessionCtx.getChannel()::writeAndFlush);
        } catch (Exception e) {
            log.trace("[{}] Failed to convert device attributes response to MQTT msg", this.sessionId, e);
        }
    }

    @Override
    public void onAttributeUpdate(TransportProtos.AttributeUpdateNotificationMsg notification) {
        try {
            this.adaptor.convertToPublish(this.deviceSessionCtx, notification).ifPresent(
                    this.deviceSessionCtx.getChannel()::writeAndFlush);
        } catch (Exception e) {
            log.trace("[{}] Failed to convert device attributes update to MQTT msg", this.sessionId, e);
        }
    }

    @Override
    public void onRemoteSessionCloseCommand(TransportProtos.SessionCloseNotificationProto sessionCloseNotification) {
        log.trace("[{}] Received the remote command to close the session", this.sessionId);
        this.processDisconnect(this.deviceSessionCtx.getChannel());
    }

    private void processDisconnect(ChannelHandlerContext ctx) {
        ctx.close();
        if (this.deviceSessionCtx.isConnected()) {
            this.transportService.process(this.sessionInfo,
                                          AbstractTransportService.getSessionEventMsg(SessionEvent.CLOSED), null);
            this.transportService.deregisterSession(this.sessionInfo);
            if (this.gatewaySessionHandler != null) {
                this.gatewaySessionHandler.onGatewayDisconnect();
            }
        }
    }

    @Override
    public void onToDeviceRpcRequest(TransportProtos.ToDeviceRpcRequestMsg rpcRequest) {
        log.trace("[{}] Received RPC command to device", this.sessionId);
        try {
            this.adaptor.convertToPublish(this.deviceSessionCtx, rpcRequest).ifPresent(
                    this.deviceSessionCtx.getChannel()::writeAndFlush);
        } catch (Exception e) {
            log.trace("[{}] Failed to convert device RPC commandto MQTT msg", this.sessionId, e);
        }
    }

    @Override
    public void onToServerRpcResponse(TransportProtos.ToServerRpcResponseMsg rpcResponse) {
        log.trace("[{}] Received RPC command to device", this.sessionId);
        try {
            this.adaptor.convertToPublish(this.deviceSessionCtx, rpcResponse).ifPresent(
                    this.deviceSessionCtx.getChannel()::writeAndFlush);
        } catch (Exception e) {
            log.trace("[{}] Failed to convert device RPC commandto MQTT msg", this.sessionId, e);
        }
    }

}
