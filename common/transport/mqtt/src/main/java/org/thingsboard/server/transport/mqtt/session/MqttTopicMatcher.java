/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.transport.mqtt.session;


import java.util.regex.Pattern;


public class MqttTopicMatcher {

    private final String topic;
    private final Pattern topicRegex;

    public MqttTopicMatcher(String topic) {
        if (topic == null) {
            throw new NullPointerException("topic");
        }
        this.topic = topic;
        this.topicRegex = Pattern.compile(topic.replace("+", "[^/]+").replace("#", ".+") + "$");
    }

    public String getTopic() {
        return this.topic;
    }

    public boolean matches(String topic) {
        return this.topicRegex.matcher(topic).matches();
    }

    @Override
    public int hashCode() {
        return this.topic.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }

        MqttTopicMatcher that = (MqttTopicMatcher) o;

        return this.topic.equals(that.topic);
    }

}
