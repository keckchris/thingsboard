/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.common.transport.service;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.thingsboard.server.common.data.EntityType;
import org.thingsboard.server.common.data.id.DeviceId;
import org.thingsboard.server.common.data.id.TenantId;
import org.thingsboard.server.common.msg.tools.TbRateLimits;
import org.thingsboard.server.common.msg.tools.TbRateLimitsException;
import org.thingsboard.server.common.transport.SessionMsgListener;
import org.thingsboard.server.common.transport.TransportService;
import org.thingsboard.server.common.transport.TransportServiceCallback;
import org.thingsboard.server.gen.transport.TransportProtos;

import java.util.UUID;
import java.util.concurrent.*;


/**
 * Created by ashvayka on 17.10.18.
 */
@Slf4j
public abstract class AbstractTransportService implements TransportService {

    protected ScheduledExecutorService schedulerExecutor;
    protected ExecutorService transportCallbackExecutor;
    @Value("${transport.rate_limits.enabled}") private boolean rateLimitEnabled;
    @Value("${transport.rate_limits.tenant}") private String perTenantLimitsConf;
    @Value("${transport.rate_limits.tenant}") private String perDevicesLimitsConf;
    @Value("${transport.sessions.inactivity_timeout}") private long sessionInactivityTimeout;
    @Value("${transport.sessions.report_timeout}") private long sessionReportTimeout;
    private ConcurrentMap<UUID, SessionMetaData> sessions = new ConcurrentHashMap<>();

    //TODO: Implement cleanup of this maps.
    private ConcurrentMap<TenantId, TbRateLimits> perTenantLimits = new ConcurrentHashMap<>();
    private ConcurrentMap<DeviceId, TbRateLimits> perDeviceLimits = new ConcurrentHashMap<>();

    private void checkInactivityAndReportActivity() {
        long expTime = System.currentTimeMillis() - this.sessionInactivityTimeout;
        this.sessions.forEach((uuid, sessionMD) -> {
            if (sessionMD.getLastActivityTime() < expTime) {
                if (log.isDebugEnabled()) {
                    log.debug("[{}] Session has expired due to last activity time: {}",
                              this.toId(sessionMD.getSessionInfo()), sessionMD.getLastActivityTime());
                }
                this.process(sessionMD.getSessionInfo(), getSessionEventMsg(TransportProtos.SessionEvent.CLOSED), null);
                this.sessions.remove(uuid);
                sessionMD.getListener().onRemoteSessionCloseCommand(
                        TransportProtos.SessionCloseNotificationProto.getDefaultInstance());
            }
            else {
                this.process(sessionMD.getSessionInfo(),
                             TransportProtos.SubscriptionInfoProto.newBuilder()
                                                                  .setAttributeSubscription(
                                                                          sessionMD.isSubscribedToAttributes())
                                                                  .setRpcSubscription(sessionMD.isSubscribedToRPC())
                                                                  .setLastActivityTime(sessionMD.getLastActivityTime())
                                                                  .build(), null);
            }
        });
    }
    protected UUID toId(TransportProtos.SessionInfoProto sessionInfo) {
        return new UUID(sessionInfo.getSessionIdMSB(), sessionInfo.getSessionIdLSB());
    }
    public static TransportProtos.SessionEventMsg getSessionEventMsg(TransportProtos.SessionEvent event) {
        return TransportProtos.SessionEventMsg.newBuilder().setSessionType(TransportProtos.SessionType.ASYNC).setEvent(
                event).build();
    }
    @Override
    public boolean checkLimits(TransportProtos.SessionInfoProto sessionInfo,
                               Object msg,
                               TransportServiceCallback<Void> callback) {
        if (log.isTraceEnabled()) {
            log.trace("[{}] Processing msg: {}", this.toId(sessionInfo), msg);
        }
        if (!this.rateLimitEnabled) {
            return true;
        }
        TenantId tenantId = new TenantId(new UUID(sessionInfo.getTenantIdMSB(), sessionInfo.getTenantIdLSB()));
        TbRateLimits rateLimits = this.perTenantLimits.computeIfAbsent(tenantId, id -> new TbRateLimits(
                this.perTenantLimitsConf));
        if (!rateLimits.tryConsume()) {
            if (callback != null) {
                callback.onError(new TbRateLimitsException(EntityType.TENANT));
            }
            if (log.isTraceEnabled()) {
                log.trace("[{}][{}] Tenant level rate limit detected: {}", this.toId(sessionInfo), tenantId, msg);
            }
            return false;
        }
        DeviceId deviceId = new DeviceId(new UUID(sessionInfo.getDeviceIdMSB(), sessionInfo.getDeviceIdLSB()));
        rateLimits = this.perDeviceLimits.computeIfAbsent(deviceId, id -> new TbRateLimits(this.perDevicesLimitsConf));
        if (!rateLimits.tryConsume()) {
            if (callback != null) {
                callback.onError(new TbRateLimitsException(EntityType.DEVICE));
            }
            if (log.isTraceEnabled()) {
                log.trace("[{}][{}] Device level rate limit detected: {}", this.toId(sessionInfo), deviceId, msg);
            }
            return false;
        }

        return true;
    }
    @Override
    public void process(TransportProtos.SessionInfoProto sessionInfo,
                        TransportProtos.SessionEventMsg msg,
                        TransportServiceCallback<Void> callback) {
        if (this.checkLimits(sessionInfo, msg, callback)) {
            this.reportActivityInternal(sessionInfo);
            this.doProcess(sessionInfo, msg, callback);
        }
    }
    @Override
    public void process(TransportProtos.SessionInfoProto sessionInfo,
                        TransportProtos.PostTelemetryMsg msg,
                        TransportServiceCallback<Void> callback) {
        if (this.checkLimits(sessionInfo, msg, callback)) {
            this.reportActivityInternal(sessionInfo);
            this.doProcess(sessionInfo, msg, callback);
        }
    }
    @Override
    public void process(TransportProtos.SessionInfoProto sessionInfo,
                        TransportProtos.PostAttributeMsg msg,
                        TransportServiceCallback<Void> callback) {
        if (this.checkLimits(sessionInfo, msg, callback)) {
            this.reportActivityInternal(sessionInfo);
            this.doProcess(sessionInfo, msg, callback);
        }
    }
    @Override
    public void process(TransportProtos.SessionInfoProto sessionInfo,
                        TransportProtos.GetAttributeRequestMsg msg,
                        TransportServiceCallback<Void> callback) {
        if (this.checkLimits(sessionInfo, msg, callback)) {
            this.reportActivityInternal(sessionInfo);
            this.doProcess(sessionInfo, msg, callback);
        }
    }
    @Override
    public void process(TransportProtos.SessionInfoProto sessionInfo,
                        TransportProtos.SubscribeToAttributeUpdatesMsg msg,
                        TransportServiceCallback<Void> callback) {
        if (this.checkLimits(sessionInfo, msg, callback)) {
            SessionMetaData sessionMetaData = this.reportActivityInternal(sessionInfo);
            sessionMetaData.setSubscribedToAttributes(!msg.getUnsubscribe());
            this.doProcess(sessionInfo, msg, callback);
        }
    }
    @Override
    public void process(TransportProtos.SessionInfoProto sessionInfo,
                        TransportProtos.SubscribeToRPCMsg msg,
                        TransportServiceCallback<Void> callback) {
        if (this.checkLimits(sessionInfo, msg, callback)) {
            SessionMetaData sessionMetaData = this.reportActivityInternal(sessionInfo);
            sessionMetaData.setSubscribedToRPC(!msg.getUnsubscribe());
            this.doProcess(sessionInfo, msg, callback);
        }
    }
    @Override
    public void process(TransportProtos.SessionInfoProto sessionInfo,
                        TransportProtos.ToDeviceRpcResponseMsg msg,
                        TransportServiceCallback<Void> callback) {
        if (this.checkLimits(sessionInfo, msg, callback)) {
            this.reportActivityInternal(sessionInfo);
            this.doProcess(sessionInfo, msg, callback);
        }
    }
    @Override
    public void process(TransportProtos.SessionInfoProto sessionInfo,
                        TransportProtos.ToServerRpcRequestMsg msg,
                        TransportServiceCallback<Void> callback) {
        if (this.checkLimits(sessionInfo, msg, callback)) {
            this.reportActivityInternal(sessionInfo);
            this.doProcess(sessionInfo, msg, callback);
        }
    }
    @Override
    public void registerAsyncSession(TransportProtos.SessionInfoProto sessionInfo, SessionMsgListener listener) {
        this.sessions.putIfAbsent(this.toId(sessionInfo),
                                  new SessionMetaData(sessionInfo, TransportProtos.SessionType.ASYNC, listener));
    }
    @Override
    public void registerSyncSession(TransportProtos.SessionInfoProto sessionInfo,
                                    SessionMsgListener listener,
                                    long timeout) {
        this.sessions.putIfAbsent(this.toId(sessionInfo),
                                  new SessionMetaData(sessionInfo, TransportProtos.SessionType.SYNC, listener));
        this.schedulerExecutor.schedule(() -> {
            listener.onRemoteSessionCloseCommand(TransportProtos.SessionCloseNotificationProto.getDefaultInstance());
            this.deregisterSession(sessionInfo);
        }, timeout, TimeUnit.MILLISECONDS);
    }
    @Override
    public void reportActivity(TransportProtos.SessionInfoProto sessionInfo) {
        this.reportActivityInternal(sessionInfo);
    }
    @Override
    public void deregisterSession(TransportProtos.SessionInfoProto sessionInfo) {
        this.sessions.remove(this.toId(sessionInfo));
    }
    protected abstract void doProcess(TransportProtos.SessionInfoProto sessionInfo,
                                      TransportProtos.ToServerRpcRequestMsg msg,
                                      TransportServiceCallback<Void> callback);
    protected abstract void doProcess(TransportProtos.SessionInfoProto sessionInfo,
                                      TransportProtos.ToDeviceRpcResponseMsg msg,
                                      TransportServiceCallback<Void> callback);
    protected abstract void doProcess(TransportProtos.SessionInfoProto sessionInfo,
                                      TransportProtos.SubscribeToRPCMsg msg,
                                      TransportServiceCallback<Void> callback);
    protected abstract void doProcess(TransportProtos.SessionInfoProto sessionInfo,
                                      TransportProtos.SubscribeToAttributeUpdatesMsg msg,
                                      TransportServiceCallback<Void> callback);
    protected abstract void doProcess(TransportProtos.SessionInfoProto sessionInfo,
                                      TransportProtos.GetAttributeRequestMsg msg,
                                      TransportServiceCallback<Void> callback);
    protected abstract void doProcess(TransportProtos.SessionInfoProto sessionInfo,
                                      TransportProtos.PostAttributeMsg msg,
                                      TransportServiceCallback<Void> callback);
    protected abstract void doProcess(TransportProtos.SessionInfoProto sessionInfo,
                                      TransportProtos.PostTelemetryMsg msg,
                                      TransportServiceCallback<Void> callback);
    private SessionMetaData reportActivityInternal(TransportProtos.SessionInfoProto sessionInfo) {
        UUID sessionId = this.toId(sessionInfo);
        SessionMetaData sessionMetaData = this.sessions.get(sessionId);
        if (sessionMetaData != null) {
            sessionMetaData.updateLastActivityTime();
        }
        return sessionMetaData;
    }
    protected abstract void doProcess(TransportProtos.SessionInfoProto sessionInfo,
                                      TransportProtos.SessionEventMsg msg,
                                      TransportServiceCallback<Void> callback);
    protected void processToTransportMsg(TransportProtos.DeviceActorToTransportMsg toSessionMsg) {
        UUID sessionId = new UUID(toSessionMsg.getSessionIdMSB(), toSessionMsg.getSessionIdLSB());
        SessionMetaData md = this.sessions.get(sessionId);
        if (md != null) {
            SessionMsgListener listener = md.getListener();
            this.transportCallbackExecutor.submit(() -> {
                if (toSessionMsg.hasGetAttributesResponse()) {
                    listener.onGetAttributesResponse(toSessionMsg.getGetAttributesResponse());
                }
                if (toSessionMsg.hasAttributeUpdateNotification()) {
                    listener.onAttributeUpdate(toSessionMsg.getAttributeUpdateNotification());
                }
                if (toSessionMsg.hasSessionCloseNotification()) {
                    listener.onRemoteSessionCloseCommand(toSessionMsg.getSessionCloseNotification());
                }
                if (toSessionMsg.hasToDeviceRequest()) {
                    listener.onToDeviceRpcRequest(toSessionMsg.getToDeviceRequest());
                }
                if (toSessionMsg.hasToServerResponse()) {
                    listener.onToServerRpcResponse(toSessionMsg.getToServerResponse());
                }
            });
            if (md.getSessionType() == TransportProtos.SessionType.SYNC) {
                this.deregisterSession(md.getSessionInfo());
            }
        }
        else {
            //TODO: should we notify the device actor about missed session?
            log.debug("[{}] Missing session.", sessionId);
        }
    }
    protected String getRoutingKey(TransportProtos.SessionInfoProto sessionInfo) {
        return new UUID(sessionInfo.getDeviceIdMSB(), sessionInfo.getDeviceIdLSB()).toString();
    }
    public void init() {
        if (this.rateLimitEnabled) {
            //Just checking the configuration parameters
            new TbRateLimits(this.perTenantLimitsConf);
            new TbRateLimits(this.perDevicesLimitsConf);
        }
        this.schedulerExecutor = Executors.newSingleThreadScheduledExecutor();
        this.transportCallbackExecutor = new ThreadPoolExecutor(0, 20, 60L, TimeUnit.SECONDS,
                                                                new LinkedBlockingQueue<>());
        this.schedulerExecutor.scheduleAtFixedRate(this::checkInactivityAndReportActivity, this.sessionReportTimeout,
                                                   this.sessionReportTimeout, TimeUnit.MILLISECONDS);
    }
    public void destroy() {
        if (this.rateLimitEnabled) {
            this.perTenantLimits.clear();
            this.perDeviceLimits.clear();
        }
        if (this.schedulerExecutor != null) {
            this.schedulerExecutor.shutdownNow();
        }
        if (this.transportCallbackExecutor != null) {
            this.transportCallbackExecutor.shutdownNow();
        }
    }


}
