/**
 * Copyright © 2016-2018 The Thingsboard Authors
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.thingsboard.server.kafka;


import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.SettableFuture;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.admin.CreateTopicsResult;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.common.errors.InterruptException;
import org.apache.kafka.common.errors.TopicExistsException;
import org.apache.kafka.common.header.Header;
import org.apache.kafka.common.header.internals.RecordHeader;

import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.*;


/**
 * Created by ashvayka on 25.09.18.
 */
@Slf4j
public class TbKafkaRequestTemplate<Request, Response> extends AbstractTbKafkaTemplate {

    private final TBKafkaProducerTemplate<Request> requestTemplate;
    private final TBKafkaConsumerTemplate<Response> responseTemplate;
    private final ConcurrentMap<UUID, ResponseMetaData<Response>> pendingRequests;
    private final boolean internalExecutor;
    private final ExecutorService executor;
    private final long maxRequestTimeout;
    private final long maxPendingRequests;
    private final long pollInterval;
    private volatile long tickTs = 0L;
    private volatile long tickSize = 0L;
    private volatile boolean stopped = false;

    @Builder
    public TbKafkaRequestTemplate(TBKafkaProducerTemplate<Request> requestTemplate,
                                  TBKafkaConsumerTemplate<Response> responseTemplate,
                                  long maxRequestTimeout,
                                  long maxPendingRequests,
                                  long pollInterval,
                                  ExecutorService executor) {
        this.requestTemplate = requestTemplate;
        this.responseTemplate = responseTemplate;
        this.pendingRequests = new ConcurrentHashMap<>();
        this.maxRequestTimeout = maxRequestTimeout;
        this.maxPendingRequests = maxPendingRequests;
        this.pollInterval = pollInterval;
        if (executor != null) {
            this.internalExecutor = false;
            this.executor = executor;
        }
        else {
            this.internalExecutor = true;
            this.executor = Executors.newSingleThreadExecutor();
        }
    }

    public void init() {
        try {
            TBKafkaAdmin admin = new TBKafkaAdmin(this.requestTemplate.getSettings());
            CreateTopicsResult result = admin.createTopic(new NewTopic(this.responseTemplate.getTopic(), 1, (short) 1));
            result.all().get();
        } catch (Exception e) {
            if ((e instanceof TopicExistsException) ||
                (e.getCause() != null && e.getCause() instanceof TopicExistsException)) {
                log.trace("[{}] Topic already exists. ", this.responseTemplate.getTopic());
            }
            else {
                log.info("[{}] Failed to create topic: {}", this.responseTemplate.getTopic(), e.getMessage(), e);
                throw new RuntimeException(e);
            }

        }
        this.requestTemplate.init();
        this.tickTs = System.currentTimeMillis();
        this.responseTemplate.subscribe();
        this.executor.submit(() -> {
            long nextCleanupMs = 0L;
            while (!this.stopped) {
                try {
                    ConsumerRecords<String, byte[]> responses = this.responseTemplate.poll(
                            Duration.ofMillis(this.pollInterval));
                    if (responses.count() > 0) {
                        log.trace("Polling responses completed, consumer records count [{}]", responses.count());
                    }
                    responses.forEach(response -> {
                        log.trace("Received response to Kafka Template request: {}", response);
                        Header requestIdHeader = response.headers().lastHeader(TbKafkaSettings.REQUEST_ID_HEADER);
                        Response decodedResponse = null;
                        UUID requestId = null;
                        if (requestIdHeader == null) {
                            try {
                                decodedResponse = this.responseTemplate.decode(response);
                                requestId = this.responseTemplate.extractRequestId(decodedResponse);
                            } catch (IOException e) {
                                log.error("Failed to decode response", e);
                            }
                        }
                        else {
                            requestId = bytesToUuid(requestIdHeader.value());
                        }
                        if (requestId == null) {
                            log.error("[{}] Missing requestId in header and body", response);
                        }
                        else {
                            log.trace("[{}] Response received", requestId);
                            ResponseMetaData<Response> expectedResponse = this.pendingRequests.remove(requestId);
                            if (expectedResponse == null) {
                                log.trace("[{}] Invalid or stale request", requestId);
                            }
                            else {
                                try {
                                    if (decodedResponse == null) {
                                        decodedResponse = this.responseTemplate.decode(response);
                                    }
                                    expectedResponse.future.set(decodedResponse);
                                } catch (IOException e) {
                                    expectedResponse.future.setException(e);
                                }
                            }
                        }
                    });
                    this.tickTs = System.currentTimeMillis();
                    this.tickSize = this.pendingRequests.size();
                    if (nextCleanupMs < this.tickTs) {
                        //cleanup;
                        this.pendingRequests.entrySet().forEach(kv -> {
                            if (kv.getValue().expTime < this.tickTs) {
                                ResponseMetaData<Response> staleRequest = this.pendingRequests.remove(kv.getKey());
                                if (staleRequest != null) {
                                    log.trace("[{}] Request timeout detected, expTime [{}], tickTs [{}]", kv.getKey(),
                                              staleRequest.expTime, this.tickTs);
                                    staleRequest.future.setException(new TimeoutException());
                                }
                            }
                        });
                        nextCleanupMs = this.tickTs + this.maxRequestTimeout;
                    }
                } catch (InterruptException ie) {
                    if (!this.stopped) {
                        log.warn("Fetching data from kafka was interrupted.", ie);
                    }
                } catch (Throwable e) {
                    log.warn("Failed to obtain responses from queue.", e);
                    try {
                        Thread.sleep(this.pollInterval);
                    } catch (InterruptedException e2) {
                        log.trace("Failed to wait until the server has capacity to handle new responses", e2);
                    }
                }
            }
        });
    }

    public void stop() {
        this.stopped = true;
        if (this.internalExecutor) {
            this.executor.shutdownNow();
        }
    }

    public ListenableFuture<Response> post(String key, Request request) {
        if (this.tickSize > this.maxPendingRequests) {
            return Futures.immediateFailedFuture(new RuntimeException("Pending request map is full!"));
        }
        UUID requestId = UUID.randomUUID();
        List<Header> headers = new ArrayList<>(2);
        headers.add(new RecordHeader(TbKafkaSettings.REQUEST_ID_HEADER, this.uuidToBytes(requestId)));
        headers.add(new RecordHeader(TbKafkaSettings.RESPONSE_TOPIC_HEADER,
                                     this.stringToBytes(this.responseTemplate.getTopic())));
        SettableFuture<Response> future = SettableFuture.create();
        ResponseMetaData<Response> responseMetaData = new ResponseMetaData<>(this.tickTs + this.maxRequestTimeout,
                                                                             future);
        this.pendingRequests.putIfAbsent(requestId, responseMetaData);
        request = this.requestTemplate.enrich(request, this.responseTemplate.getTopic(), requestId);
        log.trace("[{}] Sending request, key [{}], expTime [{}]", requestId, key, responseMetaData.expTime);
        this.requestTemplate.send(key, request, headers, (metadata, exception) -> {
            if (exception != null) {
                log.trace("[{}] Failed to post the request", requestId, exception);
            }
            else {
                log.trace("[{}] Posted the request", requestId, metadata);
            }
        });
        return future;
    }

    private static class ResponseMetaData<T> {

        private final long expTime;
        private final SettableFuture<T> future;

        ResponseMetaData(long ts, SettableFuture<T> future) {
            this.expTime = ts;
            this.future = future;
        }

    }

}
